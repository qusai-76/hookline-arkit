﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeController : MonoBehaviour
{

    public AudioSource source;
    public float sqrShakeDetectionThreshold;
    public float timeSinceLastShake;

    public int MinShakeInterval { get; set; }

    void Start()
    {
       // Debug.Log("Acceleration : " + Input.acceleration.sqrMagnitude);   
    }
    
    void Update()
    {
        if (Input.acceleration.sqrMagnitude >= sqrShakeDetectionThreshold
            && Time.unscaledTime >= timeSinceLastShake + MinShakeInterval)
        {
            
            timeSinceLastShake = Time.unscaledTime;
            Debug.Log("Shake event detected at time "+Time.time);
            source.Play(); 
            
        }
    }
}
