﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Timers;
using Lean.Touch;
using Newtonsoft.Json;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR.iOS;

public class ApplicationLoader : MonoBehaviour
{

#if UNITY_IOS
    [DllImport("__Internal")]
    public static extern bool _InPhoneCall();
    [DllImport("__Internal")]
    public static extern bool CallStatus();
#endif


    public List<string> videosIDS;
    public List<string> phptosIDS;
    public List<string> avatarIDS;
    public List<string> thumbnailIDS;

    public object mutex;
    public Mutex downloadingMutex;

    public Dictionary<string, Vector3> avatarsToPlace;

    private int picturesCount;

    public GameObject mainOb;

    public GameObject webController;

    public GameObject bubble;

    public Button addAvatar;

    public GameObject cleanAvatar;

    public GameObject databaseService;

    public GameObject avatarsControlPanel;

    public GameObject addAvatars;
    public GameObject vedioRecording;
    public GameObject categorySlider;

    public GameObject avatarsSlider;

    public GameObject avatarButtonPrefab;
    public GameObject avatarPanel;
    public GameObject scrollSnapPanel;

    public GameObject shadowCaster;

    private float AvatarPanelHight = 680f;
    private float CategoryPanelHight = 420f;

    [Header("Avatars and Categories Panels")]
    public GameObject avatarPanelContent;
    public GameObject categoryButtonPrefab;
    public GameObject categoryPanel;
    public GameObject categoryPanelContent;

    [Header("Update Version Panel")]
    public GameObject updatePanel;
    public GameObject Papers;
    public GameObject Circleobject;
    public GameObject loadingLogo;
    public GameObject loadingAvatarAnimation;

    public static List<string> ComplexAvatarsIds;

    // Reference to Loading Avatars Assynchrounously
    Thread thread;

    public GameObject RepeatButton;

    public FocusSquare FocusIndicator;

    //public GameObject PlaneMark;
    public GameObject avatarHighlight;

    public GameObject LockButton;
    public GameObject ResizeButton;
    public GameObject DublicateButton;

    public GameObject goToGallery;

    public GameObject soundAvatar;

    public GameObject switchCameraButton;

    public bool cancelDownload = false;

    public static bool isDownloading = false;
    public static bool isPlacingAvatar = false;
    public static bool hasAvatarWithOneAnimation;
    public static bool hasHighlitedObject = false;

    public GameObject errorMessage;

    public static string lastSelectedObject = string.Empty;

    public bool Online = true;
    // Color F5872B

    // Loading Button
    [SerializeField]
    [Tooltip("How long must pointer be down on this object to trigger a long press")]
    private float holdTime = 1.0f;

    // downloading status;
    public UnityEngine.UI.Image button, countdown;

    [SerializeField]
    [Tooltip("The panel where new images will be added as children")]
    private RectTransform content;

    private List<Texture2D> textures;
    public GameObject downloadingMessageStatus;
    public GameObject downloadStatusPanel;

    public GameObject buttonPrefab;

    public GameObject meassageDetectSurface;
    public GameObject messageInternetConnectoin;
    public GameObject messageMaxAvatars;


    public static List<GameObject> avatarsList;
    public static List<AssetBundle> bundlesList;

    public Dictionary<string, int> avatarsDictionary;
    public Dictionary<int, string> avatarsLinks;

    public Dictionary<int, List<int>> avatarsDuplicates;

    public Dictionary<int, List<MusicItem>> musicDictionary;

    public static bool isPlayingMusic = false;
    public float timeDelay = 0.25f;
    public static bool isRepeating = false;
    public static bool isPausing = false;


    public string[] thumbnails;
    public List<string> numsThumbnailsInDevice;
    public List<string> numsThumbnailsInServer;

    [Header(" Category Content Parent ")]
    public GameObject CategoryPanelParent;
    public AudioSource sourceAudioForAvatar;


    [Header("Music Content")]
    public GameObject musicTrack;

    public List<GameObject> audioTracks;
    public List<string> audiosIds;
    

    /// <summary>
    /// Music Variables
    /// </summary>
    [Header(" Music Playing")]
    public GameObject ButtonPalyMuteMusic;
    //public GameObject DefaultMusicButton;
    public Texture MuteSoundTexture;
    public Texture PlaySoundTexture;

    [Header(" Avatar Identifires ")]
    public GameObject flesh;
    public GameObject IdentifiresArea;

    [Header(" Replay Button ")]
    public Sprite playTexture;
    public Sprite pauseTexture;

    System.Timers.Timer timer;
    System.Timers.Timer animationTimer;
    System.Timers.Timer loadingTimer;

    public List<float> animationsLength;

    bool timerFinished;
    bool animationTimerFinisted = false;
    bool loadingAnimationFinished = false;

    [Header("Graphics Settings")]
    GraphicRaycaster graphicRayCaster;
    PointerEventData pointerEventData;
    EventSystem eventSystem;


    public GameObject recordButton;
    public GameObject handsFreeRecording;
    public GameObject HandsFreePanel;

    public bool shadow_placed;
    int anim_loop_nb;

    public GameObject downloadingDataIndicator;

    // Phone Call Related Objects
    public GameObject HandsFreeButton;
    public GameObject HandsFreeRecordButton;

    public GameObject surface_detector;

    bool notfirstPlaceAvatar = false;

    bool showInternetMeassage = false;
    bool showMaxAvatarsMessage = false;

    public float Y_AXIS = 0;
    /// <summary>
    /// Places the avatar in the Unity World 
    /// </summary>
    /// <param name="avatar">Avatar.</param>
    /// <param name="value">Value.</param>
    public void PlaceAvatar(string avarar_name, GameObject avatar, float value)
    {
        //Vector3 pos = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 3f));
        Vector3 pos = avatarsToPlace[avarar_name];
        //Vector3 pos = PlaneIndecator.PlaneIndicator.transform.position;

        Debug.Log("Place Avatar at : " + pos);

        if (shadow_placed == false)
        {
            GameObject avatarShadowCaster = Instantiate(shadowCaster, new Vector3(0, pos.y -0.001f, 0), Quaternion.identity);
            avatarShadowCaster.transform.localScale = new Vector3(1000, 0.1f, 1000);
            Y_AXIS = pos.y;
            shadow_placed = true;
            UnityARUserAnchorData anchorData = UnityARSessionNativeInterface.GetARSessionNativeInterface().AddUserAnchorFromGameObject(mainOb);
        }

        if (!notfirstPlaceAvatar)
        {
            avatar.transform.localScale = Vector3.one * value;
            avatar.transform.SetParent(mainOb.transform);
            avatar.transform.position = new Vector3(pos.x, Y_AXIS, pos.z);

            Vector3 cam_position = Camera.main.transform.position;
            cam_position.y = avatar.transform.position.y;

            // Roate the Avatar to the Camera
            avatar.transform.LookAt(cam_position);

#pragma warning disable CS0618 // Type or member is obsolete
            avatar.transform.Rotate(Vector3.up, 0f);
#pragma warning restore CS0618 // Type or member is obsolete

            notfirstPlaceAvatar = true;

            avatarsList.Add(avatar);

            //Debug.Log("First Avatar Placed at : "+ PositioningPlace.Position);
        }
        else
        {
            avatar.transform.SetParent(mainOb.transform);
            avatar.transform.position = new Vector3(pos.x, Y_AXIS, pos.z);
            avatar.transform.localScale = Vector3.one * value;
            avatar.transform.localRotation = Quaternion.identity;

            Vector3 cam_position = Camera.main.transform.position;
            cam_position.y = avatar.transform.position.y;

            // Roate the Avatar to the Camera
            avatar.transform.LookAt(cam_position);

#pragma warning disable CS0618 // Type or member is obsolete
            avatar.transform.Rotate(Vector3.up, 0f);
#pragma warning restore CS0618 // Type or member is obsolete
            avatarsList.Add(avatar);

        }

        //PlaneIndecator.placementIndicator.SetActive(false);
    }

    public void SelectAvatar(GameObject avatar)
    {
        avatarHighlight.transform.position = avatar.transform.position + new Vector3(0, 0.01f, 0);
        avatarHighlight.transform.localRotation = Quaternion.Euler(90f, 0f, 0f);
        avatarHighlight.transform.SetParent(avatar.transform);

        avatarHighlight.SetActive(true);

        hasHighlitedObject = true;
        lastSelectedObject = avatar.name;

        RepeatButton.SetActive(false);

        avatar.GetComponent<Lean.Touch.LeanSelectable>().SelectMe();

        HighlitingController.hasHighlightedObject = true;
    }

    public void HighlightObject(GameObject avatar)
    {
        avatarHighlight.transform.position = avatar.transform.position + new Vector3(0, 0.01f, 0);
        avatarHighlight.transform.localRotation = Quaternion.Euler(90f, 0f, 0f);
        avatarHighlight.transform.SetParent(avatar.transform);

        avatarHighlight.SetActive(true);
        cleanAvatar.SetActive(true);

        //DublicateButton.SetActive(true);

        hasHighlitedObject = true;
        lastSelectedObject = avatar.name;

        //if (avatar.name.Contains("D"))
        //{
        if (AvatarHasLink(int.Parse(avatar.name.Split('-')[0].Split('D')[0])))
        {
            AddBubbleLink(avatar);
        }
        //}

        RepeatButton.SetActive(false);

        HighlitingController.hasHighlightedObject = true;

        Debug.Log("Highlight Avatar : " + avatar.name);
    }

    public bool AvatarHasLink(int id)
    {
        BubbleDB bubble = databaseService.GetComponent<DBScript>().GetBubbleInfo(id);
        if (bubble != null)
        {
            if (!avatarsLinks.ContainsKey(bubble.AvatarId))
            {
                avatarsLinks.Add(bubble.AvatarId, bubble.BubbleLink);
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public void AddBubbleLink(GameObject avatar)
    {
        GameObject newBubble = Instantiate(bubble);
        newBubble.name = "bubble#" + avatar.name;
        string imagePath = Application.persistentDataPath + "/bubble/" + avatar.name + ".png";
        Texture2D image = LoadPNG(imagePath);

        newBubble.transform.Find("frontFace").GetComponent<MeshRenderer>().material.shader = Shader.Find("Legacy Shaders/Transparent/Diffuse");
        newBubble.transform.Find("frontFace").GetComponent<MeshRenderer>().material.SetTexture("_MainTex", image);
        newBubble.transform.Find("backFace").GetComponent<MeshRenderer>().material.shader = Shader.Find("Legacy Shaders/Transparent/Diffuse");
        newBubble.transform.Find("backFace").GetComponent<MeshRenderer>().material.SetTexture("_MainTex", image);

        newBubble.transform.position = avatar.transform.position + new Vector3(0.03f, 0.2f, 0);
        newBubble.transform.SetParent(avatar.transform);
    }

    public Texture2D LoadPNG(string filePath)
    {

        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }
        return tex;
    }

    int MAIN_ID = -1;
    int LEFT = -1;
    int RIGHT = -1;

    public void PlaceComplexAvatar(GameObject avatar, float value, string position)
    {
        //Vector3 pos = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width/2,Screen.height/2, 2f));
        //Vector3 pos = PlaneIndecator.PlaneIndicator.transform.position;

        Vector3 pos = avatarsToPlace[avatar.name.Split('-')[0]];

        if (position == "center")
        {
            avatar.transform.position = pos;
        }
        else
        {
            if (position == "left")
            {
                float distance = 0.3f;
                avatar.transform.position = pos + new Vector3(distance, 0, 0);  //PlaneIndecator.PlaneIndicator.transform.position;
            }
            else
            {
                if (position == "right")
                {
                    float distance = 0.3f;
                    avatar.transform.position = pos + new Vector3(-distance, 0, 0);  //PlaneIndecator.PlaneIndicator.transform.position;
                }
            }
        }

        avatar.transform.localRotation = Quaternion.identity;
        //avatar.transform.SetParent(mainOb.transform);

        Vector3 cam_position = Camera.main.transform.position;
        cam_position.y = avatar.transform.position.y;

        // Roate the Avatar to the Camera
        avatar.transform.LookAt(cam_position);

#pragma warning disable CS0618 // Type or member is obsolete
        avatar.transform.Rotate(Vector3.up, 180f);
#pragma warning restore CS0618 // Type or member is obsolete

        avatarsList.Add(avatar);

        // Scale the Avatar into the World Scale
        // Debug.Log("Avatar Scale Value : "+ value + " , "+ Vector3.one * (value / 100));
        avatar.transform.localScale = Vector3.one * (value / 100);

        // Disable the Plane Indicator after adding the avatar
        // PlaneIndecator.placementIndicator.SetActive(false);
    }

    Dictionary<int, string> requestIds;
    Dictionary<int, int> fileRequests;
    Dictionary<int, int> musicRequests;

    bool downloadCompleted = false;
    bool downloadInProcess = false;


    // Use this for initialization
    void Start()
    {
        mainOb = GameObject.FindGameObjectWithTag("mainob");

        //PlaneIndecator.HitTestMode = HitTestMode.AUTOMATIC;

        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            if (_InPhoneCall())
            {
                PhoneCall.existedCall = true;
                // Disable Hands Free Mode
                HandsFreeButton.SetActive(false);
                HandsFreeRecordButton.SetActive(false);
            }
        }

        avatarsToPlace = new Dictionary<string, Vector3>();

        this.loadingTimer = new System.Timers.Timer();
        this.loadingTimer.Interval = 1.77 * 1000f;
        this.loadingTimer.Elapsed += LoadingTimerFinished;
        this.loadingTimer.AutoReset = false;
        this.loadingTimer.Enabled = true;


        mutex = new object();

        ComplexAvatarsIds = new List<string>();
        avatarsList = new List<GameObject>();
        bundlesList = new List<AssetBundle>();

        avatarsDictionary = new Dictionary<string, int>();
        avatarsLinks = new Dictionary<int, string>();

        avatarsDuplicates = new Dictionary<int, List<int>>();

        musicDictionary = new Dictionary<int, List<MusicItem>>();
        animationsLength = new List<float>();

        audiosIds = new List<string>();
        audioTracks = new List<GameObject>();

        avatarHighlight.SetActive(false);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));

        StartCoroutine(CreateThumbnailPictureDirectory());
        StartCoroutine(CreateVideosDirectory());
        StartCoroutine(CreateBubblesDirectory());
   
        //// Set Loding Animation Logo at the center
        Vector3 logoPosition = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 3));
        loadingLogo.transform.position = logoPosition;

        //string URL = "https://hooklinestaging.azurewebsites.net/api/action/AppVersion";
        //StartCoroutine(StartHooklineApp(URL));

            string ALL_CATEGORIES_URL = API_Settings.ONLINE_SERVICE_URL + API_Settings.GET_ALL_CATEGORIES;
            try
            {
                StartCoroutine(LoadAllCategories(ALL_CATEGORIES_URL));
            }
            catch (Exception ex)
            {
                Debug.Log("Loading Tray Exception : " + ex.Message);

                downloadInProcess = false;
                downloadCompleted = false;

                while (!downloadCompleted && !downloadInProcess)
                {
                    StartCoroutine(LoadAllCategories(ALL_CATEGORIES_URL));
                }
            }

        
    }



    public IEnumerator StartHooklineApp(string URL)
    {
        using (UnityWebRequest www = UnityWebRequest.Get(URL))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(" Download Categoris Error : " + www.error);
                yield return new WaitForEndOfFrame();
            }
            else
            {
                JsonVersionHandler currentVersion = JsonUtility.FromJson<JsonVersionHandler>(www.downloadHandler.text);
                int onlineVersion = int.Parse(currentVersion.version.Replace(".", ""));
                int mobileVersion = int.Parse(VersionHandler.CURRENT_VERSION.Replace(".", ""));

                Debug.Log("Online Version : " + onlineVersion);
                Debug.Log("Offline Version : " + mobileVersion);

                if (onlineVersion > mobileVersion)
                {
                    updatePanel.SetActive(true);
                    Debug.Log("Please Update..");
                    yield return new WaitForEndOfFrame();
                }
                else
                {
                    string ALL_CATEGORIES_URL = API_Settings.ONLINE_SERVICE_URL + API_Settings.GET_ALL_CATEGORIES;
                    StartCoroutine(LoadAllCategories(ALL_CATEGORIES_URL));
                    yield return new WaitForEndOfFrame();
                }
                yield return new WaitForEndOfFrame();
            }
        }
    }

    public IEnumerator CreateThumbnailPictureDirectory()
    {
        DirectoryInfo photoInfo = new DirectoryInfo(Application.persistentDataPath + "/photos");
        if (!photoInfo.Exists)
        {
            photoInfo.Create();
            Debug.Log(" Photos Directory Created " + Environment.NewLine);
        }
        else
        {
            Debug.Log(" Photos Directory Existed ");
        }
        yield return null;
    }

    /// <summary>
    /// Create Videos Directory
    /// </summary>
    /// <returns></returns>
    public IEnumerator CreateVideosDirectory()
    {
        DirectoryInfo vidInfo = new DirectoryInfo(Application.persistentDataPath + "/videos");
        if (!vidInfo.Exists)
        {
            vidInfo.Create();
            Debug.Log(" Photos Directory Created " + Environment.NewLine);
        }
        else
        {
            Debug.Log(" Photos Directory Existed ");
        }
        yield return null;
    }

    /// <summary>
    /// Create Bubbles Directory
    /// </summary>
    /// <returns></returns>
    public IEnumerator CreateBubblesDirectory()
    {
        DirectoryInfo vidInfo = new DirectoryInfo(Application.persistentDataPath + "/bubble");
        if (!vidInfo.Exists)
        {
            vidInfo.Create();
            Debug.Log(" Photos Directory Created " + Environment.NewLine);
        }
        else
        {
            Debug.Log(" Photos Directory Existed ");
        }
        yield return null;
    }

    /// <summary>
    /// Creates the directory of the Category.
    /// </summary>
    /// <param name="dirName">Dir name.</param>
    public void CreateDirectory(string dirName)
    {
        DirectoryInfo dirInfo = new DirectoryInfo(Application.persistentDataPath + "/" + dirName);
        if (!dirInfo.Exists)
        {
            dirInfo.Create();
            Debug.Log(" Directory Created : " + dirName);
        }
        else
        {
            Debug.Log(" Directory Existed : " + dirName);
        }
    }

    /// <summary>
    /// Saves the category thumbnail.
    /// </summary>
    /// <param name="name">Name.</param>
    /// <param name="content">Content.</param>
    public void SaveCategoryThumbnail(string name, string content)
    {
        string categoryThumbnailPath = Application.persistentDataPath + "/" + name;
        // Write Category Image in the Category Folder
        if (!File.Exists(categoryThumbnailPath))
        {
            try {
                System.IO.File.WriteAllBytes(categoryThumbnailPath, System.Convert.FromBase64String(content));
            }
            catch (Exception ex) {
                Debug.Log(" Cann't write category image : " + ex.Message);
            }
        }
    }

    public static bool NoAvatarSelected = true;
    public static bool AvatarsHasAnimation = false;

    bool isPlaying = false;

    [Obsolete]
    public void Update()
    {
        #region Hding Sliders if Click out there Borders
        if (Input.touchCount > 0)
        {
            if (!IsPointerOverUIObject()) {
                if (avatarPanel.activeInHierarchy)
                {
                    if (Input.mousePosition.y > AvatarPanelHight) {
                        HideSlider();
                    }
                }
                else
                {
                    if (categorySlider.activeInHierarchy) {
                        if (Input.mousePosition.y > CategoryPanelHight)
                        {
                            HideSlider();
                        }
                    }
                }
            }

            if (Input.touches[0].phase == TouchPhase.Began)
            {
                GameObject hitted = ReturnClickedObject();

                // Click Bubble 
                if (hitted != null && hitted.transform.name.StartsWith("bubble", System.StringComparison.Ordinal))
                {
                    int avatarId = int.Parse(hitted.transform.name.Split('#')[1]);
                    string Url = avatarsLinks[avatarId];
                    Debug.Log("Visiting : " + Url);
                    webController.GetComponent<WebViewController>().GoToWebsite(Url);
                }
            }

        }
        #endregion

        avatarHighlight.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);

        // Hide Surface Detection Message
        if (FocusIndicator.SquareState == FocusSquare.FocusState.Found)
        {
            meassageDetectSurface.SetActive(false);
        }

        #region Place Avatars in (To Be Placed Avatars)

        //if (PlaneIndecator.SurfceDetected)
        //{
        //    // Hide Message if Existed
        //    meassageDetectSurface.SetActive(false);
        //}

        if (avatarsList.Count == 0)
        {
            //if (RepeatButton.activeInHierarchy)
            //{
            RepeatButton.SetActive(false);
            //}
        }


        if (animationTimerFinisted && anim_loop_nb <= 0)
        {
            SwichToPlayAnimation();
            animationTimerFinisted = false;
        }

        // Loading Animation Finished
        if (loadingAnimationFinished)
        {
            loadingAnimationFinished = false;
            loadingTimer.Stop();
        }

        NoAvatarSelected = true;
        AvatarsHasAnimation = false;

        int count = avatarsList.Count;

        for (int i = 0; i < count; i++)
        {
            if (avatarsList[i] != null)
            {
                if (avatarsList[i].GetComponent<Lean.Touch.LeanSelectable>() != null)
                {
                    if (avatarsList[i].GetComponent<Lean.Touch.LeanSelectable>().IsSelected)
                    {
                        NoAvatarSelected = false;
                        //break;
                    }
                }
            }
        }

        if (NoAvatarSelected == true)
        {
            cleanAvatar.SetActive(false);
        }
        else
        {
            if (!cleanAvatar.activeInHierarchy)
            {
                cleanAvatar.SetActive(true);
            }
        }

        #endregion
    }

    public void CheckAvatarsAnimation()
    {
        AvatarsHasAnimation = false;

#pragma warning disable CS0618 // Type or member is obsolete
        int count = avatarsList.Count;
#pragma warning restore CS0618 // Type or member is obsolete

        for (int i = 0; i < count; i++)
        {
            if (avatarsList[i] != null)
            {
                Animation anim = avatarsList[i].GetComponent<Animation>();
                if (anim != null)
                {
                    if (anim.GetClipCount() > 0)
                    {
                        AvatarsHasAnimation = true;
                    }
                }
            }
        }
        // Show Animation Button 
        RepeatButton.SetActive(AvatarsHasAnimation);
    }

    public GameObject ReturnClickedObject()
    {
        GameObject target = null;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit[] hits;
        hits = Physics.RaycastAll(ray.origin, ray.direction * 100);

        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].transform.name.StartsWith("bubble", StringComparison.Ordinal))
            {
                target = hits[i].transform.gameObject;
                return target;
            }
        }
        return target;
    }

    //IEnumerator LoadnewAvatarToTheScene()
    //{
    //    yield return new WaitForSeconds(0.5f);

    //    lock (mutex) {
    //        //Debug.Log("Loading Avatar to the Scene .. 1");
    //        //if (Surface.Detected) {
    //            // Hide Message if Existed
    //            meassageDetectSurface.SetActive(false);
    //            if (avatarsToPlace.Count > 0)   {
    //                    foreach (string avatarID in avatarsToPlace.Keys) {
    //                        if (!isPalced(avatarID))  
    //                            { 
    //                                    if (!isPlacingAvatar && !isDownloading) {
    //                                    //Debug.Log("Loading Avatar to the Scene : "+avatarID );
    //                                    if (NumberOfAvatarsUnderLimit()) 
    //                                    {
    //                                        LoadAvatarAssencByName(avatarID);
    //                                    }
    //                            }
    //                        }
    //                    }
    //            }
    //        //}
    //    }
    //    yield return new WaitForEndOfFrame();
    //}

    public bool NumberOfAvatarsUnderLimit()
    {
        return (avatarsList.Count < ApplicationSettings.MAX_AVATARS);
    }

    public void CheckIfHasAnimation()
    {
        int numClildren = avatarsList.Count;
        Debug.Log("CHECKING FOR ANIMATION");

        if (numClildren >= 1)
        {
            for (int i = 0; i < numClildren; i++)
            {
                if (avatarsList[i].name != "Highlight")
                {
                    Animation anim = avatarsList[i].GetComponent<Animation>();

                    if (anim != null)
                    {
                        if (anim.GetClipCount() > 0)
                        {
                            RepeatButton.SetActive(true);
                        }
                    }
                    else
                    {
                        Debug.Log("Check Animations in Child .. \n");
                        if (avatarsList[i].transform.childCount > 0)
                        {
                            anim = avatarsList[i].transform.GetChild(0).GetComponent<Animation>();

                            if (anim != null)
                            {
                                if (anim.GetClipCount() > 0)
                                {
                                    RepeatButton.SetActive(true);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    public float CalculateDistance(Vector3 origin, Vector3 distination)
    {
        return Vector3.Distance(origin, distination);
    }

    public float CalculateDistanceManually(Vector3 origin, Vector3 distination)
    {
        return (float)Math.Sqrt(Math.Pow((origin.x - distination.x), 2) + Math.Pow((origin.y - distination.y), 2) + Math.Pow((origin.z - distination.z), 2));
    }

    private string GetFileNameWithoutExtention(FileInfo info)
    {
        int index = info.FullName.LastIndexOf('/');
        int LastIndex = info.FullName.LastIndexOf('.');
        string thumbname = info.FullName.Substring(index + 1, LastIndex - index - 1);
        return thumbname;
    }

    /// <summary>
    /// Loads all categories.
    /// </summary>
    /// <returns>The all categories.</returns>
    /// <param name="URL">URL.</param>
    public IEnumerator LoadAllCategories(string URL)
    {
        downloadInProcess = true;
        //double internetSpeed = 0;
        //yield return StartCoroutine( NetworkInterface.CalculateDownloadSpeed(internetSpeed));

        yield return new WaitForSeconds(1f);

        bool needToUpdate = false;
        List<int> categoryIDS = databaseService.GetComponent<DBScript>().GetAllCategories().Where<AvatarCategory>(c => c.Type == "Tray").ToList<AvatarCategory>().Select(c => c.Id).ToList();
        DirectoryInfo di = new DirectoryInfo(Application.persistentDataPath);
        FileInfo [] filesOfThumnails = di.GetFiles("Category-*.png");


        if (filesOfThumnails.Length > 0) {
            foreach (var file in filesOfThumnails) {
                int filename = int.Parse(file.Name.Replace("Category-", "").Replace(".png", ""));
                if (categoryIDS.Contains(filename)) {
                    needToUpdate = true;
                }
            }
        }

        if (needToUpdate) {
                yield return StartCoroutine(LoadThumnails());
                yield return StartCoroutine(UpdateCategories(URL));
        }
        else {
            if (InternetReachable())
            {
                yield return StartCoroutine(DownloadAllCategories(URL));
            }
            else
            {
                yield return StartCoroutine(ShowInternetConnectinMessage(20));
            }
           
        }
        downloadCompleted = true;
        downloadInProcess = false;
    }

    /// <summary>
    /// Downloads all categories.
    /// </summary>
    /// <returns>The all categories.</returns>
    /// <param name="URL">URL.</param>
    public IEnumerator DownloadAllCategories(string URL)
    {
        DirectoryInfo di = new DirectoryInfo(Application.persistentDataPath);
        Debug.Log(" Starting Downloading  Categories ...");

        downloadingDataIndicator.SetActive(true);

        DateTime start ;
        DateTime finish ;


        using (UnityWebRequest www = UnityWebRequest.Get(URL))
        {
            www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
            www.timeout = 600;
            start = DateTime.Now;

            yield return www.SendWebRequest();


            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(" Download Categoris Error : " + www.error);
            }
            else
            {
                    DateTime beforeParse = DateTime.Now;
                    Debug.Log("Download Time : " + (beforeParse - start).TotalMilliseconds);
                    // Load Response 
                    List<JsonCategory> allCategories = JsonConvert.DeserializeObject<List<JsonCategory>>(www.downloadHandler.text);
                    DateTime afterParse = DateTime.Now;
                    
                    Debug.Log("Parse Time : " + (afterParse - beforeParse).TotalMilliseconds);

                    Debug.Log("Downloaded Categories are : " + allCategories.Count());

                    foreach (JsonCategory category in allCategories)
                    {
                        // Create Folder foreach category
                        string catDirectory = "Category-" + category.id;
                        CreateDirectory(catDirectory);

                        // Save Categories in Database 
                        int cat_id = category.id;

                        // Inser Category in Database
                        databaseService.GetComponent<DBScript>().InsertCategory(cat_id, category.tag, category.type);

                        // Save Category Thumbnail
                        string categoryThumbname = "Category-" + category.id + ".png";
                        SaveCategoryThumbnail(categoryThumbname, category.tagPhotoContent);

                        // Get Avatars Thumbnails Related to the Category
                        foreach (JsonAvatar avatar in category.avatars)
                        {
                            yield return StartCoroutine(SaveAvatarWithAllInfo(avatar, category.id));
                            yield return StartCoroutine(SaveAvatarThumbnail(category.id, avatar));
                        }

                        string thumbnailPath = Application.persistentDataPath + "/Category-" + category.id + ".png";
                        yield return StartCoroutine(CreateTexture(thumbnailPath));
                    }
            }
        }

        downloadingDataIndicator.SetActive(false);

        yield return null;
    }

    /// <summary>
    /// Updates the categories.
    /// </summary>
    /// <returns>The categories.</returns>
    /// <param name="URL">URL.</param>
    public IEnumerator UpdateCategories(string URL)
    {
        DirectoryInfo di = new DirectoryInfo(Application.persistentDataPath);
        Debug.Log(" Starting Updating  Categories ...");

        using (UnityWebRequest www = UnityWebRequest.Get(URL))
        {
            www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError) {
                Debug.Log(" Download Categoris Error : " + www.error);
                yield return null;
            }

            else
            {
                // Load Response 
                List<JsonCategory> allCategories = JsonConvert.DeserializeObject<List<JsonCategory>>(www.downloadHandler.text);

                List<int> allWebCategories = allCategories.Select(c => c.id).ToList();

                List<int> categoryIDS = databaseService.GetComponent<DBScript>().GetAllCategories().Where<AvatarCategory>(c => c.Type == "Tray").ToList<AvatarCategory>().Select(c => c.Id).ToList();

                Debug.Log("Web Categories : " + allWebCategories.Count);
                Debug.Log("Category Ids From Database : " + categoryIDS.Count());

                List<int> categoriesToBeDeleted = new List<int>();

                Debug.Log("Categories Count : " + allCategories.Count());

                foreach (JsonCategory category in allCategories)
                {
                    // if Category Not Existed In DataBase
                    Debug.Log("Category : " + category.tag + " : " + category.id);
                    int id = category.id;

                    if (!categoryIDS.Contains(id))
                    {
                        Debug.Log("Category : " + category.id + " Not Existed In Database");

                        // Create Folder foreach category
                        string catDirectory = "Category-" + category.id;
                        CreateDirectory(catDirectory);

                        // Save Categories in Database 
                        int cat_id = category.id;

                        // Inser Category in Database
                        databaseService.GetComponent<DBScript>().InsertCategory(cat_id, category.tag, category.type);

                        // Save Category Thumbnail
                        string categoryThumbname = "Category-" + category.id + ".png";
                        SaveCategoryThumbnail(categoryThumbname, category.tagPhotoContent);

                        // Get Avatars Thumbnails Related to the Category
                        foreach (JsonAvatar avatar in category.avatars)
                        {
                            yield return StartCoroutine(SaveAvatarWithAllInfo(avatar, category.id));
                            yield return StartCoroutine(SaveAvatarThumbnail(category.id, avatar));
                        }
                    }

                    // If Category Existed
                    else
                    {
                        int cat_id = category.id;
                        // Inser Category in Database
                        databaseService.GetComponent<DBScript>().UpdateCategory(cat_id, category.tag, category.type);

                        // Create Folder foreach category
                        string catDirectory = "Category-" + category.id;
                        CreateDirectory(catDirectory);

                        // Save Category Thumbnail
                        string categoryThumbname = "Category-" + category.id + ".png";
                        SaveCategoryThumbnail(categoryThumbname, category.tagPhotoContent);

                        List<JsonAvatar> avatars = category.avatars;

                        List<int> allAvatarsIDS = category.avatars.Select(c => c.avatarId).Distinct().ToList();

                        Debug.Log("Category Avatars Count   : " + allAvatarsIDS.Count);

                        List<AvatarInfo> db_category_avatars = databaseService.GetComponent<DBScript>().GetAvatarsByCategoryID(category.id).ToList<AvatarInfo>();

                        Debug.Log("Database Category Avatars Count   : " + db_category_avatars.Count);

                        List<int> db_category_avatarsIDS = db_category_avatars.Select(c => c.avatarId).Distinct().ToList();
                        List<int> avatarsToBeDeleted = new List<int>();
                        List<JsonAvatar> avatarsToBeAdded = new List<JsonAvatar>();

                        foreach (JsonAvatar ja in avatars)
                        {
                            if (!db_category_avatarsIDS.Contains(ja.avatarId))
                            {
                                avatarsToBeAdded.Add(ja);
                            }
                        }

                        Debug.Log("Avatars to be added : " + avatarsToBeAdded.Count);
                        foreach (JsonAvatar ja in avatarsToBeAdded)
                        {
                            yield return StartCoroutine(SaveAvatarWithAllInfo(ja, category.id));
                            yield return StartCoroutine(SaveAvatarThumbnail(category.id, ja));
                        }

                        foreach (AvatarInfo db_avatar in db_category_avatars)
                        {
                            if (!allAvatarsIDS.Contains(db_avatar.avatarId)) {
                                avatarsToBeDeleted.Add(db_avatar.avatarId);
                            }
                        }

                        // Delete Avatars
                        Debug.Log("Avatars to be deleted : " + avatarsToBeDeleted.Count);
                        foreach (int avatar_id in avatarsToBeDeleted)
                        {
                            // DELETE FBX FILE AND THUMBNAIL
                            AvatarInfo _avatar = databaseService.GetComponent<DBScript>().GetAvatarInfoById(avatar_id);
                            // DELETE FROM DATABASE WITH FBX AND ANIMATION
                            yield return StartCoroutine(DeleteAvatarInfoFiles(_avatar));
                            yield return StartCoroutine(DeleteAvatar(_avatar));

                            databaseService.GetComponent<DBScript>().DeleteAvatarInfo(_avatar);
                        }

                        // Update AvatarsInfo
                        Debug.Log("Avatars in category : " + category.avatars.Count);
                        foreach (JsonAvatar ja in category.avatars) {
                            UpdateAvatarWithAllInfo(ja, category.id);

                            if (!ThumbnailExist(category.id, ja))
                            {
                                yield return StartCoroutine(SaveAvatarThumbnail(category.id, ja));
                            }
                        }
                    }
                    Debug.Log("End Category : " + category.tag);
                }

                foreach (int catID in categoryIDS)
                {
                    if (!allWebCategories.Contains(catID)) {
                        categoriesToBeDeleted.Add(catID);
                    }
                }

                // Delete Categories
                foreach (int catId in categoriesToBeDeleted)
                {
                    Debug.Log("Start Deleting");
                    yield return StartCoroutine( DeleteCategory(catId) ) ;
                }

            }

            DeleteCategoriesFromPanel();

            yield return StartCoroutine(LoadThumnails());

        }
        yield return new WaitForEndOfFrame();
    }


    public void SaveBubbleThumbnail(JsonAvatar avatar)
    {
        string filePath = Application.persistentDataPath + "/bubble/" + avatar.avatarId + ".png";
        System.IO.File.WriteAllBytes(filePath, System.Convert.FromBase64String(avatar.bubbleIcon));
    }

    public void DeleteBubbleThumbnail(JsonAvatar avatar)
    {
        string filePath = Application.persistentDataPath + "/bubble/" + avatar.avatarId + ".png";
        if (File.Exists(filePath))
        {
            System.IO.File.Delete(filePath);
        }
    }

    /// <summary>
    /// Check if the Thumbnail of the Avatar Exists
    /// </summary>
    /// <returns><c>true</c>, if not exist was thumbnailed, <c>false</c> otherwise.</returns>
    /// <param name="cat_id">Cat identifier.</param>
    /// <param name="avatar">Avatar.</param>
    public bool ThumbnailExist(int cat_id, JsonAvatar avatar)
    {
        string filePath = Application.persistentDataPath + "/Category-" + cat_id + "/" + avatar.avatarId + ".png";
        bool isExisted = File.Exists(filePath);
        return isExisted;
    }

    /// <summary>
    /// Saves the avatar thumbnail.
    /// </summary>
    /// <returns>The avatar thumbnail.</returns>
    /// <param name="cat_id">Cat identifier.</param>
    /// <param name="avatar">Avatar.</param>
    public IEnumerator SaveAvatarThumbnail(int cat_id, JsonAvatar avatar)
    {
        string filePath = Application.persistentDataPath + "/Category-" + cat_id + "/" + avatar.avatarId + avatar.fileExtension;
        System.IO.File.WriteAllBytes(filePath, System.Convert.FromBase64String(avatar.thumbnailContent));
        yield return null;
    }

    /// <summary>
    /// Update Avatar With All Info
    /// </summary>
    /// <param name="avatar"></param>
    /// <param name="id"></param>
    public void UpdateAvatarWithAllInfo(JsonAvatar avatar, int id)
    {
        try
        {
            // Map Avatar To Database Object
            AvatarInfo db_avatarInfo = new AvatarInfo();
            db_avatarInfo.category = id;
            db_avatarInfo.avatarId = avatar.avatarId;
            db_avatarInfo.fileName = avatar.fileName;
            db_avatarInfo.avatarName = avatar.avatarName;
            db_avatarInfo.fileExtension = avatar.fileExtension;
            db_avatarInfo.complex = avatar.complex;
            db_avatarInfo.mode = avatar.mode;

            // Insert it To Database
            databaseService.GetComponent<DBScript>().InsertAvatarInfo(db_avatarInfo);

            if (!string.IsNullOrEmpty(avatar.bubbleLink) && !string.IsNullOrWhiteSpace(avatar.bubbleLink))
            {
                // Insert it To Database
                if (avatar.bubbleLink.Length > 0)
                {
                    databaseService.GetComponent<DBScript>().InsertBubbleInfo(avatar.avatarId, avatar.bubbleLink);
                    SaveBubbleThumbnail(avatar);
                }
            }
            else
            {
                //Debug.Log("Bubble Link Null : "+ avatar.avatarId);
                databaseService.GetComponent<DBScript>().DeleteBubbleInfo(avatar.avatarId);
                DeleteBubbleThumbnail(avatar);
            }

            // Insert FBX File Details to DB
            foreach (JsonFBX fbx in avatar.fbx)
            {
                DB_FBX db_fbx = new DB_FBX();
                db_fbx.Id = fbx.fbxId;
                db_fbx.AvatarID = avatar.avatarId;
                db_fbx.IsMain = fbx.isMain;
                db_fbx.ColliderShape = fbx.colliderShape;
                db_fbx.CutOutMaterial = fbx.cutOutMaterial;

                db_fbx.Behaviour = fbx.behaviour;
                db_fbx.RunMultipleAnimation = fbx.runMultipleAnimation;

                // Insert FBX in Database
                databaseService.GetComponent<DBScript>().InsertFBXDetails(db_fbx);

                foreach (JsonAnimation animInfo in fbx.animation)
                {
                    AnimationInfo db_animation = new AnimationInfo();
                    db_animation.Id = animInfo.id;
                    db_animation.AvatarId = animInfo.avatarId;
                    db_animation.FbxId = fbx.fbxId;
                    db_animation.Order = animInfo.order;
                    db_animation.Repeat = animInfo.repeat;
                    db_animation.RepeatCount = animInfo.repeatCount;
                    db_animation.Path = animInfo.path;
                    db_animation.FileName = animInfo.fileName;
                    db_animation.Delay = animInfo.delay;
                    db_animation.AnimationId = animInfo.animationId;
                    db_animation.AnimationName = animInfo.animationName;
                    db_animation.AvatarName = animInfo.avatarName;

                    //Debug.Log("Update Music List : " + animInfo.music_list);
                    if (animInfo.music_list.Count > 0)
                    {
                        foreach (JsonMusic jsonMusic in animInfo.music_list)
                        {
                            MusicDB musicDB = new MusicDB
                            {
                                MusicId = jsonMusic.musicId,
                                animationId = animInfo.id,
                                order = jsonMusic.order,
                                delay = jsonMusic.delay
                            };
                            databaseService.GetComponent<DBScript>().UpdateMusic(musicDB);
                        }
                    }
                    // Insert Animation to DataBase
                    databaseService.GetComponent<DBScript>().InsertAnimationInfo(db_animation);
                }
            }
        }
        catch (Exception ex)
        {
            Debug.Log("DB Exception : " + ex.Message);
        }

    }


    /// <summary>
    /// Save Avatar With All Info
    /// </summary>
    /// <param name="avatar"></param>
    /// <param name="id"></param>
    public IEnumerator SaveAvatarWithAllInfo(JsonAvatar avatar, int id)
    {
        // Map Avatar To Database Object
        AvatarInfo db_avatarInfo = new AvatarInfo();
        db_avatarInfo.category = id;
        db_avatarInfo.avatarId = avatar.avatarId;
        db_avatarInfo.fileName = avatar.fileName;
        db_avatarInfo.avatarName = avatar.avatarName;
        db_avatarInfo.fileExtension = avatar.fileExtension;
        db_avatarInfo.complex = avatar.complex;

        db_avatarInfo.mode = avatar.mode;

        // Insert it To Database
        databaseService.GetComponent<DBScript>().InsertAvatarInfo(db_avatarInfo);

        if (!string.IsNullOrEmpty(avatar.bubbleLink) && !string.IsNullOrWhiteSpace(avatar.bubbleLink))
        {
            // Insert it To Database
            databaseService.GetComponent<DBScript>().InsertBubbleInfo(avatar.avatarId, avatar.bubbleLink);
            SaveBubbleThumbnail(avatar);
        }

        // Insert FBX File Details to DB
        foreach (JsonFBX fbx in avatar.fbx)
        {
            DB_FBX db_fbx = new DB_FBX();
            db_fbx.Id = fbx.fbxId;
            db_fbx.AvatarID = avatar.avatarId;
            db_fbx.IsMain = fbx.isMain;
            db_fbx.ColliderShape = fbx.colliderShape;
            db_fbx.CutOutMaterial = fbx.cutOutMaterial;

            //Debug.Log(" Colider Shape : "+fbx.colliderShape);

            db_fbx.Behaviour = fbx.behaviour;
            db_fbx.RunMultipleAnimation = fbx.runMultipleAnimation;

            // Insert FBX in Database
            databaseService.GetComponent<DBScript>().InsertFBXDetails(db_fbx);

            foreach (JsonAnimation animInfo in fbx.animation)
            {
                AnimationInfo db_animation = new AnimationInfo();
                db_animation.Id = animInfo.id;
                db_animation.AvatarId = animInfo.avatarId;
                db_animation.FbxId = fbx.fbxId;
                db_animation.Order = animInfo.order;
                db_animation.Repeat = animInfo.repeat;
                db_animation.RepeatCount = animInfo.repeatCount;

                //Debug.Log("Insert Music List : " + animInfo.music_list);
                db_animation.Path = animInfo.path;
                db_animation.FileName = animInfo.fileName;
                db_animation.Delay = animInfo.delay;
                db_animation.AnimationId = animInfo.animationId;
                db_animation.AnimationName = animInfo.animationName;
                db_animation.AvatarName = animInfo.avatarName;

                //Insert Multiple Music information in DB
                if (animInfo.music_list.Count > 0)
                {
                    foreach (JsonMusic jsonMusic in animInfo.music_list)
                    {
                        MusicDB musicDB = new MusicDB
                        {
                            MusicId = jsonMusic.musicId,
                            animationId = animInfo.id,
                            order = jsonMusic.order,
                            delay = jsonMusic.delay
                        };
                        databaseService.GetComponent<DBScript>().InsertMusic(musicDB);
                    }
                }

                // Insert Animation to DataBase
                databaseService.GetComponent<DBScript>().InsertAnimationInfo(db_animation);
            }
        }
        yield return new WaitForEndOfFrame();
        //yield return StartCoroutine(SaveAvatarThumbnail(id, avatar));
    }

    /// <summary>
    /// Adds the loaded texture to downloading status
    /// </summary>
    /// <param name="texture">Texture.</param>
    public void AddLoadedTextureToDownloading(Texture2D texture)
    {
        Circleobject.GetComponent<RawImage>().texture = texture;
    }

    /// <summary>
    /// On the category button click.
    /// </summary>
    /// <param name="categoryID">Category identifier.</param>
    public void onCategoryButtonClick(string categoryID)
    {
#pragma warning disable CS0618 // Type or member is obsolete
        int length = avatarPanelContent.transform.childCount;
#pragma warning restore CS0618 // Type or member is obsolete

        if (length > 0)
        {
            for (int i = 0; i < length; i++)
            {
                Destroy(avatarPanelContent.transform.GetChild(i).gameObject);
            }
        }

        string categoryPath = Application.persistentDataPath + "/Category-" + categoryID;
        Debug.Log(" Button Category : " + categoryID);

        DirectoryInfo di = new DirectoryInfo(categoryPath);
        FileInfo[] avatarsThumnails = di.GetFiles("*.png");

        foreach (var file in avatarsThumnails)
        {
            StartCoroutine(CreateAvatarPrefabButton(categoryID, file.FullName));
        }

        AvatarCategory category = databaseService.GetComponent<DBScript>().GetCategoryById(int.Parse(categoryID));
        avatarPanel.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = category.Name;
        avatarsSlider.SetActive(true);
    }

    /// <summary>
    /// Deletes the categories from panel.
    /// </summary>
    public void DeleteCategoriesFromPanel()
    {
        for (int i = 0; i < CategoryPanelParent.transform.childCount; i++)
        {
            Destroy(CategoryPanelParent.transform.GetChild(i).gameObject);
        }
    }

    public void PrintAvatarsDictionary()
    {
        foreach (KeyValuePair<string, int> item in avatarsDictionary)
        {
            Debug.Log(string.Format("Key = {0}, Value = {1}", item.Key, item.Value));
        }
    }

    

    /// <summary>
    /// Handling the Click Button of the Avatar
    /// </summary>
    /// <param name="name">Name.</param>
    public void onButtonClick(string categoryID, string name)
    {
        showInternetMeassage = true;

        if (NumberOfAvatarsUnderLimit())
        {
            if (!isDownloading && !isPlacingAvatar)
            {
                Vector3 position = new Vector3(0, 0, 0);
                if (FocusIndicator.SquareState == FocusSquare.FocusState.Found)
                {
                    position = new Vector3(FocusIndicator.foundSquare.transform.position.x,
                                                FocusIndicator.foundSquare.transform.position.y,
                                                FocusIndicator.foundSquare.transform.position.z);


                    //Debug.Log("You click Button : " + name + " Avatar Psition : " + position);

                    if (avatarsDictionary.ContainsKey(name))
                    {
                        Debug.Log(string.Format("Duplicating :  {0} At {1}", name, position));
                        if (!isDownloading && !isPlacingAvatar)
                        {
                            DuplicateAvatar(name, position);
                        }
                    }
                    else
                    {
                        // Duplicate Avatar
                        Debug.Log(string.Format("Adding Avatar : {0} At {1}", name, position));

                        if (!isDownloading && !isPlacingAvatar)
                        {
                            if (!avatarsToPlace.Keys.Contains(name))
                            {
                                avatarsToPlace.Add(name, position);
                            }
                            else
                            {
                                avatarsToPlace[name] = position;
                            }
                            StartCoroutine(LoadAvatarAssencByName(name));
                        }
                    }
                }
                //else
                //{
                //    //isPlacingAvatar = false;
                //    //StartCoroutine(ShowDetectSurfaceMessage());
                //}

            }
            else
            {
                //isPlacingAvatar = false;
                //StartCoroutine(ShowDetectSurfaceMessage());
            }
        }
        else {
            if (!showMaxAvatarsMessage)
            {
                showMaxAvatarsMessage = true;
                StartCoroutine(ShowMaxAvatarsMessage());
            }
        }
    }
    AssetBundle asseBundle;
    // LoadAsset From Bundle
    public GameObject LoadAvatarBundle(string filePath, AvatarInfo avatarInfo)
    {
        Debug.Log("File Path : " + filePath);

        Caching.ClearCache();

        asseBundle = AssetBundle.LoadFromFile(filePath);

        //bundlesList.Add(asseBundle);
        asseBundle.name = avatarInfo.avatarId.ToString();

        string[] allAssetNames = asseBundle.GetAllAssetNames();

        for (int i = 0; i < allAssetNames.Length; i++) {
            Debug.Log("Asset : " + allAssetNames[i]);
        }

        string assetName = "assets/" + avatarInfo.avatarName.ToLower() + ".prefab";

        GameObject bundle = asseBundle.LoadAsset<GameObject>(assetName);

        GameObject avatar = Instantiate(bundle) as GameObject;

        if (asseBundle != null)
        {
            try
            {
                asseBundle.Unload(false);
            }
            catch (Exception ex)
            {
                isPlacingAvatar = false;
                isDownloading = false;
                cancelDownload = false;
                Debug.Log("Exception : "+ex.Message);
            }
            
        }

        return avatar;
    }

    /// <summary>
    /// Loads the avatar assenc.
    /// </summary>
    /// <param name="avatarInfo">Avatar info.</param>
    [Obsolete]
    public void LoadAvatarBundleAssenc(AvatarInfo avatarInfo)
    {
        isPlacingAvatar = true;
        AvatarCategory category = databaseService.GetComponent<DBScript>().GetCategoryById(avatarInfo.category);

        if (!avatarInfo.complex)
        {
            string filePath = Application.persistentDataPath + "/Category-" + avatarInfo.category + "/" + avatarInfo.avatarId + ".unity3d";
            List<DB_FBX> fbx = databaseService.GetComponent<DBScript>().GetFbxByAvatarId(avatarInfo.avatarId).ToList<DB_FBX>();
            List<AnimationInfo> animations = databaseService.GetComponent<DBScript>().GetAnimationByFbxID(fbx[0].Id).ToList<AnimationInfo>();

            loadingLogo.SetActive(true);

            GameObject avatar = LoadAvatarBundle(filePath, avatarInfo);
            //avatar.AddComponent<ReverseNormals>();

            avatar.name = avatarInfo.avatarId.ToString();

            // Adding the ability to move
            AddAbilityToMove(avatar, category.Name);

            // Place Avatar in the scene
            PlaceAvatar(avatarInfo.avatarId.ToString(), avatar, 1f);

            // Hide Loading Logo
            loadingLogo.SetActive(false);

            // The Avatar is Placed in the Scene
            isPlacingAvatar = false;

            // Remove it from To Be Placed Avatar
            RemoveFromToBePlaced(avatarInfo.avatarId.ToString());

            if (animations.Count > 0)
            {
                Animation anim = avatar.GetComponent<Animation>();

                if (!anim)
                {
                    Debug.Log(" Animation Existed in Children ... \n");

                    if (avatar.transform.childCount > 0)
                    {
                        anim = avatar.transform.GetChild(0).GetComponent<Animation>();
                    }
                }

                if (anim != null)
                {
                    if (AvatarHasMusic(avatarInfo.avatarId.ToString()))
                    {
                        Debug.Log("Avatar Has Music ..");
                        StartCoroutine(LoadMusicForAvatar(avatarInfo.category.ToString(), animations[0], anim.clip.length));
                    }

                    if (animations[0].Repeat == "Once")
                    {
                        foreach (AnimationState animState in anim)
                        {
                            animationsLength.Add(animState.length);
                        }
                    }

                    if (animations[0].Repeat == "Loop")
                    {
                        avatar.AddComponent<AudioVideoSync>();
                        anim_loop_nb += 1;
                    }
                }

            }

            // add it to Avatars List
            avatarsDictionary.Add(avatarInfo.avatarId.ToString(), 1);
            

            //AddAvatarIdentifier(avatar);

            addAvatars.SetActive(true);

            HighlightObject(avatar);

            //AddAvatarIdentifier(avatar);
        }

    }


    /// <summary>
    /// Repeats the animation.
    /// </summary>
    public void RepeateAnimation()
    {
        // mute of un-mute the avatar sound
        CheckAudioSources();

        if (!isRepeating)
        {
            Debug.Log("Repeating .. ");
            isRepeating = true;
            RepeatButton.GetComponent<Button>().image.sprite = pauseTexture;

            if (animationsLength.Count > 0)
            {
                float maxAnimationLength = animationsLength.Max();
                Debug.Log("Max Time : " + maxAnimationLength);
                this.animationTimer = new System.Timers.Timer();
                this.animationTimer.Interval = maxAnimationLength * 1000;
                this.animationTimer.Elapsed += AnimationTimerFinished;
                this.animationTimer.AutoReset = false;
                this.animationTimer.Enabled = true;
                this.animationTimer.Start();
            }

            StartCoroutine(RepeatAvatarAnimation());
        }
        else
        {
            Debug.Log("Calling Pause Animation .. ");
            StartCoroutine(PauseAvatarAnimation());
            isRepeating = false;
            RepeatButton.GetComponent<Button>().image.sprite = playTexture;
        }
    }

    /// <summary>
    /// Loads the music for avatar.
    /// </summary>
    /// <returns>The music for avatar.</returns>
    /// <param name="CategoryID">Category identifier.</param>
    /// <param name="animModel">Animation model.</param>
    public IEnumerator LoadMusicForAvatar(string CategoryID, AnimationInfo animModel, float anim_lenght)
    {
        if (animModel != null)
        {
            string filePath = Application.persistentDataPath + "/Category-" + CategoryID;
            DirectoryInfo di = new DirectoryInfo(filePath);
            FileInfo[] musicFiles = di.GetFiles(animModel.AvatarId + "-" + animModel.AnimationName + "*.mp3");
            Debug.Log("Music File Count : " + musicFiles.Length);

            if (musicFiles.Length == 1)
            {
                // GET THE MUSIC FILE NAME
                // Get the music ID From the Path
                string musicID = musicFiles[0].Name.Replace(".mp3", "").Split('-')[2];

                //// Get Music Item From the Database

                // Find The Music that is related to the Animation  

                // TODO
                MusicDB musicDB = databaseService.GetComponent<DBScript>().GetMusicItemById(animModel.Id, int.Parse(musicID));

                // Define Music Item
                Debug.Log("Music Delay : " + musicDB.delay);

                MusicItem item = new MusicItem(musicDB.MusicId, musicDB.delay);
                List<MusicItem> listMusic = new List<MusicItem>();
                listMusic.Add(item);

                // Add Music Item to Dictionary
                if (!musicDictionary.ContainsKey(animModel.AvatarId))
                {
                    musicDictionary.Add(animModel.AvatarId, listMusic);
                }

                //Debug.Log("Music ID : "+ musicID);
                if (!audiosIds.Contains(animModel.AvatarId.ToString()))
                {
                    // Get the Music Path

                    string musicPath = musicFiles[0].FullName;

                    if (File.Exists(musicPath))
                    {
                        // Play the music for avatar
                        WWW www = new WWW("file://" + musicPath);

                        yield return www;
                        if (www != null)
                        {
                            GameObject avatarMusic = Instantiate(musicTrack);

                            avatarMusic.AddComponent<AudioSource>();
                            avatarMusic.name = "music-" + animModel.AvatarId;
                            avatarMusic.GetComponent<AudioSource>().clip = www.GetAudioClip(false, true) as AudioClip;

                            if (animModel.Repeat != "Loop")
                            {
                                avatarMusic.GetComponent<AudioSource>().loop = false;
                            }
                            else
                            {
                                avatarMusic.GetComponent<AudioSource>().loop = true;
                            }
                            audiosIds.Add(animModel.AvatarId.ToString());
                            audioTracks.Add(avatarMusic);
                        }
                        else
                        {
                            Debug.Log("WWWW is null");
                            yield return null;
                        }
                    }
                    else
                    {
                        Debug.Log("This avatar is not content on Music");
                        yield return null;
                    }
                }
                else
                {
                    yield return null;
                }
            }
            else
            {
                if (musicFiles.Length > 1)
                {

                    GameObject avatarMusic = Instantiate(musicTrack);
                    avatarMusic.name = "music-" + animModel.AvatarId;

                    List<string> musicIds = new List<string>();
                    for (int i = 0; i < musicFiles.Length; i++)
                    {
                        musicIds.Add(musicFiles[i].Name.Replace(".mp3", "").Split('-')[2]);
                    }

                    List<MusicItem> musicItems = new List<MusicItem>();
                    for (int i = 0; i < musicIds.Count; i++)
                    {
                        //// Get Music Item From the Database
                        MusicDB musicItem = databaseService.GetComponent<DBScript>().GetMusicItemById(animModel.Id, int.Parse(musicIds[i]));
                        MusicItem item = new MusicItem(musicItem.MusicId, musicItem.delay);
                        musicItems.Add(item);
                    }

                    // Add Music Item to Dictionary
                    if (!musicDictionary.ContainsKey(animModel.AvatarId))
                    {
                        musicDictionary.Add(animModel.AvatarId, musicItems);
                    }

                    for (int i = 0; i < musicFiles.Length; i++)
                    {

                        // Get the Music Path
                        string musicPath = musicFiles[i].FullName;

                        Debug.Log("Music Path : " + musicPath);

                        if (File.Exists(musicPath))
                        {
                            // Play the music for avatar
                            WWW www = new WWW("file://" + musicPath);
                            yield return www;
                            if (www != null)
                            {
                                AudioSource source = avatarMusic.AddComponent<AudioSource>();
                                source.clip = www.GetAudioClip(false, true) as AudioClip;
                                source.loop = false;
                                if (!audiosIds.Contains(animModel.AvatarId.ToString()))
                                {
                                    audiosIds.Add(animModel.AvatarId.ToString());
                                }
                            }
                            else
                            {
                                Debug.Log("WWWW is null");
                                yield return null;
                            }
                        }
                        else
                        {
                            Debug.Log("This avatar is not content on Music");
                            yield return null;
                        }
                    }
                    audioTracks.Add(avatarMusic);
                }
            }
        }
        yield return null;
    }


    //public void DelayedExecute(float delay, Action onExecute) {
    //    // Check if delay is valid.
    //    if (delay < 0) return;
    //    StartCoroutine(DelayRoutine(delay, onExecute));
    //}

    //private IEnumerator DelayRoutine(float delay, Action onExecute)
    //{
    //    // Wait for given delay
    //    yield return new WaitForSeconds(delay);
    //    onExecute.Invoke();
    //}


    string mainAvatar = "";
    private bool isPlayingMusicMuted = false;

    //public IEnumerator LoadComplexAvatar(string avatar_name, FileInfo[] files, AvatarCategory category, List<DB_FBX> listFbx)
    //{
    //    DB_FBX mainFBX = listFbx.Where<DB_FBX>(avatar => avatar.IsMain == true).SingleOrDefault<DB_FBX>();

    //    loadingLogo.SetActive(true);

    //    loadingAvatarAnimation.SetActive(true);
    //    //if (PlaneIndecator.placementIndicator.activeInHierarchy)
    //    //{
    //    // PlaneIndecator.placementIndicator.SetActive(false);
    //    //}

    //    loadingTimer.Start();
    //    //yield return new WaitForSeconds(0.5f);

    //    for (int i = 0; i < files.Length; i++)
    //    {
    //        if (files[i].Name.Split('-')[1].Replace(".fbx", "") == mainFBX.Id.ToString())
    //        {
    //            MAIN_ID = i;
    //            mainAvatar = avatar_name + "-" + mainFBX.Id.ToString();
    //        }
    //    }

    //    if (MAIN_ID == 0) { LEFT = 1; RIGHT = 2; }
    //    if (MAIN_ID == 1) { LEFT = 0; RIGHT = 2; }
    //    if (MAIN_ID == 2) { LEFT = 0; RIGHT = 1; }

    //    //DelayedExecute(0.7f, () =>
    //    //{
    //    for (int i = 0; i < files.Length; i++)
    //    {

    //        using (var assetLoader = new AssetLoader())
    //        {
    //            var assetLoaderOptions = AssetLoaderOptions.CreateInstance();
    //            assetLoaderOptions.DontLoadMeshes = false;
    //            assetLoaderOptions.GenerateMeshColliders = true;
    //            assetLoaderOptions.AutoPlayAnimations = false;
    //            assetLoaderOptions.DontLoadCameras = false;
    //            assetLoaderOptions.DontLoadLights = false;
    //            assetLoaderOptions.AddAssetUnloader = true;
    //            assetLoaderOptions.GenerateMeshColliders = true;

    //            //assetLoaderOptions.UseCutoutMaterials = fbx[0].CutOutMaterial;
    //            //assetLoaderOptions.UseCutoutMaterials = fbx[0].CutOutMaterial;

    //            // Animation Mode
    //            assetLoaderOptions.AnimationWrapMode = WrapMode.Once;
    //            float unitScaleFactor = 1; //Sets scale factor (quarter) default value.

    //            assetLoader.OnMetadataProcessed += delegate (AssimpMetadataType metadataType, uint metadataIndex, string metadataKey, object metadataValue)
    //            {
    //                //Checks if the metadata key is "UnitScaleFactor", which is the scale factor metadata used in FBX files.
    //                if (metadataKey == "UnitScaleFactor")
    //                {
    //                    unitScaleFactor = (float)metadataValue; //Sets the scale factor value.
    //                }
    //            }; //Sets a callback to process model metadata.


    //            float scale = 1f;
    //            GameObject myGameObject = assetLoader.LoadFromFile(files[i].FullName, assetLoaderOptions, null);

    //            myGameObject.name = files[i].Name.Replace(".fbx", "");

    //            string fbx_id = files[i].Name.Replace(".fbx", "").Split('-')[1];

    //            ComplexAvatarsIds.Add(myGameObject.name);

    //            DB_FBX fbx = databaseService.GetComponent<DBScript>().GetFBXByID(int.Parse(fbx_id));

    //            //AddCollider(myGameObject, category.Name, fbx.ColliderShape);

    //            if (i == MAIN_ID)
    //            {
    //                PlaceComplexAvatar(myGameObject, scale, "center");  // place the avatar on scene
    //                AddAbilityToMoveWithoutScaleComplex(myGameObject, category.Name);
    //            }
    //            else
    //            {
    //                if (i == LEFT)
    //                {
    //                    PlaceComplexAvatar(myGameObject, scale, "left");
    //                    AddAbilityToMoveWithoutScaleComplex(myGameObject, category.Name);
    //                }
    //                if (i == RIGHT)
    //                {
    //                    PlaceComplexAvatar(myGameObject, scale, "right");
    //                    AddAbilityToMoveWithoutScaleComplex(myGameObject, category.Name);
    //                }
    //            }

    //            myGameObject.transform.SetParent(mainOb.transform);

    //            // add it to Avatars List
    //            if (!avatarsDictionary.ContainsKey(avatar_name))
    //            {
    //                avatarsDictionary.Add(avatar_name, 1);
    //            }

    //            AddAvatarIdentifier(myGameObject);

    //        }
    //    }

    //    //GameObject.Find(mainAvatar).GetComponent<Lean.Touch.LeanSelectable>().SelectMe();
    //    //Debug.Log("Now Highlight Complex");

    //    GameObject.Find(mainAvatar).GetComponent<Lean.Touch.LeanSelectable>().SelectMe();

    //    HighlightObject(GameObject.Find(mainAvatar));

    //    // Remove it from To Be Placed Avatar
    //    RemoveFromToBePlaced(avatar_name);
    //    isPlacingAvatar = false;
    //    HideSlider();
    //    addAvatars.SetActive(true);

    //    //downloadStatusPanel.SetActive(false);
    //    loadingLogo.SetActive(false);
    //    loadingAvatarAnimation.SetActive(false);

    //    //});

    //    yield return null;
    //}


    /// <summary>
    /// Load Avatar to the Scenen Assynchronously
    /// </summary>
    /// <param name="avatar_name">Avatar name.</param>
    public IEnumerator LoadAvatarBundleByName(string avatar_name)
    {
        AvatarInfo avatarInfo = databaseService.GetComponent<DBScript>().GetAvatarInfoById(int.Parse(avatar_name));

        LoadAvatarBundleAssenc(avatarInfo);

        yield return null;
    }

    /// <summary>
    /// Load Avatar to the Scenen Assynchronously
    /// </summary>
    /// <param name="avatar_name">Avatar name.</param>
    public IEnumerator LoadAvatarAssencByName(string avatar_name)
    {
        AvatarInfo avatarInfo = databaseService.GetComponent<DBScript>().GetAvatarInfoById(int.Parse(avatar_name));
        bool isExisted = false;
        if (avatarInfo.complex == false)
        {
            string filePath = Application.persistentDataPath + "/Category-" + avatarInfo.category + "/" + avatar_name + ".unity3d";
            if (File.Exists(filePath))
            {
                isExisted = true;
                Debug.Log("Avatar Existed");
            }
        }
        else
        {
            string avatarDir = Application.persistentDataPath + "/Category-" + avatarInfo.category;
            DirectoryInfo dir = new DirectoryInfo(avatarDir);
            FileInfo[] avatarFiles = dir.GetFiles(avatar_name);
            foreach (FileInfo info in avatarFiles)
            {
                if (info.Extension == "unity3d")
                {
                    Debug.Log(" File Existed with Noformat ");
                    isExisted = true;
                }
            }
        }

        if (isExisted)
        {
            isPlacingAvatar = true;
            messageInternetConnectoin.SetActive(false);

            Debug.Log("Load Avatar By Name .. ");

            HideSlider();

            // Getting Avatar By its Name
            StartCoroutine(LoadAvatarBundleByName(avatar_name));
        }
        else
        {
            if (InternetReachable() && !isDownloading)
            {
                yield return StartCoroutine(Download_Bundle(avatarInfo.avatarId.ToString(), avatarInfo.category.ToString()));
            }
            else
            {
                if (showInternetMeassage == true)
                {
                    showInternetMeassage = false;
                    // Show Internet Connection Message
                    if (canshowmessage)
                    {
                        yield return StartCoroutine(ShowInternetConnectinMessage());
                    }
                }
            }
        }
    }

    public bool InternetReachable()
    {
        return (Application.internetReachability != NetworkReachability.NotReachable);
    }

    /// <summary>
    /// Adds the avatar identifier.
    /// </summary>
    /// <param name="avatar">Avatar.</param>
    private void AddAvatarIdentifier(GameObject avatar)
    {
        GameObject newFlesh = Instantiate(flesh, IdentifiresArea.transform);
        newFlesh.SetActive(true);
        newFlesh.transform.position = IdentifiresArea.transform.position;
        LookAtAvatar lookat = newFlesh.GetComponent<LookAtAvatar>();
        lookat.Target = avatar;
        newFlesh.name = "Identifier-" + avatar.name;
    }

   
    /// <summary>
    /// Adds the ability to move.
    /// </summary>
    /// <param name="avatar">Avatar.</param>
    /// <param name="category">Category.</param>
    private void AddAbilityToMove(GameObject avatar, string category)
    {
        // Add Ability to  be Selectable 
        Lean.Touch.LeanSelectable selectable = avatar.AddComponent<Lean.Touch.LeanSelectable>();
        selectable.SelectMe();

        // Add the Ability to Scale
        Lean.Touch.LeanScale scalable = avatar.AddComponent<Lean.Touch.LeanScale>();

        scalable.ScaleClamp = true;

        scalable.ScaleMin = new Vector3(0.015f, 0.015f, 0.015f);
        scalable.ScaleMax = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);

        scalable.RequiredSelectable = selectable;
        scalable.RequiredFingerCount = 2;

        Lean.Touch.LeanRotateCustomAxis rotatable = avatar.AddComponent<Lean.Touch.LeanRotateCustomAxis>();
        rotatable.Space = Space.Self;
        rotatable.Axis = new Vector3(0, -1, 0);

        // Add the Ability to Move
        Lean.Touch.LeanTranslateForAvatar movable = avatar.AddComponent<Lean.Touch.LeanTranslateForAvatar>();
        movable.RequiredSelectable = selectable;
        movable.RequiredFingerCount = 1;
    }

    private void AddAbilityToMoveWithoutScale(GameObject avatar, string category)
    {
        // Add Ability to  be Selectable 
        Lean.Touch.LeanSelectable selectable = avatar.AddComponent<Lean.Touch.LeanSelectable>();
        selectable.SelectMe();

        // Add the Ability to Scale
        Lean.Touch.LeanScale scalable = avatar.AddComponent<Lean.Touch.LeanScale>();

        scalable.ScaleClamp = true;
        scalable.ScaleMin = new Vector3(0.005f, 0.005f, 0.005f);
        scalable.ScaleMax = new Vector3(0.2f, 0.2f, 0.2f);

        scalable.RequiredSelectable = selectable;
        scalable.RequiredFingerCount = 2;

        Lean.Touch.LeanRotateCustomAxis rotatable = avatar.AddComponent<Lean.Touch.LeanRotateCustomAxis>();
        rotatable.Space = Space.Self;
        rotatable.Axis = new Vector3(0, -1, 0);

        // Add the Ability to Move
        Lean.Touch.LeanTranslateForAvatar movable = avatar.AddComponent<Lean.Touch.LeanTranslateForAvatar>();
        movable.RequiredSelectable = selectable;
        movable.RequiredFingerCount = 1;

    }

    private void AddAbilityToMoveWithoutScaleComplex(GameObject avatar, string category)
    {
        // Add Ability to  be Selectable 
        Lean.Touch.LeanSelectable selectable = avatar.AddComponent<Lean.Touch.LeanSelectable>();
        //selectable.SelectMe();

        // Add the Ability to Scale
        Lean.Touch.LeanScale scalable = avatar.AddComponent<Lean.Touch.LeanScale>();

        scalable.ScaleClamp = true;
        scalable.ScaleMin = new Vector3(0.005f, 0.005f, 0.005f);
        scalable.ScaleMax = new Vector3(0.2f, 0.2f, 0.2f);

        scalable.RequiredSelectable = selectable;
        scalable.RequiredFingerCount = 2;

        Lean.Touch.LeanRotateCustomAxis rotatable = avatar.AddComponent<Lean.Touch.LeanRotateCustomAxis>();
        rotatable.Space = Space.Self;
        rotatable.Axis = new Vector3(0, -1, 0);

        // Add the Ability to Move
        Lean.Touch.LeanTranslateForAvatar movable = avatar.AddComponent<Lean.Touch.LeanTranslateForAvatar>();
        movable.RequiredSelectable = selectable;
        movable.RequiredFingerCount = 1;
    }


    private void AddAbilityToMoveWithNoSelect(GameObject avatar, string category)
    {
        // Add Ability to  be Selectable 
        Lean.Touch.LeanSelectable selectable = avatar.AddComponent<Lean.Touch.LeanSelectable>();
        //selectable.SelectMe();

        // Add the Ability to Scale
        Lean.Touch.LeanScale scalable = avatar.AddComponent<Lean.Touch.LeanScale>();

        scalable.ScaleClamp = true;
        scalable.ScaleMin = new Vector3(0.005f, 0.005f, 0.005f);
        scalable.ScaleMax = new Vector3(0.2f, 0.2f, 0.2f);


        scalable.RequiredSelectable = selectable;
        scalable.RequiredFingerCount = 2;

        Lean.Touch.LeanRotateCustomAxis rotatable = avatar.AddComponent<Lean.Touch.LeanRotateCustomAxis>();
        rotatable.Space = Space.Self;
        rotatable.Axis = new Vector3(0, -1, 0);

        // Add the Ability to Move
        Lean.Touch.LeanTranslateForAvatar movable = avatar.AddComponent<Lean.Touch.LeanTranslateForAvatar>();
        movable.RequiredSelectable = selectable;
        movable.RequiredFingerCount = 1;

    }

    void Deselect()
    {
        DublicateButton.SetActive(false);
        cleanAvatar.SetActive(false);
    }

    public bool canshowmessage = true;

    IEnumerator ShowInternetConnectinMessage()
    {
        canshowmessage = false;
        messageInternetConnectoin.SetActive(true);
        yield return new WaitForSeconds(2);
        messageInternetConnectoin.SetActive(false);

        canshowmessage = true;
        showInternetMeassage = false;
    }

   

    IEnumerator ShowMaxAvatarsMessage()
    {
        messageMaxAvatars.SetActive(true);
        yield return new WaitForSeconds(2);
        messageMaxAvatars.SetActive(false);
        showMaxAvatarsMessage = false;
    }



    IEnumerator ShowInternetConnectinMessage(float value)
    {
        canshowmessage = false;
        messageInternetConnectoin.SetActive(true);
        yield return new WaitForSeconds(value);
        messageInternetConnectoin.SetActive(false);

        canshowmessage = true;
        showInternetMeassage = false;
    }

    /// <summary>
    /// Shows the detect surface message.
    /// </summary>
    /// <returns>The detect surface message.</returns>
    IEnumerator ShowDetectSurfaceMessage()
    {
        if (downloadStatusPanel.activeInHierarchy)
        {
            downloadStatusPanel.SetActive(false);
        }

        //isDownloading = false;
        //isPlacingAvatar = false;

        meassageDetectSurface.SetActive(true);

        //if (loadingLogo.activeInHierarchy) {
        //    loadingLogo.SetActive(false);
        //}

        if (loadingAvatarAnimation.activeInHierarchy)
        {
            loadingAvatarAnimation.SetActive(false);
        }

        yield return null;

    }

    ///// <summary>
    ///// Downloads the avatar Bundle from the Server By ID
    ///// </summary>
    ///// <returns>The avatar.</returns>
    ///// <param name="avatarID">Avatar identifier.</param>
    //public IEnumerator Download_Bundle(string avatarID, string categoryID)
    //{
    //    // Show the Download progress
    //    yield return StartCoroutine(LoadTextureForDownladAsync(Application.persistentDataPath + "/Category-" + categoryID + "/" + avatarID + ".png", AddLoadedTextureToDownloading));
    //    AvatarCategory category = databaseService.GetComponent<DBScript>().GetCategoryById(int.Parse(categoryID));
    //    Debug.Log("Download From Category " + category.Name);
    //    downloadStatusPanel.SetActive(true);
    //    isDownloading = true;

    //    // check if there is Internet Connection if available
    //    if (PlayerPrefs.GetString("auth_token").Length > 1)
    //    {
    //        // Check If Avatar ID Has Music
    //        List<DB_FBX> AvatarFBXs = databaseService.GetComponent<DBScript>().GetFbxByAvatarId(int.Parse(avatarID)).Distinct().ToList<DB_FBX>();
    //        yield return StartCoroutine(DownloadBundleWithoutMusic(categoryID, avatarID, AvatarFBXs));
    //    }
    //}

    /// <summary>
    /// Downloads the avatar from the Server By ID
    /// </summary>
    /// <returns>The avatar.</returns>
    /// <param name="avatarID">Avatar identifier.</param>
    [Obsolete]
    public IEnumerator Download_Bundle(string avatarID, string categoryID)
    {
        // Show the Download progress
        yield return StartCoroutine(LoadTextureForDownladAsync(Application.persistentDataPath + "/Category-" + categoryID + "/" + avatarID + ".png", AddLoadedTextureToDownloading));
        AvatarCategory category = databaseService.GetComponent<DBScript>().GetCategoryById(int.Parse(categoryID));
        Debug.Log("Download With Category " + category.Name);
        downloadStatusPanel.SetActive(true);

        // check if there is Internet Connection if available
        if (PlayerPrefs.GetString("auth_token").Length > 1)
        {
            isDownloading = true;
            // Check If Avatar ID Has Music

            List<DB_FBX> AvatarFBXs = databaseService.GetComponent<DBScript>().GetFbxByAvatarId(int.Parse(avatarID)).ToList<DB_FBX>();

            List<AnimationInfo> animations = new List<AnimationInfo>();

            foreach (DB_FBX fbx in AvatarFBXs)
            {
                IEnumerable<AnimationInfo> infox = databaseService.GetComponent<DBScript>().GetAnimationByFbxID(fbx.Id);
                animations.AddRange(infox);
            }

            // Getting the Music IDS

            #region GET LIST OF MUSIC

            List<MusicDB> musicItems = new List<MusicDB>();

            if (animations.Count > 0)
            {

                foreach (AnimationInfo info in animations)
                {
                    musicItems.AddRange(databaseService.GetComponent<DBScript>().GetMusicIdsByAnimationID(info.Id).ToList<MusicDB>());
                }
            }

            List<int> musicIds = musicItems.Select(item => item.MusicId).Distinct<int>().ToList();

            try
            {
                if (musicIds.Count == 0)
                {
                    Debug.Log("Download Avatar Without Music : " + avatarID + " category : " + categoryID);
                    StartCoroutine(DownloadBundleWithoutMusic(categoryID, avatarID, AvatarFBXs));
                }
                else
                {
                    if (musicIds.Count > 0)
                    {
                        Debug.Log("Start Download With Music ");
                        StartCoroutine(DownloadBundleWithMusic(categoryID, avatarID, musicIds, animations[0].AnimationName, AvatarFBXs));
                    }
                }
            }
            catch (Exception ex)
            {
                HideSlider();
                Debug.Log("Exception Is : "+ ex.Message);
            }
            #endregion

        }
    }


    List<UnityWebRequest> requests;

    /// <summary>
    /// Downloads the avatar with music.
    /// </summary>
    /// <returns>The avatar with music.</returns>
    /// <param name="categoryID">Category identifier.</param>
    /// <param name="avatarID">Avatar identifier.</param>
    public IEnumerator DownloadBundleWithMusic(string categoryID, string avatarID, List<int> musicIds, string animationName, List<DB_FBX> AvatarFBXs)
    {
        isDownloading = true;
        DB_FBX[] filesArray = AvatarFBXs.Distinct<DB_FBX>().ToArray<DB_FBX>();

        int arrayLength = filesArray.Length + musicIds.Count;

        Debug.Log("Avatars Files Count : " + arrayLength);

        requests = new List<UnityWebRequest>(arrayLength);

        // Populate Avarar Files into the Array
        for (int i = 0; i < filesArray.Length; i++)
        {
            UnityWebRequest request = UnityWebRequest.Get(API_Settings.ONLINE_SERVICE_URL + API_Settings.DOWNLOAD_FBX_BY_ID + filesArray[i].Id);
            request.timeout = 600;
            request.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
            requests.Add(request);
        }

        //// Populate Music Files into the Array
        for (int j = filesArray.Length; j < arrayLength; j++)
        {
            int index = j - filesArray.Length;
            Debug.Log("Creating Index : " + index + "music Index : " + musicIds[index]);
            string musicUrl = API_Settings.ONLINE_SERVICE_URL + API_Settings.DOWNLOAD_MUSIC_BY_ID + musicIds[index];

            UnityWebRequest request = UnityWebRequest.Get(musicUrl);
            request.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
            requests.Add(request);
        }

        for (int i = 0; i < requests.Count; i++)
        {
            requests[i].SendWebRequest();
        }

        yield return StartCoroutine(CheckDownloadProcessAscenc(requests, avatarID));

        Debug.Log("User Cancelled the Download : " + cancelDownload);

        if (!HasNetworkError(requests))
        {
            // Save Avatar Files 
            for (int i = 0; i < filesArray.Length; i++)
            {
                string filePath = Application.persistentDataPath + "/Category-" + categoryID + "/" + avatarID + ".unity3d";
                System.IO.File.WriteAllBytes(filePath, requests[i].downloadHandler.data);
            }

            // Save Music Files
            for (int j = filesArray.Length; j < arrayLength; j++)
            {
                int musicID = musicIds[j - filesArray.Length];
                string musicFilePath = Application.persistentDataPath + "/Category-" + categoryID + "/" + avatarID + "-" + animationName + "-" + musicID + ".mp3";
                System.IO.File.WriteAllBytes(musicFilePath, requests[j].downloadHandler.data);
            }

            HideSlider();

            AvatarInfo avatarInfo = databaseService.GetComponent<DBScript>().GetAvatarInfoById(int.Parse(avatarID));

            LoadAvatarBundleAssenc(avatarInfo);

            isPlacingAvatar = false;
            isDownloading = false;
            cancelDownload = false;

            DisposeRequests(requests);

            yield return null;
        }
        else
        {
            Debug.Log(" Downloading With Music Error ");

            if (!cancelDownload)
            {
                errorMessage.SetActive(true);
            }


            isPlacingAvatar = false;
            cancelDownload = false;
            isDownloading = false;


            yield return null;
        }

        yield return null;
    }


    public float ProgreesOfDownloads(List<UnityWebRequest> requests)
    {
        float progress = 0.0f;
        for (int i = 0; i < requests.Count; i++)
        {
            progress += requests[i].downloadProgress;
        }
        return progress;
    }

    public float ProgreesOfDownloads(UnityWebRequest[] requests)
    {
        float progress = 0.0f;
        for (int i = 0; i < requests.Length; i++)
        {
            progress += requests[i].downloadProgress;
        }
        return progress;
    }

    IEnumerator CheckDownloadProcessAscenc(List<UnityWebRequest> requests, string avatar_name)
    {
        downloadStatusPanel.SetActive(true);
        // Load Photo Form system
        int Count = requests.Count;

        while (ProgreesOfDownloads(requests) < Count)
        { 
            float all_progress = ProgreesOfDownloads(requests) * 100;
            float process = (float)Math.Round(all_progress / Count, 0);

            if ((process) > 0)
            {
                downloadingMessageStatus.GetComponent<Text>().text = process.ToString() + "%";
            }

            float res = process / 100;
            countdown.fillAmount = res;
            button.fillAmount = 1f - res;

            if (cancelDownload)
            {
                avatarsToPlace.Remove(avatar_name);
                Debug.Log("Canceled Download");
                for (int i = 0; i < requests.Count; i++)
                {
                    requests[i].Abort();
                }

                AbortDownloads(requests);
                ResetDownload();
                isPlacingAvatar = false;
                downloadStatusPanel.SetActive(false);

                yield return new WaitForSeconds(0.2f);
            }

            yield return new WaitForSeconds(0.2f);
        }

        if (cancelDownload)
        {
            AbortDownloads(requests);
            avatarsToPlace.Remove(avatar_name);
            Debug.Log("Canceled Download");
            for (int i = 0; i < requests.Count; i++)
            {
                requests[i].Abort();
            }
            ResetDownload();
            isPlacingAvatar = false;
            yield return new WaitForSeconds(0.1f);
            downloadStatusPanel.SetActive(false);
        }


        if (downloadStatusPanel.activeInHierarchy)
        {
            downloadStatusPanel.SetActive(false);
        }

        ResetDownload();

        yield return null;
    }


    IEnumerator CheckDownloadProcessAscenc(UnityWebRequest[] requests, string avatar_name)
    {
        downloadStatusPanel.SetActive(true);
        // Load Photo Form system
        int Count = requests.Length;

        while (ProgreesOfDownloads(requests) < Count)
        {
            float all_progress = ProgreesOfDownloads(requests) * 100;
            float process = (float)Math.Round(all_progress / Count, 0);

            if ((process) > 0)
            {
                downloadingMessageStatus.GetComponent<Text>().text = process.ToString() + "%";
            }

            float res = process / 100;
            countdown.fillAmount = res;
            button.fillAmount = 1f - res;

            if (cancelDownload)
            {
                avatarsToPlace.Remove(avatar_name);
                Debug.Log("Canceled Download");
                for (int i = 0; i < requests.Length; i++)
                {
                    requests[i].Abort();
                }
                AbortDownloads(requests);
                ResetDownload();
                isPlacingAvatar = false;
                downloadStatusPanel.SetActive(false);

                yield return new WaitForSeconds(0.2f);
            }

            yield return new WaitForSeconds(0.2f);
        }

        if (cancelDownload)
        {
            AbortDownloads(requests);
            avatarsToPlace.Remove(avatar_name);
            Debug.Log("Canceled Download");
            for (int i = 0; i < requests.Length; i++)
            {
                requests[i].Abort();
            }
            ResetDownload();
            isPlacingAvatar = false;
            yield return new WaitForSeconds(0.1f);
            downloadStatusPanel.SetActive(false);
        }


        if (downloadStatusPanel.activeInHierarchy)
        {
            downloadStatusPanel.SetActive(false);
        }

        ResetDownload();

        yield return null;
    }


    public void AbortDownloads(UnityWebRequest[] requests)
    {
        for (int i = 0; i < requests.Length; i++)
        {
            requests[i].Abort();
        }
    }

    public void DisposeRequests(List<UnityWebRequest> requests)
    {
        if (requests != null)
        {
            for (int i = 0; i < requests.Count; i++)
            {
                if (requests[i] != null)
                {
                    requests[i].Dispose();
                }
            }
        }
        requests.Clear();
    }

    /// <summary>
    /// Downloads the avatar Bundle without music.
    /// </summary>
    /// <returns>The avatar without music.</returns>
    /// <param name="categoryID">Category identifier.</param>    
    /// <param name="avatarID">Avatar identifier.</param>
    public IEnumerator DownloadBundleWithoutMusic(string categoryID, string avatarID, List<DB_FBX> AvatarFBXs)
    {
        isDownloading = true;
        DB_FBX[] filesArray = AvatarFBXs.ToArray<DB_FBX>();
        int arrayCount = filesArray.Length;

        requests = new List<UnityWebRequest>();

        Debug.Log("Requests Count : " + arrayCount);

        for (int i = 0; i < arrayCount; i++)
        {
            UnityWebRequest request = UnityWebRequest.Get(API_Settings.ONLINE_SERVICE_URL + API_Settings.DOWNLOAD_FBX_BY_ID + filesArray[i].Id);
            request.timeout = 600;
            request.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
            requests.Add(request);
        }

        for (int i = 0; i < requests.Count; i++)
        {
            requests[i].SendWebRequest();
        }

        yield return StartCoroutine(CheckDownloadProcessAscenc(requests, avatarID));

        Debug.Log("User Cancelled the Download : " + cancelDownload);

        if (!HasNetworkError(requests))
        {
            if (requests.Count == 1)
            {
                string filePath = Application.persistentDataPath + "/Category-" + categoryID + "/" + avatarID + ".unity3d";
                System.IO.File.WriteAllBytes(filePath, requests[0].downloadHandler.data);
            }
            if (requests.Count > 1)
            {
                for (int i = 0; i < requests.Count; i++)
                {
                    string filePath = Application.persistentDataPath + "/Category-" + categoryID + "/" + avatarID + "-" + AvatarFBXs[i].Id + ".unity3d";
                    System.IO.File.WriteAllBytes(filePath, requests[i].downloadHandler.data);
                }
            }

            HideSlider();

            AvatarInfo avatarInfo = databaseService.GetComponent<DBScript>().GetAvatarInfoById(int.Parse(avatarID));

            yield return new WaitForEndOfFrame();

            LoadAvatarBundleAssenc(avatarInfo);

            isDownloading = false;
            cancelDownload = false;
            isDownloading = false;

            DisposeRequests(requests);

            yield return null;

        }
        else
        {
            Debug.Log(" Downloading Error ");

            if (!cancelDownload)
            {
                errorMessage.SetActive(true);
            }


            isPlacingAvatar = false;
            cancelDownload = false;
            isDownloading = false;

            avatarsToPlace.Remove(avatarID);

            yield return null;

        }

        yield return null;

    }


    public IEnumerator AbortDownloads(List<UnityWebRequest> requests)
    {
        try
        {
            Debug.Log("Number of Requests to Abort is : " + requests.Count);
            for (int i = 0; i < requests.Count; i++)
            {
                if (requests[i] != null)
                {
                    requests[i].Abort();
                }
            }
            //Resources.UnloadUnusedAssets();
        }
        catch (Exception ex)
        {
            Debug.Log("Error in Aborting Download :" + ex.Message);
        }
        yield return new WaitForEndOfFrame();
    }

    public bool HasNetworkError(List<UnityWebRequest> requests)
    {
        try
        {
            if (requests.Count > 0)
            {
                for (int i = 0; i < requests.Count; i++)
                {
                    if (requests[i].isNetworkError || requests[i].isHttpError)
                    {
                        Dictionary<string, string> headers = requests[i].GetResponseHeaders();
                        foreach (KeyValuePair<string, string> header in headers)
                        {
                            Console.WriteLine("Key: {0}, Value: {1}", header.Key, header.Value);
                        }
                        Debug.Log(" Downloaded Data : " + requests[i].downloadHandler.text);
                        Debug.Log(" Response Type : " + requests[i].GetResponseHeader("Content-Type"));
                        Debug.Log(" Download Avatar Error : " + requests[i].url + " , " + requests[i].error + Environment.NewLine);

                        return true;
                    }

                }
                return false;
            }
            else
            {
                Debug.Log("Exception Request Count 0");
                return true;
            }
        }
        catch (Exception ex)
        {
            Debug.Log("Exception : " + ex.Message);
            return true;
        }
    }


    public bool HasNetworkError(UnityWebRequest[] requests)
    {
        try
        {
            if (requests.Length > 0)
            {
                for (int i = 0; i < requests.Length; i++)
                {
                    if (requests[i] != null)
                    {
                        if (requests[i].isNetworkError || requests[i].isHttpError)
                        {
                            Dictionary<string, string> headers = requests[i].GetResponseHeaders();
                            foreach (KeyValuePair<string, string> header in headers)
                            {
                                Console.WriteLine("Key: {0}, Value: {1}", header.Key, header.Value);
                            }
                            Debug.Log(" Downloaded Data : " + requests[i].downloadHandler.text);
                            Debug.Log(" Response Type : " + requests[i].GetResponseHeader("Content-Type"));
                            Debug.Log(" Download Avatar Error : " + requests[i].url + " , " + requests[i].error + Environment.NewLine);
                            return true;
                        }
                    }
                    else
                    {
                        Debug.Log(" Request is Null ");
                        return true;
                    }
                }
                return false;
            }
            else
            {
                Debug.Log("Exception Request Count 0");
                return true;
            }
        }
        catch (Exception ex)
        {
            Debug.Log("Exception : " + ex.Message);
            return true;
        }
    }


    /// <summary>
    /// Resets the download Indicators
    /// </summary>
    public void ResetDownload()
    {
        countdown.fillAmount = 0;
        button.fillAmount = 1;
        downloadingMessageStatus.GetComponent<Text>().text = "";
    }


    /// <summary>
    /// Loads the textur for downlad async.
    /// </summary>
    /// <returns>The textur for downlad async.</returns>
    /// <param name="fileName">File name.</param>
    /// <param name="result">Result.</param>
    public IEnumerator LoadTextureForDownladAsync(string fileName, Action<Texture2D> result)
    {
        WWW www = new WWW("file://" + fileName);
        yield return www;
        Texture2D loadedTexture = new Texture2D(160, 160);
        www.LoadImageIntoTexture(loadedTexture);
        result(loadedTexture);
    }

    /// <summary>
    /// Gets Cleaned filename.
    /// </summary>
    /// <returns>The clean file name.</returns>
    /// <param name="originalFileName">Original file name.</param>
    public static string GetCleanFileName(string originalFileName)
    {
        string fileToLoad = originalFileName.Replace('\\', '/');
        fileToLoad = string.Format("file://{0}", fileToLoad);
        return fileToLoad;
    }

    /// <summary>
    /// Shows the Tray slider.
    /// </summary>
    public void ShowSlider()
    {
        SetUIElement(avatarsControlPanel, false);

        HideScroolSnap(scrollSnapPanel);

        categorySlider.SetActive(true);
        //ShowUIElement(categorySlider);
        addAvatars.SetActive(false);

        //HideUIElement(addAvatars);
        //switchCameraButton.SetActive(false);
    }


    /// <summary>
    /// Hides the Tray slider.
    /// </summary>
    [Obsolete]
    public void HideSlider()
    {
        SetUIElement(avatarsControlPanel, true);

        ShowScroolSnap(scrollSnapPanel);

        categorySlider.SetActive(false);

        addAvatars.SetActive(true);

        avatarPanel.SetActive(false);
    }

    public void ShowScroolSnap(GameObject element)
    {
        TextMeshProUGUI[] titles = element.GetComponentsInChildren<TextMeshProUGUI>();
        foreach (TextMeshProUGUI title in titles)
        {
            title.enabled = true;
        }

        UnityEngine.UI.Image[] images = element.GetComponentsInChildren<UnityEngine.UI.Image>();

        foreach (UnityEngine.UI.Image image in images)
        {
            image.enabled = true;
        }
        

        Button[] buttons = element.GetComponentsInChildren<Button>();
        foreach (Button btn in buttons)
        {
            btn.enabled = true;
        }

    }

    public void HideScroolSnap(GameObject element)
    {
        TextMeshProUGUI[] titles = element.GetComponentsInChildren<TextMeshProUGUI>();
        foreach (TextMeshProUGUI title in titles)
        {
            title.enabled = false;
        }

        UnityEngine.UI.Image [] images = element.GetComponentsInChildren<UnityEngine.UI.Image>();
        foreach (UnityEngine.UI.Image image in images)
        {
            image.enabled = false;
        }

        Button[] buttons = element.GetComponentsInChildren<Button>();
        foreach (Button btn in buttons)
        {
            btn.enabled = false;
        }
    }


    [Obsolete]
    public void SetUIElement(GameObject element, bool value)
    {
        if (element.GetComponent<UnityEngine.UI.Image>() != null)
        {
            element.GetComponent<UnityEngine.UI.Image>().enabled = value;
            UnityEngine.UI.Image[] images = element.GetComponentsInChildren<UnityEngine.UI.Image>();
            foreach (UnityEngine.UI.Image image in images)
            {
                image.enabled = value;
            }

            TextMeshProUGUI[] titles = element.GetComponentsInChildren<TextMeshProUGUI>();
            foreach (TextMeshProUGUI title in titles)
            {
                if (title.name == "RecordIndicator")
                {
                    title.enabled = value;
                }
            }
        }

        else
        {
            UnityEngine.UI.Image[] images = element.GetComponentsInChildren<UnityEngine.UI.Image>();
            foreach (UnityEngine.UI.Image image in images)
            {
                image.enabled = value;
            }

            TextMeshProUGUI[] titles = element.GetComponentsInChildren<TextMeshProUGUI>();
            foreach (TextMeshProUGUI title in titles)
            {
                if (title.name == "RecordIndicator")
                {
                    title.enabled = value;
                }
            }
        }
    }

    /// <summary>
    /// Cancels the download process.
    /// </summary>
    public void CancelDownload()
    {
        cancelDownload = true;
        AbortDownloads(requests);

        //isPlacingAvatar = false;
        //isDownloading = false;

        //StopCoroutine("CheckDownloadProcessAscenc");
        //StopCoroutine("DownloadAvatar");

        Debug.Log(" Downloading Canceled ");
    }

    /// <summary>
    /// Loads the thumnails from the server .
    /// </summary>
    /// <returns>The thumnails.</returns>
    public IEnumerator LoadThumnails()
    {
        List<int> categoryIDS = databaseService.GetComponent<DBScript>().GetAllCategories().Where<AvatarCategory>(c => c.Type == "Tray").ToList<AvatarCategory>().Select(c => c.Id).ToList();

        DirectoryInfo di = new DirectoryInfo(Application.persistentDataPath);
        FileInfo [] filesOfThumnails = di.GetFiles("Category-*.png");

        if (filesOfThumnails.Length > 0)
        {
            foreach (var file in filesOfThumnails)
            {
                int filename = int.Parse(file.Name.Replace("Category-","").Replace(".png",""));

                if (categoryIDS.Contains(filename))
                {
                    yield return StartCoroutine(CreateTexture(file.FullName));
                }
            }
        }
    }

    /// <summary>
    /// Create path of image to sprite and instantiate the "ButtonPrefab" For generate cell of image inside Avatars slider
    /// </summary>
    /// <returns>The texture.</returns>
    /// <param name="pathOfFile">Path of file.</param>
    public IEnumerator CreateAvatarPrefabButton(string categoryID, String pathOfFile)
    {
        String fileToLoad = GetCleanFileName(pathOfFile);

        WWW www = new WWW(fileToLoad);
        yield return www;

        //  Instantiate the template of image for add texture in collectionView for gallery
        GameObject avatarObject = (GameObject)Instantiate(avatarButtonPrefab, transform);

        avatarObject.transform.SetParent(avatarPanelContent.transform);

        avatarObject.GetComponent<Transform>().localScale = new Vector3(1, 1, 1);

        // Raw Image is the First Child
        avatarObject.transform.GetChild(0).GetComponent<RawImage>().texture = www.texture;

        // Button 
        avatarObject.GetComponent<Button>().onClick.AddListener(() => onButtonClick(categoryID, GetFileName(pathOfFile)));
    }

    /// <summary>
    /// Create path of image to sprite and instantiate the "ButtonPrefab" For generate cell of image inside Avatars slider
    /// </summary>
    /// <returns>The texture.</returns>
    /// <param name="pathOfFile">Path of file.</param>
    public IEnumerator CreateTexture(String pathOfFile)
    {
        String fileToLoad = GetCleanFileName(pathOfFile);
        string[] nameParts = GetFileName(pathOfFile).Split('-');
        int CategoryID = int.Parse(nameParts[1]);
        AvatarCategory category = databaseService.GetComponent<DBScript>().GetCategoryById(CategoryID);
        Debug.Log("Loading Image of thumnail from path: " + fileToLoad);

        WWW www = new WWW(fileToLoad);
        yield return www;

        //  Instantiate the template of image for add texture in collectionView for gallery
        GameObject imageObject = (GameObject)Instantiate(buttonPrefab, transform);
        imageObject.transform.SetParent(CategoryPanelParent.transform);
        imageObject.name = CategoryID.ToString();

        imageObject.GetComponent<Transform>().localScale = new Vector3(1, 1, 1);

        // Child Number (1) is the Button
        // Set the Mask in the Parent (Button) and let the Image be a child
        imageObject.transform.GetChild(1).transform.GetChild(0).GetComponent<RawImage>().texture = www.texture;

        // Get CategoryName from DataBase

        //pathImageInTemplate.text = pathOfFile;
        imageObject.transform.Find("pathOfThumnail").GetComponent<Text>().text = pathOfFile;

        //Debug.Log("Thumb Path : " + pathOfFile);

        // Child Number (1) is the Button
        imageObject.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(() => onCategoryButtonClick(category.Id.ToString()));
        imageObject.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = category.Name;

        yield return null;
    }

    /// <summary>
    /// Gets the name of the file from its Path.
    /// </summary>
    /// <returns>The file name.</returns>
    /// <param name="fname">Fname.</param>
    public string GetFileName(string fname)
    {
        return fname.Substring(fname.LastIndexOf('/') + 1).Replace(".png", "");
    }

    /// <summary>
    /// Plays the music for avatar.
    /// </summary>
    /// <returns>The music for avatar.</returns>
    /// <param name="musicID">Music identifier.</param>
    public IEnumerator PlayMusicForAvatar(string CategoryID, string musicID, AnimationInfo animModel, Animation anim)
    {
        Debug.Log("Play Music For Avatar From Device");

        CheckAudioSources();

        int avatarMusicID = int.Parse(musicID.Split('-')[0].Split('D')[0]);

        if (animModel != null)
        {
            GameObject avatarMusic = GameObject.Find("music-" + avatarMusicID);
            AudioSource[] audios = avatarMusic.GetComponents<AudioSource>();

            isPlayingMusic = true;

            if (audios.Length == 1)
            {
                if (animModel.Repeat != "Loop")
                {
                    List<MusicItem> items = new List<MusicItem>();
                    if (musicDictionary.ContainsKey(avatarMusicID))
                    {
                        items = musicDictionary[avatarMusicID];
                    }
                    if (items.Count == 1)
                    {
                        if (items[0].delay > 0)
                        {
                            if (!avatarMusic.GetComponent<AudioSource>().isPlaying)
                            {
                                avatarMusic.GetComponent<AudioSource>().Stop();
                                if (!avatarMusic.GetComponent<AudioSource>().isPlaying)
                                {
                                    //foreach(AnimationState state in anim )
                                    //{
                                    //    state.time = 0; 
                                    //}

                                    anim.Stop();
                                    avatarMusic.GetComponent<AudioSource>().time = 0;
                                    avatarMusic.GetComponent<AudioSource>().Stop();

                                    anim.Play();
                                    avatarMusic.GetComponent<AudioSource>().PlayDelayed(items[0].delay);

                                }
                            }
                            else
                            {
                                //foreach(AnimationState state in anim )
                                //{
                                //    state.time = 0; 
                                //}

                                anim.Stop();
                                anim.Play();

                                avatarMusic.GetComponent<AudioSource>().time = 0;
                                avatarMusic.GetComponent<AudioSource>().Stop();
                                avatarMusic.GetComponent<AudioSource>().PlayDelayed(items[0].delay);
                            }
                        }
                        else
                        {
                            //if (!avatarMusic.GetComponent<AudioSource>().isPlaying)
                            //{
                            anim.Stop();
                            avatarMusic.GetComponent<AudioSource>().time = 0;
                            avatarMusic.GetComponent<AudioSource>().Stop();
                            anim.Play();
                            avatarMusic.GetComponent<AudioSource>().Play();
                        }
                    }
                }
                else
                {
                    avatarMusic.GetComponent<AudioSource>().Stop();
                    anim.Stop();

                    anim.Play();
                    avatarMusic.GetComponent<AudioSource>().Play();
                }
            }
            if (audios.Length > 1)
            {
                List<MusicItem> items = new List<MusicItem>();
                if (musicDictionary.ContainsKey(avatarMusicID)) {
                    items = musicDictionary[avatarMusicID];
                }

                bool isMusicPlaying = false;
                for (int i = 0; i < audios.Length; i++) {
                    if (audios[i].isPlaying) {
                        isMusicPlaying = true;
                    }
                }

                if (isMusicPlaying) {
                    for (int i = 0; i < audios.Length; i++)
                    {
                        audios[i].Stop();
                    }

                    StartCoroutine(PlayQueuedMusic(audios, items));
                    //if (!avatarMusic.GetComponent<AudioSource>().isPlaying)
                    //{
                    //avatarMusic.GetComponent<AudioSource>().PlayDelayed(animModel.Delay);

                    //}
                }
                else
                {
                    StartCoroutine(PlayQueuedMusic(audios, items));
                }
            }
        }

        isPlayingMusic = true;
        //SetPlayMusicImage();

        soundAvatar.SetActive(true);

        ButtonPalyMuteMusic.SetActive(true);

        yield return null;
    }

    /// <summary>
    /// Plaies the music files queued 
    /// </summary>
    /// <returns>The queued music.</returns>
    /// <param name="sources">Sources.</param>
    public IEnumerator PlayQueuedMusic(AudioSource[] sources, List<MusicItem> items)
    {
        int i = 0;
        do
        {
            if (!sources[i].isPlaying)
            {
                Debug.Log("Delayed : " + items[i].delay);
                sources[i].PlayDelayed(items[i].delay);
                yield return null;
            }
            else
            {
                while (sources[i].isPlaying)
                {
                    yield return null;
                }
                i++;
                yield return null;
            }
        }
        while (i < sources.Length);
        yield return null;
    }


    /// <summary>
    /// Switch between Mute and Un-Mute Music
    /// </summary>
    public void ButtonPlayMuteMusic()
    {
        if (!isPlayingMusicMuted)
        {
            ButtonPalyMuteMusic.transform.Find("MutePlayImage").GetComponent<RawImage>().texture = MuteSoundTexture;
            foreach (GameObject track in audioTracks)
            {
                if (track != null)
                {
                    AudioSource[] sources = track.GetComponents<AudioSource>();
                    for (int i = 0; i < sources.Length; i++)
                    {
                        sources[i].mute = true;
                    }
                    track.GetComponent<AudioSource>().mute = true;
                }
            }
            isPlayingMusicMuted = true;
        }
        else
        {
            ButtonPalyMuteMusic.transform.Find("MutePlayImage").GetComponent<RawImage>().texture = PlaySoundTexture;
            foreach (GameObject track in audioTracks)
            {
                if (track != null)
                {
                    AudioSource[] sources = track.GetComponents<AudioSource>();
                    for (int i = 0; i < sources.Length; i++)
                    {
                        sources[i].mute = false;
                    }
                    track.GetComponent<AudioSource>().mute = false;
                }
            }
            isPlayingMusicMuted = false;
        }
    }

    /// <summary>
    /// Sets the play music image.
    /// </summary>
    public void SetPlayMusicImage()
    {
        ButtonPalyMuteMusic.transform.Find("MutePlayImage").GetComponent<RawImage>().texture = PlaySoundTexture;
    }

    /// <summary>
    /// Cleans the avatars.
    /// </summary>
    public void CleanAvatars()
    {
        int index = -1;
        string avatarToDelete = "";
        Debug.Log("Clean Avatars");

        if (avatarHighlight.activeInHierarchy)
        {
            avatarHighlight.transform.position = mainOb.transform.position;
            avatarHighlight.transform.SetParent(mainOb.transform);
            avatarHighlight.SetActive(false);
        }

        Debug.Log("avatarsList.Count : " + avatarsList.Count);

        if (avatarsList.Count > 0) {
            for (int i = 0; i < avatarsList.Count; i++) {
                if (avatarsList[i].GetComponent<Lean.Touch.LeanSelectable>() != null) {
                    if (avatarsList[i].GetComponent<Lean.Touch.LeanSelectable>().IsSelected){
                        index = i;
                        avatarToDelete = avatarsList[i].name.Split('-')[0];
                        break;
                    }
                }
            }
        }

        AvatarInfo avatarInfo = databaseService.GetComponent<DBScript>().GetAvatarInfoById(int.Parse(avatarToDelete.Split('D')[0]));

        List<DB_FBX> fbx = databaseService.GetComponent<DBScript>().GetFbxByAvatarId(avatarInfo.avatarId).ToList<DB_FBX>();

        List<AnimationInfo> animInfos = databaseService.GetComponent<DBScript>().GetAnimationByFbxID(fbx[0].Id).ToList<AnimationInfo>();

        if (animInfos.Count > 0)
        {
            if (animInfos[0].Repeat == "Loop")
            {
                anim_loop_nb -= 1;
            }
        }

        //smartTerrain.HitTest()
        if (index >= 0)
        {
            string ava_name = avatarsList[index].name.Split('D')[0].Split('-')[0];
            string avatar_name = avatarsList[index].name.Split('D')[0];

            Debug.Log("Delete Avatar " + ava_name);

            if (!avatarToDelete.Contains('D'))
            {
                avatarsDictionary.Remove(ava_name);
                avatarsToPlace.Remove(ava_name);
            }


            // Destroy Avatars Identifiers
#pragma warning disable CS0618 // Type or member is obsolete
            int Count = IdentifiresArea.transform.childCount;
#pragma warning restore CS0618 // Type or member is obsolete
            if (Count > 0)
            {
                for (int j = 0; j < Count; j++)
                {
                    if (IdentifiresArea.transform.GetChild(j).name.StartsWith("Identifier-" + avatarToDelete, StringComparison.Ordinal))
                    {
                        Destroy(IdentifiresArea.transform.GetChild(j).gameObject);
                    }
                }
            }

            Debug.Log(" Avatars Count Before: " + avatarsList.Count);

            // Delete The Music only if the Avatar not Duplicated
            if (!avatarToDelete.Contains('D'))
            {
                // Check Avatar if Has Music
                if (AvatarHasMusic(ava_name))
                {
                    if (IsTheAvatarIsLastOneOfDublicates(ava_name))
                    {
                        Debug.Log(" Deleting Music : " + ava_name);
                        for (int j = 0; j < audioTracks.Count; j++)
                        {
                            string avatarName = audioTracks[j].name;

                            if (avatarName.Replace("music-", "").Equals(ava_name))
                            {
                                Debug.Log(" Accessing to Delete Music : " + ava_name);

                                // Stop the Music
                                AudioSource[] audios = audioTracks[j].GetComponents<AudioSource>();
                                for (int aud = 0; aud < audios.Length; aud++)
                                {
                                    audios[aud].Stop();
                                }

                                audioTracks.RemoveAt(j);
                                audiosIds.RemoveAt(j);
                                Destroy(GameObject.Find("music-" + ava_name));

                                //break;
                            }
                        }
                    }
                }
            }

            if (avatarsList[index] != null)
            {
                int count = mainOb.transform.childCount;

                if (!avatarsList[index].name.Contains('D'))
                {
                    for (int i = 0; i < count; i++)
                    {
                        if (mainOb.transform.GetChild(i).name.StartsWith(avatarToDelete, StringComparison.Ordinal)
                        && !mainOb.transform.GetChild(i).name.Contains('D'))
                        {
                            for (int j = 0; j < avatarsList.Count; j++)
                            {
                                if (avatarsList[j].name.StartsWith(avatarToDelete, StringComparison.Ordinal) && !avatarsList[j].name.Contains('D'))
                                {
                                    avatarsList.RemoveAt(j);
                                }
                            }

                            avatarsDictionary.Remove(ava_name);
                            avatarsToPlace.Remove(ava_name);

                            if (mainOb.transform.GetChild(i).gameObject != null)
                            {
                                // Destroy Avatar BUndle
                                // bundlesList.Where(bundle => bundle.name == bundleName).SingleOrDefault().Unload(true);
                                // bundlesList.Where(bundle => bundle.name == mainOb.transform.GetChild(i).name).SingleOrDefault().Unload(true);
                                Destroy(mainOb.transform.GetChild(i).gameObject);
                            }
                        }
                    }
                }
                else
                {
                    avatarsList.RemoveAt(index);
                    Destroy(GameObject.Find(avatarToDelete));
                }
            }

            Debug.Log("Avatars List After Remove: " + avatarsList.Count);

            if (avatarsList.Count == 0)
            {
                //cleanAvatarButton.SetActive(false);
                Debug.Log(" Repeat Button To Be Active False ");

                IdentifiresArea.SetActive(false);

                //if (sourceAudioForAvatar.clip != null)
                //{
                //    sourceAudioForAvatar.Stop();
                //}

                isPlayingMusic = false;
                RepeatButton.SetActive(false);
                animationsLength.Clear();
            }

            if (audiosIds.Count == 0)
            {
                ButtonPalyMuteMusic.SetActive(false);
            }

            cleanAvatar.SetActive(false);
            DublicateButton.SetActive(false);

        }

        //}
        //else {
        //      Destroy(GameObject.Find(lastSelectedObject));
        //}

        //Resources.UnloadUnusedAssets();

        CheckIfHasAnimation();
    }

    /// <summary>
    /// Avatars the has one animation.
    /// </summary>
    /// <returns><c>true</c>, if has one animation was avatared, <c>false</c> otherwise.</returns>
    /// <param name="name">Name.</param>
    public bool AvatarHasOneAnimation(string name)
    {
        int avatarId = int.Parse(name);
        List<DB_FBX> fbxFiles = databaseService.GetComponent<DBScript>().GetFbxByAvatarId(avatarId).ToList<DB_FBX>();

        // TODO 
        foreach (DB_FBX fbx in fbxFiles)
        {
            List<AnimationInfo> animations = databaseService.GetComponent<DBScript>().GetAnimationByFbxID(fbx.Id).ToList<AnimationInfo>();
            foreach (AnimationInfo animInfo in animations)
            {
                if (animInfo.Repeat == "Once")
                {
                    return true;
                }
            }
        }
        return false;
    }


    public bool AvatarHasMusic(GameObject avatar)
    {
        if (avatar.GetComponent<AudioSource>() != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Avatars the has music.
    /// </summary>
    /// <returns><c>true</c>, if has music was avatared, <c>false</c> otherwise.</returns>
    /// <param name="name">Name.</param>
    public bool AvatarHasMusic(string name)
    {
        Debug.Log("Check if Avatar Has Music");

        int avatarId = int.Parse(name.Split('-')[0].Split('D')[0]);

        List<DB_FBX> fbxFiles = databaseService.GetComponent<DBScript>().GetFbxByAvatarId(avatarId).ToList<DB_FBX>();

        // TODO 
        foreach (DB_FBX fbx in fbxFiles)
        {
            List<AnimationInfo> animations = databaseService.GetComponent<DBScript>().GetAnimationByFbxID(fbx.Id).ToList<AnimationInfo>();

            List<MusicDB> musicItems = new List<MusicDB>();

            if (animations.Count > 0)
            {
                foreach (AnimationInfo animInfo in animations)
                {
                    // GETMUSIC
                    musicItems.AddRange(databaseService.GetComponent<DBScript>().GetMusicIdsByAnimationID(animInfo.Id));
                }
                if (musicItems.Count > 0)
                {
                    return true;
                }
            }
        }
        return false;
    }

    /// <summary>
    /// Highlights the avatar.
    /// </summary>
    /// <param name="avatar">Avatar.</param>
    public void HighlightAvatar(GameObject avatar)
    {
        //SkinnedMeshRenderer[] skinrenderers = avatar.GetComponentsInChildren<SkinnedMeshRenderer>();
        //if(avatar.name)
        if (avatar.name != "124")
        {
            MeshRenderer[] renderers = avatar.GetComponentsInChildren<MeshRenderer>();
            if (renderers != null)
            {
                for (int i = 0; i < renderers.Length; i++)
                {
                    // Outlined/Silhouetted Diffuse
                    // 
                    renderers[i].material.shader = Shader.Find("Outlined/Silhouetted Diffuse");
                    Color outColor = new Color(0.95f, 0.83f, 0.35f, 1f);
                    // ColorUtility.TryParseHtmlString("C7D100",out outColor);
                    renderers[i].material.SetColor("_RimColor", outColor);
                }
            }

            SkinnedMeshRenderer[] skinrenderers = avatar.GetComponentsInChildren<SkinnedMeshRenderer>();
            if (skinrenderers != null)
            {
                for (int i = 0; i < skinrenderers.Length; i++)
                {
                    //Custom/GlowShader
                    //Outlined/Silhouetted Skinned Diffuse
                    skinrenderers[i].sharedMaterial.shader = Shader.Find("Custom/GlowShader");
                    Material[] mats = skinrenderers[i].materials;
                    for (int j = 0; j < mats.Length; j++)
                    {
                        mats[j].shader = Shader.Find("Custom/GlowShader");
                        Color outColor = new Color(0.95f, 0.83f, 0.35f, 1f);
                        // ColorUtility.TryParseHtmlString("C7D100",out outColor);
                        mats[j].SetColor("_RimColor", outColor);
                    }
                }
            }
        }

        else
        {
            SkinnedMeshRenderer[] skinrenderers = avatar.GetComponentsInChildren<SkinnedMeshRenderer>();
            if (skinrenderers != null)
            {
                for (int i = 0; i < skinrenderers.Length; i++)
                {
                    //Custom/GlowShader
                    //Outlined/Silhouetted Skinned Diffuse
                    //Silhouetted Diffuse
                    skinrenderers[i].sharedMaterial.shader = Shader.Find("Outlined/Silhouetted Diffuse");
                    Color outColor = new Color(0.95f, 0.83f, 0.35f, 1f);
                    // ColorUtility.TryParseHtmlString("C7D100",out outColor);
                    skinrenderers[i].material.SetColor("_Color", outColor);
                    Material[] mats = skinrenderers[i].materials;
                    for (int j = 0; j < mats.Length; j++)
                    {
                        mats[j].shader = Shader.Find("Outlined/Silhouetted Diffuse");
                        mats[j].SetColor("_Color", outColor);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Removes the highlight.
    /// </summary>
    /// <param name="avatar">Avatar.</param>
    public void RemoveHighlight(GameObject avatar)
    {
        //shaderGlow glow = avatar.GetComponent<shaderGlow>();
        //if (glow != null)
        //{
        //    glow.lightOff();
        //}
        MeshRenderer[] renderers = avatar.GetComponentsInChildren<MeshRenderer>();
        if (renderers != null)
        {
            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].material.shader = Shader.Find("Standard");
                Material[] mats = renderers[i].materials;
                for (int j = 0; j < mats.Length; j++)
                {
                    mats[j].shader = Shader.Find("Standard");
                }
            }
        }

        SkinnedMeshRenderer[] skinrenderers = avatar.GetComponentsInChildren<SkinnedMeshRenderer>();
        if (renderers != null)
        {
            for (int i = 0; i < skinrenderers.Length; i++)
            {
                skinrenderers[i].material.shader = Shader.Find("Standard");
                skinrenderers[i].material.SetColor("_Color", Color.white);
                Material[] mats = skinrenderers[i].materials;
                for (int j = 0; j < mats.Length; j++)
                {
                    mats[j].shader = Shader.Find("Standard");
                }
            }
        }
    }

    /// <summary>
    /// Repeats the animation.
    /// </summary>
    public void RepeatAnimation()
    {
        if (!isRepeating)
        {
            Debug.Log("Repeating .. ");

            isRepeating = true;
            isPausing = false;

            RepeatButton.GetComponent<Button>().image.sprite = pauseTexture;

            if (animationsLength.Count > 0)
            {
                float maxAnimationLength = animationsLength.Max();
                Debug.Log("Max Time : " + maxAnimationLength);

                this.animationTimer = new System.Timers.Timer();
                this.animationTimer.Interval = maxAnimationLength * 1000;
                this.animationTimer.Elapsed += AnimationTimerFinished;
                this.animationTimer.AutoReset = false;
                this.animationTimer.Enabled = true;
                this.animationTimer.Start();
            }

            StartCoroutine(RepeatAvatarAnimation());
        }
        else
        {
            Debug.Log("Calling Pause Animation .. ");
            StartCoroutine(PauseAvatarAnimation());

            isRepeating = false;
            isPausing = true;

            RepeatButton.GetComponent<Button>().image.sprite = playTexture;

        }

        //if (sourceAudioForAvatar.isPlaying)
        //{
        //    sourceAudioForAvatar.Stop();
        //}

        //if (sourceAudioForAvatar.isPlaying) {
        //        ButtonPalyMuteMusic.transform.Find("MutePlayImage").GetComponent<RawImage>().texture = MuteSoundTexture;
        //        sourceAudioForAvatar.Pause();
        //}

    }

    private void SwichToPlayAnimation()
    {
        RepeatButton.GetComponent<Button>().image.sprite = playTexture;
        animationTimer.Stop();
        isRepeating = false;
    }

    private void AnimationTimerFinished(object sender, ElapsedEventArgs e)
    {
        animationTimerFinisted = true;
    }

    private void LoadingTimerFinished(object sender, ElapsedEventArgs e)
    {
        loadingAnimationFinished = true;
    }

    /// <summary>
    /// Pauses the avatar animation.
    /// </summary>
    /// <returns>The avatar animation.</returns>
    private IEnumerator PauseAvatarAnimation()
    {
        int numClildren = mainOb.transform.childCount;

        if (numClildren > 0)
        {
            for (int i = 0; i < numClildren; i++)
            {
                if (mainOb.transform.GetChild(i).gameObject.name != "Highlight")
                {
                    string avatarName = mainOb.transform.GetChild(i).gameObject.name;
                    Animation anim = mainOb.transform.GetChild(i).gameObject.GetComponent<Animation>();
                    if (anim != null)
                    {
                        //if (anim.GetClipCount() > 0)
                        //{
                        foreach (AnimationState animationState in anim)
                        {
                            anim[animationState.clip.name].speed = 0;
                        }
                        //}
                    }
                    else
                    {
                        if (mainOb.transform.GetChild(i).transform.childCount > 0)
                        {
                            anim = mainOb.transform.GetChild(i).GetChild(0).gameObject.GetComponent<Animation>();
                            if (anim != null)
                            {
                                foreach (AnimationState animationState in anim)
                                {
                                    anim[animationState.clip.name].speed = 0;
                                }
                            }
                        }
                    }

                    if (mainOb.transform.GetChild(i).GetComponent<AudioVideoSync>())
                    {
                        mainOb.transform.GetChild(i).GetComponent<AudioVideoSync>().aniamtionTimer = 0;
                    }

                    else
                    {
                        if (mainOb.transform.GetChild(i).GetChild(0).GetComponent<AudioVideoSync>())
                        {
                            mainOb.transform.GetChild(i).GetChild(0).GetComponent<AudioVideoSync>().aniamtionTimer = 0;
                        }
                    }

                    if (audiosIds.Contains(avatarName))
                    {
                        Debug.Log("Stopping Animation ...");
                        AudioSource[] audios = GameObject.Find("music-" + avatarName).GetComponents<AudioSource>();

                        for (int aud = 0; aud < audios.Length; aud++)
                        {
                            audios[aud].time = 0;
                            audios[aud].Stop();
                        }
                    }
                }
            }

        }
        //Time.timeScale = 0;

        yield return new WaitForEndOfFrame();

    }

    //    /// <summary>
    //    /// Repeate Animations in the Scenes
    //    /// </summary>
    //    /// <returns></returns>
    //    public void RepeateAnimation()
    //    {
    //        // mute of un-mute the avatar sound
    //        CheckAudioSources();

    //#pragma warning disable CS0618 // Type or member is obsolete
    //        int numChildren = mainOb.transform.childCount;
    //#pragma warning restore CS0618 // Type or member is obsolete

    //        if (numChildren >= 1)
    //        {
    //            for (int i = 0; i < numChildren; i++)
    //            {
    //                GameObject childAvatar = mainOb.transform.GetChild(i).gameObject;
    //                PlayAvatarMusic(childAvatar);

    //                if (childAvatar.name != "Highlight")
    //                {
    //                    Animation avatarAnim = childAvatar.GetComponent<Animation>();
    //                    PlayAvatarMusic(childAvatar);
    //                    if (avatarAnim != null)
    //                    {
    //                        if (avatarAnim.GetClipCount() > 0)
    //                        {
    //                            foreach (AnimationState animationState in avatarAnim)
    //                            {
    //                                avatarAnim[animationState.clip.name].speed = 1;
    //                            }
    //                            childAvatar.GetComponent<Animation>().Play();

    //                            //PlayAvatarMusic(childAvatar, animations[0]);

    //                            AvatarInfo avatarInfo = databaseService.GetComponent<DBScript>().GetAvatarInfoById(int.Parse(childAvatar.name.Split('-')[0].Split('D')[0]));
    //                            List<DB_FBX> fbx = databaseService.GetComponent<DBScript>().GetFbxByAvatarId(avatarInfo.avatarId).ToList<DB_FBX>();
    //                            List<AnimationInfo> animations = databaseService.GetComponent<DBScript>().GetAnimationByFbxID(fbx[0].Id).ToList<AnimationInfo>();

    //                            string childAvatarCategory = avatarInfo.category.ToString();

    //                            if (animations.Count > 0)
    //                            {
    //                                Debug.Log("Animations Repeate Count  " + animations.Count);

    //                                if (!childAvatar.name.Contains("-"))
    //                                {
    //                                    //avatarAnim.Stop();
    //                                    //avatarAnim.Play();
    //                                }
    //                                else
    //                                {
    //                                    // ComplexAvatarController.isPlaying = false;
    //                                    // Debug.Log("AirPlane Playing : " + ComplexAvatarController.isPlaying);
    //                                    avatarAnim.Stop();
    //                                    avatarAnim.Play("Fly");
    //                                    avatarAnim["Fly"].speed = 0;
    //                                    avatarAnim["Fly"].time = 0;
    //                                }

    //                                //if (AvatarHasMusic(childAvatar)) {

    //                                //    PlayAvatarMusic(childAvatar, animations[0] );
    //                                //}
    //                                //else {
    //                                //    avatarAnim.Stop();
    //                                //    avatarAnim.Play();
    //                                //}
    //                            }

    //                            else
    //                            {
    //                                if (avatarAnim.GetClipCount() > 0)
    //                                {
    //                                    //Debug.Log("AirPlane Playing : " + ComplexAvatarController.isPlaying);
    //                                    childAvatar.SetActive(true);
    //                                }
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //        }

    //    }

    /// <summary>
    /// Plays the music for avatar
    /// </summary>
    /// <param name="avatar"></param>
    /// <returns></returns>
    public IEnumerator PlayAvatarMusic(GameObject avatar)
    {
        Debug.Log("Play Music For Avatar From Device");
        AudioSource[] audios = avatar.GetComponents<AudioSource>();
        isPlayingMusic = true;
        avatar.GetComponent<AudioSource>().Play(0);

        foreach (AudioSource source in audios)
        {
            source.Play(0);
        }

        Debug.Log("Audio Source Play .. ");
        avatar.GetComponent<Animation>().Stop();
        avatar.GetComponent<Animation>().Play();

        isPlayingMusic = true;
        soundAvatar.SetActive(true);
        ButtonPalyMuteMusic.SetActive(true);
        yield return null;
    }


    /// <summary>
    /// Plays the music for avatar
    /// </summary>
    /// <param name="avatar"></param>
    /// <param name="animModel"></param>
    /// <returns></returns>
    public IEnumerator PlayAvatarMusic(GameObject avatar, AnimationInfo animModel)
    {
        Debug.Log("Play Music For Avatar From Device");
        int avatarMusicID = int.Parse(avatar.name.Split('-')[0].Split('D')[0]);

        if (animModel != null)
        {
            AudioSource[] audios = avatar.GetComponents<AudioSource>();
            isPlayingMusic = true;
            if (audios.Length == 1)
            {
                if (animModel.Repeat != "Loop")
                {
                    List<MusicItem> items = new List<MusicItem>();
                    if (musicDictionary.ContainsKey(avatarMusicID))
                    {
                        items = musicDictionary[avatarMusicID];
                    }
                    if (items.Count == 1)
                    {
                        if (items[0].delay > 0)
                        {
                            if (!avatar.GetComponent<AudioSource>().isPlaying)
                            {
                                avatar.GetComponent<AudioSource>().Stop();

                                if (!avatar.GetComponent<AudioSource>().isPlaying)
                                {
                                    avatar.GetComponent<Animation>().Stop();
                                    avatar.GetComponent<Animation>().Play();
                                    avatar.GetComponent<AudioSource>().time = 0;
                                    avatar.GetComponent<AudioSource>().PlayDelayed(items[0].delay);
                                }
                            }
                            else
                            {
                                avatar.GetComponent<Animation>().Stop();
                                avatar.GetComponent<Animation>().Play();

                                avatar.GetComponent<AudioSource>().time = 0;
                                avatar.GetComponent<AudioSource>().Stop();

                                avatar.GetComponent<AudioSource>().PlayDelayed(items[0].delay);
                            }
                        }
                        else
                        {
                            avatar.GetComponent<AudioSource>().time = 0;
                            avatar.GetComponent<AudioSource>().Stop();
                            avatar.GetComponent<AudioSource>().Play();

                            avatar.GetComponent<Animation>().Stop();
                            avatar.GetComponent<Animation>().Play();
                        }
                    }
                }
                else
                {
                    avatar.GetComponent<AudioSource>().Stop();
                    avatar.GetComponent<Animation>().Stop();
                    avatar.GetComponent<Animation>().Play();
                    avatar.GetComponent<AudioSource>().Play();
                    foreach (AnimationState state in avatar.GetComponent<Animation>())
                    {
                        avatar.GetComponent<AudioSource>().time = state.time;
                        break;
                    }
                    yield return null;
                }
            }
            if (audios.Length > 1)
            {
                List<MusicItem> items = new List<MusicItem>();
                if (musicDictionary.ContainsKey(avatarMusicID))
                {
                    items = musicDictionary[avatarMusicID];
                }

                bool isMusicPlaying = false;
                for (int i = 0; i < audios.Length; i++)
                {
                    if (audios[i].isPlaying)
                    {
                        isMusicPlaying = true;
                    }
                }

                if (isMusicPlaying)
                {
                    for (int i = 0; i < audios.Length; i++)
                    {
                        audios[i].Stop();
                    }
                    StartCoroutine(PlayQueuedMusic(audios, items));
                }
                else
                {
                    StartCoroutine(PlayQueuedMusic(audios, items));
                }
            }
        }

        isPlayingMusic = true;
        soundAvatar.SetActive(true);
        ButtonPalyMuteMusic.SetActive(true);
        yield return null;
    }

    /// <summary>
    /// Repeats the avatar animation.
    /// </summary>
    /// <returns>The avatar animation.</returns>
    public IEnumerator RepeatAvatarAnimation()
    {
#pragma warning disable CS0618 // Type or member is obsolete
        int numChildren = mainOb.transform.childCount;
#pragma warning restore CS0618 // Type or member is obsolete

        if (numChildren >= 1)
        {
            for (int i = 0; i < numChildren; i++)
            {
                GameObject childAvatar = mainOb.transform.GetChild(i).gameObject;

                if (childAvatar.name != "Highlight")
                {
                    Animation avatarAnim = childAvatar.GetComponent<Animation>();


                    if (childAvatar.GetComponent<AudioVideoSync>())
                    {
                        childAvatar.GetComponent<AudioVideoSync>().avatar_pause = false;
                    }

                    if (avatarAnim != null)
                    {
                        if (avatarAnim.GetClipCount() > 0)
                        {
                            foreach (AnimationState animationState in avatarAnim)
                            {
                                avatarAnim[animationState.clip.name].speed = 1;
                            }

                            //avatarAnim.Stop();
                            //avatarAnim.Play();

                            AvatarInfo avatarInfo = databaseService.GetComponent<DBScript>().GetAvatarInfoById(int.Parse(childAvatar.name.Split('-')[0].Split('D')[0]));
                            List<DB_FBX> fbx = databaseService.GetComponent<DBScript>().GetFbxByAvatarId(avatarInfo.avatarId).ToList<DB_FBX>();
                            List<AnimationInfo> animations = databaseService.GetComponent<DBScript>().GetAnimationByFbxID(fbx[0].Id).ToList<AnimationInfo>();

                            string childAvatarCategory = avatarInfo.category.ToString();

                            if (animations.Count > 0)
                            {
                                Debug.Log("Animations Repeate Count  " + animations.Count);
                                if (!childAvatar.name.Contains("-"))
                                {
                                    //avatarAnim.Stop();
                                    //avatarAnim.Play();
                                }
                                else
                                {
                                    //ComplexAvatarController.isPlaying = false;
                                    //Debug.Log("AirPlane Playing : " + ComplexAvatarController.isPlaying);
                                    avatarAnim.Stop();
                                    avatarAnim.Play("Fly");
                                    avatarAnim["Fly"].speed = 0;
                                    avatarAnim["Fly"].time = 0;
                                }

                                if (AvatarHasMusic(childAvatar.name))
                                {
                                    //CheckAudioSources();

                                    StartCoroutine(PlayMusicForAvatar(childAvatarCategory, childAvatar.name, animations[0], avatarAnim));
                                }

                                else
                                {
                                    avatarAnim.Stop();
                                    avatarAnim.Play();
                                }
                            }
                            else
                            {
                                if (avatarAnim.GetClipCount() > 0)
                                {
                                    //Debug.Log("AirPlane Playing : " + ComplexAvatarController.isPlaying);
                                    childAvatar.SetActive(true);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (childAvatar.transform.childCount > 0)
                        {
                            avatarAnim = childAvatar.transform.GetChild(0).GetComponent<Animation>();

                            if (childAvatar.transform.GetChild(0).GetComponent<AudioVideoSync>())
                            {
                                childAvatar.transform.GetChild(0).GetComponent<AudioVideoSync>().avatar_pause = false;
                            }

                            if (avatarAnim != null)
                            {
                                if (avatarAnim.GetClipCount() > 0)
                                {
                                    foreach (AnimationState animationState in avatarAnim)
                                    {
                                        avatarAnim[animationState.clip.name].speed = 1;
                                    }

                                    //avatarAnim.Stop();
                                    //avatarAnim.Play();

                                    AvatarInfo avatarInfo = databaseService.GetComponent<DBScript>().GetAvatarInfoById(int.Parse(childAvatar.name.Split('-')[0].Split('D')[0]));
                                    List<DB_FBX> fbx = databaseService.GetComponent<DBScript>().GetFbxByAvatarId(avatarInfo.avatarId).ToList<DB_FBX>();
                                    List<AnimationInfo> animations = databaseService.GetComponent<DBScript>().GetAnimationByFbxID(fbx[0].Id).ToList<AnimationInfo>();

                                    string childAvatarCategory = avatarInfo.category.ToString();

                                    if (animations.Count > 0)
                                    {
                                        Debug.Log("Animations Repeate Count  " + animations.Count);
                                        if (!childAvatar.name.Contains("-"))
                                        {
                                            //avatarAnim.Stop();
                                            //avatarAnim.Play();
                                        }
                                        else
                                        {
                                            //ComplexAvatarController.isPlaying = false;
                                            //Debug.Log("AirPlane Playing : " + ComplexAvatarController.isPlaying);
                                            avatarAnim.Stop();
                                            avatarAnim.Play("Fly");
                                            avatarAnim["Fly"].speed = 0;
                                            avatarAnim["Fly"].time = 0;
                                        }

                                        if (AvatarHasMusic(childAvatar.name))
                                        {
                                            //CheckAudioSources();

                                            StartCoroutine(PlayMusicForAvatar(childAvatarCategory, childAvatar.name, animations[0], avatarAnim));
                                        }

                                        else
                                        {
                                            avatarAnim.Stop();
                                            avatarAnim.Play();
                                        }
                                    }
                                    else
                                    {
                                        if (avatarAnim.GetClipCount() > 0)
                                        {
                                            //Debug.Log("AirPlane Playing : " + ComplexAvatarController.isPlaying);
                                            childAvatar.SetActive(true);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        yield return new WaitForEndOfFrame();
    }

    private void CheckAudioSources()
    {
        GameObject[] audios = GameObject.FindGameObjectsWithTag("music");

        foreach (GameObject avatar in audios)
        {
            if (avatar.GetComponents<AudioSource>() != null)
            {
                AudioSource[] sources = avatar.GetComponents<AudioSource>();
                for (int aud = 0; aud < sources.Length; aud++)
                {
                    sources[aud].mute = isPlayingMusicMuted;
                }
                if (avatar.GetComponent<AudioSource>() != null)
                {
                    avatar.GetComponent<AudioSource>().mute = isPlayingMusicMuted;
                }
            }
        }
    }

    /// <summary>
    /// Is Placed in the Scene
    /// </summary>
    /// <returns><c>true</c>, if palced was ised, <c>false</c> otherwise.</returns>
    /// <param name="avatarID">Avatar identifier.</param>
    public bool isPalced(string avatarID)
    {
        return avatarsDictionary.ContainsKey(avatarID);
    }

    /// <summary>
    /// Is to be placed.
    /// </summary>
    /// <returns><c>true</c>, if existedin to be placed was ised, <c>false</c> otherwise.</returns>
    /// <param name="avatarID">Avatar identifier.</param>
    public bool isExistedinToBePlaced(string avatarID)
    {
        return avatarsToPlace.Keys.Contains(avatarID);
    }

    /// <summary>
    /// Removes from to be placed.
    /// </summary>
    public void RemoveFromToBePlaced(string name)
    {

        avatarsToPlace.Remove(name);

    }

    /// <summary>
    /// Deletes the avatar info files.
    /// </summary>
    /// <param name="avatar">Avatar.</param>
    public IEnumerator DeleteAvatarInfoFiles(AvatarInfo avatar)
    {
        DirectoryInfo directory = new DirectoryInfo(Application.persistentDataPath + "/Category-" + avatar.category);
        if (directory.Exists)
        {
            FileInfo[] avatarFiles = directory.GetFiles(avatar.avatarId + "*");
            foreach (FileInfo file in avatarFiles)
            {
                File.Delete(file.FullName);
            }
        }
        yield return new WaitForEndOfFrame();
    }

    /// <summary>
    /// Deletes the category.
    /// </summary>
    /// <param name="catId">Cat identifier.</param>
    public IEnumerator DeleteCategory(int catId)
    {
        if (File.Exists((Application.persistentDataPath + "/Category-" + catId + ".png")))
        {
            File.Delete(Application.persistentDataPath + "/Category-" + catId + ".png");
        }

        // Delete From Category Panel
        for (int i = 0; i < categoryPanelContent.transform.childCount; i++)
        {
            if (CategoryPanelParent.transform.GetChild(i).name == catId.ToString())
            {
                Destroy(CategoryPanelParent.transform.GetChild(i).gameObject);
            }
        }

        DirectoryInfo di = new DirectoryInfo(Application.persistentDataPath + "/Category-" + catId);

        if (di.Exists)
        {
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                dir.Delete(true);
            }
            if (di.Exists)
            {
                Directory.Delete(Application.persistentDataPath + "/Category-" + catId);
            }
        }

        // Delete Avatars Information From Database that are related to Categories
        List<AvatarInfo> avatars = databaseService.GetComponent<DBScript>().GetAvatarsByCategoryID(catId).ToList<AvatarInfo>();

        for (int i = 0; i < avatars.Count(); i++)
        {
            yield return StartCoroutine(DeleteAvatar(avatars[i]));
            yield return StartCoroutine(DeleteAvatarInfoFiles(avatars[i]));
            // Delete AvatarInfo
            databaseService.GetComponent<DBScript>().DeleteAvatarInfo(avatars[i]);
        }

        // Delete From Database
        AvatarCategory CatDb = databaseService.GetComponent<DBScript>().GetCategoryById(catId);
        if (CatDb != null)
        {
            databaseService.GetComponent<DBScript>().DeleteCategoryByID(CatDb);
        }

        yield return null;
    }

    public IEnumerator DeleteAvatar(AvatarInfo _avatar)
    {
        Debug.Log("Delete Avatar : " + _avatar.avatarName);

        // DELETE FROM DATABASE WITH FBX AND ANIMATION
        #region Delete Avatar Related Information

        List<DB_FBX> relatedFbx = databaseService.GetComponent<DBScript>().GetFbxByAvatarId(_avatar.avatarId).ToList<DB_FBX>();
        List<AnimationInfo> relatedAnimations = new List<AnimationInfo>();
        List<MusicDB> musicItems = new List<MusicDB>();

        if (relatedFbx.Count() > 0)
        {

            for (int i = 0; i < relatedFbx.Count(); i++)
            {
                relatedAnimations.AddRange(databaseService.GetComponent<DBScript>().GetAnimationByFbxID(relatedFbx[i].Id));
            }

            // Get related Music Items
            for (int mi = 0; mi < relatedAnimations.Count; mi++)
            {
                musicItems.AddRange(databaseService.GetComponent<DBScript>().GetMusicIdsByAnimationID(relatedAnimations[mi].Id));
            }

            // Delete Related Music Items
            for (int dm = 0; dm < musicItems.Count(); dm++)
            {
                databaseService.GetComponent<DBScript>().DeleteMusicItem(musicItems[dm]);
            }

            // Delete Related Animation Info
            if (relatedAnimations.Count() > 0)
            {
                for (int i = 0; i < relatedAnimations.Count(); i++)
                {
                    databaseService.GetComponent<DBScript>().DeleteAnimationInfo(relatedAnimations[i]);
                }
            }

            // Delete Related FBX files
            for (int j = 0; j < relatedFbx.Count(); j++)
            {
                databaseService.GetComponent<DBScript>().DeleteFBX(relatedFbx[j]);
            }

        }

        yield return new WaitForEndOfFrame();
        #endregion

    }

    public void UnlockAvatars()
    {
        LeanSelectable.DeselectAll();
        avatarHighlight.SetActive(false);
        avatarHighlight.transform.position = mainOb.transform.position;
        avatarHighlight.transform.SetParent(mainOb.transform);

        cleanAvatar.SetActive(false);
        LockButton.SetActive(false);
        ResizeButton.SetActive(false);
        DublicateButton.SetActive(false);
    }

    //    public void ScaleAvatar()
    //    {
    //#pragma warning disable CS0618 // Type or member is obsolete
    //        int numChildren = mainOb.transform.childCount;
    //#pragma warning restore CS0618 // Type or member is obsolete

    //        for (int i = 0; i < numChildren; i++)
    //        {
    //            GameObject avatar = mainOb.transform.GetChild(i).gameObject;
    //            Lean.Touch.LeanSelectable selectable = avatar.GetComponent<Lean.Touch.LeanSelectable>();
    //            if (selectable != null)
    //            {
    //                if (selectable.IsSelected) {
    //                    avatar.transform.localScale *= 1.25f;
    //                }
    //            }
    //        }
    //    }


    public bool IsCanvasButtonPressed()
    {
        pointerEventData = new PointerEventData(this.eventSystem)
        {
            position = Input.mousePosition
        };
        List<RaycastResult> results = new List<RaycastResult>();
        this.graphicRayCaster.Raycast(pointerEventData, results);

        bool resultIsButton = false;
        foreach (RaycastResult result in results)
        {
            if (result.gameObject.GetComponentInParent<Toggle>() ||
                result.gameObject.GetComponent<Button>())
            {
                resultIsButton = true;
                break;
            }
        }
        return resultIsButton;
    }

    public void GoToSelfie()
    {
        SceneManager.LoadScene("FaceMesh");
    }

    public void GoToLogin()
    {
        PlayerPrefs.SetInt("logged", 0);
        PlayerPrefs.SetString("user_id", "");
        PlayerPrefs.SetString("social_site", "");
        PlayerPrefs.SetString("firstname", "");
        PlayerPrefs.SetString("lastname", "");
        PlayerPrefs.SetString("auth_token", "");

        isPlacingAvatar = false;
        cancelDownload = false;
        isDownloading = false;

        ARKitWorldTrackingSessionConfiguration sessionConfig = new ARKitWorldTrackingSessionConfiguration(UnityARAlignment.UnityARAlignmentGravity, UnityARPlaneDetection.Horizontal);
        UnityARSessionNativeInterface.GetARSessionNativeInterface().RunWithConfigAndOptions(sessionConfig, UnityARSessionRunOption.ARSessionRunOptionRemoveExistingAnchors | UnityARSessionRunOption.ARSessionRunOptionResetTracking);

        SceneManager.LoadScene("Login");
    }

    public bool IsTheAvatarIsLastOneOfDublicates(string name)
    {
        int avatarsCount = avatarsList.Count;
        int clonedAvatars = 0;

        for (int i = 0; i < avatarsCount; i++)
        {
            if (avatarsList[i].name.StartsWith(name, StringComparison.Ordinal))
            {
                clonedAvatars++;
            }
        }

        if (clonedAvatars == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // Duplicate Avatar

    public void DuplicateAvatar()
    {
        UnlockAvatar(lastSelectedObject);

        int avatartNumber = int.Parse(lastSelectedObject.Split('D')[0]);
        GameObject avatarToClone = GameObject.Find(lastSelectedObject);

        avatarToClone.GetComponent<Animation>().playAutomatically = false;

        GameObject avatarClone = Instantiate(avatarToClone, mainOb.transform);

        if (avatarsDuplicates.Keys.Contains(avatartNumber))
        {
            int maxNumber = avatarsDuplicates[avatartNumber].Max();
            maxNumber++;
            avatarsDuplicates[avatartNumber].Add(maxNumber);
            avatarClone.name = avatartNumber + "D" + maxNumber;
        }
        else
        {
            avatarClone.name = avatartNumber + "D" + 1;
            List<int> duplicates = new List<int>();
            duplicates.Add(1);
            avatarsDuplicates.Add(avatartNumber, duplicates);
        }

        avatarsList.Add(avatarClone);

        System.Random random = new System.Random();

        int XCoordinates = random.Next(-1, 1);
        int ZCoordinates = random.Next(-1, 1);

        float XCoordinate = 0;
        float ZCoordinate = 0;

        if (XCoordinate < 0)
        {
            XCoordinate = XCoordinates - 0.5f;
        }
        else
        {
            XCoordinate = XCoordinates + 0.5f;
        }

        if (ZCoordinate < 0)
        {
            ZCoordinate = ZCoordinates - 0.5f;
        }
        else
        {
            ZCoordinate = ZCoordinates + 0.5f;
        }

        avatarClone.transform.position += new Vector3(XCoordinate / 2, 0, ZCoordinate / 2);

        SelectAvatar(GameObject.Find(lastSelectedObject));

    }


    public void UnlockAvatar(string AvatarName)
    {
        GameObject avatar = GameObject.Find(AvatarName);
        avatar.GetComponent<Lean.Touch.LeanSelectable>().DeSelect();

        avatarHighlight.transform.position = mainOb.transform.position;
        avatarHighlight.transform.SetParent(mainOb.transform);
        avatarHighlight.transform.localRotation = Quaternion.Euler(90f, 0f, 0f);
    }


    public void DuplicateAvatar(string avatarID, Vector3 position)
    {
        UnlockAvatar(avatarID);

        int avatartNumber = int.Parse(avatarID);

        Debug.Log("Selecting  Avatar to Clone : " + name);

        GameObject avatarToClone = GameObject.Find(avatarID); //avatarsList.Where(avatar => avatar.name == name).SingleOrDefault();

        Debug.Log(string.Format("Avatar {0} Selected ", avatarToClone.name));

        if (avatarToClone.GetComponent<Animation>() != null)
        {
            avatarToClone.GetComponent<Animation>().playAutomatically = false;
        }

        GameObject avatarClone = Instantiate(avatarToClone, mainOb.transform);

        if (avatarsDuplicates.ContainsKey(avatartNumber))
        {
            int maxNumber = avatarsDuplicates[avatartNumber].Max();
            maxNumber++;
            avatarsDuplicates[avatartNumber].Add(maxNumber);
            avatarClone.name = avatartNumber + "D" + maxNumber;
        }
        else
        {
            avatarClone.name = avatartNumber + "D" + 1;
            List<int> duplicates = new List<int>();
            duplicates.Add(1);
            avatarsDuplicates.Add(avatartNumber, duplicates);
        }

        Vector3 cam_position = Camera.main.transform.position;
        cam_position.y = avatarClone.transform.position.y;

        // Roate the Avatar to the Camera
        avatarClone.transform.LookAt(cam_position);


        Debug.Log(string.Format("Avatar {0} Instantiated ", avatarToClone.name));

        //Set the avatar in the Plane Indicator Position
        avatarClone.transform.position = new Vector3(position.x, Y_AXIS, position.z); 

        Debug.Log(string.Format("Avatar {0} Setted in Position {1} ", avatarToClone.name, position));

        // Add the Avatar to the Avatars List 
        avatarsList.Add(avatarClone);

        //UnlockAvatar(avatarID);

        // Select the Duplicated Avatar
        SelectAvatar(avatarClone);

    }


    public void GoToSelfieScene()
    {
        SceneManager.LoadScene("SelfieFeature");
    }

    public void HideErrorMessage()
    {
        errorMessage.SetActive(false);
    }
}
