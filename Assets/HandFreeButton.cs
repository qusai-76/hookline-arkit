﻿
using System.Collections;
using System.Runtime.InteropServices;
using NatCorder.Examples;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HandFreeButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    #if UNITY_IOS
        [DllImport("__Internal")]
        public static extern bool _InPhoneCall();
    #endif
    

    [SerializeField]
    [Tooltip("How long must pointer be down on this object to trigger a long press")]
    private float holdTime = 1f;

    // Remove all comment tags (except this one) to handle the onClick event!
    private bool held = false;
    private bool pressed;
    public int secondsBeforeRecording=3;
    public float MaxRecordingTime = 30f; // seconds
    
    public UnityEvent onClick = new UnityEvent();

    public UnityEvent onTouchDown = new UnityEvent();
    public UnityEvent onTouchUp = new UnityEvent();
    public UnityEvent onLongPress = new UnityEvent();
    public Image button, countdown;
   

    public GameObject recording;
    public GameObject stop;
    public GameObject countText;

    public ReplayCam videoRecoreder;
    
    private bool isInCounter;
    public static bool isRecording = false;

    private void Start()
    {
        isInCounter = false;
        Reset();
    }
    
    public void Reset()
    {
        // Reset fill amounts
        if (button) button.fillAmount = 1.0f;
        if (countdown) countdown.fillAmount = 0.0f;
        
        stop.SetActive(false);
        recording.SetActive(true);
        isRecording = false;
    }


    public void OnPointerUp(PointerEventData eventData)
    {
        if (_InPhoneCall() == false && !PhoneCall.existedCall)
        {
            if (!held)
            onClick.Invoke();
        }
        else
        {
            PhoneCall.existedCall = true;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (_InPhoneCall() == false && !PhoneCall.existedCall)
        {
            held = false;
        }
        else
        {
            PhoneCall.existedCall = true;
        }
    }


    public void StartRecording()
    {
        if (!isRecording)
        {
            isRecording = true;
            StartCoroutine(Countdown());
        }
        else
        {
            if (!isInCounter)
            {
                //StopCoroutine(Countdown());
                Reset();
                videoRecoreder.StopRecording();
            }
        }
    }
   
    private IEnumerator Countdown()
    {
        isInCounter = true;
        
        recording.SetActive(false);
        
        int counter = secondsBeforeRecording;
        countText.SetActive(true);
        countText.GetComponent<TextMeshProUGUI>().SetText(counter.ToString());
        
        while (counter > 0)
        {
            yield return new WaitForSeconds(1);
            counter--;
            countText.GetComponent<TextMeshProUGUI>().SetText(counter.ToString());
        }
       
        countText.SetActive(false);
        stop.SetActive(true);
        
        // Start Recording
        videoRecoreder.StartRecording();
        
        isInCounter = false;
        
        // Animate the countdown
        float startTime = Time.time, ratio = 0f;

        while ((ratio = (Time.time - startTime) / MaxRecordingTime) < 1.0f && isRecording)
        {
            countdown.fillAmount = ratio;
            button.fillAmount = 1f - ratio;
            yield return null;
        }

        // Reset
        if (isRecording)
        {
            // Stop Recording
            videoRecoreder.StopRecording();
            Reset();
        }
        
    }

}