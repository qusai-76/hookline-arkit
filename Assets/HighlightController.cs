﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HighlightController : MonoBehaviour
{
    public GameObject Highlighter;
    public GameObject mainOb;
    public GameObject LockButton;

    public GameObject RepeatButton;
    public GameObject ResizeButton;
    public GameObject DublicateButton;
    public GameObject CleanAvatarsButton;
 
    public static string lastAvatarName = "";
    public static string lastAvatarSelected = "";
    
    private float holdTime = 0.8f; //or whatever
    private float acumTime = 0;
    
    
    [Header("Graphics Settings")]
    GraphicRaycaster graphicRayCaster;
    PointerEventData pointerEventData;
    EventSystem eventSystem;

    public static bool hasHighlightedObject = false;
    
    public bool AvatarsHasAnimation = false;
    
    // Start is called before the first frame update
    void Start()
    {
        this.graphicRayCaster = FindObjectOfType<GraphicRaycaster>();
        this.eventSystem = FindObjectOfType<EventSystem>();
    }
    public static bool isInHold = false;
    public static bool Ended = false;
    int touchCount = 0;
    // Update is called once per frame
    
    void Update()
    {
        if (!IsPointerOverUIObject() && Input.touchCount>=1  ) 
        {
            Debug.Log("Pressed Touched : " + Input.touchCount);
            
            touchCount = Input.touchCount;
            
            
            if(Input.GetTouch(0).phase == TouchPhase.Began)
            {
                Ended = false; 
                isInHold = false;
            }

            if ( (Input.GetTouch(0).phase == TouchPhase.Stationary || Input.GetTouch(0).phase == TouchPhase.Moved ) 
            &&  !(Input.GetTouch(0).phase == TouchPhase.Ended))
            {
                acumTime += Time.deltaTime;

                if (acumTime >= holdTime)
                {
                    //Long tap
                    HitObject(Input.touches[0].position);
                    isInHold = true;
                }
                 
            }
            if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                acumTime = 0;
            }
            
            //Debug.Log("Accumulation Time : " +acumTime+ " Is in Hold : "+ isInHold);
            //Debug.Log("Phase 1 :"+ Input.GetTouch(0).phase);

            if (Input.touchCount == 1)
            {
                //Debug.Log(" Phase 2 : " + Input.GetTouch(0).phase);
                if (Input.GetTouch(0).phase == TouchPhase.Ended)
                {
                    if (!Ended)
                    {
                        //Debug.Log("Phase Ended Time : " + acumTime + " , Is in Hold : " + isInHold + " Touch Count : " + touchCount + "has Highlighted Object : " + hasHighlightedObject);
                        acumTime = 0;
                        if ((!isInHold) && hasHighlightedObject == true)
                        {
                            EnableLockMode();
                        }
                        Ended = true;
                    }
                }
            }
        }
        
    
               
    }
   
    public GameObject ReturnClickedObject()
    {
        GameObject target = null;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit[] hits;
        hits = Physics.RaycastAll(ray.origin, ray.direction * 100);
        
        for(int i=0;i<hits.Length;i++) {
             if(hits[i].transform.name != "MainOb" 
             && hits[i].transform.name != "Capsule" 
             && !hits[i].transform.name.Contains("bubble") 
             && !hits[i].transform.name.Contains("Face") 
             && hits[i].transform.name != "Highlight" 
             && !hits[i].transform.name.Contains("ShadowCast") 
             && hits[i].transform.name != "Sphere")
                {
                    target = hits[i].transform.gameObject;
                    return target;
                }
        }
        
        return target;
    }
     
    public void HitObject(Vector2 position)
    {
                GameObject hittedAvatar = ReturnClickedObject();
            
                if (hittedAvatar != null) 
                { 
                    Debug.Log("Hitting Object : "+ hittedAvatar.transform.name);
                    
                    if (hittedAvatar.GetComponent<Lean.Touch.LeanSelectable>() == null)
                    {
                        Debug.Log("Search In Parent");
                        GameObject main = hittedAvatar.GetComponentInParent<Lean.Touch.LeanSelectable>().gameObject;
                        Highlighter.transform.position = main.transform.position + new Vector3(0,0.01f,0);
                        Highlighter.transform.localRotation = Quaternion.Euler(90f, 0f, 0f);

                        if (!Highlighter.activeInHierarchy) { 
                             Debug.Log("Search In Parent - 1");
                             
                             main.GetComponentInParent<Lean.Touch.LeanSelectable>().SelectMe();
                             Highlighter.transform.SetParent(main.transform);
                             Highlighter.SetActive(true);
                             
                             EnableEditMode(main);
                             //AppLoader.lastSelectedObject = main.name;
                             //AppLoader.NoAvatarSelected = false;
                             
                        }
                        else { 
                            Debug.Log("Search In Parent - 2");
                            
                            main.GetComponentInParent<Lean.Touch.LeanSelectable>().SelectMe();
                            Highlighter.transform.SetParent(main.transform);
                            EnableEditMode(main); 
                            
                            Debug.Log("Selected Avatar is  : " + main.name);
                            lastAvatarSelected = main.name;
                            
                            //AppLoader.lastSelectedObject = main.name;
                            //AppLoader.NoAvatarSelected = false;
                            
                        }
                        
                        CleanAvatarsButton.SetActive(true);
                        //DublicateButton.SetActive(true);
                    }
                    
                    else 
                    {
                        hittedAvatar.GetComponentInParent<Lean.Touch.LeanSelectable>().SelectMe();
                        if (!Highlighter.activeInHierarchy)
                        {
                             Highlighter.transform.position = hittedAvatar.transform.position + new Vector3(0,0.01f,0);
                         
                             Debug.Log(" Not Reactive - 1 "); 
                             
                             Highlighter.transform.localRotation = Quaternion.Euler(90f, 0f, 0f);
                             //Highlighter.transform.localScale = hittedAvatar.transform.localScale*100;
                             Highlighter.transform.SetParent(hittedAvatar.transform);
                             Highlighter.SetActive(true);
                             
                            
                             CleanAvatarsButton.SetActive(true);
                             //DublicateButton.SetActive(true);
                             
                             EnableEditMode(hittedAvatar);
                             
                             //AppLoader.lastSelectedObject = hittedAvatar.name;
                             //AppLoader.NoAvatarSelected = false;
                        }
                        
                        else { 
                                Debug.Log("Activated ");
                                Highlighter.transform.position = hittedAvatar.transform.position + new Vector3(0,0.01f,0);
                                Highlighter.transform.localRotation = Quaternion.Euler(90f, 0f, 0f);
                                Highlighter.transform.SetParent(hittedAvatar.transform);
                                CleanAvatarsButton.SetActive(true);
                                
                                //DublicateButton.SetActive(true);
                                
                                EnableEditMode(hittedAvatar);
                                //AppLoader.lastSelectedObject = hittedAvatar.name;
                                //AppLoader.NoAvatarSelected = false;
                        }
                    }
                }
                hasHighlightedObject = true;
    }

    private void EnableLockMode()
    {        
        Lean.Touch.LeanSelectable.DeselectAll();
        
        Highlighter.transform.position = mainOb.transform.position + new Vector3(0,0.01f,0);
        Highlighter.transform.localRotation = Quaternion.Euler(90f, 0f, 0f);
        Highlighter.transform.SetParent(mainOb.transform);
                
        Highlighter.SetActive(false);
        CleanAvatarsButton.SetActive(false);
        //DublicateButton.SetActive(false);
        
        //AppLoader.hasHighlitedObject = false;
        //AppLoader.lastSelectedObject = string.Empty;


        CheckIfHasAnimation();
        
        hasHighlightedObject = false;
        
        //yield return null;
    }
    
    public void CheckIfHasAnimation()
    { 
    
        //int numClildren = AppLoader.avatarsList.Count;
        
        //if(numClildren>0) 
        //{
        //    for (int i = 0; i < numClildren; i++)
        //    {
        //        if (AppLoader.avatarsList[i].name != "Highlight")
        //        {
        //            Animation anim = AppLoader.avatarsList[i].GetComponent<Animation>();
        //            if (anim != null)
        //            {
        //                if (anim.GetClipCount() > 0)
        //                {
        //                     RepeatButton.SetActive(true);
        //                     return;
        //                }
        //            }
        //        }
        //    }
        //}
        RepeatButton.SetActive(false);
    }
   
    public void EnableEditMode(GameObject avatar)
    {
            Animation anim = avatar.GetComponent<Animation>();
            
            if (anim != null)
            {
                if (anim.GetClipCount() > 0)
                {
                    foreach (AnimationState animationState in anim)
                    {
                        anim[animationState.clip.name].speed = 0;
                    }

                    GameObject avatarAudio = GameObject.Find("music-"+avatar.name);

                        if (avatarAudio != null)
                        {
                            //if (!avatar.GetComponent<Lean.Touch.LeanSelectable>().IsSelected)
                            //{
                                if (avatarAudio.GetComponent<AudioSource>().isPlaying)
                                {
                                    //AudioListener.pause = true;
                                    avatarAudio.GetComponent<AudioSource>().Stop();
                                }
                            //}
                        }
                }
            }
            
            //AppLoader.hasHighlitedObject = true;
            //AppLoader.lastSelectedObject = avatar.name;
            
            RepeatButton.SetActive(false);
            
    }

    private bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    private bool IsPointerOnUIElement()
    { 
        foreach (Touch touch in Input.touches)
        {
            if (touch.phase != TouchPhase.Ended || touch.phase != TouchPhase.Canceled || touch.phase != TouchPhase.Moved)
            {
                int id = touch.fingerId;
                if (EventSystem.current.IsPointerOverGameObject(id))
                {
                    // ui touched
                    return true;
                }
            }
         }
         return false;    
    }

}
