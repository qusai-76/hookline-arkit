﻿using System;
using System.Collections;
using System.Collections.Generic;
using AppSeetingsManagement;
using Assets.Models;
using UnityEngine;
using UnityEngine.UI;
public class RegistrationSystem : MonoBehaviour {


    [SerializeField] private InputField firstName;
    [SerializeField] private InputField lastName;
    [SerializeField] private InputField email;
    [SerializeField] private InputField password;
    [SerializeField] private InputField password_confirm;
    [SerializeField] private Text validationText;


    public void Register()
    {
        RegistrationModel model = new RegistrationModel();

        //if (!string.IsNullOrEmpty(firstName.text) && !string.IsNullOrEmpty(password.text))
        if (!string.IsNullOrEmpty(email.text) )
        {
            model.firstname = firstName.text;
            model.lastname = lastName.text;
            model.email = email.text;
            model.password = password.text;

            string registerUrl = API_Settings.ONLINE_SERVICE_URL + API_Settings.REGISTER_ROUTE;
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/json");

            ///////////////////////////
            if (!IsValidEmail(email.text))
            {   
                validationText.text = "Email format is incorrect";
            }
            //else if (password.text.Length < 8 || password.text.Length > 12)
            //{
            //    validationText.text = "Password between 8-12 characters";
            //}
            //else if (password.text != password_confirm.text)
            //{
            //    validationText.text = "Passwords doesn't matches";
            //}
            else
            {
                validationText.text = "";
                StartCoroutine(SendRegisterRequest(registerUrl, model.ToPostData(), headers));
            }
        }
        else {
            //Debug.Log("Username or password is Empty");
            //validationText.text = "Username or password is Empty";
            validationText.text = "Email is Empty";
        }
    }


    IEnumerator SendRegisterRequest(string URL, byte[] postData, Dictionary<string, string> headers)
    {
        WWW www = new WWW(URL, postData, headers);
        yield return www;
        //print("Error" + www.text);

        if (www.error == null) {
            // Registration Succeed
            Debug.Log("Register Succeed");
            Debug.Log(www.text);

            validationText.text = "please check your email for verify your account";

            StartCoroutine(CallFunctionLoginAfterTime(5));

        }
        else {
            ErrorMessage errorMessage = JsonUtility.FromJson<ErrorMessage>(www.text);
            validationText.text = errorMessage.status;
        }
    }

    public void GoToLogin()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Login");
    }


    bool IsValidEmail(string Useremail)
    {
        System.Text.RegularExpressions.Regex emailRegex = new System.Text.RegularExpressions.Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
        System.Text.RegularExpressions.Match emailMatch = emailRegex.Match(Useremail);
        return emailMatch.Success;
    }



    IEnumerator CallFunctionLoginAfterTime(int time_second)
    {

        yield return new WaitForSeconds(time_second);
        GoToLogin();
    }

}
