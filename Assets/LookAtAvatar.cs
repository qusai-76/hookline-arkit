﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtAvatar : MonoBehaviour
{
    public GameObject Target;

    RectTransform rt;

    void Start()
    {
        rt = GetComponent<RectTransform>();
    }

    void Update()
    {
        // Get the position of the object in screen space
        if (Target != null)
        {
            Vector3 objScreenPos = Camera.main.WorldToScreenPoint(Target.transform.position);
            // Get the directional vector between your arrow and the object
            
            Vector3 dir = (objScreenPos - rt.position).normalized;

            float angle = Mathf.Rad2Deg * Mathf.Atan2(dir.y, dir.x);

            //Debug.Log("Angle " + angle + " Object " + objScreenPos + " Rt : " + rt.position);
           
            if (objScreenPos.z >=0)
            {
                rt.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            }
            else
            {
                if (angle > 0)
                {
                    rt.rotation = Quaternion.AngleAxis( (180 - angle), -Vector3.forward);
                }
                else { 
                    rt.rotation = Quaternion.AngleAxis( - (180 + angle), Vector3.forward);
                }
            }
        }
        
    }
}



//if (objScreenPos.z > =0)
//{
//    rt.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
//}
//else
//{
//    if (angle > 0)
//    {
//        rt.rotation = Quaternion.AngleAxis(-(180 - angle), -Vector3.forward);
//    }
//    else { 
//        rt.rotation = Quaternion.AngleAxis((180 + angle), -Vector3.forward);
//    }
//}