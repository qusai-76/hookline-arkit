﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ApplicationLogout : MonoBehaviour
{
    
	public void Logout()
	{
        SessionLogout();
		GoToLogin() ;
	}

	public void SessionLogout()
	{
        PlayerPrefs.SetInt("logged", 0);
        PlayerPrefs.SetString("user_id", "");
        PlayerPrefs.SetString("social_site", "");
        PlayerPrefs.SetString("firstname", "");
        PlayerPrefs.SetString("lastname", "");
        PlayerPrefs.SetString("auth_token", "");
        DestroyAllGameObjects();
    }

    public void GoToLogin()
    {
        SceneManager.LoadScene(0);
    }

    public void DestroyAllGameObjects()
    {
        GameObject[] GameObjects = (FindObjectsOfType<GameObject>() as GameObject[]);

        for (int i = 0; i < GameObjects.Length; i++)
        {
            Destroy(GameObjects[i]);
        }
    }
}
