﻿using System;
using UnityEngine;

namespace Assets.Models
{
    [Serializable]
    public class UserAuthModel
    {
        public string AccessToken;

        public string ToJson()
        {
            return JsonUtility.ToJson(this);
        }

        public byte[] ToPostData()
        {
            return System.Text.Encoding.UTF8.GetBytes(ToJson());
        }
    }
}
