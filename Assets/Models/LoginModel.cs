﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Models
{
    [Serializable]
    public class LoginModel
    {
        public string email;
        public string password;

        public string Username
        {
            get
            {
                return this.email;
            }
            set
            {
                this.email = value;
            }
        }
        public string Password
        {
            get
            {
                return this.password;
            }
            set
            {
                this.password = value;
            }
        }

        public string ToJson()
        {
            return JsonUtility.ToJson(this);
        }

        public byte[] ToPostData()
        {
            return System.Text.Encoding.UTF8.GetBytes(ToJson());
        }
    }
}
