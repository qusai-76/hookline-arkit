﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Models
{
    [Serializable]
    public class ThumbModel
    {
        public string thumbnailContent;
        public string id;
        public string fileName;
        public string fileExtension;
        public string tagId;
    }
}