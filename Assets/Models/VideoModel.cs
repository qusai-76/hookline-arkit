﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class VideoModel
{
    public string videoContent;
    public string id;
    public string fileName;
    public string fileExtension;
    public string filePath;
    public string fileType;
}

