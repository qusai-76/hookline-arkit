﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AnimationModel
{
    public string id;
    public string order;
    public string repeat; // "Multiple"
    public string repeatCount; // 3
    public string avatarId; // 129
    public string musicId; // 2
    public string path; 
    public string fileName; 
    public string delay; // 0.3
    public string animationId; 
    public string animationName; // "test2"
    public string avatarName; // "Aries"
}
