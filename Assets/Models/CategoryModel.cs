﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CategoryModel {
    public string id;
    public string tag;
    public string tagPhotoContent;
    public string avatarsId;
}