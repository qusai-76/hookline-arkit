﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HasMusicModel {
    public bool hasmusic;
}

[System.Serializable]
public class HasAnimation {
    public string hasAnimation;
}
