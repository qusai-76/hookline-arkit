﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
namespace Assets.Models
{
    [Serializable]
    class RegistrationModel
    {
        public string firstname;
        public string lastname;
        public string email;
        public string password;

        public string Email
        {
            get
            {
                return this.email;
            }
            set
            {
                this.email = value;
            }
        }
        public string Password
        {
            get
            {
                return this.password;
            }
            set
            {
                this.password = value;
            }
        }

        public string Firstname
        {
            get
            {
                return this.firstname;
            }
            set
            {
                this.firstname = value;
            }
        }
        public string Lastname
        {
            get
            {
                return this.lastname;
            }
            set
            {
                this.lastname = value;
            }
        }

        public string ToJson()
        {
            return JsonUtility.ToJson(this);
        }

        public byte[] ToPostData()
        {
            return System.Text.Encoding.UTF8.GetBytes(ToJson());
        }
    }

}
