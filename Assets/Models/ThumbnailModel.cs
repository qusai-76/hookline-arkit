﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Assets.Models
{
    [Serializable]
    public class ThumbnailModel
    {
        public string avatarId;
        public string tagsId;
        public string thumbnailContent;
        
        public string fileName;
        public string fileExtension;
        public string filePath;
        public string fileType;

        public string hasAnimation;
        public string animation;
        
        // indicate that the animation is Continous 
        public string continuousAnimation;
    }
}

