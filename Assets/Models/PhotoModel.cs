using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Models
{
    [Serializable]
    public class PhotoModel
    {
        public int id;
        public string fileName;
        public string fileExtension;
        public string filePath;
        public string fileType;
        public string photoContent;
    }
}
