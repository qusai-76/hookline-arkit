﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AvatarModel {
    public string avatarID;
    public string avatarName;
    public string thumbnailID;
    public string thumbnailName;
}
