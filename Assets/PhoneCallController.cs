﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class PhoneCall {
    public static bool existedCall =false;
}

public class PhoneCallController : MonoBehaviour
{
    #if UNITY_IOS
        [DllImport("__Internal")]
        public static extern bool _InPhoneCall();
        [DllImport("__Internal")]
        public static extern bool _IsMicInUsage();
        [DllImport("__Internal")]
        public static extern int NumcerOfCalls();
        [DllImport("__Internal")]
        public static extern bool _Is_Mic_Present();
        [DllImport("__Internal")]
        public static extern bool CallStatus();
        
    #endif
    

    // Update is called once per frame
    void Update() {
        
        
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            Debug.Log(" Is There Call : " + _InPhoneCall().ToString());
            
            //Debug.Log(" Is Microphone Available : " + _IsMicInUsage().ToString());
            //Debug.Log(" Number of calls  : " + NumcerOfCalls().ToString());
            
            Debug.Log(" Call Status : " + CallStatus().ToString());
        }
    }
}
