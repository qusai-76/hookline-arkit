﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonRotateX : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
    [SerializeField]
    [Tooltip("How long must pointer be down on this object to trigger a long press")]
    private float holdTime = 1f;


    public Transform light; 
    private float rotationAngle = 45.1f;

    // Remove all comment tags (except this one) to handle the onClick event!
    private bool held = false;
    private bool pressed;
    public UnityEvent onClick = new UnityEvent();

    public UnityEvent onTouchDown = new UnityEvent();
    public UnityEvent onTouchUp = new UnityEvent();
    public UnityEvent onLongPress = new UnityEvent();

    public float RotationSpeed = 50;
    
    //private const float MaxRecordingTime = 30f; // seconds


    private void Start()
    {
        Reset();
    }
    private void Reset()
    {
        //// Reset fill amounts
        //if (button) button.fillAmount = 1.0f;
        //if (countdown) countdown.fillAmount = 0.0f;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        held = false;
        Invoke("OnLongPress", holdTime);
        StartCoroutine(Countdown());
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        CancelInvoke("OnLongPress");

        if (!held)
          onClick.Invoke();
        pressed = false;

        if (!held)
           Debug.Log("After Click");
        else
          Debug.Log("After Long");
    }
    
    private IEnumerator Countdown()
    {
        pressed = true;
        // First wait a short time to make sure it's not a tap
        yield return new WaitForSeconds(holdTime);
        if (!pressed) yield break;
        // Start recording
        if (onTouchDown != null) onTouchDown.Invoke();
        // Animate the countdown
       
        while (pressed)
        {
               #pragma warning disable CS0618 // Type or member is obsolete
               
               if(rotationAngle<89 && rotationAngle>45){
                    float angle =RotationSpeed * Time.deltaTime;
                    light.eulerAngles += new Vector3( angle,0,0);
                    rotationAngle+=angle;
                }
                else
                {
                     light.eulerAngles = new Vector3(45.1f ,light.eulerAngles.y,light.eulerAngles.z);
                     rotationAngle=45.1f;
                }
                //light.eulerAngles.x += RotationSpeed * Time.deltaTime;
                //light.RotateAround(Vector3.zero, Vector3.right, RotationSpeed * Time.deltaTime);
               #pragma warning restore CS0618 // Type or member is obsolete
               
            yield return null;
        }
        // Reset
        Reset();
        // Stop recording
        if (onTouchUp != null) onTouchUp.Invoke();
    }
    
    public void OnPointerExit(PointerEventData eventData)
    {
        CancelInvoke("OnLongPress");
    }

    private void OnLongPress()
    {
        held = true;
        onLongPress.Invoke();
    }
}
