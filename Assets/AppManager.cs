﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Timers;
using Lean.Touch;
using Newtonsoft.Json;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AppManager : MonoBehaviour
{
    #if UNITY_IOS
        [DllImport("__Internal")]
        public static extern bool _InPhoneCall();
        [DllImport("__Internal")]
        public static extern bool CallStatus();
    #endif

    public GameObject mainOb;
    public GameObject databaseService;

   
    private List<GameObject> avatarsList;
    public Dictionary<string, int> avatarsDictionary;
    public Dictionary<int, List<MusicItem>> musicDictionary;

    public Dictionary<string, Vector3> avatarsToPlace;

    public FileInfo[] filesOfThumnails;

    public GameObject avatarButtonPrefab;

    public Transform avatarsPanelContainer;

    public GameObject scrollSnapAvatars;

    private List<UnityWebRequest> requests;

    public bool isDownloading;
    public bool isPlacingAvatar;
    private bool cancelDownload;
    private bool showInternetMeassage = false;

    public GameObject downloadStatusPanel;
    public GameObject Circleobject;

    // downloading status;
    public UnityEngine.UI.Image button, countdown;

    public GameObject downloadingMessageStatus;

    public GameObject messageInternetConnectoin;

    public static List<AssetBundle> bundlesList;

    [Header("Music Content")]
    public GameObject musicTrack;
    public List<GameObject> audioTracks;
    public List<string> audiosIds;
    public static bool isPlayingMusic = false;

    public static bool isRepeating = false;
    public static bool isPausing = false;


    public List<float> animationsLength;

    public int anim_loop_nb;

    private bool canshowmessage = true;

    public static int selectedAvatar=0;

    System.Timers.Timer timer;
    System.Timers.Timer animationTimer;

    // Start is called before the first frame update
    void Start()
    {
        anim_loop_nb = 0;
        avatarsList = new List<GameObject>();
        bundlesList = new List<AssetBundle>();
        avatarsDictionary = new Dictionary<string, int>();
        musicDictionary = new Dictionary<int, List<MusicItem>>();
        animationsLength = new List<float>();
        avatarsToPlace = new Dictionary<string, Vector3>();
        audioTracks = new List<GameObject>();
        audiosIds = new List<string>();

        //StartCoroutine(LoadThumnails());

        string URL = API_Settings.STAGING_SERVICE_URL + API_Settings.GET_SELFIE_CATEGORIES;
        StartCoroutine(LoadAllCategories(URL));
    }

    void Update()
    {
        if (animationTimerFinisted && anim_loop_nb <= 0) {
            animationTimerFinisted = false;
        }
    }

    public int AvatarsCount()
    {
        return avatarsList.Count();
    }

    /// <summary>
    /// Loads all categories.
    /// </summary>
    /// <returns>The all categories.</returns>
    /// <param name="URL">URL.</param>
    //public IEnumerator LoadAllCategories(string URL)
    //{
    //    yield return new WaitForSeconds(0.5f);
    //    List<int> categoryIDS = databaseService.GetComponent<DBScript>().GetAllCategories().Where<AvatarCategory>(c => c.Type == "Selfie").ToList<AvatarCategory>().Select(c => c.Id).ToList();

    //    if (categoryIDS.Count() > 0)
    //        {
    //            yield return StartCoroutine(LoadThumnails());
    //            yield return StartCoroutine(UpdateAvatars(URL));
    //        }

    //        else {
    //            yield return StartCoroutine(DownloadAllAvatars(URL));
    //        }

    //        yield return new WaitForEndOfFrame();

    //}

    public IEnumerator LoadAllCategories(string URL)
    {
        yield return new WaitForSeconds(1f);

        bool needToUpdate = false;

        List<int> categoryIDS = databaseService.GetComponent<DBScript>().GetAllCategories().Where<AvatarCategory>(c => c.Type == "Selfie").ToList<AvatarCategory>().Select(c => c.Id).ToList();
        DirectoryInfo di = new DirectoryInfo(Application.persistentDataPath);
        filesOfThumnails = di.GetFiles("Category*.png");
        if (filesOfThumnails.Length > 0)
        {
            foreach (var file in filesOfThumnails)
            {
                int filename = int.Parse(file.Name.Replace("Category-", "").Replace(".png", ""));

                if (categoryIDS.Contains(filename)) {
                    needToUpdate = true;
                }
            }
        }

        

        if (DataRefresher.isRefreshing == false)
        {

            if (needToUpdate)
            {
                yield return StartCoroutine(LoadThumnails());
                yield return StartCoroutine(UpdateAvatars(URL));
            }
            else
            {
                yield return StartCoroutine(DownloadAllAvatars(URL));
            }

            yield return new WaitForEndOfFrame();
        }
    }

    /// <summary>
    /// Loads the thumnails from the server .
    /// </summary>
    /// <returns>The thumnails.</returns>
    //public IEnumerator LoadThumnails()
    //{
    //    List<int> categoryIDS = databaseService.GetComponent<DBScript>().GetAllCategories().Where<AvatarCategory>(c => c.Type == "Selfie").ToList<AvatarCategory>().Select(c => c.Id).ToList();

    //    if (categoryIDS.Count() > 0)
    //    {
    //        foreach (int id in categoryIDS) {
    //             string Path = Application.persistentDataPath + "/" + "Category-" + id.ToString();
    //             yield return StartCoroutine(CreateTextures(Path));
    //        }
    //    }

    //    scrollSnapAvatars.SetActive(true);
    //}

    public IEnumerator LoadThumnails()
    {
        //List<AvatarCategory> categories = databaseService.GetComponent<DBScript>().GetAllCategories().ToList<AvatarCategory>();
        //yield return new WaitForSeconds(0.2f);
        //scrollSnapAvatars.SetActive(false);

        List<int> categoryIDS = databaseService.GetComponent<DBScript>().GetAllCategories().ToList<AvatarCategory>().Where<AvatarCategory>(c=>c.Type=="Selfie").Select(c => c.Id).ToList();

        foreach (int id in categoryIDS) {
            string categoryPath = Application.persistentDataPath + "/" + "Category-" + id;
            yield return StartCoroutine(CreateTextures(categoryPath));
        }

        scrollSnapAvatars.SetActive(true);

        //if (scrollSnapAvatars.GetComponent<ScrollSnapLens>().CurrentPage() != 0)
        //{
            //scrollSnapAvatars.GetComponent<ScrollSnapLens>().update_lens();
        //}
    }

    public IEnumerator LoadThumnailsAfterUpdate()
    {
        int childrenCount = avatarsPanelContainer.transform.childCount;
        for (int i = 1; i < childrenCount; i++)
        {
            Destroy(avatarsPanelContainer.transform.GetChild(i).gameObject);
        }

        List<int> categoryIDS = databaseService.GetComponent<DBScript>().GetAllCategories().ToList<AvatarCategory>().Where<AvatarCategory>(c => c.Type == "Selfie").Select(c => c.Id).ToList();

        foreach (int id in categoryIDS)
        {
            string categoryPath = Application.persistentDataPath + "/" + "Category-" + id;
            yield return StartCoroutine(CreateTextures(categoryPath));
        }

        //if (scrollSnapAvatars.GetComponent<ScrollSnapLens>().CurrentPage() != 0)
        //{

        HideImages(avatarsPanelContainer.gameObject);

        yield return StartCoroutine(scrollSnapAvatars.GetComponent<ScrollSnapLens>().update_lens());

        yield return new WaitForSeconds(0.25f);

        ShowImages(avatarsPanelContainer.gameObject);

        //}
    }


    public int GetCategoryId(string name)
    {
        name = name.Replace("Category-", "");
        name = name.Replace(".png", "");

        return int.Parse(name);
    }
    /// <summary>
    /// Downloads all categories.
    /// </summary>
    /// <returns>The all categories.</returns>
    /// <param name="URL">URL.</param>
    public IEnumerator DownloadAllAvatars(string URL)
    {
        DirectoryInfo di = new DirectoryInfo(Application.persistentDataPath);
        Debug.Log(" Starting Downloading  Categories ...");

        using (UnityWebRequest www = UnityWebRequest.Get(URL))
        {
            www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(" Download Categoris Error : " + www.error);
            }
            else
            {
                // Load Response 
                List<JsonCategory> allCategories = JsonConvert.DeserializeObject<List<JsonCategory>>(www.downloadHandler.text);

                Debug.Log("Downloaded Categories are : " + allCategories.Count());

                foreach (JsonCategory category in allCategories)
                {
                    // Create Folder foreach category
                    string catDirectory = "Category-" + category.id;
                    CreateDirectory(catDirectory);

                    // Save Categories in Database 
                    int cat_id = category.id;

                    // Inser Category in Database
                    databaseService.GetComponent<DBScript>().InsertCategory(cat_id, category.tag, category.type);

                    // Save Category Thumbnail
                    string categoryThumbname = "Category-" + category.id + ".png";
                    SaveCategoryThumbnail(categoryThumbname, category.tagPhotoContent);

                    // Get Avatars Thumbnails Related to the Category
                    foreach (JsonAvatar avatar in category.avatars)
                    {
                        yield return StartCoroutine(SaveAvatarWithAllInfo(avatar, category.id));
                        yield return StartCoroutine(SaveAvatarThumbnail(category.id, avatar));
                    }
                }
            }
        }
     
        yield return StartCoroutine(LoadThumnails());

        yield return null;
    }

    /// <summary>
    /// Gets Cleaned filename.
    /// </summary>
    /// <returns>The clean file name.</returns>
    /// <param name="originalFileName">Original file name.</param>
    public static string GetCleanFileName(string originalFileName)
    {
        string fileToLoad = originalFileName.Replace('\\', '/');
        fileToLoad = string.Format("file://{0}", fileToLoad);
        return fileToLoad;
    }

    /// <summary>
    /// Gets the name of the file from its Path.
    /// </summary>
    /// <returns>The file name.</returns>
    /// <param name="fname">Fname.</param>
    public string GetFileName(string fname)
    {
        return fname.Substring(fname.LastIndexOf('/') + 1).Replace(".png", "");
    }

    /// <summary>
    /// Create path of image to sprite and instantiate the "ButtonPrefab" For generate cell of image inside Avatars slider
    /// </summary>
    /// <returns>The texture.</returns>
    /// <param name="pathOfFile">Path of file.</param>
    public IEnumerator CreateTextures(String pathOfFile)
    {
        String fileToLoad = GetCleanFileName(pathOfFile);
        string[] nameParts = GetFileName(pathOfFile).Split('-');
        int categoryID = int.Parse(nameParts[1]);

        string categoryPath = Application.persistentDataPath + "/Category-" + categoryID;

        DirectoryInfo di = new DirectoryInfo(categoryPath);
        FileInfo[] avatarsThumnails = di.GetFiles("*.png");
        int i = 1;

        foreach (var file in avatarsThumnails)
        {
            yield return StartCoroutine(CreateAvatarPrefabButton( i , file.FullName));
            i++;
        }

        //yield return new WaitForSeconds(2f);

        scrollSnapAvatars.SetActive(true);

        yield return null;
    }

    /// <summary>
    /// Create path of image to sprite and instantiate the "ButtonPrefab" For generate cell of image inside Avatars slider
    /// </summary>
    /// <returns>The texture.</returns>
    /// <param name="pathOfFile">Path of file.</param>
    public IEnumerator CreateAvatarPrefabButton(int id, String pathOfFile)
    {
        String fileToLoad = GetCleanFileName(pathOfFile);

        WWW www = new WWW(fileToLoad);
        yield return www;

        ////  Instantiate the template of image for add texture in collectionView for gallery
        GameObject avatarObject = (GameObject)Instantiate(avatarButtonPrefab);
        avatarObject.transform.SetParent(avatarsPanelContainer);
        avatarObject.GetComponent<RectTransform>().anchoredPosition = new Vector3(0,44f,0);

        // Raw Image is the First Child
        avatarObject.transform.GetChild(0).GetChild(0).GetComponent<RawImage>().texture = www.texture;

        //// Button 
        avatarObject.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(() => scrollSnapAvatars.GetComponent<ScrollSnapLens>().LerpToPage(id));

        avatarObject.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(() => onButtonClick(id.ToString(), GetFileName(pathOfFile)));
    }

    //public void onAvatarButtonClick(string name )
    //{
    //    Debug.Log("Avatar Path : " + name);
    //}

    /// <summary>
    /// Saves the avatar thumbnail.
    /// </summary>
    /// <returns>The avatar thumbnail.</returns>
    /// <param name="cat_id">Cat identifier.</param>
    /// <param name="avatar">Avatar.</param>
    public IEnumerator SaveAvatarThumbnail(int cat_id, JsonAvatar avatar)
    {
        string filePath = Application.persistentDataPath + "/Category-" + cat_id + "/" + avatar.avatarId + ".png";
        System.IO.File.WriteAllBytes(filePath, System.Convert.FromBase64String(avatar.thumbnailContent));
        yield return null;
    }

    /// <summary>
    /// Save Avatar With All Info
    /// </summary>
    /// <param name="avatar"></param>
    /// <param name="id"></param>
    public IEnumerator SaveAvatarWithAllInfo(JsonAvatar avatar, int id)
    {
        // Map Avatar To Database Object
        AvatarInfo db_avatarInfo = new AvatarInfo();
        db_avatarInfo.category = id;
        db_avatarInfo.avatarId = avatar.avatarId;
        db_avatarInfo.fileName = avatar.fileName;
        db_avatarInfo.avatarName = avatar.avatarName;
        db_avatarInfo.fileExtension = avatar.fileExtension;
        db_avatarInfo.complex = avatar.complex;

        db_avatarInfo.mode = avatar.mode;

        // Insert it To Database
        databaseService.GetComponent<DBScript>().InsertAvatarInfo(db_avatarInfo);

        if (!string.IsNullOrEmpty(avatar.bubbleLink) && !string.IsNullOrWhiteSpace(avatar.bubbleLink))
        {
            // Insert it To Database
            databaseService.GetComponent<DBScript>().InsertBubbleInfo(avatar.avatarId, avatar.bubbleLink);
            SaveBubbleThumbnail(avatar);
        }

        // Insert FBX File Details to DB
        foreach (JsonFBX fbx in avatar.fbx)
        {
            DB_FBX db_fbx = new DB_FBX();
            db_fbx.Id = fbx.fbxId;
            db_fbx.AvatarID = avatar.avatarId;
            db_fbx.IsMain = fbx.isMain;
            db_fbx.ColliderShape = fbx.colliderShape;
            db_fbx.CutOutMaterial = fbx.cutOutMaterial;

            //Debug.Log(" Colider Shape : "+fbx.colliderShape);

            db_fbx.Behaviour = fbx.behaviour;
            db_fbx.RunMultipleAnimation = fbx.runMultipleAnimation;

            // Insert FBX in Database
            databaseService.GetComponent<DBScript>().InsertFBXDetails(db_fbx);

            foreach (JsonAnimation animInfo in fbx.animation)
            {
                AnimationInfo db_animation = new AnimationInfo();
                db_animation.Id = animInfo.id;
                db_animation.AvatarId = animInfo.avatarId;
                db_animation.FbxId = fbx.fbxId;
                db_animation.Order = animInfo.order;
                db_animation.Repeat = animInfo.repeat;
                db_animation.RepeatCount = animInfo.repeatCount;

                //Debug.Log("Insert Music List : " + animInfo.music_list);
                db_animation.Path = animInfo.path;
                db_animation.FileName = animInfo.fileName;
                db_animation.Delay = animInfo.delay;
                db_animation.AnimationId = animInfo.animationId;
                db_animation.AnimationName = animInfo.animationName;
                db_animation.AvatarName = animInfo.avatarName;

                //Insert Multiple Music information in DB
                if (animInfo.music_list.Count > 0)
                {
                    foreach (JsonMusic jsonMusic in animInfo.music_list)
                    {
                        MusicDB musicDB = new MusicDB
                        {
                            MusicId = jsonMusic.musicId,
                            animationId = animInfo.id,
                            order = jsonMusic.order,
                            delay = jsonMusic.delay
                        };
                        databaseService.GetComponent<DBScript>().InsertMusic(musicDB);
                    }
                }

                // Insert Animation to DataBase
                databaseService.GetComponent<DBScript>().InsertAnimationInfo(db_animation);
            }
        }
        yield return new WaitForEndOfFrame();
        //yield return StartCoroutine(SaveAvatarThumbnail(id, avatar));
    }

    public void SaveBubbleThumbnail(JsonAvatar avatar) {
        string filePath = Application.persistentDataPath + "/bubble/" + avatar.avatarId + ".png";
        System.IO.File.WriteAllBytes(filePath, System.Convert.FromBase64String(avatar.bubbleIcon));
    }

    public void DeleteBubbleThumbnail(JsonAvatar avatar) {
        string filePath = Application.persistentDataPath + "/bubble/" + avatar.avatarId + ".png";
        if (File.Exists(filePath)) {
            System.IO.File.Delete(filePath);
        }
    }

    /// <summary>
    /// Saves the category thumbnail.
    /// </summary>
    /// <param name="name">Name.</param>
    /// <param name="content">Content.</param>
    public void SaveCategoryThumbnail(string name, string content)
    {
        string categoryThumbnailPath = Application.persistentDataPath + "/" + name;
        // Write Category Image in the Category Folder
        try
        {
            System.IO.File.WriteAllBytes(categoryThumbnailPath, System.Convert.FromBase64String(content));
        }
        catch (Exception ex)
        {
            Debug.Log(" Cann't write category image : " + ex.Message);
        }
    }

    /// <summary>
    /// Creates the directory of the Category.
    /// </summary>
    /// <param name="dirName">Dir name.</param>
    public void CreateDirectory(string dirName)
    {
        DirectoryInfo dirInfo = new DirectoryInfo(Application.persistentDataPath + "/" + dirName);
        if (!dirInfo.Exists)
        {
            dirInfo.Create();
            Debug.Log(" Directory Created : " + dirName);
        }
        else
        {
            Debug.Log(" Directory Existed : " + dirName);
        }
    }

    /// <summary>
    /// Updates the categories.
    /// </summary>
    /// <returns>The categories.</returns>
    /// <param name="URL">URL.</param>
    public IEnumerator UpdateAvatars(string URL)
    {
        DirectoryInfo di = new DirectoryInfo(Application.persistentDataPath);
        Debug.Log(" Starting Updating  Categories ...");

        using (UnityWebRequest www = UnityWebRequest.Get(URL))
        {
            www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(" Download Categoris Error : " + www.error);
                yield return null;
            }

            else
            {
                // Load Response 
                List<JsonCategory> allCategories = JsonConvert.DeserializeObject<List<JsonCategory>>(www.downloadHandler.text);

                List<int> allWebCategories = allCategories.Select(c => c.id).ToList();

                List<int> categoryIDS = databaseService.GetComponent<DBScript>().GetAllCategories().Where<AvatarCategory>(c => c.Type == "Selfie").ToList<AvatarCategory>().Select(c => c.Id).ToList();

                Debug.Log("Web Categories : " + allWebCategories.Count);

                Debug.Log("Category Ids From Database : " + categoryIDS.Count());

                List<int> categoriesToBeDeleted = new List<int>();

                Debug.Log("Categories Count : " + allCategories.Count());

                foreach (JsonCategory category in allCategories)
                {
                    // if Category Not Existed In DataBase
                    Debug.Log("Category : " + category.tag + " : " + category.id);
                    int id = category.id;

                    if (!categoryIDS.Contains(id))
                    {
                        Debug.Log("Category : " + category.id + " Not Existed In Database");

                        // Create Folder foreach category
                        string catDirectory = "Category-" + category.id;
                        CreateDirectory(catDirectory);

                        // Save Categories in Database 
                        int cat_id = category.id;

                        // Inser Category in Database
                        databaseService.GetComponent<DBScript>().InsertCategory(cat_id, category.tag, category.type);

                        // Save Category Thumbnail
                        string categoryThumbname = "Category-" + category.id + ".png";
                        SaveCategoryThumbnail(categoryThumbname, category.tagPhotoContent);

                        // Get Avatars Thumbnails Related to the Category
                        foreach (JsonAvatar avatar in category.avatars) {
                            yield return StartCoroutine(SaveAvatarWithAllInfo(avatar, category.id));
                            yield return StartCoroutine(SaveAvatarThumbnail(category.id, avatar));
                        }
                    }

                    // If Category Existed
                    else
                    {
                        // Save Categories in Database 
                        int cat_id = category.id;
                        // Inser Category in Database
                        databaseService.GetComponent<DBScript>().UpdateCategory(cat_id, category.tag, category.type);

                        List<JsonAvatar> avatars = category.avatars;

                        List<int> allAvatarsIDS = category.avatars.Select(c => c.avatarId).Distinct().ToList();

                        Debug.Log("Category Avatars Count   : " + allAvatarsIDS.Count);

                        List<AvatarInfo> db_category_avatars = databaseService.GetComponent<DBScript>().GetAvatarsByCategoryID(category.id).ToList<AvatarInfo>();

                        Debug.Log("Database Category Avatars Count   : " + db_category_avatars.Count);

                        List<int> db_category_avatarsIDS = db_category_avatars.Select(c => c.avatarId).Distinct().ToList();
                        List<int> avatarsToBeDeleted = new List<int>();
                        List<JsonAvatar> avatarsToBeAdded = new List<JsonAvatar>();

                        foreach (JsonAvatar ja in avatars) {
                            if (!db_category_avatarsIDS.Contains(ja.avatarId)) {
                                avatarsToBeAdded.Add(ja);
                            }
                        }

                        Debug.Log("Avatars to be added : " + avatarsToBeAdded.Count);

                        foreach (JsonAvatar ja in avatarsToBeAdded) {
                            yield return StartCoroutine(SaveAvatarWithAllInfo(ja, category.id));
                            yield return StartCoroutine(SaveAvatarThumbnail(category.id, ja));
                        }

                        // Update AvatarsInfo
                        Debug.Log("Avatars in category : " + category.avatars.Count);

                        foreach (JsonAvatar ja in category.avatars) {
                            UpdateAvatarWithAllInfo(ja, category.id);
                            if (!ThumbnailExist(category.id, ja)) {
                                yield return StartCoroutine(SaveAvatarThumbnail(category.id, ja));
                            }
                        }

                        if (db_category_avatars.Count > 0)
                        {
                            foreach (AvatarInfo db_avatar in db_category_avatars)
                            {
                                if (!allAvatarsIDS.Contains(db_avatar.avatarId)) {
                                    avatarsToBeDeleted.Add(db_avatar.avatarId);
                                }
                            }

                            Debug.Log("Avatars to be deleted : " + avatarsToBeDeleted.Count);

                            foreach (int avatar_id in avatarsToBeDeleted)
                            {
                                // DELETE FBX FILE AND THUMBNAIL
                                AvatarInfo _avatar = databaseService.GetComponent<DBScript>().GetAvatarInfoById(avatar_id);
                                // DELETE FROM DATABASE WITH FBX AND ANIMATION
                                if (_avatar != null)
                                {
                                    yield return StartCoroutine(DeleteAvatarInfoFiles(_avatar));
                                    yield return StartCoroutine(DeleteAvatar(_avatar));
                                }
                            }
                        }
                        else
                        {
                            Debug.Log("files to be deleted : " + cat_id);
                            DirectoryInfo directory = new DirectoryInfo(Application.persistentDataPath + "/Category-" + cat_id);

                            if (directory.Exists) {
                                foreach (FileInfo file in directory.GetFiles()) {
                                    Debug.Log("File :" + file.Name);
                                    file.Delete();
                                }
                            }
                        }
                    }
                    Debug.Log("End Category : " + category.tag);
                }

                foreach (int catID in categoryIDS)
                {
                    if (!allWebCategories.Contains(catID))
                    {
                        categoriesToBeDeleted.Add(catID);
                    }
                }

                // Delete Categories
                foreach (int catId in categoriesToBeDeleted) {
                    Debug.Log("Start Deleting");
                    yield return StartCoroutine(DeleteCategory(catId));
                }

            }
            Debug.Log("Update Finished ...");

            if (isDownloading)
            {
                yield return new WaitUntil(()=> isDownloading==false);
            }

            //avatarsPanelContainer.gameObject.SetActive(false);
           
            yield return StartCoroutine(LoadThumnailsAfterUpdate());

            //avatarsPanelContainer.gameObject.SetActive(true);


        }
        yield return new WaitForEndOfFrame();
    }

    public void HideImages(GameObject controlPanel)
    {
        RawImage[] rawImages = controlPanel.GetComponentsInChildren<RawImage>();
        Image[] images = controlPanel.GetComponentsInChildren<Image>();

        for (int i = 0; i < rawImages.Length; i++) {
            rawImages[i].enabled = false;
        }

        for (int i = 0; i < images.Length; i++) {
            images[i].enabled = false;
        }
    }

    public void ShowImages(GameObject controlPanel)
    {
        RawImage[] rawImages = controlPanel.GetComponentsInChildren<RawImage>();
        Image[] images = controlPanel.GetComponentsInChildren<Image>();

        for (int i = 0; i < rawImages.Length; i++)
        {
            rawImages[i].enabled = true;
        }
        for (int i = 0; i < images.Length; i++)
        {
            images[i].enabled = true;
        }

    }

    /// <summary>
    /// Deletes the category.
    /// </summary>
    /// <param name="catId">Cat identifier.</param>
    public IEnumerator DeleteCategory(int catId)
    {
        if (File.Exists((Application.persistentDataPath + "/Category-" + catId + ".png")))
        {
            File.Delete(Application.persistentDataPath + "/Category-" + catId + ".png");
        }
        
        DirectoryInfo di = new DirectoryInfo(Application.persistentDataPath + "/Category-" + catId);

        if (di.Exists)
        {
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }

            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                dir.Delete(true);
            }
            if (di.Exists)
            {
                Directory.Delete(Application.persistentDataPath + "/Category-" + catId);
            }
        }

        // Delete Avatars Information From Database that are related to Categories
        List<AvatarInfo> avatars = databaseService.GetComponent<DBScript>().GetAvatarsByCategoryID(catId).ToList<AvatarInfo>();
        for (int i = 0; i < avatars.Count(); i++)
        {
            yield return StartCoroutine(DeleteAvatarInfoFiles(avatars[i]));
            yield return StartCoroutine(DeleteAvatar(avatars[i]));
        }

        // Delete From Database
        AvatarCategory CatDb = databaseService.GetComponent<DBScript>().GetCategoryById(catId);

        if (CatDb != null)
        {
            databaseService.GetComponent<DBScript>().DeleteCategoryByID(CatDb);
        }
        yield return new WaitForEndOfFrame();
    }

    /// <summary>
    /// Deletes the avatar info files.
    /// </summary>
    /// <param name="avatar">Avatar.</param>
    public IEnumerator DeleteAvatarInfoFiles(AvatarInfo avatar)
    {
        DirectoryInfo directory = new DirectoryInfo(Application.persistentDataPath + "/Category-" + avatar.category);
        if (directory.Exists)
        {
            FileInfo[] avatarFiles = directory.GetFiles(avatar.avatarId + "*");
            foreach (FileInfo file in avatarFiles)
            {
                File.Delete(file.FullName);
            }
        }
        yield return new WaitForEndOfFrame();
    }

    public IEnumerator DeleteAvatar(AvatarInfo _avatar)
    {
        Debug.Log("Delete Avatar : " + _avatar.avatarName);
        databaseService.GetComponent<DBScript>().DeleteAvatarInfo(_avatar);
        // DELETE FROM DATABASE WITH FBX AND ANIMATION
        #region Delete Avatar Related Information
        List<DB_FBX> relatedFbx = databaseService.GetComponent<DBScript>().GetFbxByAvatarId(_avatar.avatarId).ToList<DB_FBX>();
        List<AnimationInfo> relatedAnimations = new List<AnimationInfo>();
        List<MusicDB> musicItems = new List<MusicDB>();

        if (relatedFbx.Count() > 0)
        {

            for (int i = 0; i < relatedFbx.Count(); i++)
            {
                relatedAnimations.AddRange(databaseService.GetComponent<DBScript>().GetAnimationByFbxID(relatedFbx[i].Id));
            }

            // Get related Music Items
            for (int mi = 0; mi < relatedAnimations.Count; mi++)
            {
                musicItems.AddRange(databaseService.GetComponent<DBScript>().GetMusicIdsByAnimationID(relatedAnimations[mi].Id));
            }

            // Delete Related Music Items
            for (int dm = 0; dm < musicItems.Count(); dm++)
            {
                databaseService.GetComponent<DBScript>().DeleteMusicItem(musicItems[dm]);
            }

            // Delete Related Animation Info
            if (relatedAnimations.Count() > 0)
            {
                for (int i = 0; i < relatedAnimations.Count(); i++)
                {
                    databaseService.GetComponent<DBScript>().DeleteAnimationInfo(relatedAnimations[i]);
                }
            }

            // Delete Related FBX files
            for (int j = 0; j < relatedFbx.Count(); j++)
            {
                databaseService.GetComponent<DBScript>().DeleteFBX(relatedFbx[j]);
            }
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForEndOfFrame();
        #endregion

    }

    /// <summary>
    /// Check if the Thumbnail of the Avatar Exists
    /// </summary>
    /// <returns><c>true</c>, if not exist was thumbnailed, <c>false</c> otherwise.</returns>
    /// <param name="cat_id">Cat identifier.</param>
    /// <param name="avatar">Avatar.</param>
    public bool ThumbnailExist(int cat_id, JsonAvatar avatar)
    {
        string filePath = Application.persistentDataPath + "/Category-" + cat_id + "/" + avatar.avatarId + ".png";
        return File.Exists(filePath);
    }

    /// <summary>
    /// Update Avatar With All Info
    /// </summary>
    /// <param name="avatar"></param>
    /// <param name="id"></param>
    public void UpdateAvatarWithAllInfo(JsonAvatar avatar, int id)
        {
            try {
                // Map Avatar To Database Object
                AvatarInfo db_avatarInfo = new AvatarInfo();
                db_avatarInfo.category = id;
                db_avatarInfo.avatarId = avatar.avatarId;
                db_avatarInfo.fileName = avatar.fileName;
                db_avatarInfo.avatarName = avatar.avatarName;
                db_avatarInfo.fileExtension = avatar.fileExtension;
                db_avatarInfo.complex = avatar.complex;
                db_avatarInfo.mode = avatar.mode;
                // Insert it To Database
                databaseService.GetComponent<DBScript>().InsertAvatarInfo(db_avatarInfo);

                if (!string.IsNullOrEmpty(avatar.bubbleLink) && !string.IsNullOrWhiteSpace(avatar.bubbleLink))
                {
                    // Insert it To Database
                    if (avatar.bubbleLink.Length > 0)
                    {
                        databaseService.GetComponent<DBScript>().InsertBubbleInfo(avatar.avatarId, avatar.bubbleLink);
                        SaveBubbleThumbnail(avatar);
                    }
                }
                else
                {

                    //Debug.Log("Bubble Link Null : "+ avatar.avatarId);
                    databaseService.GetComponent<DBScript>().DeleteBubbleInfo(avatar.avatarId);
                    DeleteBubbleThumbnail(avatar);
                }

                // Insert FBX File Details to DB
                foreach (JsonFBX fbx in avatar.fbx)
                {
                    DB_FBX db_fbx = new DB_FBX();
                    db_fbx.Id = fbx.fbxId;
                    db_fbx.AvatarID = avatar.avatarId;
                    db_fbx.IsMain = fbx.isMain;
                    db_fbx.ColliderShape = fbx.colliderShape;
                    db_fbx.CutOutMaterial = fbx.cutOutMaterial;

                    db_fbx.Behaviour = fbx.behaviour;
                    db_fbx.RunMultipleAnimation = fbx.runMultipleAnimation;

                    // Insert FBX in Database
                    databaseService.GetComponent<DBScript>().InsertFBXDetails(db_fbx);

                    foreach (JsonAnimation animInfo in fbx.animation)
                    {
                        AnimationInfo db_animation = new AnimationInfo();
                        db_animation.Id = animInfo.id;
                        db_animation.AvatarId = animInfo.avatarId;
                        db_animation.FbxId = fbx.fbxId;
                        db_animation.Order = animInfo.order;
                        db_animation.Repeat = animInfo.repeat;
                        db_animation.RepeatCount = animInfo.repeatCount;
                        db_animation.Path = animInfo.path;
                        db_animation.FileName = animInfo.fileName;
                        db_animation.Delay = animInfo.delay;
                        db_animation.AnimationId = animInfo.animationId;
                        db_animation.AnimationName = animInfo.animationName;
                        db_animation.AvatarName = animInfo.avatarName;

                        //Debug.Log("Update Music List : " + animInfo.music_list);
                        if (animInfo.music_list.Count > 0)
                        {
                            foreach (JsonMusic jsonMusic in animInfo.music_list)
                            {
                                MusicDB musicDB = new MusicDB
                                {
                                    MusicId = jsonMusic.musicId,
                                    animationId = animInfo.id,
                                    order = jsonMusic.order,
                                    delay = jsonMusic.delay
                                };
                                databaseService.GetComponent<DBScript>().UpdateMusic(musicDB);
                            }
                        }
                        // Insert Animation to DataBase
                        databaseService.GetComponent<DBScript>().InsertAnimationInfo(db_animation);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Log("DB Exception : " + ex.Message);
            }

        }


    public bool NumberOfAvatarsUnderLimit()
    {
        return (avatarsList.Count < ApplicationSettings.MAX_AVATARS);
    }

    /// <summary>
    /// Handling the Click Button of the Avatar
    /// </summary>
    /// <param name="name">Name.</param>
    public void onButtonClick(string categoryID, string name)
    {
        showInternetMeassage = true;

        if (NumberOfAvatarsUnderLimit()) {
            if (!isDownloading && !isPlacingAvatar) {
                Vector3 position = mainOb.transform.position;
                Debug.Log(string.Format("Adding Avatar : {0} At {1}", name, position));

                if (!isDownloading && !isPlacingAvatar) {
 
                    StartCoroutine(LoadAvatarAssencByName(name));
                }
            }
            else
            {
                //isPlacingAvatar = false;
                //StartCoroutine(ShowDetectSurfaceMessage());
            }
        }
    }

    public bool InternetReachable()
    {
        return (Application.internetReachability != NetworkReachability.NotReachable);
    }


    // LoadAsset From Bundle
    public GameObject LoadAvatarBundle(string filePath, AvatarInfo avatarInfo)
    {
        Debug.Log("File Path : " + filePath);

        AssetBundle asseBundle = AssetBundle.LoadFromFile(filePath);

        bundlesList.Add(asseBundle);

        asseBundle.name = avatarInfo.avatarId.ToString();

        string[] allAssetNames = asseBundle.GetAllAssetNames();

        for (int i = 0; i < allAssetNames.Length; i++)
        {
            Debug.Log("Asset : " + allAssetNames[i]);
        }

        string assetName = "assets/" + avatarInfo.avatarName.ToLower() + ".prefab";

        GameObject bundle = asseBundle.LoadAsset<GameObject>(assetName);

        GameObject avatar = Instantiate(bundle) as GameObject;

        asseBundle.Unload(false);

        return avatar;
    }

    [Obsolete]
    public IEnumerator LoadAvatarBundleByName(string avatar_name) {
        AvatarInfo avatarInfo = databaseService.GetComponent<DBScript>().GetAvatarInfoById(int.Parse(avatar_name));
        LoadAvatarBundleAssenc(avatarInfo);
        yield return null;
    }

    /// <summary>
    /// Adds the ability to move.
    /// </summary>
    /// <param name="avatar">Avatar.</param>
    /// <param name="category">Category.</param>
    private void AddAbilityToMove(GameObject avatar, string category)
    {
        // Add Ability to  be Selectable 
        Lean.Touch.LeanSelectable selectable = avatar.AddComponent<Lean.Touch.LeanSelectable>();
        selectable.SelectMe();

        // Add the Ability to Scale
        Lean.Touch.LeanScale scalable = avatar.AddComponent<Lean.Touch.LeanScale>();

        //scalable.ScaleClamp = true;
        //scalable.ScaleMin = new Vector3(0.25f, 0.25f, 0.25f);
        //scalable.ScaleMax = new Vector3(1f, 1f, 1f);

        scalable.RequiredSelectable = selectable;
        scalable.RequiredFingerCount = 2;

        Lean.Touch.LeanRotateCustomAxis rotatable = avatar.AddComponent<Lean.Touch.LeanRotateCustomAxis>();
        rotatable.Space = Space.Self;
        rotatable.Axis = new Vector3(0, -1, 0);

        // Add the Ability to Move
        Lean.Touch.LeanTranslate movable = avatar.AddComponent<Lean.Touch.LeanTranslate>();
        movable.RequiredSelectable = selectable;
        movable.RequiredFingerCount = 1;
    }

    /// <summary>
    /// Loads the avatar assenc.
    /// </summary>
    /// <param name="avatarInfo">Avatar info.</param>
    [Obsolete]
    public void LoadAvatarBundleAssenc(AvatarInfo avatarInfo)
    {
        isPlacingAvatar = true;
        AvatarCategory category = databaseService.GetComponent<DBScript>().GetCategoryById(avatarInfo.category);

        if (!avatarInfo.complex)
        {
            string filePath = Application.persistentDataPath + "/Category-" + avatarInfo.category + "/" + avatarInfo.avatarId + ".unity3d";
            List<DB_FBX> fbx = databaseService.GetComponent<DBScript>().GetFbxByAvatarId(avatarInfo.avatarId).ToList<DB_FBX>();
            List<AnimationInfo> animations = databaseService.GetComponent<DBScript>().GetAnimationByFbxID(fbx[0].Id).ToList<AnimationInfo>();

            //loadingLogo.SetActive(true);

            GameObject avatar = LoadAvatarBundle(filePath, avatarInfo);
            //avatar.AddComponent<ReverseNormals>();

            avatar.name = avatarInfo.avatarId.ToString();

            // Adding the ability to move
            AddAbilityToMove(avatar, category.Name);

            // Place Avatar in the scene
            PlaceAvatar(avatarInfo.avatarId, avatar, 1f);
            isPlacingAvatar = false;

            // Hide Loading Logo
            //loadingLogo.SetActive(false);

            // The Avatar is Placed in the Scene

            if (animations.Count > 0)
            {
                Animation anim = avatar.GetComponent<Animation>();

                if (!anim) {
                    Debug.Log(" Animation Existed in Children ... \n");

                    if (avatar.transform.childCount > 0) {
                        anim = avatar.transform.GetChild(0).GetComponent<Animation>();
                    }
                }

                if (anim != null)
                {
                    if (AvatarHasMusic(avatarInfo.avatarId.ToString()))
                    {
                        Debug.Log("Avatar Has Music ..");
                        StartCoroutine(LoadMusicForAvatar(avatarInfo.category.ToString(), animations[0], anim.clip.length));
                    }

                    if (animations[0].Repeat == "Once")
                    {
                        foreach (AnimationState animState in anim)
                        {
                            animationsLength.Add(animState.length);
                        }
                    }

                    if (animations[0].Repeat == "Loop")
                    {
                        avatar.AddComponent<AudioVideoSync>();
                        anim_loop_nb += 1;
                    }
                }

            }

            // add it to Avatars List

            HighlightObject(avatar);
        }

    }


    public void HighlightObject(GameObject avatar)
    {


    }

    /// <summary>
        /// Load Avatar to the Scenen Assynchronously
        /// </summary>
        /// <param name="avatar_name">Avatar name.</param>
    public IEnumerator LoadAvatarAssencByName(string avatar_name)
    {
        Debug.Log("avatar Name : "+avatar_name);

        AvatarInfo avatarInfo = databaseService.GetComponent<DBScript>().GetAvatarInfoById(int.Parse(avatar_name));
        if (avatarInfo != null)
        {
            Debug.Log("Avatar Info : " + avatarInfo.ToString());
        }
        else {
            Debug.Log("Avatar Info is Null");
        }
        bool isExisted = false;

        if (avatarInfo.complex == false)
        {
            string filePath = Application.persistentDataPath + "/Category-" + avatarInfo.category + "/" + avatar_name + ".unity3d";
            if (File.Exists(filePath))
            {
                isExisted = true;
                Debug.Log("Avatar Existed");
            }
        }
        else
        {
            string avatarDir = Application.persistentDataPath + "/Category-" + avatarInfo.category;
            DirectoryInfo dir = new DirectoryInfo(avatarDir);
            FileInfo[] avatarFiles = dir.GetFiles(avatar_name);
            foreach (FileInfo info in avatarFiles)
            {
                if (info.Extension == "unity3d")
                {
                    Debug.Log(" File Existed with Noformat ");
                    isExisted = true;
                }
            }
        }

        if (isExisted) {

            if (NumberOfAvatarsInScene() == 0)
            {
                isPlacingAvatar = true;
                //messageInternetConnectoin.SetActive(false);
                Debug.Log("Load Avatar By Name .. " + avatar_name);

                // Getting Avatar By its Name
                StartCoroutine(LoadAvatarBundleByName(avatar_name));
            }
            else {
                // Delete the avatar from the scene
                CleanAvatars();

                isPlacingAvatar = true;
                //messageInternetConnectoin.SetActive(false);
                Debug.Log("Load Avatar By Name .. " + avatar_name);

                // Getting Avatar By its Name
                yield return StartCoroutine(LoadAvatarBundleByName(avatar_name));
            }

        }
        else
        {
            if (InternetReachable() && !isDownloading)
            {
                if (NumberOfAvatarsInScene() == 0)
                {
                    yield return StartCoroutine(Download_Bundle(avatarInfo.avatarId.ToString(), avatarInfo.category.ToString()));
                }
                else {
                    // Delete the Avatar from the scene
                    CleanAvatars();
                    yield return StartCoroutine(Download_Bundle(avatarInfo.avatarId.ToString(), avatarInfo.category.ToString()));
                    // Download the Avatar
                }
            }
            else
            {
                
                if (showInternetMeassage == true)
                {
                    showInternetMeassage = false;
                    // Show Internet Connection Message
                    if (canshowmessage)
                    {
                        yield return StartCoroutine(ShowInternetConnectinMessage());
                    }
                }
            }
        }
    }

    public int NumberOfAvatarsInScene()
    {
        return avatarsList.Count;
    }

    IEnumerator ShowInternetConnectinMessage()
    {
        canshowmessage = false;

        messageInternetConnectoin.SetActive(true);

        yield return new WaitForSeconds(2);

        messageInternetConnectoin.SetActive(false);

        canshowmessage = true;
        showInternetMeassage = false;
    }

    public IEnumerator LoadTextureForDownladAsync(string fileName, Action<Texture2D> result)
    {
        WWW www = new WWW("file://" + fileName);
        yield return www;
        Texture2D loadedTexture = new Texture2D(160, 160);
        www.LoadImageIntoTexture(loadedTexture);
        result(loadedTexture);
    }
    /// <summary>
    /// Adds the loaded texture to downloading status
    /// </summary>
    /// <param name="texture">Texture.</param>
    public void AddLoadedTextureToDownloading(Texture2D texture)
    {
        Circleobject.GetComponent<RawImage>().texture = texture;
    }

    /// <summary>
    /// Downloads the avatar from the Server By ID
    /// </summary>
    /// <returns>The avatar.</returns>
    /// <param name="avatarID">Avatar identifier.</param>
    public IEnumerator Download_Bundle(string avatarID, string categoryID)
    {
        // Show the Download progress
        yield return StartCoroutine(LoadTextureForDownladAsync(Application.persistentDataPath + "/Category-" + categoryID + "/" + avatarID + ".png", AddLoadedTextureToDownloading));
        AvatarCategory category = databaseService.GetComponent<DBScript>().GetCategoryById(int.Parse(categoryID));
        Debug.Log("Download With Category " + category.Name);
        downloadStatusPanel.SetActive(true);

        // check if there is Internet Connection if available
        if (PlayerPrefs.GetString("auth_token").Length > 1)
        {
            isDownloading = true;
            // Check If Avatar ID Has Music

            List<DB_FBX> AvatarFBXs = databaseService.GetComponent<DBScript>().GetFbxByAvatarId(int.Parse(avatarID)).ToList<DB_FBX>();

            List<AnimationInfo> animations = new List<AnimationInfo>();

            foreach (DB_FBX fbx in AvatarFBXs)
            {
                IEnumerable<AnimationInfo> infox = databaseService.GetComponent<DBScript>().GetAnimationByFbxID(fbx.Id);
                animations.AddRange(infox);
            }

            // Getting the Music IDS

            #region GET LIST OF MUSIC

            List<MusicDB> musicItems = new List<MusicDB>();

            if (animations.Count > 0) {
                foreach (AnimationInfo info in animations) {
                    musicItems.AddRange(databaseService.GetComponent<DBScript>().GetMusicIdsByAnimationID(info.Id).ToList<MusicDB>());
                }
            }

            List<int> musicIds = musicItems.Select(item => item.MusicId).Distinct<int>().ToList();

            if (musicIds.Count == 0) {
                Debug.Log("Download Avatar Without Music : " + avatarID + " category : " + categoryID);
                StartCoroutine(DownloadBundleWithoutMusic(categoryID, avatarID, AvatarFBXs));
            }
            else {
                if (musicIds.Count > 0){
                    Debug.Log("Start Download With Music ");
                    StartCoroutine(DownloadBundleWithMusic(categoryID, avatarID, musicIds, animations[0].AnimationName, AvatarFBXs));
                }
            }

            #endregion

        }
    }

    /// <summary>
    /// Places the avatar in the Unity World 
    /// </summary>
    /// <param name="avatar">Avatar.</param>
    /// <param name="value">Value.</param>
    public void PlaceAvatar(int avatarID, GameObject avatar, float value)
    {
        selectedAvatar = avatarID;
        avatar.transform.localScale = Vector3.one * value;

        avatar.transform.SetParent(mainOb.GetComponent<ExpressionManager>().GetAnchor());

        // Positioning Avatars
        avatar.transform.position = new Vector3(
            mainOb.GetComponent<ExpressionManager>().GetAnchor().position.x,
            mainOb.GetComponent<ExpressionManager>().GetAnchor().position.y,
            mainOb.GetComponent<ExpressionManager>().GetAnchor().position.z);

        Vector3 cam_position = Camera.main.transform.position;
        cam_position.y = avatar.transform.position.y;

        // Roate the Avatar to the Camera
        avatar.transform.LookAt(cam_position);

#pragma warning disable CS0618 // Type or member is obsolete
        avatar.transform.Rotate(Vector3.up, 0f);
#pragma warning restore CS0618 // Type or member is obsolete

        avatarsList.Add(avatar);
    
    }

    /// <summary>
    /// Avatars the has music.
    /// </summary>
    /// <returns><c>true</c>, if has music was avatared, <c>false</c> otherwise.</returns>
    /// <param name="name">Name.</param>
    public bool AvatarHasMusic(string name)
    {
        Debug.Log("Check if Avatar Has Music");

        int avatarId = int.Parse(name.Split('-')[0].Split('D')[0]);

        List<DB_FBX> fbxFiles = databaseService.GetComponent<DBScript>().GetFbxByAvatarId(avatarId).ToList<DB_FBX>();

        // TODO 
        foreach (DB_FBX fbx in fbxFiles)
        {
            List<AnimationInfo> animations = databaseService.GetComponent<DBScript>().GetAnimationByFbxID(fbx.Id).ToList<AnimationInfo>();

            List<MusicDB> musicItems = new List<MusicDB>();

            if (animations.Count > 0)
            {
                foreach (AnimationInfo animInfo in animations)
                {
                    // GETMUSIC
                    musicItems.AddRange(databaseService.GetComponent<DBScript>().GetMusicIdsByAnimationID(animInfo.Id));
                }
                if (musicItems.Count > 0)
                {
                    return true;
                }
            }
        }
        return false;
    }

    /// <summary>
    /// Resets the download Indicators
    /// </summary>
    public void ResetDownload()
    {
        countdown.fillAmount = 0;
        button.fillAmount = 1;
        downloadingMessageStatus.GetComponent<Text>().text = "";
    }

    public IEnumerator AbortDownloads(List<UnityWebRequest> requests)
    {
        try
        {
            Debug.Log("Number of Requests to Abort is : " + requests.Count);
            for (int i = 0; i < requests.Count; i++)
            {
                if (requests[i] != null)
                {
                    requests[i].Abort();
                }
            }
            //Resources.UnloadUnusedAssets();
        }
        catch (Exception ex)
        {
            Debug.Log("Error in Aborting Download :" + ex.Message);
        }
        yield return new WaitForEndOfFrame();
    }

    private float ProgreesOfDownloads(List<UnityWebRequest> requests)
    {
        float progress = 0.0f;
        for (int i = 0; i < requests.Count; i++)
        {
            progress += requests[i].downloadProgress;
        }
        return progress;
    }

    private IEnumerator CheckDownloadProcessAscenc(List<UnityWebRequest> requests, string avatar_name)
    {
        if (requests is null)
        {
            throw new ArgumentNullException(nameof(requests));
        }

        downloadStatusPanel.SetActive(true);
        // Load Photo Form system
        int Count = requests.Count;

        while (ProgreesOfDownloads(requests) < Count)
        {
            float all_progress = ProgreesOfDownloads(requests) * 100;
            float process = (float)Math.Round(all_progress / Count, 0);

            if ((process) > 0)
            {
                downloadingMessageStatus.GetComponent<Text>().text = process.ToString() + "%";
            }

            float res = process / 100;
            countdown.fillAmount = res;
            button.fillAmount = 1f - res;

            if (cancelDownload)
            {
                avatarsToPlace.Remove(avatar_name);
                Debug.Log("Canceled Download");
                for (int i = 0; i < requests.Count; i++)
                {
                    requests[i].Abort();
                }
                AbortDownloads(requests);
                ResetDownload();
                isPlacingAvatar = false;
                downloadStatusPanel.SetActive(false);

                yield return new WaitForSeconds(0.2f);
            }

            yield return new WaitForSeconds(0.2f);
        }

        if (cancelDownload)
        {
            AbortDownloads(requests);
            avatarsToPlace.Remove(avatar_name);
            Debug.Log("Canceled Download");
            for (int i = 0; i < requests.Count; i++)
            {
                requests[i].Abort();
            }
            ResetDownload();
            isPlacingAvatar = false;
            yield return new WaitForSeconds(0.1f);
            downloadStatusPanel.SetActive(false);
        }


        if (downloadStatusPanel.activeInHierarchy)
        {
            downloadStatusPanel.SetActive(false);
        }

        ResetDownload();

        yield return null;
    }

    public void DisposeRequests(List<UnityWebRequest> requests)
    {
        if (requests != null)
        {
            for (int i = 0; i < requests.Count; i++)
            {
                if (requests[i] != null)
                {
                    requests[i].Dispose();
                }
            }
        }
        requests.Clear();
    }

    /// <summary>
    /// Cancels the Download
    /// </summary>
    public void CancelDownload()
    {
        cancelDownload = true;
        AbortDownloads(requests);

        //isPlacingAvatar = false;
        //isDownloading = false;

        //StopCoroutine("CheckDownloadProcessAscenc");
        //StopCoroutine("DownloadAvatar");

        Debug.Log(" Downloading Canceled ");
    }

    /// <summary>
    /// Downloads the avatar Bundle without music.
    /// </summary>
    /// <returns>The avatar without music.</returns>
    /// <param name="categoryID">Category identifier.</param>    
    /// <param name="avatarID">Avatar identifier.</param>
    public IEnumerator DownloadBundleWithoutMusic(string categoryID, string avatarID, List<DB_FBX> AvatarFBXs)
    {
        isDownloading = true;
        DB_FBX[] filesArray = AvatarFBXs.ToArray<DB_FBX>();
        int arrayCount = filesArray.Length;

        requests = new List<UnityWebRequest>();

        Debug.Log("Requests Count : " + arrayCount);

        for (int i = 0; i < arrayCount; i++)
        {
            UnityWebRequest request = UnityWebRequest.Get(API_Settings.STAGING_SERVICE_URL + API_Settings.DOWNLOAD_FBX_BY_ID + filesArray[i].Id);
            request.timeout = 3600;
            request.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
            requests.Add(request);
        }

        for (int i = 0; i < requests.Count; i++)
        {
            requests[i].SendWebRequest();
        }

        yield return StartCoroutine(CheckDownloadProcessAscenc(requests, avatarID));

        Debug.Log("User Cancelled the Download : " + cancelDownload);

        if (!HasNetworkError(requests))
        {
            if (requests.Count == 1)
            {
                string filePath = Application.persistentDataPath + "/Category-" + categoryID + "/" + avatarID + ".unity3d";
                System.IO.File.WriteAllBytes(filePath, requests[0].downloadHandler.data);
            }
            if (requests.Count > 1)
            {
                for (int i = 0; i < requests.Count; i++) {
                    string filePath = Application.persistentDataPath + "/Category-" + categoryID + "/" + avatarID + "-" + AvatarFBXs[i].Id + ".unity3d";
                    System.IO.File.WriteAllBytes(filePath, requests[i].downloadHandler.data);
                }
            }

            AvatarInfo avatarInfo = databaseService.GetComponent<DBScript>().GetAvatarInfoById(int.Parse(avatarID));
            LoadAvatarBundleAssenc(avatarInfo);

            isDownloading = false;
            cancelDownload = false;
            isPlacingAvatar = false;

            DisposeRequests(requests);

            yield return null;

        }
        else
        {
            Debug.Log(" Downloading Error ");

            isPlacingAvatar = false;
            cancelDownload = false;
            isDownloading = false;

            avatarsToPlace.Remove(avatarID);

            yield return null;

        }

        yield return null;

    }

    /// <summary>
    /// Downloads the avatar with music.
    /// </summary>
    /// <returns>The avatar with music.</returns>
    /// <param name="categoryID">Category identifier.</param>
    /// <param name="avatarID">Avatar identifier.</param>
    public IEnumerator DownloadBundleWithMusic(string categoryID, string avatarID, List<int> musicIds, string animationName, List<DB_FBX> AvatarFBXs)
    {
        isDownloading = true;
        DB_FBX[] filesArray = AvatarFBXs.Distinct<DB_FBX>().ToArray<DB_FBX>();

        int arrayLength = filesArray.Length + musicIds.Count;

        Debug.Log("Avatars Files Count : " + arrayLength);

        requests = new List<UnityWebRequest>(arrayLength);

        // Populate Avarar Files into the Array
        for (int i = 0; i < filesArray.Length; i++) {
            UnityWebRequest request = UnityWebRequest.Get(API_Settings.STAGING_SERVICE_URL + API_Settings.DOWNLOAD_FBX_BY_ID + filesArray[i].Id);
            request.timeout = 3600;
            request.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
            requests.Add(request);
        }

        //// Populate Music Files into the Array
        for (int j = filesArray.Length; j < arrayLength; j++)
        {
            int index = j - filesArray.Length;
            Debug.Log("Creating Index : " + index + "music Index : " + musicIds[index]);
            string musicUrl = API_Settings.STAGING_SERVICE_URL + API_Settings.DOWNLOAD_MUSIC_BY_ID + musicIds[index];
            UnityWebRequest request = UnityWebRequest.Get(musicUrl);
            request.timeout = 3600;
            request.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
            requests.Add(request);
        }

        for (int i = 0; i < requests.Count; i++)
        {
            requests[i].SendWebRequest();
        }

        yield return StartCoroutine(CheckDownloadProcessAscenc(requests, avatarID));

        Debug.Log("User Cancelled the Download : " + cancelDownload);

        if (!HasNetworkError(requests))
        {
            // Save Avatar Files 
            for (int i = 0; i < filesArray.Length; i++)
            {
                string filePath = Application.persistentDataPath + "/Category-" + categoryID + "/" + avatarID + ".unity3d";
                System.IO.File.WriteAllBytes(filePath, requests[i].downloadHandler.data);
            }

            // Save Music Files
            for (int j = filesArray.Length; j < arrayLength; j++)
            {
                int musicID = musicIds[j - filesArray.Length];
                string musicFilePath = Application.persistentDataPath + "/Category-" + categoryID + "/" + avatarID + "-" + animationName + "-" + musicID + ".mp3";
                System.IO.File.WriteAllBytes(musicFilePath, requests[j].downloadHandler.data);
            }


            AvatarInfo avatarInfo = databaseService.GetComponent<DBScript>().GetAvatarInfoById(int.Parse(avatarID));

            LoadAvatarBundleAssenc(avatarInfo);

            isPlacingAvatar = false;
            isDownloading = false;
            cancelDownload = false;

            DisposeRequests(requests);

            yield return null;
        }
        else
        {
            Debug.Log(" Downloading With Music Error ");

            isPlacingAvatar = false;
            cancelDownload = false;
            isDownloading = false;


            yield return null;
        }

        yield return null;
    }

    public bool HasNetworkError(List<UnityWebRequest> requests)
    {
        try
        {
            if (requests.Count > 0)
            {
                for (int i = 0; i < requests.Count; i++)
                {
                    if (requests[i].isNetworkError || requests[i].isHttpError)
                    {
                        Dictionary<string, string> headers = requests[i].GetResponseHeaders();
                        foreach (KeyValuePair<string, string> header in headers)
                        {
                            Console.WriteLine("Key: {0}, Value: {1}", header.Key, header.Value);
                        }
                        Debug.Log(" Downloaded Data : " + requests[i].downloadHandler.text);
                        Debug.Log(" Response Type : " + requests[i].GetResponseHeader("Content-Type"));
                        Debug.Log(" Download Avatar Error : " + requests[i].url + " , " + requests[i].error + Environment.NewLine);
                        return true;
                    }

                }
                return false;
            }
            else
            {
                Debug.Log("Exception Request Count 0");
                return true;
            }
        }
        catch (Exception ex)
        {
            Debug.Log("Exception : " + ex.Message);
            return true;
        }
    }


    /// <summary>
    /// Loads the music for avatar.
    /// </summary>
    /// <returns>The music for avatar.</returns>
    /// <param name="CategoryID">Category identifier.</param>
    /// <param name="animModel">Animation model.</param>
    public IEnumerator LoadMusicForAvatar(string CategoryID, AnimationInfo animModel, float anim_lenght)
    {
        if (animModel != null)
        {
            string filePath = Application.persistentDataPath + "/Category-" + CategoryID;
            DirectoryInfo di = new DirectoryInfo(filePath);
            FileInfo[] musicFiles = di.GetFiles(animModel.AvatarId + "-" + animModel.AnimationName + "*.mp3");
            Debug.Log("Music File Count : " + musicFiles.Length);

            if (musicFiles.Length == 1)
            {
                // GET THE MUSIC FILE NAME
                // Get the music ID From the Path
                string musicID = musicFiles[0].Name.Replace(".mp3", "").Split('-')[2];

                //// Get Music Item From the Database

                // Find The Music that is related to the Animation  

                // TODO
                MusicDB musicDB = databaseService.GetComponent<DBScript>().GetMusicItemById(animModel.Id, int.Parse(musicID));

                // Define Music Item
                Debug.Log("Music Delay : " + musicDB.delay);

                MusicItem item = new MusicItem(musicDB.MusicId, musicDB.delay);
                List<MusicItem> listMusic = new List<MusicItem>();
                listMusic.Add(item);

                // Add Music Item to Dictionary
                if (!musicDictionary.ContainsKey(animModel.AvatarId))
                {
                    musicDictionary.Add(animModel.AvatarId, listMusic);
                }

                //Debug.Log("Music ID : "+ musicID);
                if (!audiosIds.Contains(animModel.AvatarId.ToString()))
                {
                    // Get the Music Path

                    string musicPath = musicFiles[0].FullName;

                    if (File.Exists(musicPath))
                    {
                        // Play the music for avatar
                        WWW www = new WWW("file://" + musicPath);

                        yield return www;
                        if (www != null)
                        {
                            GameObject avatarMusic = Instantiate(musicTrack);

                            avatarMusic.AddComponent<AudioSource>();
                            avatarMusic.name = "music-" + animModel.AvatarId;
                            avatarMusic.GetComponent<AudioSource>().clip = www.GetAudioClip(false, true) as AudioClip;

                            if (animModel.Repeat != "Loop")
                            {
                                avatarMusic.GetComponent<AudioSource>().loop = false;
                            }
                            else
                            {
                                avatarMusic.GetComponent<AudioSource>().loop = true;
                            }
                            audiosIds.Add(animModel.AvatarId.ToString());
                            audioTracks.Add(avatarMusic);
                        }
                        else
                        {
                            Debug.Log("WWWW is null");
                            yield return null;
                        }
                    }
                    else
                    {
                        Debug.Log("This avatar is not content on Music");
                        yield return null;
                    }
                }
                else
                {
                    yield return null;
                }
            }
            else
            {
                if (musicFiles.Length > 1)
                {
                    GameObject avatarMusic = Instantiate(musicTrack);
                    avatarMusic.name = "music-" + animModel.AvatarId;

                    List<string> musicIds = new List<string>();
                    for (int i = 0; i < musicFiles.Length; i++)
                    {
                        musicIds.Add(musicFiles[i].Name.Replace(".mp3", "").Split('-')[2]);
                    }

                    List<MusicItem> musicItems = new List<MusicItem>();
                    for (int i = 0; i < musicIds.Count; i++)
                    {
                        //// Get Music Item From the Database
                        MusicDB musicItem = databaseService.GetComponent<DBScript>().GetMusicItemById(animModel.Id, int.Parse(musicIds[i]));
                        MusicItem item = new MusicItem(musicItem.MusicId, musicItem.delay);
                        musicItems.Add(item);
                    }

                    // Add Music Item to Dictionary
                    if (!musicDictionary.ContainsKey(animModel.AvatarId))
                    {
                        musicDictionary.Add(animModel.AvatarId, musicItems);
                    }

                    for (int i = 0; i < musicFiles.Length; i++)
                    {

                        // Get the Music Path
                        string musicPath = musicFiles[i].FullName;

                        Debug.Log("Music Path : " + musicPath);

                        if (File.Exists(musicPath))
                        {
                            // Play the music for avatar
                            WWW www = new WWW("file://" + musicPath);
                            yield return www;
                            if (www != null)
                            {
                                AudioSource source = avatarMusic.AddComponent<AudioSource>();
                                source.clip = www.GetAudioClip(false, true) as AudioClip;
                                source.loop = false;
                                if (!audiosIds.Contains(animModel.AvatarId.ToString()))
                                {
                                    audiosIds.Add(animModel.AvatarId.ToString());
                                }
                            }
                            else
                            {
                                Debug.Log("WWWW is null");
                                yield return null;
                            }
                        }
                        else
                        {
                            Debug.Log("This avatar is not content on Music");
                            yield return null;
                        }
                    }
                    audioTracks.Add(avatarMusic);
                }
            }
        }
        yield return null;
    }



    /// <summary>
    /// Cleans the avatars.
    /// </summary>
    public void CleanAvatars()
    {
        string ava_name = avatarsList[0].name;

        Debug.Log("Clean Avatars");
        Debug.Log("avatarsList.Count : " + avatarsList.Count);

        if (avatarsList.Count > 0) {
            for (int i = 0; i < avatarsList.Count; i++)
            {
                avatarsList.RemoveAt(i);
                Destroy(GameObject.Find(ava_name));
            }

            avatarsDictionary.Clear();
            avatarsToPlace.Clear();
            animationsLength.Clear();
            isPlayingMusic = false;
        }

        AvatarInfo avatarInfo = databaseService.GetComponent<DBScript>().GetAvatarInfoById(int.Parse(ava_name));
        List<DB_FBX> fbx = databaseService.GetComponent<DBScript>().GetFbxByAvatarId(avatarInfo.avatarId).ToList<DB_FBX>();
        List<AnimationInfo> animInfos = databaseService.GetComponent<DBScript>().GetAnimationByFbxID(fbx[0].Id).ToList<AnimationInfo>();
        if (animInfos.Count > 0)
        {
            if (animInfos[0].Repeat == "Loop")
            {
                anim_loop_nb -= 1;
            }
        }
        Resources.UnloadUnusedAssets();
    }

    public bool IsTheAvatarIsLastOneOfDublicates(string name)
    {
        int avatarsCount = avatarsList.Count;
        int clonedAvatars = 0;

        for (int i = 0; i < avatarsCount; i++) {
            if (avatarsList[i].name.StartsWith(name, StringComparison.Ordinal)){
                clonedAvatars++;
            }
        }

        if (clonedAvatars == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool animationTimerFinisted = false;

    private void AnimationTimerFinished(object sender, ElapsedEventArgs e)
    {
        isRepeating = false;
        animationTimerFinisted = true;
    }

    public bool IsNotRepeating()
    {
        return !isRepeating;
    }
  
    /// Repeats the avatar animation.
    public IEnumerator RepeatAvatarAnimation()
    {
#pragma warning disable CS0618 // Type or member is obsolete
        int numChildren = mainOb.GetComponent<ExpressionManager>().GetAnchor().childCount;
#pragma warning restore CS0618 // Type or member is obsolete

        if (numChildren >= 1)
        {
            for (int i = 0; i < numChildren; i++) {
                GameObject childAvatar = mainOb.GetComponent<ExpressionManager>().GetAnchor().GetChild(i).gameObject;

                if (childAvatar.name != "Highlight") {
                    Animation avatarAnim = childAvatar.GetComponent<Animation>();

                    if (childAvatar.GetComponent<AudioVideoSync>()) {
                        childAvatar.GetComponent<AudioVideoSync>().avatar_pause = false;
                    }

                    if (avatarAnim != null)
                    {
                        if (avatarAnim.GetClipCount() > 0) {
                            foreach (AnimationState animationState in avatarAnim) {
                                avatarAnim[animationState.clip.name].speed = 1;
                            }

                            AvatarInfo avatarInfo = databaseService.GetComponent<DBScript>().GetAvatarInfoById(int.Parse(childAvatar.name.Split('-')[0].Split('D')[0]));
                            List<DB_FBX> fbx = databaseService.GetComponent<DBScript>().GetFbxByAvatarId(avatarInfo.avatarId).ToList<DB_FBX>();
                            List<AnimationInfo> animations = databaseService.GetComponent<DBScript>().GetAnimationByFbxID(fbx[0].Id).ToList<AnimationInfo>();

                            string childAvatarCategory = avatarInfo.category.ToString();

                            if (animations.Count > 0)
                            {
                                Debug.Log("Animations Repeate Count  " + animations.Count);
                                if (!childAvatar.name.Contains("-"))
                                {
                                    //avatarAnim.Stop();
                                    //avatarAnim.Play();
                                }
                                else
                                {
                                    //ComplexAvatarController.isPlaying = false;
                                    //Debug.Log("AirPlane Playing : " + ComplexAvatarController.isPlaying);
                                    avatarAnim.Stop();
                                    avatarAnim.Play("Fly");
                                    avatarAnim["Fly"].speed = 0;
                                    avatarAnim["Fly"].time = 0;
                                }

                                if (AvatarHasMusic(childAvatar.name)) {
                                    //CheckAudioSources();
                                    StartCoroutine(PlayMusicForAvatar(childAvatarCategory, childAvatar.name, animations[0], avatarAnim));
                                }

                                else
                                {
                                    avatarAnim.Stop();
                                    avatarAnim.Play();
                                }
                            }
                            else
                            {
                                if (avatarAnim.GetClipCount() > 0)
                                {
                                    //Debug.Log("AirPlane Playing : " + ComplexAvatarController.isPlaying);
                                    childAvatar.SetActive(true);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (childAvatar.transform.childCount > 0)
                        {
                            avatarAnim = childAvatar.transform.GetChild(0).GetComponent<Animation>();

                            if (childAvatar.transform.GetChild(0).GetComponent<AudioVideoSync>())
                            {
                                childAvatar.transform.GetChild(0).GetComponent<AudioVideoSync>().avatar_pause = false;
                            }

                            if (avatarAnim != null)
                            {
                                if (avatarAnim.GetClipCount() > 0)
                                {
                                    foreach (AnimationState animationState in avatarAnim) {
                                        avatarAnim[animationState.clip.name].speed = 1;
                                    }

                                    //avatarAnim.Stop();
                                    //avatarAnim.Play();

                                    AvatarInfo avatarInfo = databaseService.GetComponent<DBScript>().GetAvatarInfoById(int.Parse(childAvatar.name.Split('-')[0].Split('D')[0]));
                                    List<DB_FBX> fbx = databaseService.GetComponent<DBScript>().GetFbxByAvatarId(avatarInfo.avatarId).ToList<DB_FBX>();
                                    List<AnimationInfo> animations = databaseService.GetComponent<DBScript>().GetAnimationByFbxID(fbx[0].Id).ToList<AnimationInfo>();

                                    string childAvatarCategory = avatarInfo.category.ToString();

                                    if (animations.Count > 0)
                                    {
                                        Debug.Log("Animations Repeate Count  " + animations.Count);
                                        if (!childAvatar.name.Contains("-"))
                                        {
                                            avatarAnim.Stop();
                                            avatarAnim.Play();
                                        }
                                        else
                                        {
                                            //ComplexAvatarController.isPlaying = false;
                                            //Debug.Log("AirPlane Playing : " + ComplexAvatarController.isPlaying);
                                            avatarAnim.Stop();
                                            avatarAnim.Play("Fly");
                                            avatarAnim["Fly"].speed = 0;
                                            avatarAnim["Fly"].time = 0;
                                        }

                                        if (AvatarHasMusic(childAvatar.name))
                                        {
                                            //CheckAudioSources();
                                            StartCoroutine(PlayMusicForAvatar(childAvatarCategory, childAvatar.name, animations[0], avatarAnim));
                                        }

                                        else
                                        {
                                            avatarAnim.Stop();
                                            avatarAnim.Play();
                                        }
                                    }
                                    else
                                    {
                                        if (avatarAnim.GetClipCount() > 0)
                                        {
                                            //Debug.Log("AirPlane Playing : " + ComplexAvatarController.isPlaying);
                                            childAvatar.SetActive(true);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        yield return new WaitForEndOfFrame();
    }

    /// <summary>
    /// Repeats the animation.
    /// </summary>
    public void RepeateAnimation()
    {
        // mute of un-mute the avatar sound
        CheckAudioSources();

        isRepeating = true;
        
        if (animationsLength.Count > 0)
        {
            float maxAnimationLength = animationsLength.Max();
            Debug.Log("Max Time : " + maxAnimationLength);
            this.animationTimer = new System.Timers.Timer();
            this.animationTimer.Interval = maxAnimationLength * 1000;
            this.animationTimer.Elapsed += AnimationTimerFinished;
            this.animationTimer.AutoReset = false;
            this.animationTimer.Enabled = true;
            this.animationTimer.Start();
        }

        StartCoroutine(RepeatAvatarAnimation());
    }


    /// <summary>
    /// Plays the music for avatar.
    /// </summary>
    /// <returns>The music for avatar.</returns>
    /// <param name="musicID">Music identifier.</param>
    public IEnumerator PlayMusicForAvatar(string CategoryID, string musicID, AnimationInfo animModel, Animation anim)
    {
        Debug.Log("Play Music For Avatar From Device");

        CheckAudioSources();

        int avatarMusicID = int.Parse(musicID.Split('-')[0].Split('D')[0]);

        if (animModel != null)
        {
            GameObject avatarMusic = GameObject.Find("music-" + avatarMusicID);
            AudioSource[] audios = avatarMusic.GetComponents<AudioSource>();

            isPlayingMusic = true;

            if (audios.Length == 1)
            {
                if (animModel.Repeat != "Loop")
                {
                    List<MusicItem> items = new List<MusicItem>();
                    if (musicDictionary.ContainsKey(avatarMusicID))
                    {
                        items = musicDictionary[avatarMusicID];
                    }
                    if (items.Count == 1)
                    {
                        if (items[0].delay > 0)
                        {
                            if (!avatarMusic.GetComponent<AudioSource>().isPlaying)
                            {
                                avatarMusic.GetComponent<AudioSource>().Stop();
                                if (!avatarMusic.GetComponent<AudioSource>().isPlaying)
                                {
                                    anim.Stop();
                                    avatarMusic.GetComponent<AudioSource>().time = 0;
                                    avatarMusic.GetComponent<AudioSource>().Stop();

                                    anim.Play();
                                    avatarMusic.GetComponent<AudioSource>().PlayDelayed(items[0].delay);

                                }
                            }
                            else
                            {
                                //foreach(AnimationState state in anim )
                                //{
                                //    state.time = 0; 
                                //}
                                anim.Stop();
                                anim.Play();

                                avatarMusic.GetComponent<AudioSource>().time = 0;
                                avatarMusic.GetComponent<AudioSource>().Stop();
                                avatarMusic.GetComponent<AudioSource>().PlayDelayed(items[0].delay);
                            }
                        }
                        else
                        {
                            //if (!avatarMusic.GetComponent<AudioSource>().isPlaying)
                            //{
                            anim.Stop();
                            avatarMusic.GetComponent<AudioSource>().time = 0;
                            avatarMusic.GetComponent<AudioSource>().Stop();
                            anim.Play();
                            avatarMusic.GetComponent<AudioSource>().Play();
                        }
                    }
                }
                else
                {
                    avatarMusic.GetComponent<AudioSource>().Stop();
                    anim.Stop();

                    anim.Play();
                    avatarMusic.GetComponent<AudioSource>().Play();
                }

            }
            if (audios.Length > 1)
            {
                List<MusicItem> items = new List<MusicItem>();
                if (musicDictionary.ContainsKey(avatarMusicID))
                {
                    items = musicDictionary[avatarMusicID];
                }

                bool isMusicPlaying = false;
                for (int i = 0; i < audios.Length; i++)
                {
                    if (audios[i].isPlaying)
                    {
                        isMusicPlaying = true;
                    }
                }


                if (isMusicPlaying)
                {
                    for (int i = 0; i < audios.Length; i++)
                    {
                        audios[i].Stop();
                    }

                    StartCoroutine(PlayQueuedMusic(audios, items));
                    //if (!avatarMusic.GetComponent<AudioSource>().isPlaying)
                    //{
                    //avatarMusic.GetComponent<AudioSource>().PlayDelayed(animModel.Delay);

                    //}
                }
                else
                {
                    StartCoroutine(PlayQueuedMusic(audios, items));
                }
            }
        }

        isPlayingMusic = true;
        //SetPlayMusicImage();

        //soundAvatar.SetActive(true);

        //ButtonPalyMuteMusic.SetActive(true);

        yield return null;
    }
    public bool isPlayingMusicMuted = false;

    private void CheckAudioSources()
    {
        GameObject[] audios = GameObject.FindGameObjectsWithTag("music");

        foreach (GameObject avatar in audios)
        {
            if (avatar.GetComponents<AudioSource>() != null)
            {
                AudioSource[] sources = avatar.GetComponents<AudioSource>();
                for (int aud = 0; aud < sources.Length; aud++)
                {
                    sources[aud].mute = isPlayingMusicMuted;
                }
                if (avatar.GetComponent<AudioSource>() != null)
                {
                    avatar.GetComponent<AudioSource>().mute = isPlayingMusicMuted;
                }
            }
        }
    }
    /// <summary>
    /// Plaies the music files queued 
    /// </summary>
    /// <returns>The queued music.</returns>
    /// <param name="sources">Sources.</param>
    public IEnumerator PlayQueuedMusic(AudioSource[] sources, List<MusicItem> items)
    {
        int i = 0;
        do
        {
            if (!sources[i].isPlaying)
            {
                Debug.Log("Delayed : " + items[i].delay);
                sources[i].PlayDelayed(items[i].delay);
                yield return null;
            }
            else
            {
                while (sources[i].isPlaying)
                {
                    yield return null;
                }
                i++;
                yield return null;
            }
        }
        while (i < sources.Length);
        yield return null;
    }


    public void GoToMainScene()
    {
        SceneManager.LoadScene("HallOfFameGround");
    }
}



