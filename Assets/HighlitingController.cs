﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HighlitingController : MonoBehaviour
{
    public GameObject Highlighter;
    public GameObject mainOb;
    public GameObject LockButton;

    public GameObject RepeatButton;
    public GameObject ResizeButton;
    public GameObject DublicateButton;
    public GameObject CleanAvatarsButton;

    public static string lastAvatarName = "";
    public static string lastAvatarSelected = "";

    private float holdTime = 0.8f; //or whatever
    private float acumTime = 0;


    [Header("Graphics Settings")]
    GraphicRaycaster graphicRayCaster;
    PointerEventData pointerEventData;
    EventSystem eventSystem;

    public static bool hasHighlightedObject = false;

    public bool AvatarsHasAnimation = false;


    public static bool isInHold = false;
    public static bool Ended = false;
    int touchCount = 0;
    // Update is called once per frame

    void Update()
    {
        HighlightOnMobile();
    }

    public void HighlightOnMobile()
    {
        if (Input.touchCount >= 1)
        {
            if (!IsPointerOverUIObject())
            {
                //Debug.Log("Pressed Touched : " + Input.touchCount);

                touchCount = Input.touchCount;
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    Ended = false;
                    isInHold = false;
                }

                if ((Input.GetTouch(0).phase == TouchPhase.Stationary || Input.GetTouch(0).phase == TouchPhase.Moved)
                && !(Input.GetTouch(0).phase == TouchPhase.Ended))
                {
                    acumTime += Time.deltaTime;

                    if (acumTime >= holdTime)
                    {
                        //Long tap
                        if (Lean.Touch.LeanSelectable.IsSelectedCount == 0)
                        {
                            HitObject(Input.touches[0].position);
                        }

                        isInHold = true;
                    }

                }
                if (Input.GetTouch(0).phase == TouchPhase.Ended)
                {
                    acumTime = 0;
                }

                if (Input.touchCount == 1)
                {
                    //Debug.Log(" Phase 2 : " + Input.GetTouch(0).phase);
                    if (Input.GetTouch(0).phase == TouchPhase.Ended)
                    {
                        if (!Ended)
                        {
                            //Debug.Log("Phase Ended Time : " + acumTime + " , Is in Hold : " + isInHold + " Touch Count : " + touchCount + "has Highlighted Object : " + hasHighlightedObject);
                            acumTime = 0;
                            if ((!isInHold) && hasHighlightedObject == true)
                            {
                                EnableLockMode();
                            }

                            Ended = true;
                        }
                    }
                }
            }
        }
    }

    public void HighlightOnPC()
    {
        if (Input.GetMouseButtonDown(0) && !IsPointerOverUIObject())
        {
            //Touch Begin - True when the finger touches the screen
            //Play animation for chicken squat
            Ended = false;
            isInHold = false;
        }

        else if (Input.GetMouseButton(0) && !IsPointerOverUIObject())
        {
            //Touch Continued - True when the finger is still touching the screen
            acumTime += Time.deltaTime;

            if (acumTime >= holdTime)
            {
                //Long tap
                if (Lean.Touch.LeanSelectable.IsSelectedCount == 0)
                {
                    HitObject(Input.mousePosition);
                }
                isInHold = true;
            }
        }

        else if (Input.GetMouseButtonUp(0) && !IsPointerOverUIObject())
        {
            //Touch End - True when the finger is lifted from the screen
            //Play animation for chicken jump
            acumTime = 0;
            if ((!isInHold) && hasHighlightedObject == true)
            {
                EnableLockMode();
            }
            Ended = true;
        }
    }

    public GameObject ReturnClickedObject()
    {
        Debug.Log("Checking Clicked Object .. ");
        GameObject target = null;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit[] hits;
        hits = Physics.RaycastAll(ray.origin, ray.direction * 100);

        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].transform.name != "MainOb"
            && hits[i].transform.name != "Capsule"
            && !hits[i].transform.name.Contains("bubble")
            && !hits[i].transform.name.Contains("Face")
            && hits[i].transform.name != "Highlight"
            && !hits[i].transform.name.Contains("ShadowCast")
            && !hits[i].transform.name.Contains("ARPlane")
            && hits[i].transform.name != "Sphere")
            {
                target = hits[i].transform.gameObject;
                return target;
            }
        }

        return target;
    }

    public void HitObject(Vector2 position)
    {
        GameObject hittedAvatar = ReturnClickedObject();
        if (hittedAvatar != null)
        {
            Debug.Log("Hitting Object : " + hittedAvatar.transform.name);
            if (hittedAvatar.GetComponent<Lean.Touch.LeanSelectable>() == null)
            {
                Debug.Log("Search In Parent");
                GameObject main = hittedAvatar.GetComponentInParent<Lean.Touch.LeanSelectable>().gameObject;
                Highlighter.transform.position = main.transform.position + new Vector3(0, 0.01f, 0);
                Highlighter.transform.localRotation = Quaternion.Euler(90f, 0f, 0f);

                if (!Highlighter.activeInHierarchy)
                {
                    Debug.Log("Search In Parent - 1");

                    main.GetComponentInParent<Lean.Touch.LeanSelectable>().SelectMe();
                    Highlighter.transform.SetParent(main.transform);
                    Highlighter.SetActive(true);

                    EnableEditMode(main);

                    ApplicationLoader.lastSelectedObject = main.name;
                    ApplicationLoader.NoAvatarSelected = false;

                }
                else
                {
                    Debug.Log("Search In Parent - 2");

                    main.GetComponentInParent<Lean.Touch.LeanSelectable>().SelectMe();
                    Highlighter.transform.SetParent(main.transform);
                    EnableEditMode(main);

                    Debug.Log("Selected Avatar is  : " + main.name);

                    ApplicationLoader.lastSelectedObject = main.name;
                    ApplicationLoader.NoAvatarSelected = false;

                }

                CleanAvatarsButton.SetActive(true);
                //DublicateButton.SetActive(true);
            }

            else
            {
                hittedAvatar.GetComponentInParent<Lean.Touch.LeanSelectable>().SelectMe();

                if (!Highlighter.activeInHierarchy)
                {
                    Highlighter.transform.position = hittedAvatar.transform.position + new Vector3(0, 0.01f, 0);

                    Debug.Log(" Not Reactive - 1 ");

                    Highlighter.transform.localRotation = Quaternion.Euler(90f, 0f, 0f);
                    //Highlighter.transform.localScale = hittedAvatar.transform.localScale*100;
                    Highlighter.transform.SetParent(hittedAvatar.transform);
                    Highlighter.SetActive(true);


                    CleanAvatarsButton.SetActive(true);
                    //DublicateButton.SetActive(true);

                    EnableEditMode(hittedAvatar);

                    ApplicationLoader.lastSelectedObject = hittedAvatar.name;
                    ApplicationLoader.NoAvatarSelected = false;
                }

                else
                {
                    Debug.Log("Activated ");
                    Highlighter.transform.position = hittedAvatar.transform.position + new Vector3(0, 0.01f, 0);
                    Highlighter.transform.localRotation = Quaternion.Euler(90f, 0f, 0f);
                    Highlighter.transform.SetParent(hittedAvatar.transform);
                    CleanAvatarsButton.SetActive(true);

                    //DublicateButton.SetActive(true);

                    EnableEditMode(hittedAvatar);

                    ApplicationLoader.lastSelectedObject = hittedAvatar.name;
                    ApplicationLoader.NoAvatarSelected = false;
                }
            }
        }
        hasHighlightedObject = true;
    }

    private void EnableLockMode()
    {
        Debug.Log(" Lock Mode Deselct all");
        Lean.Touch.LeanSelectable.DeselectAll();

        Highlighter.transform.position = mainOb.transform.position + new Vector3(0, 0.01f, 0);
        Highlighter.transform.localRotation = Quaternion.Euler(90f, 0f, 0f);
        Highlighter.transform.SetParent(mainOb.transform);

        Highlighter.SetActive(false);
        CleanAvatarsButton.SetActive(false);

        ApplicationLoader.hasHighlitedObject = false;
        ApplicationLoader.lastSelectedObject = string.Empty;

        hasHighlightedObject = false;

        CheckIfHasAnimation();

    }

    public void CheckIfHasAnimation()
    {
        int numClildren = ApplicationLoader.avatarsList.Count;

        if (numClildren >= 1)
        {
            for (int i = 0; i < numClildren; i++)
            {
                if (ApplicationLoader.avatarsList[i].name != "Highlight")
                {
                    Animation anim = ApplicationLoader.avatarsList[i].GetComponent<Animation>();
                    if (anim != null)
                    {
                        if (anim.GetClipCount() > 0)
                        {
                            RepeatButton.SetActive(true);
                        }
                    }
                    else
                    {
                        if (ApplicationLoader.avatarsList[i].transform.childCount > 0)
                        {

                            anim = ApplicationLoader.avatarsList[i].transform.GetChild(0).GetComponent<Animation>();


                            if (anim != null)
                            {
                                if (anim.GetClipCount() > 0)
                                {
                                    RepeatButton.SetActive(true);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    [Obsolete]
    public void EnableEditMode(GameObject avatar)
    {
        Animation anim = avatar.GetComponent<Animation>();
        if (anim != null)
        {
            if (anim.GetClipCount() > 0)
            {
                anim.Stop();

                //if (GameObject.Find("music-" + avatar.name.Split('D')[0]).GetComponent<AudioSource>()) {
                //    AudioSource audioSource = GameObject.Find("music-" + avatar.name.Split('D')[0]).GetComponent<AudioSource>();
                //    if (audioSource.GetComponent<AudioSource>().isPlaying){
                //        audioSource.Stop();
                //    }
                //}

                if (avatar.GetComponent<AudioVideoSync>())
                {
                    avatar.GetComponent<AudioVideoSync>().avatar_pause = true;
                }

                //GameObject avatarAudio = GameObject.Find("music-" + avatar.name);

                //if (avatarAudio != null)
                //{
                //    //if (!avatar.GetComponent<Lean.Touch.LeanSelectable>().IsSelected)
                //    //{
                //    if (avatarAudio.GetComponent<AudioSource>().isPlaying)
                //    {
                //        //AudioListener.pause = true;
                //        avatarAudio.GetComponent<AudioSource>().Stop();
                //    }
                //    //}
                //}
            }
        }
        else
        {
            if (avatar.transform.childCount > 0)
            {
                anim = avatar.transform.GetChild(0).GetComponent<Animation>();
                if (anim != null)
                {
                    if (anim.GetClipCount() > 0)
                    {
                        anim.Stop();
                        if (avatar.GetComponent<AudioVideoSync>())
                        {
                            avatar.GetComponent<AudioVideoSync>().avatar_pause = true;
                        }
                    }
                }
            }
        }

        ApplicationLoader.hasHighlitedObject = true;
        ApplicationLoader.lastSelectedObject = avatar.name;

        RepeatButton.SetActive(false);
    }

    private bool IsPointerOverUIObject()
    {

        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

}
