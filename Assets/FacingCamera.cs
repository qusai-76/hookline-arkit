﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FacingCamera : MonoBehaviour
{

    public GameObject avatar;
    // Start is called before the first frame update
    void Start()
    {
        Vector3 position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width/2,Screen.height/2,10));
        avatar.transform.position = position;

        
        
        BoxCollider box = avatar.AddComponent<BoxCollider>();
        box.center = new Vector3(-2.5f,2.3f,0.5f);
        box.size = new Vector3(2,4.5f,2f);

        // Add Glow Object CMD
        GlowObjectCmd glow = avatar.AddComponent<GlowObjectCmd>();
        Color toGlowColor ;
        ColorUtility.TryParseHtmlString("BC70BC",out toGlowColor);
        glow.GlowColor = toGlowColor;

        SkinnedMeshRenderer[] renderers = avatar.GetComponentsInChildren<SkinnedMeshRenderer>();
        
        for(int i = 0; i < renderers.Length; i++)
        {
            renderers[i].material.shader = Shader.Find("StandardGlow");
        }

    }

}
