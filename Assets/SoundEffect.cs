﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffect : MonoBehaviour {

    public AudioClip clip;
    public AudioSource source;

	// Use this for initialization
	void Start () {
        source.clip = clip;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlaySound()
    {
        source.clip = clip;
        source.Play();
    }
}
