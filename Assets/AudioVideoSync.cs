﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioVideoSync : MonoBehaviour
{
    public float aniamtionTimer;
	float clip_time;
	private bool isAssigned=false;
	private AudioSource audioSource;
	private AnimationState state;

    public bool avatar_pause;

	// Start is called before the first frame update
	void Start()
	{
		clip_time = transform.GetChild(0).GetComponent<Animation>().clip.length;
        aniamtionTimer = 0;
        avatar_pause = false;
        foreach (AnimationState animState in transform.GetChild(0).GetComponent<Animation>())
		{
			state = animState;
			Debug.Log("Animation Length : "+ state.length);
			break;
		}

        if (!this.isAssigned)
        {
            if (GameObject.Find("music-" + transform.name.Split('D')[0]) != null) {
                Debug.Log("Assigned .. ");

                audioSource = GameObject.Find("music-" + transform.name.Split('D')[0]).GetComponent<AudioSource>();
                audioSource.time = state.time;
            }
        }

        //StartCoroutine(CheckSynchronization());
    }

	void Update()
	{
		if (!this.isAssigned) {
			if (GameObject.Find("music-" + transform.name.Split('D')[0]) != null) {
                Debug.Log("Assigned .. ");
				audioSource = GameObject.Find("music-" + transform.name.Split('D')[0]).GetComponent<AudioSource>();
				this.isAssigned = true;
				audioSource.time = state.time;
			}
		}

		if (ApplicationLoader.isRepeating) {
			//timer -= Time.deltaTime;
            aniamtionTimer += Time.deltaTime;
        }

		if (ApplicationLoader.isPausing || avatar_pause) {
			//timer = clip_time;
            aniamtionTimer = 0;
        }

		if (isAssigned) {
            //Debug.Log("State Time " + state.time + "State Length : "+ state.length);
			if (aniamtionTimer >= state.length) {
				Reset();
			}
        }
        else{
            if (aniamtionTimer >= state.length)
            {
                ResetWithNoAudio();
            }
        }

	}

	private void Reset()
	{
        Debug.Log("Reset ... ");
        aniamtionTimer = 0;
        audioSource.time = 0f;
		state.time = 0;

		audioSource.time = state.time;

        transform.GetChild(0).GetComponent<Animation>().Stop();
        audioSource.Stop();


        transform.GetChild(0).GetComponent<Animation>().Play();
        audioSource.Play();
    }

    private void ResetWithNoAudio()
    {
        Debug.Log("Reset Without Audio... ");
        aniamtionTimer = 0;
        state.time = 0;
        transform.GetChild(0).GetComponent<Animation>().Stop();
        transform.GetChild(0).GetComponent<Animation>().Play();
    }
}
