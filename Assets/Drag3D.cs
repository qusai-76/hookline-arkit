﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drag3D : MonoBehaviour
{
    // Start is called before the first frame update

    
    private bool isMouseDrag;
    private Vector3 screenPosition;
    
    private Vector3 offset;
    GameObject hitted;
    private string currentCostume;
    private int touchCount = 0;
    private Vector3 currentCustomePosition;

    private bool movingAvatar = false;
    // Avatars GameObjects
    public GameObject avatar1;
    public GameObject avatar2;
    public GameObject avatar3;

    public GameObject mainOB;

    // Cloathes and OutFits Objects
    public GameObject Clothes;

    public GameObject bubble;

    // Refrence to the OoutFits
    public List<Clothe> clothes;


    // Flash Light
    public GameObject flash;
    
    
    // Web Controller
    public GameObject webController;

    public void Start()
    {
        clothes = new List<Clothe>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount == 1) {
            if (Input.touches[0].phase==TouchPhase.Began) {
                RaycastHit hitInfo;
                
                hitted = ReturnClickedObject(out hitInfo);
                
                // Click Bubble 
                if(hitted !=null && hitted.transform.name.StartsWith("bubble", System.StringComparison.Ordinal)) {
                   // Application.OpenURL("http://www.google.com");
                    //webController.GetComponent<WebViewController>().GoToWebsite();
                }
                
                // Click Custome
                if ( (hitted != null) && hitted.transform.name.StartsWith("costume", System.StringComparison.Ordinal))  {
                    touchCount++;
                    
                    if(touchCount==1)  {

                        clothes.Clear();
                        // Get the Positions of the Clothes
                        int num = Clothes.transform.childCount;
                        
                        for(int i=0;i<num;i++) {
                            Vector3 position = Clothes.transform.GetChild(i).transform.position;
                            string clothname = Clothes.transform.GetChild(i).transform.name;
                            Clothe c = new Clothe();
                            c.position = position;
                            c.name = clothname;
                            clothes.Add(c);
                        }

                    }
                    
                    currentCostume = hitted.transform.name;
                    isMouseDrag = true;
                    
                    // Debug.Log("target position :" + hitted.transform.position);
                    // Convert world position to screen position.
                    
                    screenPosition = Camera.main.WorldToScreenPoint(hitted.transform.position);
                    offset = hitted.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.touches[0].position.x, Input.touches[0].position.y, screenPosition.z));
                }
                
                if ( (hitted != null) && hitted.transform.name.StartsWith("Avatar", System.StringComparison.Ordinal))  {
                    movingAvatar = true;
                    screenPosition = Camera.main.WorldToScreenPoint(hitted.transform.position);
                    offset = hitted.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.touches[0].position.x, Input.touches[0].position.y, screenPosition.z));
                    
                }
            }

            if ( (Input.touches[0].phase==TouchPhase.Ended || Input.touches[0].phase==TouchPhase.Canceled) && (isMouseDrag) ) {
                isMouseDrag = false;
                touchCount = 0;
                
                Vector3 pos = Camera.main.ScreenToWorldPoint(Input.touches[0].position);
            
                GameObject avatar = ReturnHittedObject();
                
                if (avatar != null ) {
                    if (avatar.transform.name.StartsWith("Avatar", System.StringComparison.Ordinal)) {
                        if (currentCostume == "costume1")  {
                            //Debug.Log(" Change Avatar ");
                            //GameObject.Find(currentCostume).SetActive(false);
                            avatar1.transform.localScale = avatar.transform.localScale;
                            avatar1.transform.position = avatar.transform.position;
                            avatar1.transform.localRotation = avatar.transform.localRotation;
                            avatar1.transform.rotation = avatar.transform.rotation;
                            
                            avatar.SetActive(false);
                            
                            flash.GetComponent<FlashBang>().MineHit();
                            bubble.SetActive(true);
                            Clothes.transform.position = avatar1.transform.position + new Vector3(0,0.5f,0);
                            
                            Clothes.transform.SetParent(avatar1.transform);
                            Clothes.SetActive(true);
                            avatar1.transform.SetParent(mainOB.transform);
                            avatar1.SetActive(true);
                            ReturnClothesToOrginalPositions();
                            
                        }
                        if(currentCostume == "costume2") { 
                           //Debug.Log(" Change Avatar ");
                            //GameObject.Find(currentCostume).SetActive(false);
                            
                            avatar2.transform.localScale = avatar.transform.localScale;
                            avatar2.transform.position = avatar.transform.position;
                           
                            avatar2.transform.localRotation = avatar.transform.localRotation;
                            avatar2.transform.rotation = avatar.transform.rotation;
                            
                            avatar.SetActive(false);
                            bubble.SetActive(false);
                            Clothes.transform.position = avatar2.transform.position + new Vector3(0,0.5f,0);
                            Clothes.transform.SetParent(avatar2.transform);
                            Clothes.SetActive(true);
                            
                            flash.GetComponent<FlashBang>().MineHit(); 
                            avatar2.transform.SetParent(mainOB.transform);
                            avatar2.SetActive(true);
                            ReturnClothesToOrginalPositions();                                
                        }
                        
                        if(currentCostume == "costume3")  { 
                            //Debug.Log(" Change Avatar ");
                            //GameObject.Find(currentCostume).SetActive(false); 
                            avatar3.transform.localScale = avatar.transform.localScale;   
                            avatar3.transform.position = avatar.transform.position;
                            
                            avatar3.transform.localRotation = avatar.transform.localRotation;
                            avatar3.transform.rotation = avatar.transform.rotation;
                            
                            avatar.SetActive(false);
                            bubble.SetActive(false);
                            Clothes.transform.position = avatar3.transform.position + new Vector3(0,0.5f,0);
                            Clothes.transform.SetParent(avatar3.transform);
                            Clothes.SetActive(true);
                            
                            flash.GetComponent<FlashBang>().MineHit(); 
                            avatar3.transform.SetParent(mainOB.transform);
                            avatar3.SetActive(true);
                            
                            ReturnClothesToOrginalPositions();                              
                        }
                        
                        
                        //currentCostume = "";
                    }
                    else {
                        ReturnClothesToOrginalPositions();
                    }
                }
                else {
                    ReturnClothesToOrginalPositions();
                }

                Debug.Log("Drooped");

            }
            
            
            if ( (Input.touches[0].phase==TouchPhase.Ended || Input.touches[0].phase==TouchPhase.Canceled) && (movingAvatar) ) {
                movingAvatar = false;
            }
            

            if (isMouseDrag)  {
                //track mouse position.
                Vector3 currentScreenSpace = new Vector3(Input.touches[0].position.x, Input.touches[0].position.y, screenPosition.z);

                //convert screen position to world position with offset changes.
                Vector3 currentPosition = Camera.main.ScreenToWorldPoint(currentScreenSpace) + offset;

                //It will update target gameobject's current postion.
                hitted.transform.position = currentPosition;
            }
            if (movingAvatar) { 
                 //track mouse position.
                Vector3 currentScreenSpace = new Vector3(Input.touches[0].position.x, Input.touches[0].position.y, screenPosition.z);

                //convert screen position to world position with offset changes.
                Vector3 currentPosition = Camera.main.ScreenToWorldPoint(currentScreenSpace) + offset;

                //It will update target gameobject's current postion.
                hitted.transform.position = currentPosition;
            }
        }
    }
    
    GameObject ReturnClickedObject(out RaycastHit hit)
    {
        GameObject target = null;
        Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
        if (Physics.Raycast(ray.origin, ray.direction * 100, out hit))  {
            target = hit.transform.gameObject;
        }
        return target;
    }
    
    public void EnableClothes()
    {
        int num = Clothes.transform.childCount;

        for (int i=0; i< num; i++) {
            Clothes.transform.GetChild(i).gameObject.SetActive(true);
        }

    }
    
    public void ReturnClothesToOrginalPositions() {
        
        // Get the Positions of the Clothes
        int num = Clothes.transform.childCount;

        for (int i = 0; i < num; i++) {
            foreach (Clothe clothe in clothes) {
                if(Clothes.transform.GetChild(i).transform.name == clothe.name)
                {
                    Clothes.transform.GetChild(i).transform.position = clothe.position;
                }
            }
        }
        
    }
    
     GameObject ReturnHittedObject()
    {
        GameObject target = null;
        Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
        RaycastHit[] hits = Physics.RaycastAll(ray);
        
        for(int i=0;i<hits.Length;i++) {
            if(hits[i].transform.name.StartsWith("Avatar", System.StringComparison.Ordinal))
            {
                return hits[i].transform.gameObject; 
            }
        }
  
        return target;
    }


}
