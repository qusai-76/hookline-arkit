﻿
public class MusicItem
{
    public int id;
    public float delay;
    
    public MusicItem(int _id, float _delay) {
        id = _id;
        delay = _delay;
    }
}
