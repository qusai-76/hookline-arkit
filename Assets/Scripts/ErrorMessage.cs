﻿using System;
using UnityEngine;

namespace AppSeetingsManagement
{
    [Serializable]
    public class ErrorMessage
    {
        public string errornum = "";
        public string status = "";

        public string ToJson()
        {
            return JsonUtility.ToJson(this);
        }

        public byte[] ToPostData()
        {
            return System.Text.Encoding.UTF8.GetBytes(ToJson());
        }

    }
}
