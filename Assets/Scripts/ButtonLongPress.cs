﻿using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonLongPress : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{

    #if UNITY_IOS
        [DllImport("__Internal")]
        public static extern bool _InPhoneCall();
    #endif
    
    [SerializeField]
    [Tooltip("How long must pointer be down on this object to trigger a long press")]
    private float holdTime = 1f;

    // Remove all comment tags (except this one) to handle the onClick event!
    private bool held = false;
    private bool pressed;
    public UnityEvent onClick = new UnityEvent();

    public UnityEvent onTouchDown = new UnityEvent();
    public UnityEvent onTouchUp = new UnityEvent();
    public UnityEvent onLongPress = new UnityEvent();

    public Image button, countdown;

    private const float MaxRecordingTime = 30f; // seconds


    public bool isRecording;

    private void Start()
    {
        isRecording = false;
        Reset();
    }
    
    private void Reset()
    {
        isRecording = false;
        // Reset fill amounts
        if (button) button.fillAmount = 1.0f;
        if (countdown) countdown.fillAmount = 0.0f;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (_InPhoneCall() == false && !PhoneCall.existedCall) { 
            held = false;
            StartCoroutine(Countdown());
            Invoke("OnLongPress", holdTime);
        }
    }
    

    public void OnPointerUp(PointerEventData eventData)
    {
        if (_InPhoneCall() == false && !PhoneCall.existedCall) {
            CancelInvoke("OnLongPress");

            if (!held)
                onClick.Invoke();

            pressed = false;

            //if (!held)
            //    Debug.Log("After Click");
            //else
            //    Debug.Log("After Long");
        }
    }
    
    private IEnumerator Countdown()
    {
        pressed = true;
        // First wait a short time to make sure it's not a tap
        yield return new WaitForSeconds(holdTime);
        if (!pressed) yield break;
        // Start recording
        if (onTouchDown != null) onTouchDown.Invoke();
        // Animate the countdown
        float startTime = Time.time, ratio = 0f;
        
        isRecording = true;
        
        while (pressed && (ratio = (Time.time - startTime) / MaxRecordingTime) < 1.0f)
        {
            countdown.fillAmount = ratio;
            button.fillAmount = 1f - ratio;
            yield return null;
        }
        // Reset
        Reset();
        // Stop recording
        if (onTouchUp != null) onTouchUp.Invoke();
    }
    
    public void OnPointerExit(PointerEventData eventData)
    {
        CancelInvoke("OnLongPress");
    }

    private void OnLongPress()
    {
        if (_InPhoneCall() == false && !PhoneCall.existedCall) { 
            held = true;
            onLongPress.Invoke();
        }
    }

}