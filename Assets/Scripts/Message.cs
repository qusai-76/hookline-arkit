﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Message 
{
    public string textMessageOne;
    public string textMessageTwo;

    public string ToJson()
    {
        return JsonUtility.ToJson(this);
    }
    public byte [] toPost()
    {
        return System.Text.Encoding.UTF8.GetBytes(ToJson());
    }
}
