﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


public class TakeScreenShot_HallOfFlame : MonoBehaviour
{ 

    private string  URL = API_Settings.ValidateMessage_URL;
    private string appPath;
    public List<GameObject> UIObjects;
    public GameObject planeIndicator;

    public GameObject ForBetterExperienceMessage;

    private Texture2D texture;
    private int picturesCount;
    private string filePath;
    private string photoName;
    private byte[] bytes;

    public GameObject avatarHighlight;

    public GameObject cleanAvatars;
    public GameObject musicButton;
    public GameObject RadarMap;
    
    static private Sprite spriteOrginal;
    static private Sprite spriteWithBubble;

    public GameObject popup_window_imageView; // Popup window for show image After Take Screenshoot
    public GameObject popup_window_addText;   // Popup window for adding text over Image to the avatar
    public GameObject popup_window_sharing;   // Popup window for sharing Image with text -- we need function for write text on image before sharing the image 

    public Image img_imageViewPopupWindow;    // ImageView for popup_window_imageView
    public Image img_addTextPopupWindow;      // ImageView for popup_window_addText
    public Image img_SharingPopupWindow;      // ImageView for popup_window_sharing

    public GameObject closeButton_addTextPopupWindow;

    //-----------  Bubble -----------
    private float x_bubbleInPopupWindow;
    private float y_bubbleInPopupWindow;
    private float x_Scale_bubbleInPopupWindow;
    private float y_Scale_bubbleInPopupWindow;
    private float x_bubble_2_InPopupWindow;
    private float y_bubble_2_InPopupWindow;
    private float x_Scale_bubble_2_InPopupWindow;
    private float y_Scale_bubble_2_InPopupWindow;
    
    public GameObject bubble_1;
    public GameObject bubble_2;

    private Text text_Bubble_1;
    private Text text_Bubble_2;
    private GameObject placeHolder_Bubble_1;
    private GameObject placeHolder_Bubble_2;

    private GameObject closeBubble_1;
    private GameObject closeBubble_2;

    private GameObject buttons_Popup_AddTextToAvatar;

    public GameObject panel_CommentBlocked;
    public GameObject panel_PleaseWait;

    public GameObject mediaSynchronizer;
    public GameObject database_DAL;

    public GameObject buttonRepeate;

    private void Awake()
    {
        //text_Bubble_1 = bubble_1.transform.GetChild(0).transform.GetChild(1).GetComponent<Text>();
        //text_Bubble_2 = bubble_2.transform.GetChild(0).transform.GetChild(1).GetComponent<Text>();

        //placeHolder_Bubble_1 = bubble_1.transform.GetChild(0).transform.GetChild(0).gameObject;
        //placeHolder_Bubble_2 = bubble_2.transform.GetChild(0).transform.GetChild(0).gameObject;

        //closeBubble_1 = bubble_1.transform.GetChild(1).gameObject;
        //closeBubble_2 = bubble_2.transform.GetChild(1).gameObject;

        //buttons_Popup_AddTextToAvatar = popup_window_addText.transform.GetChild(1).gameObject;

    }

    // Start is called before the first frame update
    void Start()
    {
        appPath = Application.persistentDataPath;
        

        // show "For Better Experience Message" only once;
        if (string.IsNullOrEmpty(PlayerPrefs.GetString("ForBetterExperienceMessage_Statue")))
        {
            PlayerPrefs.SetString("ForBetterExperienceMessage_Statue", "False");

            if(ForBetterExperienceMessage != null)
                ForBetterExperienceMessage.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {


        // ---- update new postion of bubble 1  ----
        //position of bubble 1
        //x_bubbleInPopupWindow = bubble_1.transform.position.x;
        //y_bubbleInPopupWindow = bubble_1.transform.position.y;
        //// scale of bubble 1
        //x_Scale_bubbleInPopupWindow = bubble_1.transform.localScale.x;
        //y_Scale_bubbleInPopupWindow = bubble_1.transform.localScale.x;

        //// ----- update new postion of bubble 2 ----
        //// position of bubble 2
        //x_bubble_2_InPopupWindow = bubble_2.transform.position.x;
        //y_bubble_2_InPopupWindow = bubble_2.transform.position.y;
        //// scale of bubble 2
        //x_Scale_bubble_2_InPopupWindow = bubble_2.transform.localScale.x;
        //y_Scale_bubble_2_InPopupWindow = bubble_2.transform.localScale.x;
    }




    // ============================= Show Elements =============================
    /// <summary>
    /// Show The Elements.
    /// </summary>
    IEnumerator ShowUIElements()
    {
        GameObject.Find ("Canvas").GetComponent<Canvas> ().enabled = true;

        if (planeIndicator != null)
        {
            MeshRenderer[] renderers = planeIndicator.GetComponentsInChildren<MeshRenderer>();
            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].enabled = true;
            }
        }

        if (avatarHighlight != null)
        {
            MeshRenderer highlightRenerer = avatarHighlight.GetComponent<MeshRenderer>();
            highlightRenerer.enabled = true;
        }

        //if (AppLoader.hasHighlitedObject || AvatarsLoader.hasHighlitedObject) {
        //    avatarHighlight.SetActive(true);

        //avatarHighlight.transform.SetParent(GameObject.Find(AppLoader.lastSelectedObject).transform);
        //avatarHighlight.transform.localScale = new Vector3(85f,85f,85f);
        //}

        //foreach (GameObject ui in UIObjects)
        //{
        //    if (ui!=null)
        //    {
        //        ui.SetActive(true);
        //    }
        //}
        //if(AppLoader.isPlayingMusic)
        //{
        //    musicButton.SetActive(true); 
        //}
        //if(AppLoader.avatarsList.Count>0)
        //{
        //    cleanAvatars.SetActive(true); 
        //}
        // if(AppLoader.hasAvatarWithOneAnimation)
        //{
        //    buttonRepeate.SetActive(true);
        //}

        yield return null;
    }


    // ============================= Hide Elements =============================
    /// <summary>
    /// Hide The Elements.
    /// </summary>
    IEnumerator HideUIElements()
    {
        GameObject.Find ("Canvas").GetComponent<Canvas> ().enabled = false;
        //Debug.Log(" Canvas Disabled ");

        if (planeIndicator != null){
            MeshRenderer[] renderers = planeIndicator.GetComponentsInChildren<MeshRenderer>();
            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].enabled = false;
            }
        }
        if (avatarHighlight != null)
        {
            MeshRenderer highlightRenerer = avatarHighlight.GetComponent<MeshRenderer>();
            highlightRenerer.enabled = false;
        }

        //if (AppLoader.hasHighlitedObject || AvatarsLoader.hasHighlitedObject) {
        //    avatarHighlight.SetActive(false); 
        //}

        //foreach (GameObject ui in UIObjects)
        //{
        //    if (ui != null && ui.activeInHierarchy)
        //    {
        //        ui.SetActive(false);
        //    }
        //}

        //RadarMap.SetActive(false);

        //if(AppLoader.isPlayingMusic)
        //{
        //    musicButton.SetActive(false); 
        //}
        //if(AppLoader.avatarsList.Count>0)
        //{
        //    cleanAvatars.SetActive(false); 
        //}
        //if(AppLoader.hasAvatarWithOneAnimation)
        //{
        //    buttonRepeate.SetActive(false);
        //}

        //closeButton_addTextPopupWindow.SetActive(false);

        yield return null;
    }

   


    // **************************************************
    //                          Step.1
    // **************************************************
    /// <summary>
    /// Button for take the screenshoot.
    /// </summary>
    public void TakeScreenshoot()
    {
        StartCoroutine(TakeScreenshootFunction());
    }

    /// <summary>
    /// Take screenshoot function and store the path of this image in filePath var.
    /// </summary>
    private IEnumerator TakeScreenshootFunction()
    {
        StartCoroutine(HideUIElements());
       // GameObject.Find ("Canvas").GetComponent<Canvas> ().enabled = false;
        yield return new WaitForEndOfFrame();
        
        texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        //texture = new Texture2D(Screen.width, Screen.height);
        texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        texture.Apply();

        // Save image in var spriteOrginal for use it when switch between popup
        yield return new WaitForEndOfFrame();
        spriteOrginal = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));

   
        //// Check if laura image is found
        if (LauraClass.found == false) // take normal screenshot
        {
            print("Laura Not Found");
            yield return StartCoroutine(SaveImage());  // save image 
        }
        else   //go to popup-addtext for show tow bubble
        {
            print("Laura Is Found");
            GoToImageViewPopupWindow();
        }
    }

    // =====================  save image   =====================
    /// <summary>
    /// Save the image.
    /// </summary>
    IEnumerator SaveImage()
    {
        //texture = ResizeTexture(texture, ImageFilterMode.Biliner, 1f);

        bytes = texture.EncodeToJPG();
        Destroy(texture);

        DirectoryInfo di = new DirectoryInfo(appPath+"/photos");
        var files = di.GetFiles("*.jpg");
        picturesCount = files.Length;
        picturesCount++;

        photoName = picturesCount + "_" + System.DateTime.Now.ToString("yyyyMMddHHmmss");

        filePath = appPath + "/photos/pic-" + photoName + ".jpg";

        // save file to presistent data path 
        File.WriteAllBytes(filePath, bytes);

        // Save to Database 
        Photo pic = database_DAL.GetComponent<DBScript>().InsertPhoto(photoName);

        //// Native Share
        new NativeShare().AddFile(filePath).Share();

        CancelPopupWindow();

        //// save the image to the native gallery in the phone
        //NativeGallery.SaveImageToGallery(bytes, "Hookline", "pic-" + photoName + ".jpg");
        
         yield return null;
        Resources.UnloadUnusedAssets();
        
       
    }

    public enum ImageFilterMode : int
    {
        Nearest = 0,
        Biliner = 1,
        Average = 2
    }
    public static Texture2D ResizeTexture(Texture2D pSource, ImageFilterMode pFilterMode, float pScale)
    {

        //*** Variables
        int i;

        //*** Get All the source pixels
        Color[] aSourceColor = pSource.GetPixels(0);
        Vector2 vSourceSize = new Vector2(pSource.width, pSource.height);

        //*** Calculate New Size
        float xWidth = Mathf.RoundToInt((float)pSource.width * pScale);
        float xHeight = Mathf.RoundToInt((float)pSource.height * pScale);

        //*** Make New
        Texture2D oNewTex = new Texture2D((int)xWidth, (int)xHeight, TextureFormat.RGBA32, false);

        //*** Make destination array
        int xLength = (int)xWidth * (int)xHeight;
        Color[] aColor = new Color[xLength];

        Vector2 vPixelSize = new Vector2(vSourceSize.x / xWidth, vSourceSize.y / xHeight);

        //*** Loop through destination pixels and process
        Vector2 vCenter = new Vector2();
        for (i = 0; i < xLength; i++)
        {

            //*** Figure out x&y
            float xX = (float)i % xWidth;
            float xY = Mathf.Floor((float)i / xWidth);

            //*** Calculate Center
            vCenter.x = (xX / xWidth) * vSourceSize.x;
            vCenter.y = (xY / xHeight) * vSourceSize.y;

            //*** Do Based on mode
            //*** Nearest neighbour (testing)
            if (pFilterMode == ImageFilterMode.Nearest)
            {

                //*** Nearest neighbour (testing)
                vCenter.x = Mathf.Round(vCenter.x);
                vCenter.y = Mathf.Round(vCenter.y);

                //*** Calculate source index
                int xSourceIndex = (int)((vCenter.y * vSourceSize.x) + vCenter.x);

                //*** Copy Pixel
                aColor[i] = aSourceColor[xSourceIndex];
            }

            //*** Bilinear
            else if (pFilterMode == ImageFilterMode.Biliner)
            {

                //*** Get Ratios
                float xRatioX = vCenter.x - Mathf.Floor(vCenter.x);
                float xRatioY = vCenter.y - Mathf.Floor(vCenter.y);

                //*** Get Pixel index's
                int xIndexTL = (int)((Mathf.Floor(vCenter.y) * vSourceSize.x) + Mathf.Floor(vCenter.x));
                int xIndexTR = (int)((Mathf.Floor(vCenter.y) * vSourceSize.x) + Mathf.Ceil(vCenter.x));
                int xIndexBL = (int)((Mathf.Ceil(vCenter.y) * vSourceSize.x) + Mathf.Floor(vCenter.x));
                int xIndexBR = (int)((Mathf.Ceil(vCenter.y) * vSourceSize.x) + Mathf.Ceil(vCenter.x));

                //*** Calculate Color
                aColor[i] = Color.Lerp(
                    Color.Lerp(aSourceColor[xIndexTL], aSourceColor[xIndexTR], xRatioX),
                    Color.Lerp(aSourceColor[xIndexBL], aSourceColor[xIndexBR], xRatioX),
                    xRatioY
                );
            }

            //*** Average
            else if (pFilterMode == ImageFilterMode.Average)
            {

                //*** Calculate grid around point
                int xXFrom = (int)Mathf.Max(Mathf.Floor(vCenter.x - (vPixelSize.x * 0.5f)), 0);
                int xXTo = (int)Mathf.Min(Mathf.Ceil(vCenter.x + (vPixelSize.x * 0.5f)), vSourceSize.x);
                int xYFrom = (int)Mathf.Max(Mathf.Floor(vCenter.y - (vPixelSize.y * 0.5f)), 0);
                int xYTo = (int)Mathf.Min(Mathf.Ceil(vCenter.y + (vPixelSize.y * 0.5f)), vSourceSize.y);

                //*** Loop and accumulate
                Vector4 oColorTotal = new Vector4();
                Color oColorTemp = new Color();
                float xGridCount = 0;
                for (int iy = xYFrom; iy < xYTo; iy++)
                {
                    for (int ix = xXFrom; ix < xXTo; ix++)
                    {

                        //*** Get Color
                        oColorTemp += aSourceColor[(int)(((float)iy * vSourceSize.x) + ix)];

                        //*** Sum
                        xGridCount++;
                    }
                }

                //*** Average Color
                aColor[i] = oColorTemp / (float)xGridCount;
            }
        }

        //*** Set Pixels
        oNewTex.SetPixels(aColor);
        oNewTex.Apply();

        //*** Return
        return oNewTex;
    }



    /// <summary>
    /// Takes the screenshot for text + bubble + image and store the path of this image in filePath variable
    /// </summary>
    private IEnumerator  TakeScreenShotWithBubble()
    {   
        yield return new WaitForEndOfFrame();
        closeBubble_1.SetActive(false);
        closeBubble_2.SetActive(false);
        placeHolder_Bubble_1.SetActive(false);
        placeHolder_Bubble_2.SetActive(false);
        closeBubble_1.SetActive(false);
        closeBubble_2.SetActive(false);
        buttons_Popup_AddTextToAvatar.SetActive(false);
        closeButton_addTextPopupWindow.SetActive(false);

        yield return new WaitForEndOfFrame();
        texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        texture.Apply();

        yield return new WaitForEndOfFrame();
        buttons_Popup_AddTextToAvatar.SetActive(true);
        spriteWithBubble = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));

    }

    // ****************************************************************************************************************************************************
    //                   popup window of ImageView --
    // ****************************************************************************************************************************************************
    /// <summary>
    /// Go to image view popup window.
    /// Set popup window of ImageView as Active and Disable other Popup windows
    /// </summary>
    public void GoToImageViewPopupWindow()
    {
        img_imageViewPopupWindow.sprite = spriteOrginal;

        popup_window_imageView.SetActive(true);
        popup_window_addText.SetActive(false);
        popup_window_sharing.SetActive(false);

    }

    // **************************************************
    //                          Step.2
    // **************************************************
    /// <summary>
    /// Go to add text popup window.
    /// Make popup window of AddText to the avatar is Active and Disable other Popup windows
    /// </summary>
    public void GoToAddTextPopupWindow()
    {   
        StartCoroutine(HideUIElements());

        buttons_Popup_AddTextToAvatar.SetActive(true); // set active the buttons in AddTextPopupWindow if it was hidden

        bubble_1.SetActive(true);
        bubble_2.SetActive(true);
        closeBubble_1.SetActive(true);
        closeBubble_2.SetActive(true);
        placeHolder_Bubble_1.SetActive(true);
        placeHolder_Bubble_2.SetActive(true);

        img_addTextPopupWindow.sprite = spriteOrginal;

        popup_window_imageView.SetActive(false);
        popup_window_addText.SetActive(true);
        popup_window_sharing.SetActive(false);

        closeButton_addTextPopupWindow.SetActive(true);

    }




    // **************************************************
    //                          Step.3
    // **************************************************
    /// <summary>
    /// Go to sharing popup window.
    /// Make popup window of Sharing is Active and Disable other Popup windows
    /// </summary>
    public void GoToSharingPopupWindow()
    {
        // show panel "please wait ... "
        panel_PleaseWait.SetActive(true);

        // Text filter
        StartCoroutine(ValidateTextAPI(URL, text_Bubble_1.text, text_Bubble_2.text));

    }

    public void GoToSharingPopupWindowWithoutBubble()
    {

    }

    // **************************************************
    //                         Cancel
    // **************************************************
    /// <summary>
    /// Cancels the popup windows.
    /// </summary>
    public void CancelPopupWindow()
    {
        //print("CancelPopupWindow");
        StartCoroutine(ShowUIElements());

        popup_window_imageView.SetActive(false);
        popup_window_addText.SetActive(false);
        popup_window_sharing.SetActive(false);


    }

    /// <summary>
    /// Validate the text API.
    /// </summary>
    /// <param name="url">URL.</param>
    /// <param name="message1">Message1.</param>
    /// <param name="message2">Message2.</param>
    IEnumerator ValidateTextAPI(string url, string message1, string message2)
    {

         Message _message = new Message();
        _message.textMessageOne = message1;
        _message.textMessageTwo = message2;

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        headers.Add("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));

#pragma warning disable CS0618 // Type or member is obsolete
        WWW www = new WWW(URL, _message.toPost(), headers);
#pragma warning restore CS0618 // Type or member is obsolete

        yield return www;

        if (www.error != null)
        {
            Debug.Log(www.error);
        }

        else
        {
            Debug.Log(www.text);
            Message responsMessage = JsonUtility.FromJson<Message>(www.text);

            Debug.Log(responsMessage.textMessageOne + " : " + responsMessage.textMessageTwo);

            string[] response = { responsMessage.textMessageOne, responsMessage.textMessageTwo };

            // { "textMessageOne":true,"textMessageTwo":true}

            if (!bool.Parse(response[0]) || !bool.Parse(response[1]))
            {
                // Change The Text

                panel_CommentBlocked.SetActive(true);
                panel_PleaseWait.SetActive(false);


            }
            else if (bool.Parse(response[0]) && bool.Parse(response[1]))
            {
                // Text Are OK
                panel_CommentBlocked.SetActive(false);
                // Hide panel "please wait ... "
                panel_PleaseWait.SetActive(false);

                yield return StartCoroutine(TakeScreenShotWithBubble());

                img_SharingPopupWindow.sprite = spriteWithBubble;
                popup_window_addText.SetActive(false);
                popup_window_sharing.SetActive(true);


            }

        }
    }

    /// <summary>
    /// Button for close the popup of the comment blocked comment.
    /// </summary>
    public void CloseCommentBlockedComment()
    {
        panel_CommentBlocked.SetActive(false);
    }


    // **************************************************
    //                          Skip Button
    // **************************************************
    /// <summary>
    /// Skip button.
    /// </summary>
    public void SkipButton()
    {
        // Go to sharing popupwindow wit set image without bubble "orginal sprite"
        img_SharingPopupWindow.sprite = spriteOrginal;

        bubble_1.SetActive(false);
        bubble_2.SetActive(false);

        popup_window_imageView.SetActive(false);
        popup_window_addText.SetActive(false);
        popup_window_sharing.SetActive(true);

    }

    // **************************************************
    //              Share Button + saveTOGallery
    // **************************************************
    /// <summary>
    /// Share the image and save it to the device gallery.
    /// </summary>
    public void ShareImage()
    {
        StartCoroutine(SaveImage());
    }

    /// <summary>
    /// Close the bubble 1.
    /// </summary>
    public void CloseBubble_1()
    {
        bubble_1.SetActive(false);
    }

    /// <summary>
    /// Close the bubble 2.
    /// </summary>
    public void CloseBubble_2()
    {
        bubble_2.SetActive(false);
    }
}
