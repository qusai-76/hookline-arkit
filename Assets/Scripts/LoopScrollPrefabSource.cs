﻿using UnityEngine;
using System.Collections;
using System;

namespace UnityEngine.UI
{
    [System.Serializable]
    public class LoopScrollPrefabSource 
    {
        public string prefabName;
        public int poolSize = 5;

        private bool inited = false;

        public virtual GameObject GetObject(string type = "image")
        {
            Debug.Log("inside function : "+type);
            if(type == "video")
            {
                prefabName = "videoTmplate";
            }
            else
            {
                prefabName = "imageTemplate";
            }

            //Debug.Log(prefabName);


            if (!inited)
            {
                SG.ResourceManager.Instance.InitPool("videoTmplate", poolSize);
                SG.ResourceManager.Instance.InitPool("imageTemplate", poolSize);
                inited = true;
            }
            return SG.ResourceManager.Instance.GetObjectFromPool(prefabName);
        }

        public virtual void ReturnObject(Transform go)
        {
            go.SendMessage("ScrollCellReturn", SendMessageOptions.DontRequireReceiver);
            SG.ResourceManager.Instance.ReturnObjectToPool(go.gameObject);
        }

        internal object GetObject(string v, object type)
        {
            throw new NotImplementedException();
        }
    }
}
