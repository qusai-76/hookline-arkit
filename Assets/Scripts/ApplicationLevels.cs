﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ApplicationLevels
{
    public static AppFeatures CurrentFeature;

    public enum AppFeatures
    {
        HallOfFame = 1,
        FalshLight = 2,
        Paper_Airplane = 3,
        Airplanes = 4,
        Candles = 5,
        BoomBox = 6
    }

}
