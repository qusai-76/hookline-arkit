﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Smile
{
    public bool leftSmile;
    public bool rightSmile;

    public Smile()
    {
        leftSmile = false;
        rightSmile = false;
    }

    /// <summary>
    /// Retrun True When Left and Right Mouth Edges Are in Smiling Status
    /// </summary>
    /// <returns></returns>
    public bool isSmiling()
    {
        return (leftSmile && rightSmile);
    }

}


