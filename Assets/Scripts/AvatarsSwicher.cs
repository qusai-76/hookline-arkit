﻿using System.Collections;
using System.Collections.Generic;
using Lean.Touch;
using UnityEngine;

public class AvatarsSwicher : MonoBehaviour
{
    public GameObject avatar0;
    public GameObject avatar1;
    public GameObject avatar2;
    public GameObject avatar3;

    public GameObject mainOb;

    public Vector3 position;

    // Start is called before the first frame update
    void Start()
    {
      // avatar0.transform.position = position;
       avatar0.SetActive(false);
       avatar1.SetActive(false);
       avatar2.SetActive(false);
       avatar3.SetActive(false);
    }
}
