﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TouchableUI : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
    [SerializeField]
    [Tooltip("How long must pointer be down on this object to trigger a long press")]
    private float holdTime = 1f;

    // Remove all comment tags (except this one) to handle the onClick event!
    private bool held = false;
    private bool onHold = false;
    private bool pressed=false;
    public UnityEvent onClick = new UnityEvent();

    public UnityEvent onTouchDown = new UnityEvent();
    public UnityEvent onTouchUp = new UnityEvent();
    public UnityEvent onLongPress = new UnityEvent();

    private const float MaxRecordingTime = 5000f; // seconds

    public GameObject avatar;


    private void Start()
    {
        Reset();
    }
    private void Reset()
    {
        // Reset fill amounts
        avatar.SetActive(false);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        held = false;
        Invoke("OnLongPress", holdTime);
        StartCoroutine(Countdown());
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        CancelInvoke("OnLongPress");

        if (!held)
            onClick.Invoke();
            
        //pressed = false;

        if (!held)
            Debug.Log("After Click");
        else
            Debug.Log("After Long");
    }
    private IEnumerator Countdown()
    {
        pressed = true;
        // First wait a short time to make sure it's not a tap
        yield return new WaitForSeconds(holdTime);
        if (!pressed) yield break;
        // Start recording
        if (onTouchDown != null) onTouchDown.Invoke();
        // Animate the countdown
        float startTime = Time.time, ratio = 0f;
        avatar.SetActive(true);  
        
        while(pressed && (ratio = (Time.time -startTime)/MaxRecordingTime)<1.0f)
        {
             //Debug.Log("init");
            int nbTouches = Input.touchCount;
 
             if(nbTouches > 0)
             {
                 // Debug.Log("Here first");
                 for (int i = 0; i < nbTouches; i++)
                 {
                     Touch touch = Input.GetTouch(i);
                      
                     if(touch.phase == TouchPhase.Began)
                     {
                        // Debug.Log("Here");
                        Vector3 position = Camera.main.ScreenToWorldPoint(new Vector3( touch.position.x,touch.position.y,1f));
                        avatar.transform.position = position;
                     }
     
                 }
             }
             
           
            
            yield return null;
        }

    }
    public void OnPointerExit(PointerEventData eventData)
    {
        CancelInvoke("OnLongPress");
    }


    private void OnLongPress()
    {
        held = true;
        onLongPress.Invoke();
    }
    
    

    
}