﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ToutchSlider : MonoBehaviour {


    public GameObject slider;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            // Check if finger is over a UI element
            if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
            {
                Debug.Log("Touched the UI");
            }
            else{
                if(slider.active==true)
                {
                    slider.SetActive(false);
                }
            }
        }


        // Check if the left mouse button was clicked
        if (Input.GetMouseButtonDown(0))
         {
            // Check if the mouse was clicked over a UI element
            if (EventSystem.current.IsPointerOverGameObject())
            {
                 Debug.Log("Clicked on the UI");
            }
            else
            {
                if (slider.active == true) {
                    slider.SetActive(false);
                }
            }
        }
    }
}
