﻿
using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.SceneManagement;

public class AndroidSplitLoadFirstScene : MonoBehaviour 
{
    //Only use in Preloader Scene for android split APK
    private string  nextScene = "Login";
    private bool obbisok = false;
    private bool loading = false;
    private bool replacefiles = false;
    //true if you wish to over copy each time
    
    private string[] paths = {  
        "Vuforia/BoomBox.dat",
        "Vuforia/BoomBox.xml",
        "Vuforia/HallOfFame.dat",
        "Vuforia/HallOfFame.xml",
        "Vuforia/Roxy_Sign.dat",
        "Vuforia/Roxy_Sign.xml",
    };
    
    
    public IEnumerator Run()
    { 
       if (Application.platform == RuntimePlatform.Android) {
            if (Application.dataPath.Contains (".obb") && !obbisok) {
                yield return StartCoroutine (CheckSetUp ());
                obbisok = true;
            }
        }
        yield return null;
        //else 
        //{
        //   if (!loading) {
           
        //        if (PlayerPrefs.GetInt("logged") == 0) { 
        //            UnityEngine.SceneManagement.SceneManager.LoadScene("Logoin");
        //        }
        //        else {
        //             TimerScript.AccessHallOfFame();
        //        }

        //        StartApp ();
        //    }
        //}
    }

    //public void StartApp ()
    //{
    //    loading = true;
    //    SceneManager.LoadSceneAsync(nextScene);
    //}

    public IEnumerator CheckSetUp ()
    {
        //Check and install!
        for (int i = 0; i < paths.Length; ++i) {
            yield return StartCoroutine (PullStreamingAssetFromObb (paths [i]));
        }
        yield return new WaitForSeconds (0.5f); 
    }
    
    //Alternatively with movie files these could be extracted on demand and destroyed or written over
    //saving device storage space, but creating a small wait time.
    public IEnumerator PullStreamingAssetFromObb (string sapath)
    { 
        if (!File.Exists (Application.persistentDataPath + sapath) || replacefiles) {
            WWW unpackerWWW = new WWW (Application.streamingAssetsPath + "/" + sapath);
            while (!unpackerWWW.isDone) {
                yield return null;
            }
            if (!string.IsNullOrEmpty (unpackerWWW.error)) {
                Debug.Log ("Error unpacking:" + unpackerWWW.error + " path: " + unpackerWWW.url);
                
                yield break; //skip it
            } else {
                Debug.Log ("Extracting " + sapath + " to Persistant Data");
                
                if (!Directory.Exists (Path.GetDirectoryName (Application.persistentDataPath + "/" + sapath))) {
                    Directory.CreateDirectory (Path.GetDirectoryName (Application.persistentDataPath + "/" + sapath));
                }
                File.WriteAllBytes (Application.persistentDataPath + "/" + sapath, unpackerWWW.bytes);
            }
        }
        yield return 0;
    }
}