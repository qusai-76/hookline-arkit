﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirPlaneTouchHandler : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            //acumTime += Input.GetTouch(0).deltaTime;
            Debug.Log(" Touch : ");

            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit hit;

            if (Input.GetTouch(0).phase == TouchPhase.Began) {
               
                 if (Physics.Raycast(ray, out hit)) {
                 
                    Debug.Log(hit.transform.name);
                    
                    GameObject paperAirplane = GameObject.Find(hit.transform.name);
                    Animator anim = paperAirplane.GetComponent<Animator>();
                    anim.Play("Fly",0,0);
                }
            }

            if (Input.GetTouch(0).phase == TouchPhase.Ended) {

            }
            
        }
    }
}

