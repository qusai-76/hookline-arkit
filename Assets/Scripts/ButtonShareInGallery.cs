﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonShareInGallery : MonoBehaviour
{

    public void ButtonShare(Text filePath)
    {
        Debug.Log("Path File in Button Share Is: " + filePath.text);
        new NativeShare().AddFile(filePath.text).Share();

    }
}
