﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickableButton : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Print()
    {
        Debug.Log(" Click ");
    }


    public void PrintLongPress()
    {
        Debug.Log(" Long Press  ");
    }

    public void TouchDown()
    {
        Debug.Log(" TouchDown  ");
    }

    public void TouchUP()
    {
        Debug.Log(" TouchUP  ");
    }
}
