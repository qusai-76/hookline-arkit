﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.IO;
using System.Linq;


namespace SG
{
    [RequireComponent(typeof(UnityEngine.UI.LoopScrollRect))]
    [DisallowMultipleComponent]
    public class InitOnStart : MonoBehaviour
    {
        public int totalCount = -1;

        public string appPath;
        FileInfo[] fileOfImagesForCells;

        private void Awake()
        {

        }
        void Start()
        {
            var ls = GetComponent<LoopScrollRect>();

            //// Read All Image and Video Files and Store them in Array or List
            LoadImageAndVideoPaths();

            totalCount = fileOfImagesForCells.Length;

            ls.totalCount = totalCount;

            // Add Parameter the List of Combined Array
            ls.RefillCells(0, fileOfImagesForCells);
        }

        private void LoadImageAndVideoPaths()
        {
            appPath = Application.persistentDataPath;

            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(appPath);

            System.IO.FileInfo[]  filesOfImages = di.GetFiles("*.jpg");   
            System.IO.FileInfo[]  filesVideosMP4 = di.GetFiles("*.mp4");
            System.IO.FileInfo[]  filesVideosMov = di.GetFiles("*.mov");

            FileInfo[] newArray1 = new FileInfo[filesOfImages.Length + filesVideosMP4.Length];
            Array.Copy(filesOfImages, newArray1, filesOfImages.Length);
            Array.Copy(filesVideosMP4, 0, newArray1, filesOfImages.Length, filesVideosMP4.Length);

            fileOfImagesForCells = new FileInfo[newArray1.Length + filesVideosMov.Length];
            Array.Copy(newArray1, fileOfImagesForCells, newArray1.Length);
            Array.Copy(filesVideosMov, 0, fileOfImagesForCells, newArray1.Length, filesVideosMov.Length);

            var result = fileOfImagesForCells.ToList<FileInfo>().OrderBy(ob => ob.CreationTime);
            fileOfImagesForCells = result.ToArray<FileInfo>();

            Array.Reverse(fileOfImagesForCells);
            
        }

        public IEnumerator GetNumberOfImagesAndVideosInDevice()
        {
           DirectoryInfo di = new DirectoryInfo(appPath);
           FileInfo[] fileOfImages = di.GetFiles("*jpg");
           FileInfo[] fileOfVideos = di.GetFiles("*mp4");

           totalCount = fileOfImages.Length + fileOfVideos.Length;
       
           yield return null;
        }

    }
}