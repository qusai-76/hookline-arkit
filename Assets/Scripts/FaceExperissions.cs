﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceExperissions 
{
    // Mouth Smile
    public static string SMILE_LEFT = "mouthSmile_L";
    public static string SMILE_RIGHT = "mouthSmile_R";

    //Eyes
    public static string EYE_BLINK_LEFT = "eyeBlink_L";
    public static string EYE_BLINK_RIGHT = "eyeBlink_R";

}
