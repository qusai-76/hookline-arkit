﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.Networking;
using Assets.Models;

public class Gallery_Videos : MonoBehaviour {



    // ****************************
    //      parametrs For Images
    // ****************************
    List<Texture2D> textures;
    public GameObject templateForImage;
    // Array for store images paths files  
    private FileInfo[] filesOfImages;


    public Text pathImageInTemplate;

    private GameObject cellToDelete;

    // ****************************
    //      parametrs For Videos
    // ****************************
    public GameObject videoCellTemplate;

    public Text pathVideoForShareing;

    public VideoPlayer videoPlayerDisplay;
    public VideoSource videoSourceDisplay;
    public AudioSource audioSourceDisplay;

    public GameObject clothes;

    public RawImage videoDisplayRawImage;

    private String statueVideo = "Play";

    public Sprite playButtonSprite;
    public Sprite pauseButtonSprite;

    public GameObject display_StatueVideo;
    public GameObject videoDisplayPanel;
    private bool videoDisplayPanelStatue = false;

    //private FileInfo[] filesVideos;
    private FileInfo[] filesVideosMP4;
    private FileInfo[] filesVideosMov;

    public GameObject imageView_Display;
    public GameObject videoDisplay;

    public GameObject buttonShare;
    public GameObject pathFileForSharing;

    public RawImage imageInVideoCellTemplate;
    public Texture testTexture;

    public GameObject Display;

    public GameObject DialogBox;

    private ulong countFrameInVideo = 10;
    private long currentFrame = 0;

    public Text FullNameForProfile;

    public Image profileImage;

    public GameObject video_NoPostYet;
    public GameObject content;


    // For Load Images And Videos From Server
    public List<string> videosIDS;
    public List<int> phptosIDS;
    public List<string> avatarIDS;
    public List<string> thumbnailIDS;

    public List<String> numsPhotosInServer;
    public List<String> numsPhotosInDeveice;

    public List<String> numsVideosInServer;
    public List<String> numsVideosInDeveice;


    public PhotoModel[] photos;
    public VideoModel[] videos;

    

    // Use this for initialization
    void Start() {
        RefreshGalleryClass.refreshGallery = false;

        //Debug.Log("STARRT GALLERY");
        Debug.Log(Application.persistentDataPath);

        //StartCoroutine("LoadImagesAndVideo");



        //print(PlayerPrefs.GetString("auth_token"));

        // Set FirstName And LastName For User Profile
        if (PlayerPrefs.GetString("firstname") != null && PlayerPrefs.GetString("lastname") != null)
        {
            FullNameForProfile.text = PlayerPrefs.GetString("firstname") +" "+ PlayerPrefs.GetString("lastname");
        }
        else if (PlayerPrefs.GetString("firstname") != null && PlayerPrefs.GetString("lastname") == null)
        {
            FullNameForProfile.text = PlayerPrefs.GetString("firstname");
        }
        else
        {
            FullNameForProfile.text = "";
        }


        // upload photo of profile from server
        if (!string.IsNullOrEmpty(PlayerPrefs.GetString("profileimage")))
        {
            StartCoroutine(UploadPhotoFromUrl(PlayerPrefs.GetString("profileimage")));
        }
    }

    void LoadGallery() {
        //RefreshGalleryClass.refreshGallery = false;
        StartCoroutine("LoadImagesAndVideo");
    }

    void Update()
    {
        //-------------
        if(RefreshGalleryClass.refreshGallery == true)
        {
            Debug.Log("Refresh The Gallery");
            RefreshGalleryClass.refreshGallery = false;

            //Delete previous images and videos
            foreach (Transform child in content.transform)
            {
                Destroy(child.gameObject);
            }

            // Reload Images and video
            LoadGallery();

        }
        //-------------



        //if(display_StatueVideo == true)
        //{
        if (videoPlayerDisplay.isPlaying)
        {
            //print("Current Frame is: " + videoPlayerDisplay.frame);
            currentFrame = videoPlayerDisplay.frame;

            display_StatueVideo.GetComponent<Image>().sprite = pauseButtonSprite;


            //if (videoDisplayPanelStatue == true && statueVideo == "Play")
            //{
            //    StartCoroutine(HidePanelDisplayAfterTime(3));
            //}

        }
        //}

        if (currentFrame == ((long)countFrameInVideo - 1))
        {
            //print("VIDEOOO IS FINISH");
            currentFrame = -2; // i put -2 only for stop loop this condition 

            videoDisplayPanel.SetActive(true);

            statueVideo = "Pause";
            display_StatueVideo.GetComponent<Image>().sprite = playButtonSprite;


        }

    }

    // ***********************************************************
    // ===========================================================
    //                      Images + Videos Function
    // ===========================================================
    // ***********************************************************

    public IEnumerator LoadImagesAndVideo()
    {
        print("Start Function LoadImages");  // removing this line

        //textures = new List<Texture2D>();
        DirectoryInfo di = new DirectoryInfo(Application.persistentDataPath);

        filesOfImages = di.GetFiles("*.jpg");   // store path  images in array

        //#if UNITY_EDITOR || UNITY_IOS
        filesVideosMP4 = di.GetFiles("*.mp4");   // store path of  videos in array
        filesVideosMov = di.GetFiles("*.mov");
        //#endif

        FileInfo[] newArray1 = new FileInfo[filesOfImages.Length + filesVideosMP4.Length];
        Array.Copy(filesOfImages, newArray1, filesOfImages.Length);
        Array.Copy(filesVideosMP4, 0, newArray1, filesOfImages.Length, filesVideosMP4.Length);

        FileInfo[] newArray = new FileInfo[newArray1.Length + filesVideosMov.Length];
        Array.Copy(newArray1, newArray, newArray1.Length);
        Array.Copy(filesVideosMov, 0, newArray, newArray1.Length, filesVideosMov.Length);

        var result = newArray.ToList<FileInfo>().OrderBy(ob => ob.CreationTime);
        newArray = result.ToArray<FileInfo>();

        Array.Reverse(newArray);

        foreach (var file in newArray)
        {
            if (file.FullName.Contains(".jpg"))
            {
                yield return StartCoroutine(CreateSprite(file.FullName));  //here create sprite  after get on all paths for image and raw-image for Image 
                AddNumPhotoInDeviceToList(file.Name);
            }
            else if (file.FullName.Contains(".mov") || file.FullName.Contains(".mp4"))
            {
                //yield return StartCoroutine(VideoDisplayInCell(file.FullName));
                AddNumVideosInDeviceToList(file.Name);
            }

            //StartCoroutine(CreateTexture(imageFile.FullName));  //here create sprite  after get on all paths for image and raw-image for video 

        }


        //yield return null;

        // For Load photos and videos From server
        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));

        //string VideosURL = API_Settings.STAGING_SERVICE_URL + API_Settings.LOAD_ALL_VIDEOS_INFOS_URL;
        //string PhotosURL = API_Settings.STAGING_SERVICE_URL + API_Settings.LOAD_ALL_PHOTOS_INFOS_URL;
        //string ThumbnailsURL = API_Settings.STAGING_SERVICE_URL + API_Settings.LOAD_ALL_THUMBNAILS_URL;

        //yield return StartCoroutine(SyncPhotos(PhotosURL, headers));
        //yield return StartCoroutine(SyncVideos(VideosURL, headers));
        yield return StartCoroutine(GalleryIsEmpty());
    }

    // 
    private void AddNumPhotoInDeviceToList(string numPhotoInDevice)
    {
        numPhotoInDevice = numPhotoInDevice.Replace("pic-", "");
        numPhotoInDevice = numPhotoInDevice.Replace(".jpg", "");
        numsPhotosInDeveice.Add(numPhotoInDevice);
    }

    // 
    private void AddNumVideosInDeviceToList(string numVideoInDevice)
    {
        numVideoInDevice = numVideoInDevice.Replace("video-", "");
        numVideoInDevice = numVideoInDevice.Replace(".mp4", "");
        numsVideosInDeveice.Add(numVideoInDevice);
    }

    // ***********************************************************
    // ===========================================================
    //                      Images Functions
    // ===========================================================
    // ***********************************************************

    // ============================================================================================================================================
    //                  Create path of image to sprite and instantiate the "TemplateOfImage" For generate cell of image inside Gallery
    // ============================================================================================================================================

    public IEnumerator CreateSprite(String pathOfFile)
    {
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //                  convert url of image to sprite
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        String fileToLoad = GetCleanFileName(pathOfFile);

        Debug.Log("Loading Image from path: " + fileToLoad);

        WWW www = new WWW(fileToLoad);
        //yield return www;

        print("WWWWW");
        print(www.texture);

        Texture2D loadedTexture = new Texture2D(1, 1);

        www.LoadImageIntoTexture(loadedTexture);

        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //                               create sprite
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        Sprite spriteForImage = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
        yield return spriteForImage;

        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //  Instantiate the template of image for add sprite in collectionView for gallery
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //GameObject imageObject = Instantiate(templateForImage) as GameObject;
        GameObject imageObject = (GameObject)Instantiate(templateForImage, transform);

        imageObject.transform.GetChild(0).GetComponent<Image>().sprite = spriteForImage;

        //pathImageInTemplate.text = pathOfFile;
        imageObject.transform.Find("pathOfImage").GetComponent<Text>().text = pathOfFile;

        Resources.UnloadUnusedAssets();

    }

    public IEnumerator CreateTexture(String pathOfFile)
    {
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //                  convert url of image to sprite
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        String fileToLoad = GetCleanFileName(pathOfFile);

        Debug.Log("Loading Image from path: " + fileToLoad);

        WWW www = new WWW(fileToLoad);
        yield return www;

        print("WWWWW");
        print(www.texture);

        Texture2D loadedTexture = new Texture2D(1, 1);

        www.LoadImageIntoTexture(loadedTexture);



        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //                               create sprite
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //Sprite spriteForImage = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
        //yield return spriteForImage;

        yield return loadedTexture;

        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //  Instantiate the template of image for add sprite in collectionView for gallery
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //GameObject imageObject = Instantiate(templateForImage) as GameObject;
        GameObject imageObject = (GameObject)Instantiate(templateForImage, transform);

        //imageObject.transform.GetChild(0).GetComponent<Image>().sprite = spriteForImage;
        imageObject.transform.GetChild(0).GetComponent<RawImage>().texture = loadedTexture;

        //pathImageInTemplate.text = pathOfFile;
        imageObject.transform.Find("pathOfImage").GetComponent<Text>().text = pathOfFile;

        Resources.UnloadUnusedAssets();

    }

    public void ViewImageInImageViewAfterClickOnImage(Button button)
    {
        imageView_Display.GetComponent<Image>().sprite = button.transform.GetChild(0).GetComponent<Image>().sprite;
        //imageView_Display.GetComponent<RawImage>().texture = button.transform.GetChild(0).GetComponent<RawImage>().texture;

        // store the gameObject in temprorary gameobjec as reference for delete it after click on trash in popup
        cellToDelete = button.gameObject;


        Display.SetActive(true);


        imageView_Display.SetActive(true);
        videoDisplay.SetActive(false);
        buttonShare.SetActive(true);
    }
    //Add path of image to pathFileForSharing In Button Share
    public void AddPathImageToShareButton(Text txt)
    {
        pathFileForSharing.GetComponent<Text>().text = txt.text;
    }

    // ***********************************************************
    // ===========================================================
    //                      Videos Functions
    // ===========================================================
    // ***********************************************************
    // Display Video In Cell
    // Use this for initialization
    IEnumerator VideoDisplayInCell(string filename)
    {
        // filename = GetCleanFileName(filename);
        filename = string.Format("file://{0}", filename);


        Debug.Log("Loading Video from the path: " + filename);

        GameObject videoObject = (GameObject)Instantiate(videoCellTemplate, transform);
        VideoPlayer videoPlayer = videoObject.GetComponent<VideoPlayer>();

        // Video clip from Url
        videoPlayer.source = VideoSource.Url;
        videoPlayer.url = filename;

        // Set video To Play then prepare Audio to prevent Buffering
        videoPlayer.Prepare();

        // Wait until video is prepared
        while (!videoPlayer.isPrepared)
        {
            yield return null;
        }

        //Debug.Log("Done Preparing Video");
        //videoObject
        //RawImage image = videoObject.GetComponent<RawImage>();
        RawImage image = videoObject.transform.Find("ImageInVideoCellTemplate").GetComponent<RawImage>();


        //RectTransform rt = image.rectTransform;
        RectTransform rt = imageInVideoCellTemplate.rectTransform;

        //Debug.Log("Cell width : "+ rt.rect.width);
        //Debug.Log("Cell height : "+ rt.rect.height);

        //Debug.Log("Vid org width : " + videoPlayer.texture.width);
        //Debug.Log("Vid org height : " + videoPlayer.texture.height);


        float originalWidth = videoPlayer.texture.width;
        float originalHeight = videoPlayer.texture.height;

        // Respect Ratio Code
        float ratio = 1;

        if (originalWidth >= originalHeight)
        {
            ratio = (float)originalHeight / (float)originalWidth;
            //Debug.Log("Ratio : " + ratio);
            float height = ratio * rt.rect.width;
            rt.sizeDelta = new Vector2(rt.rect.width, height);
        }

        videoPlayer.Play();


        //image.texture = videoPlayer.texture;
        image.texture = videoPlayer.texture;
        //image.texture = testTexture;  // for testing

        videoPlayer.Pause();
    }


    // ===============================
    //          Play Video 
    // -------------------------------
    // Use this for initialization
    IEnumerator PlayVideo(string filename)
    {

        print("Start");
        //Add VideoPlayer to the GameObject
        videoPlayerDisplay = videoDisplayRawImage.GetComponent<VideoPlayer>();

        //Add AudioSource
        audioSourceDisplay = videoDisplayRawImage.GetComponent<AudioSource>();

        //Disable Play on Awake for both Video and Audio
        videoPlayerDisplay.playOnAwake = false;
        audioSourceDisplay.playOnAwake = false;
        audioSourceDisplay.Pause();

        // Video clip from Url
        videoPlayerDisplay.source = VideoSource.Url;
        videoPlayerDisplay.url = filename;


        //Set Audio Output to AudioSource
        videoPlayerDisplay.audioOutputMode = VideoAudioOutputMode.AudioSource;

        //Assign the Audio from Video to AudioSource to be played
        videoPlayerDisplay.EnableAudioTrack(0, true);
        videoPlayerDisplay.SetTargetAudioSource(0, audioSourceDisplay);

        //Set video To Play then prepare Audio to prevent Buffering
        videoPlayerDisplay.Prepare();

        //Wait until video is prepared
        while (!videoPlayerDisplay.isPrepared)
        {

            yield return null;
        }

        Debug.Log("Done Preparing Video");


        RectTransform rt = videoDisplayRawImage.rectTransform;

        float originalWidth = videoPlayerDisplay.texture.width;
        float originalHeight = videoPlayerDisplay.texture.height;

        // Respect Ratio Code
        //float ratio = 1;


        //if (originalWidth > originalHeight)
        //{
        //    ratio = (float)originalHeight / (float)originalWidth;

        //    float height = ratio * rt.rect.width;
        //    rt.sizeDelta = new Vector2(rt.rect.width, height);
        //}

        //else
        //{
        //    ratio = (float)originalWidth / (float)originalHeight;
        //    float width = ratio * rt.rect.height;
        //    rt.sizeDelta = new Vector2(width, rt.rect.height);
        //}

        videoDisplayRawImage.texture = videoPlayerDisplay.texture;

        //Play Video
        videoPlayerDisplay.Play();

        //Play Sound
        audioSourceDisplay.Play();

        //Debug.Log("Playing Video");


        //Debug.Log("Done Playing Video");

        print("Number of Frames is: " + videoPlayerDisplay.frameCount);
        //print("Current Frame is: " + videoPlayerDisplay.frame);

        countFrameInVideo = videoPlayerDisplay.frameCount;


    }

    // Pause the video
    public void PauseVideo()
    {
        videoPlayerDisplay.Pause();
    }

    //  play the video
    public void PlayVideo()
    {
        videoPlayerDisplay.Play();
    }


    // Action Click On Cell video  In CollectionView for play video in pupup
    public void OnClickedCell(Button button)
    {

        String videoPath = button.GetComponent<VideoPlayer>().url;
        //print("videoPath IS: "+ videoPath);
        //Handheld.PlayFullScreenMovie( videoPath);

        StartCoroutine(PlayVideo(videoPath));

        // Store Path in Text
        pathVideoForShareing.GetComponent<Text>().text = button.GetComponent<VideoPlayer>().url;



#pragma warning disable CS0618 // Type or member is obsolete

        // #Change  Statue Video in  other Cells that user click on it To Puse
        int numOfChildOfVideoCollectionView = gameObject.transform.childCount;

        for (int i = 0; i < numOfChildOfVideoCollectionView; i++)
        {
            Transform childInVideoCollectionView = gameObject.transform.GetChild(i);
            GameObject childGameObjectVideoCollectionView = childInVideoCollectionView.gameObject;

            int numOfChildInChildInVideoCollectionView = childGameObjectVideoCollectionView.transform.childCount;

            // for get all child in child in  videoCollectionView
            for (int j = 0; j < numOfChildInChildInVideoCollectionView; j++)
            {
                Transform childInChildVideoCollectionView = childGameObjectVideoCollectionView.transform.GetChild(j);
                GameObject childInChildGameObjectVideoCollectionView = childInChildVideoCollectionView.gameObject;

                // Change Statue Video From Puse To Play
                if (childInChildGameObjectVideoCollectionView.name == "Play")
                {
                    childInChildGameObjectVideoCollectionView.SetActive(true);
                }
                if (childInChildGameObjectVideoCollectionView.name == "Pause")
                {
                    childInChildGameObjectVideoCollectionView.SetActive(false);
                }

                //Debug.Log("child Name: " + child.name);
            }

        }


        // # Change Statue Video From Puse To Play
        int numOfChild = button.transform.childCount;
        // for get all child in Button
        for (int i = 0; i < numOfChild; i++)
        {
            Transform child = button.transform.GetChild(i);
            GameObject childGameoBJECT = child.gameObject;

            // Change Statue Video From Puse To Play
            if (child.name == "Play")
            {
                childGameoBJECT.SetActive(false);
            }
            if (child.name == "Pause")
            {
                childGameoBJECT.SetActive(true);
            }

            Debug.Log("child Name: " + child.name);

            Display.SetActive(true);
        }



#pragma warning restore CS0618 // Type or member is obsolete


        statueVideo = "Play";
        display_StatueVideo.GetComponent<Image>().sprite = pauseButtonSprite;

        // Hide StatuVideoButton when playing Video

        videoDisplay.SetActive(true);
        imageView_Display.SetActive(false);
        buttonShare.SetActive(true);

        // store the gameObject in temprorary gameobjec as reference for delete it after click on trash in popup
        cellToDelete = button.gameObject;



        //store path of video after click on it to text in buttonShare
        var pathVideo = button.GetComponent<VideoPlayer>().url;  // i uswe str.replace for delete file:///
        pathVideo = pathVideo.Replace("file://", "");
        pathFileForSharing.GetComponent<Text>().text = pathVideo;
        ; }

    public void OnClickedDisplayVidoPlayPuse()
    {

        // Changing Statue of the video-display
        if (statueVideo == "Play")
        {
            statueVideo = "Pause";
            videoPlayerDisplay.Pause();

            display_StatueVideo.GetComponent<Image>().sprite = playButtonSprite;

        }
        else if (statueVideo == "Pause")
        {
            statueVideo = "Play";
            videoPlayerDisplay.Play();

            display_StatueVideo.GetComponent<Image>().sprite = pauseButtonSprite;
        }



    }

    public void OnClickDisplayVideo()
    {
        // shwo Panel of Statue Video After touching of video-display 

        // if() {
        ToggleVideoDisplayPanel();
        // }



    }

    private static string GetCleanFileName(string originalFileName)
    {
        string fileToLoad = originalFileName.Replace('\\', '/');

        if (fileToLoad.StartsWith("http", StringComparison.CurrentCulture) == false)
        {
            fileToLoad = string.Format("file://{0}", fileToLoad);
        }
        return fileToLoad;
    }


    void ToggleVideoDisplayPanel()
    {
        if (videoDisplayPanelStatue == false)
        {
            videoDisplayPanelStatue = true;
            videoDisplayPanel.SetActive(true);


        }
        else
        {

            videoDisplayPanelStatue = false;
            videoDisplayPanel.SetActive(false);

            // Hide Panel of Statue Video After peroid of Time
            //StartCoroutine(HidePanelDisplayAfterTime(2));

        }




    }

    IEnumerator HidePanelDisplayAfterTime(int time)
    {
        yield return new WaitForSeconds(time);

        if (videoDisplayPanelStatue == true && statueVideo == "Play")
        {
            videoDisplayPanel.SetActive(false);
        }
    }

    public void GoToHallOfFame()
    {
#pragma warning disable CS0618 // Type or member is obsolete
        SceneManager.UnloadScene("Gallery");
#pragma warning restore CS0618 // Type or member is obsolete
        
        SceneManager.LoadScene(TimerScript.CurrentScene);
    }

    public void CloseDisplay()
    {
        Display.SetActive(false);
        videoDisplayPanel.SetActive(false);
    }

    // Button For Delete Image Or Video
    public void ButtonDeleteFile()
    {
        DialogBox.SetActive(true);

    }

    public void ButtonDeleteFile_InDialogBox()

    {   // delete file from deveice
        string pathFileForSharing_ForDeleteing = pathFileForSharing.GetComponent<Text>().text;
        File.Delete(pathFileForSharing_ForDeleteing);

        // delete file from server
        int index = pathFileForSharing_ForDeleteing.ToString().LastIndexOf('/') + 1;
        string nameFileForDeletingFromServer = pathFileForSharing_ForDeleteing.ToString().Substring(index);
        //Debug.Log("File name For Delete this file from server : " + nameFileForDeletingFromServer);

        // If this file is photo call DeletePhoto -- else call delete video
        if (nameFileForDeletingFromServer.Contains(".jpg")){
            StartCoroutine(DeletePhotoFromServer(nameFileForDeletingFromServer));
        }
        else if (nameFileForDeletingFromServer.Contains(".mp4") || nameFileForDeletingFromServer.Contains(".mov")){
            StartCoroutine(DeleteVideoFromServer(nameFileForDeletingFromServer));
        }


        //cellToDelete.SetActive(false);
        Destroy(cellToDelete);
        Display.SetActive(false);

        DialogBox.SetActive(false);

    }

    public void CancelDeleteFile_InDialogBox()
    {
        DialogBox.SetActive(false);

    }


    // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    //         Copy the next Code from Qusai and Edit i 
    // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // ===========================================================
    // -----------------------------------------------------------
    //                 Sync Photos and load photos from server
    // -----------------------------------------------------------
    // ===========================================================



    // ******************************************************
    // ======================================================
    //                       Load Photos From Server
    // ======================================================
    // ******************************************************
    private IEnumerator SyncPhotos(string PhotosURL, Dictionary<string, string> headers)
    {   
        Debug.Log(" Calling Photos ");

        using (UnityWebRequest www = UnityWebRequest.Get(PhotosURL))
        {
            www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(" Download Photos Error : " + www.error);
            }
            else
            {   
                //Debug.Log(www.downloadHandler.text);
                string response = www.downloadHandler.text;
                photos = JsonHelper.FromJson<PhotoModel>(response);

                foreach (PhotoModel photo in photos)
                {
                    //Debug.Log("photo ID: " + photo.id);
                    //Debug.Log("photo File Name: " + photo.fileName);

                    // Get Only on the num of photo
                    String numPhotoInServer = photo.fileName.Replace("pic-", "");


                    if (!(numsPhotosInDeveice.Contains(numPhotoInServer)))
                    {
                        // start load photo to device 
                        //Debug.Log("Start Load Photo To Device");
                        StartCoroutine(DownloadPhotoByID(API_Settings.DOWNLOAD_PHOTO_URL + photo.id));
                    }


                }

            }
        }
    }


    // ======================================================
    //                Download Photo By ID
    // ======================================================
    public IEnumerator DownloadPhotoByID(string URL)
    {
        Debug.Log("Start DownloadPhotoByID");

        using (UnityWebRequest www = UnityWebRequest.Get(URL))
        {
            www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(" Download Photo Error : " + www.error);
            }
            else
            {   
                //Debug.Log(www.downloadHandler.text);
                string response = www.downloadHandler.text;

                PhotoModel model = JsonUtility.FromJson<PhotoModel>(response);

                //Debug.Log("Extention : " + model.fileExtention);

                string filePath = Application.persistentDataPath + "/" + model.fileName + model.fileExtension;
                System.IO.File.WriteAllBytes(filePath, System.Convert.FromBase64String(model.photoContent));
                Debug.Log("Write Photo to persistentDataPath");

                yield return StartCoroutine(CreateSprite(filePath));
            }
        }

        // show image in gallery after download it to presistaint data


        //yield return 0;

    }


    // ******************************************************
    // ======================================================
    //                       Load Videos From Server
    // ======================================================
    // ******************************************************
    private IEnumerator SyncVideos(string VideosURL, Dictionary<string, string> headers)
    {   
        Debug.Log(" Calling Videos ");

        using (UnityWebRequest www = UnityWebRequest.Get(VideosURL))
        {   
            www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(" Download Photos Error : " + www.error);
            }
            else
            {
                //Debug.Log(www.downloadHandler.text);
                string response = www.downloadHandler.text;
                videos = JsonHelper.FromJson<VideoModel>(response);

                foreach (VideoModel video in videos)
                {
                    // Get Only on the num of video
                    String numVideoInServer = video.fileName.Replace("video-", "");

                    if (!(numsPhotosInDeveice.Contains(numVideoInServer)))
                    {
                        // start load video to device 
                        Debug.Log("Start Load video To Device");
                        StartCoroutine(DownloadVideoByID(video.id));
                    }

                }

            }
        }
    }



    //public void DownlodVideos(List<string> videosIds)
    //{
    //    foreach (string vidID in videosIds)
    //    {
    //        StartCoroutine(DownloadVideo(vidID));
    //    }
    //}


    private IEnumerator DownloadVideoByID(string vidID)
    {   
        Debug.Log("Downloading video with id : " + vidID);
        using (UnityWebRequest www = UnityWebRequest.Get(API_Settings.DOWNLOAD_VIDEO_URL + vidID))
        {
            //Debug.Log("Token : "+ PlayerPrefs.GetString("auth_token"));
            www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(" Download Video Error : " + www.error);
            }
            else
            {
                string response = www.downloadHandler.text;
                Debug.Log("response : " + response);
                VideoModel vid = JsonUtility.FromJson<VideoModel>(response);

                Debug.Log("Model : " + vid.videoContent.Length);
                string filePath = Application.persistentDataPath + "/" + vid.fileName + vid.fileExtension;
                Debug.Log(filePath);

                Debug.Log("Data Length : " + System.Convert.FromBase64String(vid.videoContent).Length);
                System.IO.File.WriteAllBytes(filePath, System.Convert.FromBase64String(vid.videoContent));

                if (File.Exists(filePath))
                {
                    Debug.Log("Success Dwonload video  : " + vid.fileName);

                    yield return StartCoroutine(VideoDisplayInCell(filePath));
                }
                else
                {
                    Debug.Log("Couldn't Write " + vid.fileName);
                }
            }
        }
    }



    // ======================================================
    //                  DELETE PHOTO FROM SERVER By Name it
    // ======================================================
    private IEnumerator  DeletePhotoFromServer(string photoName )
    {
        photoName = photoName.Replace(".jpg", "");
        Debug.Log("Delete The Next Photo From Server:  " + photoName);
        using (UnityWebRequest www = UnityWebRequest.Delete(API_Settings.STAGING_SERVICE_URL + API_Settings.DELETE_PHOTO_URL+ photoName))
        {
            www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(" Download Photo Error : " + www.error);
            }
            else
            {
                yield return null;
                StartCoroutine(GalleryIsEmpty());

            }
        }

    }


    // ======================================================
    //                  DELETE VIDEO FROM  SERVER By Name it
    // ======================================================
    private IEnumerator DeleteVideoFromServer(string videoName)
    {
        videoName = videoName.Replace(".mp4", "");
        videoName = videoName.Replace(".mov", "");
        Debug.Log("Delete The Next Video From Server:  " + videoName);
        using (UnityWebRequest www = UnityWebRequest.Delete(API_Settings.STAGING_SERVICE_URL + API_Settings.DELETE_VIDEO_URL + videoName))
        {
            www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(" Download Photo Error : " + www.error);
            }
            else
            {
                yield return null;
                StartCoroutine(GalleryIsEmpty());

            }
        }
    }

    // Upload Photo Profile from url
    IEnumerator UploadPhotoFromUrl(string url)
    {
        WWW www = new WWW(url);
        yield return www;
        profileImage.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
    }


    // Go To Setting Scene
    public void GoToSettingScene()
    {
        SceneManager.LoadScene("Setting");
    }

    // show word if gallery is empty
    IEnumerator GalleryIsEmpty()
    {
        if(content.transform.childCount > 0){

            //Text_NoPostYet.text = "";
            video_NoPostYet.SetActive(false);
        }
        else
        {
            //Text_NoPostYet.text = "Oh No! No posts yet.";
            video_NoPostYet.SetActive(true);
            StartCoroutine(PlayVideoEmptyGallery());
        }

        yield return null;

    }



    // 
    IEnumerator PlayVideoEmptyGallery()
    {
        VideoPlayer videoPlayer_NoPostYet = video_NoPostYet.GetComponent<VideoPlayer>();
        RawImage rawImage_NoPostYet = video_NoPostYet.GetComponent<RawImage>();

        videoPlayer_NoPostYet.Prepare();
        WaitForSeconds waitForSeconds = new WaitForSeconds(0.5f);
        while (!videoPlayer_NoPostYet.isPrepared)
        {
            yield return waitForSeconds;
            break;
        }
        rawImage_NoPostYet.texture = videoPlayer_NoPostYet.texture;
        videoPlayer_NoPostYet.Play();
        //audioSource.Play();
    }
}
