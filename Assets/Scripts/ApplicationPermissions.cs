﻿using System.Collections;
using Unity.Notifications.iOS;
using UnityEngine;

public class ApplicationPermissions : MonoBehaviour
{

    private const string RECORDAUDIO_PERMISSION_URI = "android.permission.RECORD_AUDIO";
    private const string LOCATION_PERMISSION_URI = "android.permission.ACCESS_FINE_LOCATION";
    private const string CAMERA_PERMISSION_URI = "android.permission.CAMERA";
    private const string BLUETOOTH_PERMISSION_URI = "android.permission.BLUETOOTH";

    private void Awake()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            StartCoroutine(AskAndroidPermissions());
        }
        else
        {
            StartCoroutine(ASKPermissions());
        }
    }


    IEnumerator ASKPermissions()
    {
        //yield return StartCoroutine(AskForFashLight());
        yield return StartCoroutine(AskForCameraPermission());
        yield return new WaitForSeconds(0.25f);

        // yield return StartCoroutine(AskForLocation());
        yield return StartCoroutine(AskForMicrophonePermission());
        yield return new WaitForSeconds(0.25f);

        //Notification Permission
        yield return StartCoroutine(ASKForNotification());
    }

    IEnumerator AskForMicrophonePermission()
    {
        if (!Application.HasUserAuthorization(UserAuthorization.Microphone))
        {
            yield return Application.RequestUserAuthorization(UserAuthorization.Microphone);
        }
    }

    IEnumerator AskForCamera()
    {
        AndroidRuntimePermissions.Permission CAMERA_PERMISSION = AndroidRuntimePermissions.RequestPermission(CAMERA_PERMISSION_URI);
        
        if (CAMERA_PERMISSION == AndroidRuntimePermissions.Permission.ShouldAsk)  {
            AndroidRuntimePermissions.Permission askenPermission = AndroidRuntimePermissions.RequestPermission(CAMERA_PERMISSION_URI);
            yield return 0;
        }
        else {
            if (CAMERA_PERMISSION == AndroidRuntimePermissions.Permission.Denied) {
                AndroidRuntimePermissions.Permission askenPermission = AndroidRuntimePermissions.RequestPermission(CAMERA_PERMISSION_URI);
                yield return 0;
            }
            else {
                yield return 0;
            }
        }
    }

    public IEnumerator ASKForNotification()
    {
        using (var req = new AuthorizationRequest(AuthorizationOption.Alert | AuthorizationOption.Badge, true))
        {
            while (!req.IsFinished)
            {
                yield return null;
            }
        }
        yield return null;
    }

    IEnumerator AskForLocation()
    {
        AndroidRuntimePermissions.Permission LOCATION_PERMISSION = AndroidRuntimePermissions.RequestPermission(LOCATION_PERMISSION_URI);
       
        if (LOCATION_PERMISSION == AndroidRuntimePermissions.Permission.ShouldAsk) {
            AndroidRuntimePermissions.Permission askenPermission = AndroidRuntimePermissions.RequestPermission(LOCATION_PERMISSION_URI);
            yield return 0;
        }
        else {
            if (LOCATION_PERMISSION == AndroidRuntimePermissions.Permission.Denied) {
                AndroidRuntimePermissions.Permission askenPermission = AndroidRuntimePermissions.RequestPermission(LOCATION_PERMISSION_URI);
                yield return 0;
            }
            else {
                yield return 0;
            }
        }
    }

    IEnumerator AskForCameraPermission()
    {
        if (!Application.HasUserAuthorization(UserAuthorization.WebCam))
        {
            yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);
        }
    }
    IEnumerator AskAndroidPermissions()
    {
        yield return StartCoroutine(AskForCamera());
        yield return StartCoroutine(AskForRecording());
        yield return StartCoroutine(ASKPermissions());
    }
    
    IEnumerator AskForRecording()
    { 
        AndroidRuntimePermissions.Permission RECORD_PERMISSION = AndroidRuntimePermissions.RequestPermission(RECORDAUDIO_PERMISSION_URI);
    
        if (RECORD_PERMISSION == AndroidRuntimePermissions.Permission.ShouldAsk)
        {
            AndroidRuntimePermissions.Permission askenPermission = AndroidRuntimePermissions.RequestPermission(RECORDAUDIO_PERMISSION_URI);
            yield return 0;
        }
        else
        {
            if (RECORD_PERMISSION == AndroidRuntimePermissions.Permission.Denied) {
                AndroidRuntimePermissions.Permission askenPermission = AndroidRuntimePermissions.RequestPermission(RECORDAUDIO_PERMISSION_URI);
                yield return 0;
            }
            else {
                yield return 0;
            }
        }
       
    }

    
}
