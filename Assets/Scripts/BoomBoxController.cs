﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomBoxController : MonoBehaviour
{
    private Animator animator;
    public GameObject boomBox;

    public float scale;
    public float MaxScale;
    public float MinScale;
      
    // Start is called before the first frame update
    void Start()
    {
        animator = boomBox.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void PlaySoundAnimation() { 
        animator.Play("SoundWaves",0,0);
    }
   
    public void PlayConfittiAnimation() { 
        animator.Play("Confetti",0,0);
    }


    public void EnLargeScale()
    {
        if (boomBox.transform.localScale.x <= MaxScale && boomBox.transform.localScale.x >  MinScale )
        {
            boomBox.transform.localScale += new Vector3(scale, scale, scale);
        }
    }
    
    public void MiniMizeScale()
    {
        if (boomBox.transform.localScale.x <= MaxScale && boomBox.transform.localScale.x > MinScale)
        {
            boomBox.transform.localScale -= new Vector3(scale, scale, scale);
        }
    }
}
