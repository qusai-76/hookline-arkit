using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaperAirPlaneDisable : MonoBehaviour
{

    public GameObject paperAirplanes;

    public GameObject AllAirPlanes;
    // Start is called before the first frame update
    void Start()
    {
        paperAirplanes.SetActive(false);
        AllAirPlanes.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
