﻿using UnityEngine;
using UnityEngine.UI;  // add to the top
using System.Collections;
 
public class FlashBang : MonoBehaviour {
 
     public CanvasGroup myCG;
     private bool flash = false;
     
     void Update ()
     {
         if (flash)
         {
            //Debug.Log("Delta : "+Time.deltaTime);
             myCG.alpha = myCG.alpha - 0.015f;
             if (myCG.alpha <= 0)
             {
                 myCG.alpha = 0;
                 flash = false;
             }
         }
     }
     
     public void MineHit ()
     {
         flash = true;
         myCG.alpha = 1;
     }
 }