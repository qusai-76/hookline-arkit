﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebViewController : MonoBehaviour
{
    public GameObject webView;
    public GameObject closeButton;
    public GameObject background;
    public string Url;
    
    public void GoToWebsite(string URl)
    {
        SampleWebView view = webView.GetComponent<SampleWebView>();
        view.Url = URl;
        StartCoroutine(view.StartWebSite());
        //webView.SetActive();
        closeButton.SetActive(true);
        background.SetActive(true);
    }

}
