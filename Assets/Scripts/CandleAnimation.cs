﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CandleAnimation : MonoBehaviour
{

    public Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LightCandle()
    {
        //RuntimeAnimatorController controller = anim.runtimeAnimatorController;
        anim.Play("ON", 0);
    }
    public void QuietCandle()
    {
        //RuntimeAnimatorController controller = anim.runtimeAnimatorController;
        anim.Play("OFF", 0);
    }
}
