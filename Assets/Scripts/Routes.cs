﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Routes : MonoBehaviour
{

    public GameObject Gallery_InHallOfFamGroundScene;
    public GameObject HallOfFameGround_InHallOfFamGroundScene;

    public void GoTOGalleryScene()
    {
        SceneManager.LoadScene("Gallery");
    }

    // Go To Setting Scene
    public void GoToSettingScene()
    {
        SceneManager.LoadScene("Setting");
    }


    public void GoToPrivacyPolicyScene()
    {
        SceneManager.LoadScene("PrivacyPolicy");
    }

    public void GoToGalleryFromHallOfFameGround()
    {
        Debug.Log("Update Canvases");
        Canvas.ForceUpdateCanvases();
        RefreshGalleryClass.refreshGallery = true;
        Gallery_InHallOfFamGroundScene.SetActive(true);
        HallOfFameGround_InHallOfFamGroundScene.SetActive(false);

    }

    public void GoToHallOfFameGroundFromHallOfFameGround()
    {
        Gallery_InHallOfFamGroundScene.SetActive(false);
        HallOfFameGround_InHallOfFamGroundScene.SetActive(true);
    }
}
