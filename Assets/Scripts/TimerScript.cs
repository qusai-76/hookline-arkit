﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TimerScript 
{
    public static string CurrentScene;
    
    public static void AccessHallOfFame()
    { 
         System.DateTime date = System.DateTime.Now;
        int thisDay = System.DateTime.Now.Day;
        int thisMonth = System.DateTime.Now.Month;

        Debug.Log("This Day : "+ thisDay );
        Debug.Log("This Month : "+  thisMonth );
        
        if(thisDay<17 && thisMonth==2)
        {
            // Go to Hall OF Fame
            CurrentScene = "HallOfFame";
            UnityEngine.SceneManagement.SceneManager.LoadScene("HallOfFame");
        }
        else { 
            // Go to Ground Hall OF Fame
            CurrentScene = "HallOfFameGround";
            UnityEngine.SceneManagement.SceneManager.LoadScene("HallOfFameGround");
            
        }
    }

}
