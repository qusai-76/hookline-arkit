﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.Video;
using Assets.Models;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

namespace SG
{
    [RequireComponent(typeof(UnityEngine.UI.LoopScrollRect))]
    [DisallowMultipleComponent]
    public class Gallery : MonoBehaviour
    {
        // ================================
        //      Define of Parameters 
        // ================================

        // **************************
        //      Scroll Parametrs
        //  **************************
        public int totalCount = -1;

        public string appPath;
        FileInfo[] fileOfImagesForCells;



        // ****************************
        //      parametrs For Images
        // ****************************
        public Text pathImageInTemplate;

        private GameObject cellToDelete;

        // ****************************
        //      parametrs For Videos
        // ****************************
        public Text pathVideoForShareing;

        public VideoPlayer videoPlayerDisplay;
        public VideoSource videoSourceDisplay;
        public AudioSource audioSourceDisplay;

        public GameObject clothes;

        public RawImage videoDisplayRawImage;

        private String statueVideo = "Play";

        public Sprite playButtonSprite;
        public Sprite pauseButtonSprite;

        public GameObject display_StatueVideo;
        public GameObject videoDisplayPanel;
        private bool videoDisplayPanelStatue = false;

        public GameObject imageView_Display;
        public GameObject videoDisplay;

        public GameObject buttonShare;
        public GameObject pathFileForSharing;

        public RawImage imageInVideoCellTemplate;
        public Texture testTexture;

        public GameObject Display;

        public GameObject DialogBox;

        private ulong countFrameInVideo = 10;
        private long currentFrame = 0;

        public Text FullNameForProfile;

        public Image profileImage;

        public GameObject video_NoPostYet;
        public GameObject content;


        // For Load Images And Videos From Server
        public List<string> videosIDS;
        public List<int> phptosIDS;
        public List<string> avatarIDS;
        public List<string> thumbnailIDS;

        public List<String> numsPhotosInServer;
        public List<String> numsPhotosInDeveice;

        public List<String> numsVideosInServer;
        public List<String> numsVideosInDeveice;


        public PhotoModel[] photos;
        public VideoModel[] videos;


        // Use this for initialization

        private void Awake()
        {

        }
        void Start()
        {
            RefreshGalleryClass.refreshGallery = false;

            // ----------- Start Scroll ------------

            var ls = GetComponent<LoopScrollRect>();
            LoadImageAndVideoPaths();
            totalCount = fileOfImagesForCells.Length;

            ls.totalCount = totalCount;
            // Add Parameter the List of Combined Array
            ls.RefillCells(0, fileOfImagesForCells);

            // ----------- End Scroll ------------


            // ----------- Start Profile ------------

            if (PlayerPrefs.GetString("firstname") != null && PlayerPrefs.GetString("lastname") != null)
                FullNameForProfile.text = PlayerPrefs.GetString("firstname") + " " + PlayerPrefs.GetString("lastname");
            else if (PlayerPrefs.GetString("firstname") != null && PlayerPrefs.GetString("lastname") == null)
                FullNameForProfile.text = PlayerPrefs.GetString("firstname");
            else
                FullNameForProfile.text = "";


            // upload photo of profile from server
            if (!string.IsNullOrEmpty(PlayerPrefs.GetString("profileimage")))
                StartCoroutine(UploadPhotoFromUrl(PlayerPrefs.GetString("profileimage")));

            // -----------  End  Profile ------------
        }

        void Update()
        {
            //-------------
            if (RefreshGalleryClass.refreshGallery == true)
            {
                Debug.Log("Refresh The Gallery");
                RefreshGalleryClass.refreshGallery = false;

                //Delete previous images and videos
                //foreach (Transform child in content.transform)
                //{
                //    Destroy(child.gameObject);
                //}

                // Reload Images and video
                LoadGallery();
            }
            //-------------


            if (videoPlayerDisplay.isPlaying)
            {
                //print("Current Frame is: " + videoPlayerDisplay.frame);
                currentFrame = videoPlayerDisplay.frame;

                display_StatueVideo.GetComponent<Image>().sprite = pauseButtonSprite;
            }

            if (currentFrame == ((long)countFrameInVideo - 1))
            {
                currentFrame = -2; // i put -2 only for stop loop this condition 

                videoDisplayPanel.SetActive(true);

                statueVideo = "Pause";
                display_StatueVideo.GetComponent<Image>().sprite = playButtonSprite;
            }

        }

        /// <summary>
        /// Loads the images and videos paths.
        /// </summary>
        private void LoadImageAndVideoPaths()
        {
            appPath = Application.persistentDataPath;

            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(appPath);

            System.IO.FileInfo[] filesOfImages = di.GetFiles("*.jpg");
            System.IO.FileInfo[] filesVideosMP4 = di.GetFiles("*.mp4");
            System.IO.FileInfo[] filesVideosMov = di.GetFiles("*.mov");

            FileInfo[] newArray1 = new FileInfo[filesOfImages.Length + filesVideosMP4.Length];
            Array.Copy(filesOfImages, newArray1, filesOfImages.Length);
            Array.Copy(filesVideosMP4, 0, newArray1, filesOfImages.Length, filesVideosMP4.Length);

            fileOfImagesForCells = new FileInfo[newArray1.Length + filesVideosMov.Length];
            Array.Copy(newArray1, fileOfImagesForCells, newArray1.Length);
            Array.Copy(filesVideosMov, 0, fileOfImagesForCells, newArray1.Length, filesVideosMov.Length);

            var result = fileOfImagesForCells.ToList<FileInfo>().OrderBy(ob => ob.CreationTime);
            fileOfImagesForCells = result.ToArray<FileInfo>();

            Array.Reverse(fileOfImagesForCells);
        }

        /// <summary>
        /// Load the gallery.
        /// </summary>
        void LoadGallery()
        {
            //RefreshGalleryClass.refreshGallery = false;
            var ls = GetComponent<LoopScrollRect>();
            LoadImageAndVideoPaths();
            totalCount = fileOfImagesForCells.Length;

            ls.totalCount = totalCount;
            // Add Parameter the List of Combined Array
            ls.RefillCells(0, fileOfImagesForCells);


        }



        //Add path of image to pathFileForSharing In Button Share
        public void AddPathImageToShareButton(Text txt)
        {
            pathFileForSharing.GetComponent<Text>().text = txt.text;
        }



       


        public void GoToHallOfFame()
        {
#pragma warning disable CS0618 // Type or member is obsolete
            SceneManager.UnloadScene("Gallery");
#pragma warning restore CS0618 // Type or member is obsolete

            SceneManager.LoadScene(TimerScript.CurrentScene);
        }

        public void CloseDisplay()
        {
            Display.SetActive(false);
            videoDisplayPanel.SetActive(false);
        }

        // Button For Delete Image Or Video
        public void ButtonDeleteFile()
        {
            DialogBox.SetActive(true);

        }


        /// <summary>
        /// Buttons the delete file in confirmed delete dialog box.
        /// </summary>
        public void ButtonDeleteFile_InDialogBox()
        {   // delete file from deveice
            string pathFileForSharing_ForDeleteing = pathFileForSharing.GetComponent<Text>().text;
            File.Delete(pathFileForSharing_ForDeleteing);

            // delete file from server
            int index = pathFileForSharing_ForDeleteing.ToString().LastIndexOf('/') + 1;
            string nameFileForDeletingFromServer = pathFileForSharing_ForDeleteing.ToString().Substring(index);
            //Debug.Log("File name For Delete this file from server : " + nameFileForDeletingFromServer);

            // If this file is photo call DeletePhoto -- else call delete video
            if (nameFileForDeletingFromServer.Contains(".jpg"))
            {
                StartCoroutine(DeletePhotoFromServer(nameFileForDeletingFromServer));
            }
            else if (nameFileForDeletingFromServer.Contains(".mp4") || nameFileForDeletingFromServer.Contains(".mov"))
            {
                StartCoroutine(DeleteVideoFromServer(nameFileForDeletingFromServer));
            }


            //cellToDelete.SetActive(false);
            Destroy(cellToDelete);
            Display.SetActive(false);

            DialogBox.SetActive(false);

        }

        public void CancelDeleteFile_InDialogBox()
        {
            DialogBox.SetActive(false);

        }


        // ===========================================================
        // -----------------------------------------------------------
        //                 Sync Photos and load photos from server
        // -----------------------------------------------------------
        // ===========================================================



        // ******************************************************
        // ======================================================
        //                       Load Photos From Server
        // ======================================================
        // ******************************************************
        /// <summary>
        /// Syncs And Load the photos From the Server.
        /// </summary>
        /// <param name="PhotosURL">Photos URL.</param>
        /// <param name="headers">Headers.</param>
        private IEnumerator SyncPhotos(string PhotosURL, Dictionary<string, string> headers)
        {
            Debug.Log(" Calling Photos ");

            using (UnityWebRequest www = UnityWebRequest.Get(PhotosURL))
            {
                www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.Log(" Download Photos Error : " + www.error);
                }
                else
                {
                    //Debug.Log(www.downloadHandler.text);
                    string response = www.downloadHandler.text;
                    photos = JsonHelper.FromJson<PhotoModel>(response);

                    foreach (PhotoModel photo in photos)
                    {
                        //Debug.Log("photo ID: " + photo.id);
                        //Debug.Log("photo File Name: " + photo.fileName);

                        // Get Only on the num of photo
                        String numPhotoInServer = photo.fileName.Replace("pic-", "");


                        if (!(numsPhotosInDeveice.Contains(numPhotoInServer)))
                        {
                            // start load photo to device 
                            //Debug.Log("Start Load Photo To Device");
                            //StartCoroutine(DownloadPhotoByID(API_Settings.DOWNLOAD_PHOTO_URL + photo.id));
                        }


                    }

                }
            }
        }


        // ======================================================
        //                Download Photo By ID
        // ======================================================
        /// <summary>
        /// Downloads the photo by ID.
        /// </summary>
        /// <param name="URL">URL.</param>
        public IEnumerator DownloadPhotoByID(string URL)
        {
            Debug.Log("Start DownloadPhotoByID");

            using (UnityWebRequest www = UnityWebRequest.Get(URL))
            {
                www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.Log(" Download Photo Error : " + www.error);
                }
                else
                {
                    //Debug.Log(www.downloadHandler.text);
                    string response = www.downloadHandler.text;

                    PhotoModel model = JsonUtility.FromJson<PhotoModel>(response);

                    //Debug.Log("Extention : " + model.fileExtention);

                    string filePath = Application.persistentDataPath + "/" + model.fileName + model.fileExtension;
                    System.IO.File.WriteAllBytes(filePath, System.Convert.FromBase64String(model.photoContent));
                    Debug.Log("Write Photo to persistentDataPath");

                    //yield return StartCoroutine(CreateSprite(filePath));
                }
            }

            // show image in gallery after download it to presistaint data


            //yield return 0;

        }


        // ******************************************************
        // ======================================================
        //                       Load Videos From Server
        // ======================================================
        // ******************************************************
        /// <summary>
        /// Syncs And Load The videos From The Server.
        /// </summary>
        /// <returns>The videos.</returns>
        /// <param name="VideosURL">Videos URL.</param>
        /// <param name="headers">Headers.</param>
        private IEnumerator SyncVideos(string VideosURL, Dictionary<string, string> headers)
        {
            Debug.Log(" Calling Videos ");

            using (UnityWebRequest www = UnityWebRequest.Get(VideosURL))
            {
                www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.Log(" Download Photos Error : " + www.error);
                }
                else
                {
                    //Debug.Log(www.downloadHandler.text);
                    string response = www.downloadHandler.text;
                    videos = JsonHelper.FromJson<VideoModel>(response);

                    foreach (VideoModel video in videos)
                    {
                        // Get Only on the num of video
                        String numVideoInServer = video.fileName.Replace("video-", "");

                        if (!(numsPhotosInDeveice.Contains(numVideoInServer)))
                        {
                            // start load video to device 
                            Debug.Log("Start Load video To Device");
                            StartCoroutine(DownloadVideoByID(video.id));
                        }

                    }

                }
            }
        }



        //public void DownlodVideos(List<string> videosIds)
        //{
        //    foreach (string vidID in videosIds)
        //    {
        //        StartCoroutine(DownloadVideo(vidID));
        //    }
        //}

        /// <summary>
        /// Download the video by ID.
        /// </summary>
        /// <param name="vidID">Vid identifier.</param>
        private IEnumerator DownloadVideoByID(string vidID)
        {
            Debug.Log("Downloading video with id : " + vidID);
            using (UnityWebRequest www = UnityWebRequest.Get(API_Settings.DOWNLOAD_VIDEO_URL + vidID))
            {
                //Debug.Log("Token : "+ PlayerPrefs.GetString("auth_token"));
                www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.Log(" Download Video Error : " + www.error);
                }
                else
                {
                    string response = www.downloadHandler.text;
                    Debug.Log("response : " + response);
                    VideoModel vid = JsonUtility.FromJson<VideoModel>(response);

                    Debug.Log("Model : " + vid.videoContent.Length);
                    string filePath = Application.persistentDataPath + "/" + vid.fileName + vid.fileExtension;
                    Debug.Log(filePath);

                    Debug.Log("Data Length : " + System.Convert.FromBase64String(vid.videoContent).Length);
                    System.IO.File.WriteAllBytes(filePath, System.Convert.FromBase64String(vid.videoContent));

                    if (File.Exists(filePath))
                    {
                        Debug.Log("Success Dwonload video  : " + vid.fileName);

                        //yield return StartCoroutine(VideoDisplayInCell(filePath));
                    }
                    else
                    {
                        Debug.Log("Couldn't Write " + vid.fileName);
                    }
                }
            }
        }



        // ======================================================
        //                  DELETE PHOTO FROM SERVER By Name it
        // ======================================================
        /// <summary>
        /// Delete the photo from the server.
        /// </summary>
        /// <param name="photoName">Photo name.</param>
        private IEnumerator DeletePhotoFromServer(string photoName)
        {
            photoName = photoName.Replace(".jpg", "");
            Debug.Log("Delete The Next Photo From Server:  " + photoName);
            using (UnityWebRequest www = UnityWebRequest.Delete(API_Settings.STAGING_SERVICE_URL + API_Settings.DELETE_PHOTO_URL + photoName))
            {
                www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.Log(" Download Photo Error : " + www.error);
                }
                else
                {
                    yield return null;
                    StartCoroutine(GalleryIsEmpty());

                }
            }

        }


        // ======================================================
        //                  DELETE VIDEO FROM  SERVER By Name it
        // ======================================================
        /// <summary>
        /// Delete the video from the server.
        /// </summary>
        /// <returns>The video from server.</returns>
        /// <param name="videoName">Video name.</param>
        private IEnumerator DeleteVideoFromServer(string videoName)
        {
            videoName = videoName.Replace(".mp4", "");
            videoName = videoName.Replace(".mov", "");
            Debug.Log("Delete The Next Video From Server:  " + videoName);
            using (UnityWebRequest www = UnityWebRequest.Delete(API_Settings.STAGING_SERVICE_URL + API_Settings.DELETE_VIDEO_URL + videoName))
            {
                www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.Log(" Download Photo Error : " + www.error);
                }
                else
                {
                    yield return null;
                    StartCoroutine(GalleryIsEmpty());

                }
            }
        }

        /// <summary>
        /// Upload Photo Profile from url.
        /// </summary>
        /// <returns>The photo from URL.</returns>
        /// <param name="url">URL.</param>
        IEnumerator UploadPhotoFromUrl(string url)
        {
            WWW www = new WWW(url);
            yield return www;
            profileImage.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
        }


        /// <summary>
        /// Go To Setting Scene.
        /// </summary>
        public void GoToSettingScene()
        {
            SceneManager.LoadScene("Setting");
        }


        /// <summary>
        /// Gallery the is empty (API).
        /// </summary>
        /// <returns>The is empty.</returns>
        IEnumerator GalleryIsEmpty()
        {
            if (content.transform.childCount > 0)
            {

                //Text_NoPostYet.text = "";
                video_NoPostYet.SetActive(false);
            }
            else
            {
                //Text_NoPostYet.text = "Oh No! No posts yet.";
                video_NoPostYet.SetActive(true);
                StartCoroutine(PlayVideoEmptyGallery());
            }

            yield return null;

        }



        /// <summary>
        /// Play Video If Gallery Is Empty.
        /// </summary>
        /// <returns>The video empty gallery.</returns>
        IEnumerator PlayVideoEmptyGallery()
        {   
            VideoPlayer videoPlayer_NoPostYet = video_NoPostYet.GetComponent<VideoPlayer>();
            RawImage rawImage_NoPostYet = video_NoPostYet.GetComponent<RawImage>();

            videoPlayer_NoPostYet.Prepare();
            WaitForSeconds waitForSeconds = new WaitForSeconds(0.5f);
            while (!videoPlayer_NoPostYet.isPrepared)
            {
                yield return waitForSeconds;
                break;
            }
            rawImage_NoPostYet.texture = videoPlayer_NoPostYet.texture;
            videoPlayer_NoPostYet.Play();
            //audioSource.Play();
        }



    }
}



