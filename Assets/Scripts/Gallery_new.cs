﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.Video;

public class Gallery_new : MonoBehaviour
{

    public Text FullNameForProfile;
    public Image profileImage;

    public GameObject pathFileForSharing;

    public GameObject Display;
    public GameObject DialogBox;
    public GameObject video_NoPostYet;
    public GameObject content;

    private GameObject cellToDelete;


    public GameObject imageView_Display;
    public GameObject videoDisplay;
    public GameObject buttonShare;



    // Start is called before the first frame update
    void Start()
    {
        // ----------- Start Profile ------------

        if (PlayerPrefs.GetString("firstname") != null && PlayerPrefs.GetString("lastname") != null)
            FullNameForProfile.text = PlayerPrefs.GetString("firstname") + " " + PlayerPrefs.GetString("lastname");
        else if (PlayerPrefs.GetString("firstname") != null && PlayerPrefs.GetString("lastname") == null)
            FullNameForProfile.text = PlayerPrefs.GetString("firstname");
        else
            FullNameForProfile.text = "";


        // upload photo of profile from server
        if (!string.IsNullOrEmpty(PlayerPrefs.GetString("profileimage")))
            StartCoroutine(UploadPhotoFromUrl(PlayerPrefs.GetString("profileimage")));

        // -----------  End  Profile ------------


    
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    /// <summary>
    /// Upload Photo Profile from url.
    /// </summary>
    /// <returns>The photo from URL.</returns>
    /// <param name="url">URL.</param>
    IEnumerator UploadPhotoFromUrl(string url)
    {
        WWW www = new WWW(url);
        yield return www;
        profileImage.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
    }



    // ======================================================
    //                  DELETE PHOTO FROM SERVER By Name it
    // ======================================================
    /// <summary>
    /// Delete the photo from the server.
    /// </summary>
    /// <param name="photoName">Photo name.</param>
    private IEnumerator DeletePhotoFromServer(string photoName)
    {
        photoName = photoName.Replace(".jpg", "");
        Debug.Log("Delete The Next Photo From Server:  " + photoName);
        using (UnityWebRequest www = UnityWebRequest.Delete(API_Settings.STAGING_SERVICE_URL + API_Settings.DELETE_PHOTO_URL + photoName))
        {
            www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(" Download Photo Error : " + www.error);
            }
            else
            {
                yield return null;
                StartCoroutine(GalleryIsEmpty());

            }
        }

    }

    // ======================================================
    //                  DELETE VIDEO FROM  SERVER By Name it
    // ======================================================
    /// <summary>
    /// Delete the video from the server.
    /// </summary>
    /// <returns>The video from server.</returns>
    /// <param name="videoName">Video name.</param>
    private IEnumerator DeleteVideoFromServer(string videoName)
    {
        videoName = videoName.Replace(".mp4", "");
        videoName = videoName.Replace(".mov", "");
        Debug.Log("Delete The Next Video From Server:  " + videoName);
        using (UnityWebRequest www = UnityWebRequest.Delete(API_Settings.STAGING_SERVICE_URL + API_Settings.DELETE_VIDEO_URL + videoName))
        {
            www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(" Download Photo Error : " + www.error);
            }
            else
            {
                yield return null;
                StartCoroutine(GalleryIsEmpty());

            }
        }
    }


    /// <summary>
    /// Gallery the is empty (API).
    /// </summary>
    /// <returns>The is empty.</returns>
    public IEnumerator GalleryIsEmpty()
    {
        Debug.Log("Check if gallery is empty");
        Debug.Log("Number of child in content is :" + content.transform.childCount);
        if (content.transform.childCount > 2)
        {
             video_NoPostYet.SetActive(false);
        }
        else
        {
            //Text_NoPostYet.text = "Oh No! No posts yet.";
            video_NoPostYet.SetActive(true);
            StartCoroutine(PlayVideoEmptyGallery());
        }

        yield return null;

    }


    /// <summary>
    /// Play Video If Gallery Is Empty.
    /// </summary>
    /// <returns>The video empty gallery.</returns>
    IEnumerator PlayVideoEmptyGallery()
    {
        VideoPlayer videoPlayer_NoPostYet = video_NoPostYet.GetComponent<VideoPlayer>();
        RawImage rawImage_NoPostYet = video_NoPostYet.GetComponent<RawImage>();

        videoPlayer_NoPostYet.Prepare();
        WaitForSeconds waitForSeconds = new WaitForSeconds(0.5f);
        while (!videoPlayer_NoPostYet.isPrepared)
        {
            yield return waitForSeconds;
            break;
        }
        rawImage_NoPostYet.texture = videoPlayer_NoPostYet.texture;
        videoPlayer_NoPostYet.Play();
        //audioSource.Play();
    }


   



   
}
