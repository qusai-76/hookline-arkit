﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.iOS;
using System.Linq;

public class ExpressionManager : MonoBehaviour
{
    public ExpressionConfiguration[] ExpresionConfigurations;
    private bool IsConfigured = false;


    public GameObject avatarsManager;

    public string FORMAT_DEBUG_TEXT = "({0}){1}(min={2} max={3}) -> val={4}";

    #region ARKIT Type Variables
    private UnityARSessionNativeInterface m_session;

    private Dictionary<string, float> currentBlendShapes;

    private Dictionary<string, Text> currentBlendShapeUIs;

    private bool blendShapesEnabled = false;

    public GameObject expressionIndicator;

    public float delayFor = 0.5f;
    private float delayTimer = 0;

    [SerializeField]
    private GameObject eyePrefab;
    private GameObject eyeLeft, eyeRight;
    private bool toggleEyeColor = false;
    private GameObject center;

    [SerializeField]
    private GameObject headPrefab;
    private GameObject headGameObject;

    [SerializeField]
    private MeshFilter meshFilter;
    private Mesh faceMesh;

    #endregion

    private Smile smile;
    private bool eyeBlink = false;

    public Material MaskShaderMaterial;
    public Material DefaultMaterial;
    public Material RedMaterial;

    public GameObject faceDetectionObject;

    public EventManager eventManager;

    void OnSmile()
    {
        Debug.Log("Smile Function was called!");
        this.GetComponent<MeshRenderer>().material = MaskShaderMaterial;

        Debug.Log(" Is Not Repeating : " + avatarsManager.GetComponent<AppManager>().IsNotRepeating());

        if (avatarsManager.GetComponent<AppManager>().IsNotRepeating() &&
            avatarsManager.GetComponent<AppManager>().AvatarsCount() > 0) {
            avatarsManager.GetComponent<AppManager>().RepeateAnimation();
        }
    }

    void OnStopSmile()
    {
        this.GetComponent<MeshRenderer>().material = DefaultMaterial;
    }


    void OnEyeBlink()
    {
        Debug.Log("Eye Left Blink Event");
        this.GetComponent<MeshRenderer>().material = MaskShaderMaterial;
    }

    void OnStopEyeBlink()
    {
        //this.GetComponent<MeshRenderer>().material = DefaultMaterial;
    }

    private void Awake()
    {
        eventManager.Init();
        eventManager.StartListening("Smile", OnSmile);
        eventManager.StartListening("StopSmile", OnStopSmile);
        eventManager.StartListening("EyeBlink", OnEyeBlink);
        eventManager.StartListening("StopEyeBlink", OnStopEyeBlink);
    }

    void Start()
    {
        m_session = UnityARSessionNativeInterface.GetARSessionNativeInterface();
        smile = new Smile();
        
        Application.targetFrameRate = 30;

		ARKitFaceTrackingConfiguration config = new ARKitFaceTrackingConfiguration();
		config.alignment = UnityARAlignment.UnityARAlignmentGravity;
		config.enableLightEstimation = true;

		if (config.IsSupported)
		{
			m_session.RunWithConfig (config);
			UnityARSessionNativeInterface.ARFaceAnchorAddedEvent += FaceAdded;
			UnityARSessionNativeInterface.ARFaceAnchorUpdatedEvent += FaceUpdated;
			UnityARSessionNativeInterface.ARFaceAnchorRemovedEvent += FaceRemoved;
		}

        if(ExpresionConfigurations == null || ExpresionConfigurations.Length == 0){
            Debug.Log("You must set at least one expression to use the ExpressionManager.cs");    
        }

        IsConfigured = true;

        CreateDebugOverlays();    

        CleanUp();  

        if(headPrefab != null){
            headGameObject = Instantiate(headPrefab) as GameObject;
        }

        if(eyePrefab != null){
            eyeLeft = Instantiate(eyePrefab) as GameObject;
            eyeRight = Instantiate(eyePrefab) as GameObject;

            center = Instantiate(eyePrefab) as GameObject;
        }
    }

    void CleanUp()
    {
        foreach (var configuration in ExpresionConfigurations)
        {
            foreach (var range in configuration.BlendShapeRanges)
            {
                range.DetectionCount = 0;
            }
        }
    }
    void CreateDebugOverlays()
    {
        //currentBlendShapeUIs = new Dictionary<string, Text>();
        //Transform overlay = GameObject.Find("Overlay").transform;
        //foreach (var configuration in ExpresionConfigurations)
        //{
        //    foreach (var range in configuration.BlendShapeRanges)
        //    {
        //        GameObject rangeGo = new GameObject(range.BlendShape.ToString());
        //        rangeGo.AddComponent<Text>();
        //        Text rangeGoText = rangeGo.GetComponent<Text>();
        //        currentBlendShapeUIs.Add(range.BlendShape.ToString(), rangeGoText);
        //        rangeGoText.font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;
        //        rangeGoText.color = Color.white;
        //        rangeGoText.fontSize = 30;
        //        rangeGoText.text = string.Format(FORMAT_DEBUG_TEXT, 0, range.BlendShape, range.LowBound, range.UpperBound, string.Empty);
        //        rangeGo.transform.parent = overlay;        
        //    }
        //}
    }

    void FaceAdded (ARFaceAnchor anchorData)
    {
        currentBlendShapes = anchorData.blendShapes;
        blendShapesEnabled = true;
        faceMesh = new Mesh ();
        UpdateMesh(anchorData);
        meshFilter.mesh = faceMesh;
        UpdatePositionAndRotation(anchorData);
        faceDetectionObject.SetActive(false);
    }

    void FaceUpdated (ARFaceAnchor anchorData) 
    {
        currentBlendShapes = anchorData.blendShapes;
        UpdateMesh(anchorData);
        UpdatePositionAndRotation(anchorData);

    }

    private void UpdateMesh(ARFaceAnchor anchorData)
    {
        if(faceMesh != null)
        {
            gameObject.transform.localPosition = UnityARMatrixOps.GetPosition (anchorData.transform);
			gameObject.transform.localRotation = UnityARMatrixOps.GetRotation (anchorData.transform);
            faceMesh.vertices = anchorData.faceGeometry.vertices;
            faceMesh.uv = anchorData.faceGeometry.textureCoordinates;
            faceMesh.triangles = anchorData.faceGeometry.triangleIndices;
            faceMesh.RecalculateBounds();
            faceMesh.RecalculateNormals();
        }
        
    }

    void FaceRemoved (ARFaceAnchor anchorData) 
    {
        blendShapesEnabled = false;
        meshFilter.mesh = null;
		faceMesh = null;
        faceDetectionObject.SetActive(true);
    }

    private void UpdatePositionAndRotation(ARFaceAnchor anchorData)
    {
        if(headPrefab != null){
            // head position and rotation
            headGameObject.transform.position = UnityARMatrixOps.GetPosition(anchorData.transform);
            headGameObject.transform.rotation = UnityARMatrixOps.GetRotation(anchorData.transform);
        }

        if(eyePrefab != null){
            // eyes position and rotation
            eyeLeft.transform.position = anchorData.leftEyePose.position;
            eyeLeft.transform.rotation = anchorData.leftEyePose.rotation;

            eyeRight.transform.position = anchorData.rightEyePose.position;
            eyeRight.transform.rotation = anchorData.rightEyePose.rotation;

            center.transform.position = (anchorData.leftEyePose.position + anchorData.rightEyePose.position)/2;
            center.transform.rotation = UnityARMatrixOps.GetRotation(anchorData.transform);
        }
    }

    void Update()
    {
        if(!IsConfigured || !blendShapesEnabled){
            //Debug.Log("Not configured or blendshapes are not enabled");
            return;
        }

        if(delayTimer >= delayFor)
        {
            DetectExpressions();
            delayTimer = 0;
        }
        else {
            delayTimer += Time.deltaTime * 1.0f;
        }
    }

    public Transform GetAnchor()
    {
        return center.transform;
    }

    void DetectExpressions()
    {
        foreach (var configuration in ExpresionConfigurations)
        {
            foreach (var range in configuration.BlendShapeRanges)
            {
                string blendshapeName = range.BlendShape.ToString();
                //Debug.Log("BlendShape : "+ blendshapeName) ;

                if (currentBlendShapes.ContainsKey(blendshapeName))
                {
                    float currentBlendshapeValue = currentBlendShapes[blendshapeName];

                    // offset values by sensitivity
                    float newLower = range.LowBound <= 0 ? 0 : range.LowBound;
                    float newUpper = range.UpperBound <= range.LowBound ? range.LowBound : range.UpperBound;

                    //currentBlendshapeText.text = string.Format(FORMAT_DEBUG_TEXT, range.DetectionCount, range.BlendShape, newLower, newUpper, currentBlendshapeValue.ToString());

                    if (blendshapeName == FaceExperissions.SMILE_LEFT)
                    {
                        if (currentBlendshapeValue >= newLower && currentBlendshapeValue <= newUpper)
                        {
                            //currentBlendshapeText.color = Color.red;
                            smile.leftSmile = true;
                            range.DetectionCount += 1;

                            //if (range.Action != null && !string.IsNullOrEmpty(range.Action.MethodName))
                            //{
                            //    Invoke(range.Action.MethodName, range.Action.Delay);
                            //}

                        }
                        else
                        {
                            //currentBlendshapeText.color = Color.white;
                            smile.leftSmile = false;
                        }

                    }

                    if (blendshapeName == FaceExperissions.SMILE_RIGHT)
                    {
                        if (currentBlendshapeValue >= newLower && currentBlendshapeValue <= newUpper)
                        {
                            range.DetectionCount += 1;
                            smile.rightSmile = true;

                            //if (range.Action != null && !string.IsNullOrEmpty(range.Action.MethodName))
                            //{
                            //    Invoke(range.Action.MethodName, range.Action.Delay);
                            //}
                        }
                        else
                        {
                            smile.rightSmile = false;
                            //currentBlendshapeText.color = Color.white;
                        }
                    }

                    if (blendshapeName == FaceExperissions.EYE_BLINK_LEFT)
                    {
                        Debug.Log("Enter Eye Left Blink ");
                        Debug.Log("Current Value : " + currentBlendshapeValue);
                        if (currentBlendshapeValue >= newLower && currentBlendshapeValue <= newUpper) {
                            range.DetectionCount += 1;
                            Debug.Log("Calling Eye Left Blink Event");
                            eyeBlink = true;
                        }
                        else {
                            eyeBlink = false;
                        }

                    }
                }
            }

            if (eyeBlink)
            {
                eventManager.TriggerEvent("EyeBlink", configuration.Action.Delay);
            }

            AreAllSet(configuration, smile);

            //expressionIndicator.SetActive();
        }
    }

    private bool AreAllSet(ExpressionConfiguration configuration)
    {
        bool areAllSet = currentBlendShapeUIs.Values.Where(v => v.color == Color.red).Count() == currentBlendShapeUIs.Keys.Count();

        if (configuration.Action != null && !string.IsNullOrEmpty(configuration.Action.MethodName))
        {
            Invoke(configuration.Action.MethodName, configuration.Action.Delay);
        }

        return areAllSet;
    }

    private bool AreAllSet(ExpressionConfiguration configuration, Smile smile)
    {
        bool areAllSet = smile.isSmiling();

        if (areAllSet)
        {
            eventManager.TriggerEvent("Smile", configuration.Action.Delay);
        }
        else {
            //eventManager.TriggerEvent("StopSmile", configuration.Action.Delay);
        }

        return areAllSet;
    }

   
    public void ChangeEyeColor()
    {
        toggleEyeColor = !toggleEyeColor;
        MeshRenderer eyeLeftMeshRenderer = eyeLeft.GetComponent<MeshRenderer>();
        MeshRenderer eyeRightMeshRenderer = eyeRight.GetComponent<MeshRenderer>();

        eyeLeftMeshRenderer.material.color = toggleEyeColor ? Color.black : Color.yellow;
        eyeRightMeshRenderer.material.color = toggleEyeColor ? Color.black : Color.yellow;
    }


    void OnDisable()
    {
        eventManager.StopListening("Smile", OnSmile);
        eventManager.StartListening("StopSmile", OnStopSmile);
        eventManager.StopListening("EyeBlink", OnEyeBlink);
        eventManager.StopListening("StopEyeBlink", OnStopEyeBlink);
    }
}
