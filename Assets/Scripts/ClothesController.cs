﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using UnityEngine;
using UnityEngine.Networking;

public class ClothesController : MonoBehaviour
{

    public GameObject clothes;
    
    private DressModel[] allDresses;
    private ClotheAvatarModel[] clothedAvatars;


    public Drag3D dragFeature;
   
    public GameObject mainOb;
    public Thread [] dressThreads;
    public Thread [] avatarThreads;
    
    public Thread  thread;
    
    public GameObject[] Dresses;
    public GameObject[] Avatars;
    
    
    public GameObject databaseService;

    public bool isSynchronized = false;
    
    void Start()
    {
        Debug.Log("is Downloaded : " + PlayerPrefs.GetString("clothsync"));
        
        PlayerPrefs.SetString("clothsync","Here");
        
        if(PlayerPrefs.GetString("clothsync").Length>0 && PlayerPrefs.GetString("clothsync")=="downloaded") {
            isSynchronized = true;
            StartCoroutine(LoadAvtarsAndClothe());
            Debug.Log("No Need To Sync");
        }
        else  {
            Debug.Log("Need To Sync");
            StartCoroutine(GetAllDressesAndClothes());         
        } 
    }
    
    /// <summary>
    /// Gets all dresses and clothes.
    /// </summary>
    /// <returns>The all dresses and clothes.</returns>
    IEnumerator GetAllDressesAndClothes()
    {
         Debug.Log("Start Load Clothes and Avatars from the API");
         yield return StartCoroutine(DownloadAllDress());
         yield return StartCoroutine(DownloadAllAvatars());
         
         // Load Avatars into the Scene
         yield return LoadAvtarsAndClothe();
         
         
         // Set them as Synchronized
         PlayerPrefs.SetString("clothsync", "downloaded");
         isSynchronized = true;
    }
    
    IEnumerator LoadAvtarsAndClothe()
    {

        string clothesDir = Application.persistentDataPath ;
        string lauraAvatarsDir = Application.persistentDataPath;
        
        DirectoryInfo clothesDirInfo = new DirectoryInfo(clothesDir);
        
        DirectoryInfo lauramodelsDirInfo = new DirectoryInfo(lauraAvatarsDir);
        
        FileInfo[] lauraClothes = clothesDirInfo.GetFiles("dress*");
        
        FileInfo[] lauraModels = lauramodelsDirInfo.GetFiles("laura*");

        //Dresses = new GameObject[lauraClothes.Length];
        //Avatars = new GameObject[lauraModels.Length];
        dressThreads = new Thread[lauraClothes.Length];
        avatarThreads = new Thread[lauraModels.Length];
        
        //for(int i=0; i<lauraClothes.Length;i++) {
        //    Debug.Log(" Dress Name : " + lauraClothes[i].Name);
        //    Debug.Log(" Dress Path : " + lauraClothes[i].FullName);
            
        //    yield return LoadAvatarClothesAssenc("costume"+(i).ToString(),lauraClothes[i].FullName,i,"dress");
           
        //}
       
        //for(int j=0; j < lauraModels.Length; j++) { 
        //    Debug.Log(" Avatar Name : " + lauraModels[j].Name);
        //    Debug.Log(" Avatar Path : " + lauraModels[j].FullName);
        //    yield return LoadAvatarAssenc("Avatar"+(j).ToString(),lauraModels[j].FullName,j,"avatar");
        //}
        

        yield return null;
    }


    ///// <summary>
    ///// Load Avatar to the Scenen Assynchronously
    ///// </summary>
    ///// <param name="avatar_name">Avatar name.</param>
    ///// <param name="filePath">File path.</param>
    //public IEnumerator LoadAvatarClothesAssenc(string avatar_name, string filePath , int number, string type)
    //{
    //    Debug.Log("Start Load Dress " + avatar_name );
    //    using (var assetLoaderAsync = new AssetLoaderAsync())
    //    {
    //        var assetLoaderOptions = AssetLoaderOptions.CreateInstance();
    //        assetLoaderOptions.AutoPlayAnimations = true;

    //        float unitScaleFactor = 1; //Sets scale factor (quarter) default value.
    //        assetLoaderAsync.OnMetadataProcessed += delegate (AssimpMetadataType metadataType, uint metadataIndex, string metadataKey, object metadataValue)
    //        {
    //            //Checks if the metadata key is "UnitScaleFactor", which is the scale factor metadata used in FBX files.
    //            if (metadataKey == "UnitScaleFactor")
    //            {
    //                unitScaleFactor = (float)metadataValue; //Sets the scale factor value.
    //            }
    //        }; //Sets a callback to process model metadata.
    //           //Loads the model synchronously and stores the reference in myGameObject.
    //        dressThreads[number] = assetLoaderAsync.LoadFromFile(filePath, assetLoaderOptions, null, delegate (GameObject myGameObject)
    //        {
    //            PlaceAvatar(myGameObject, 1f);  // place the avatar on scene

    //            Debug.Log("Avatar Loaded ");
                
    //            myGameObject.transform.SetParent(mainOb.transform);

    //            // set avatar name
    //            myGameObject.name = avatar_name;
    //            // add it to Avatars List

    //            // Add the Ability to Scale
    //            myGameObject.AddComponent<Lean.Touch.LeanScale>();

    //            // Add the Ability to Move
    //            myGameObject.AddComponent<Lean.Touch.LeanTranslateForAvatar>();

    //            //if (type == "dress") {
    //            //    Dresses[number] = myGameObject;
    //            //}
    //            //else {
    //            //    Avatars[number] = myGameObject;
    //            //}

    //        });
    //    }
    //    yield return null;
    //}



    ///// <summary>
    ///// Load Avatar to the Scenen Assynchronously
    ///// </summary>
    ///// <param name="avatar_name">Avatar name.</param>
    ///// <param name="filePath">File path.</param>
    //public IEnumerator LoadAvatarAssenc(string avatar_name, string filePath , int number, string type)
    //{
    //    Debug.Log("Start Load Dress " + avatar_name );
    //    using (var assetLoaderAsync = new AssetLoaderAsync())
    //    {
    //        var assetLoaderOptions = AssetLoaderOptions.CreateInstance();
    //        assetLoaderOptions.AutoPlayAnimations = true;

    //        float unitScaleFactor = 1; //Sets scale factor (quarter) default value.
    //        assetLoaderAsync.OnMetadataProcessed += delegate (AssimpMetadataType metadataType, uint metadataIndex, string metadataKey, object metadataValue)
    //        {
    //            //Checks if the metadata key is "UnitScaleFactor", which is the scale factor metadata used in FBX files.
    //            if (metadataKey == "UnitScaleFactor")
    //            {
    //                unitScaleFactor = (float)metadataValue; //Sets the scale factor value.
    //            }
    //        }; //Sets a callback to process model metadata.
    //           //Loads the model synchronously and stores the reference in myGameObject.
    //        avatarThreads[number] = assetLoaderAsync.LoadFromFile(filePath, assetLoaderOptions, null, delegate (GameObject myGameObject)
    //        {
    //            PlaceAvatar(myGameObject, 1f);  // place the avatar on scene

    //            Debug.Log("Avatar Loaded ");
                
    //            myGameObject.transform.SetParent(mainOb.transform);

    //            // set avatar name
    //            myGameObject.name = avatar_name;
    //            // add it to Avatars List

    //            // Add the Ability to Scale
    //            myGameObject.AddComponent<Lean.Touch.LeanScale>();

    //            // Add the Ability to Move
    //            myGameObject.AddComponent<Lean.Touch.LeanTranslateForAvatar>();

    //            //if (type == "dress") {
    //            //    Dresses[number] = myGameObject;
    //            //}
    //            //else {
    //            //    Avatars[number] = myGameObject;
    //            //}

    //        });
    //    }
    //    yield return null;
    //}

    /// <summary>
    /// Places the avatar in the Unity World 
    /// </summary>
    /// <param name="avatar">Avatar.</param>
    /// <param name="value">Value.</param>
    public void PlaceAvatar(GameObject avatar, float value)
    {
        Vector3 position  = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width/2,Screen.height/2,3));
        avatar.transform.position = position;
                
        avatar.transform.localRotation = Quaternion.identity;
        
        Vector3 cam_position = Camera.main.transform.position;
        cam_position.y = avatar.transform.position.y;
      
        // Roate the Avatar to the Camera
        avatar.transform.LookAt(cam_position);
        #pragma warning disable CS0618 // Type or member is obsolete
        avatar.transform.Rotate(Vector3.up,180f);
        #pragma warning restore CS0618 // Type or member is obsolete

        // Scale the Avatar into the World Scale
        avatar.transform.localScale = Vector3.one * (value/100);
       
    }
    
    
    /// <summary>
    /// Hides the clothes.
    /// </summary>
    public void HideClothes()  {
        clothes.SetActive(false);
    }
    
    /// <summary>
    /// Shows the clothes.
    /// </summary>
    public void ShowClothes()  {
        clothes.SetActive(true);
    }
    
    /// <summary>
    /// Gel All Clothes From the Server
    /// </summary>
    /// <returns>The all clothes.</returns>
    public IEnumerator DownloadAllAvatars()
    {
        string DressURL = API_Settings.STAGING_SERVICE_URL + API_Settings.DOWNLOAD_CLOTHED_AVATARS_LIST;
        
        using (UnityWebRequest www = UnityWebRequest.Get(DressURL)) {
            www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError) {
                Debug.Log(" Download Clothed Avatar Error : " + www.error);
            }
            else
            {
                // check the response
                string response = www.downloadHandler.text;
                clothedAvatars = JsonHelper.FromJson<ClotheAvatarModel>(response);
                
                Debug.Log("Avatars Response : " + clothedAvatars);
                 
                // Save them In DB
                foreach(ClotheAvatarModel clothed_avatar in clothedAvatars)  {
                    ClotheAvatar _avatar = new ClotheAvatar();
                    _avatar.id =int.Parse(clothed_avatar.id);
                    _avatar.filename = clothed_avatar.filename;
                    
                    databaseService.GetComponent<DBScript>().InsertAvatar(_avatar);
                    Debug.Log(" Avatar Inserted in Database ");
                }
 
                //// Create Laura Models SubDirectory
                //DirectoryInfo lauraAvatarsDir = new DirectoryInfo(Application.persistentDataPath + "/" + "laura"); 
               
                //if(!lauraAvatarsDir.Exists){ 
                //    Debug.Log ("Creating laura subdirectory"); 
                //    lauraAvatarsDir.Create(); 
                //}
          
                foreach(ClotheAvatarModel _avatar in clothedAvatars) {
                    StartCoroutine(DownloadAvatar(_avatar.id.ToString()));
                }
            }
        }
        yield return null;
    }
    
    /// <summary>
    /// Downloads all dress.
    /// </summary>
    /// <returns>The all dress.</returns>
    public IEnumerator DownloadAllDress()
    {
        string DressURL = API_Settings.STAGING_SERVICE_URL + API_Settings.DOWNLOAD_DRESS_LIST;
        
        using (UnityWebRequest www = UnityWebRequest.Get(DressURL)) {
            www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError) {
                Debug.Log(" Download Dress Error : " + www.error);
            }
            else
            {
                // check the response
                string response = www.downloadHandler.text;

                Debug.Log("Dresses : " + response);
             
                allDresses = JsonHelper.FromJson<DressModel>(response);
              
                Debug.Log("All Dresses Length : "+ allDresses.Length);
                // Save them In DB
                for(int i=0; i<allDresses.Length; i++) {  
                    Dress d = new Dress();
                    d.id = int.Parse(allDresses[i].id);
                    d.filename = allDresses[i].filename;
                    
                    databaseService.GetComponent<DBScript>().InsertDress(d);
                }
                
                //// Create Laura Clothes SubDirectory
                //DirectoryInfo dressDir = new DirectoryInfo(Application.persistentDataPath + "/" + "dress"); 
                //if(!dressDir.Exists){ 
                //    Debug.Log ("Creating subdirectory"); 
                //    dressDir.Create(); 
                //}
                
                // Download Dresses from server
                foreach(DressModel _dress in allDresses)  {
                    StartCoroutine(DownloadDress(_dress.id.ToString()));
                }
            }
        }
       
        yield return null;
    }
    
    /// <summary>
    /// Downloads the Clothed avatar from the Server By ID
    /// </summary>
    /// <returns>The avatar.</returns>
    /// <param name="avatarID">Avatar identifier.</param>
    public IEnumerator DownloadAvatar(string avatarID)
    {
        //Show the Download progress
        Debug.Log("Start Download Avatars");
        
        // check if there is Internet Connection if available
        if (PlayerPrefs.GetString("auth_token").Length > 1) {
            
            using (UnityWebRequest www = UnityWebRequest.Get(API_Settings.STAGING_SERVICE_URL + API_Settings.DOWNLOAD_AVATAT_LOCAL_URL + avatarID)) {
                www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
                
                //StartCoroutine(CheckDownloadProcess(www, avatarID));
                
                yield return www.SendWebRequest();
                if (www.isNetworkError || www.isHttpError) {
                    Debug.Log(" Download Clothed Avatar Error : " + www.error);
                }
                else {
                    string contentDiseption = www.GetResponseHeader("Content-Disposition");
                    
                    string fileName = contentDiseption.Split(';')[1].Trim().Split('=')[1];
                    string fileExtention = fileName.Split('.')[1];
                    
                    string filePath = Application.persistentDataPath + "/laura" + avatarID + ".fbx";
                    Debug.Log("Avatar Data Length : " + www.downloadHandler.data.Length);
                    
                    System.IO.File.WriteAllBytes(filePath, www.downloadHandler.data);
                    
                 
                    Debug.Log(" Avatar : "+ avatarID + " Downloaded");
                }
            }
        }
    }
 
    /// <summary>
    /// Downloads the dress By Id
    /// </summary>
    /// <returns>The dress.</returns>
    /// <param name="dressID">Avatar identifier.</param>
    public IEnumerator DownloadDress(string dressID) {
        //Show the Download progress
        Debug.Log("Start Download Dress");
        // check if there is Internet Connection if available
        if (PlayerPrefs.GetString("auth_token").Length > 1) {
            
            using (UnityWebRequest www = UnityWebRequest.Get(API_Settings.STAGING_SERVICE_URL +API_Settings.DOWNLOAD_DRESS_BY_ID + dressID)) {
                www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
    
                yield return www.SendWebRequest();
                if (www.isNetworkError || www.isHttpError) {
                    Debug.Log(" Download Dress Error : " + www.error);
                }
                else {
                    string contentDiseption = www.GetResponseHeader("Content-Disposition");
                    
                    string fileName = contentDiseption.Split(';')[1].Trim().Split('=')[1];
                    string fileExtention = fileName.Split('.')[1];
                    string filePath = Application.persistentDataPath + "/dress" + dressID + ".fbx";
          
                    System.IO.File.WriteAllBytes(filePath, www.downloadHandler.data);
                    Debug.Log(" Downloaded Dress  : " + dressID );        
                }
            }
        }
    }

}
