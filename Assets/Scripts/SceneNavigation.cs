using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneNavigation : MonoBehaviour {

   /// <summary>
   /// Gos to ground hall of fame.
   /// </summary>
    public void GoToGroundHallOfFame()
    {
        Surface.Detected = false;
         
        SceneManager.LoadScene("HallOfFameGround");
       
    }
    /// <summary>
    /// Gos to clothes Feature.
    /// </summary>
    public void GoToClothes()
    {
        Surface.Detected = false;
        
        SceneManager.LoadScene("ClothingFeature");    
    }
    
    /// <summary>
    /// Gos to paper airplanes Feature.
    /// </summary>
    public void GoToPaperAirplanes()
    {
        //ApplicationLevelsNavigator.GoToPaperAirPlanes();
    }
    
    /// <summary>
    /// Gos to candles Feature
    /// </summary>
    public void GoToCandles()
    { 
        //ApplicationLevelsNavigator.EnableCandles();
    }
    
    
    /// <summary>
    /// Gos to boom box Feature
    /// </summary>
    public void GoToBoomBox()
    { 
       //ApplicationLevelsNavigator.GoToBoomBox();
    }
    
    
    /// <summary>
    /// Gos to hall of fame Feature
    /// </summary>
    public void GoToHallOfFame()
    { 
        //ApplicationLevelsNavigator.GoToHallOfFame();
        //Candle.Found = false;
    }
    
    
}
