/* 
*   NatCorder
*   Copyright (c) 2019 Yusuf Olokoba
*/

namespace NatCorder.Examples
{

#if UNITY_EDITOR
    using UnityEditor;
#endif
    using UnityEngine;
    using UnityEngine.UI;
    using System.Collections;
    using Clocks;
    using Inputs;
    using System.IO;

    public class ReplayCam : MonoBehaviour
    {

        /**
        * ReplayCam Example
        * -----------------
        * This example records the screen using a `CameraRecorder`.
        * When we want mic audio, we play the mic to an AudioSource and record the audio source using an `AudioRecorder`
        * -----------------
        * Note that UI canvases in Overlay mode cannot be recorded, so we use a different mode (this is a Unity issue)
        */

        [Header("Microphone")]
        public bool recordMicrophone;
        public AudioSource microphoneSource;

        private MP4Recorder videoRecorder;
        private IClock recordingClock;
        private CameraInput cameraInput;
        private AudioInput audioInput;


        public GameObject planeIndicator;
        public GameObject avatarHighlight;

        private int videosCount;


        public void StartRecording()
        {

            if (planeIndicator != null)
            {
                MeshRenderer[] renderers = planeIndicator.GetComponentsInChildren<MeshRenderer>();
                for (int i = 0; i < renderers.Length; i++)
                {
                    renderers[i].enabled = false;
                }
            }

            if (avatarHighlight != null)
            {
                MeshRenderer highlightRenerer = avatarHighlight.GetComponent<MeshRenderer>();
                highlightRenerer.enabled = false;
            }

            // Start recording
            recordingClock = new RealtimeClock();
            videoRecorder = new MP4Recorder(
                Camera.main.pixelWidth,
                Camera.main.pixelHeight,
                60,
                recordMicrophone ? AudioSettings.outputSampleRate : 0,
                recordMicrophone ? (int)AudioSettings.speakerMode : 0,
                OnReplay
            );
            // Create recording inputs
            cameraInput = new CameraInput(videoRecorder, recordingClock, Camera.main);
            if (recordMicrophone)
            {
                StartMicrophone();
                audioInput = new AudioInput(videoRecorder, recordingClock, microphoneSource, true);
            }
        }

        private void StartMicrophone()
        {
#if !UNITY_WEBGL || UNITY_EDITOR // No `Microphone` API on WebGL :(
            // Create a microphone clip
            microphoneSource.clip = Microphone.Start(null, true, 60, 48000);
            while (Microphone.GetPosition(null) <= 0) ;
            // Play through audio source
            microphoneSource.timeSamples = Microphone.GetPosition(null);
            microphoneSource.loop = true;
            microphoneSource.Play();
#endif
        }

        public void StopRecording()
        {

            if (planeIndicator != null)
            {
                MeshRenderer[] renderers = planeIndicator.GetComponentsInChildren<MeshRenderer>();
                for (int i = 0; i < renderers.Length; i++)
                {
                    renderers[i].enabled = true;
                }
            }

            if (avatarHighlight != null)
            {
                MeshRenderer highlightRenerer = avatarHighlight.GetComponent<MeshRenderer>();
                highlightRenerer.enabled = true;
            }


            // Stop the recording inputs
            if (recordMicrophone)
            {
                StopMicrophone();
                audioInput.Dispose();
            }
            cameraInput.Dispose();
            // Stop recording
            videoRecorder.Dispose();
        }

        private void StopMicrophone()
        {
#if !UNITY_WEBGL || UNITY_EDITOR
            Microphone.End(null);
            microphoneSource.Stop();
#endif
        }

        void OnReplay(string path)
        {
            DirectoryInfo di = new DirectoryInfo(Application.persistentDataPath);
            var filesMP4 = di.GetFiles("*.mp4");
            var filesMOV = di.GetFiles("*.mov");

            videosCount = filesMP4.Length + filesMOV.Length;
            videosCount++;

            //// rename the video in presistaint data pathnewVideoName
            string videoName = videosCount + "_" + System.DateTime.Now.ToString("yyyyMMddHHmmss");

            string newVideoName = Application.persistentDataPath + "/videos/" + videoName + ".mp4";


            System.IO.File.Move(path, newVideoName);

            NativeShare share = new NativeShare();
            share.AddFile(newVideoName).Share();

            print("Path After Editing : " + newVideoName);


        }
    }
}