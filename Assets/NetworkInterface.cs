﻿using System;
using System.Net.NetworkInformation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkInterface : MonoBehaviour
{
    public static IEnumerator CalculateDownloadSpeed(double speed)
    {
        UnityWebRequest request = UnityWebRequest.Get("https://google.com");

        DateTime start = DateTime.Now;
        yield return request.SendWebRequest();
        DateTime finish = DateTime.Now;

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log("Error : " + request.error);
        }
        else
        {
            byte[] data = request.downloadHandler.data;
            double downloadTime = (finish - start).TotalSeconds;
            speed = Math.Round( (data.Length / 1024) / downloadTime, 2);
        }

        Debug.Log("Speed in Kb : " + speed);
        yield return null;

        //Ping ping = new Ping("77.42.130.32");
        //while (!ping.isDone)
        //{}
        //Debug.Log("Ping Time : " + ping.time);

    }
}
