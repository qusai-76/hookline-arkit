﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleTouch : MonoBehaviour
{
    int tapCount=0;
    float doubleTapTimer=0;
    public Lean.Touch.LeanSelect selectable;
    // Start is called before the first frame update
    void Start()
    {
       tapCount=0;
       doubleTapTimer=0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
         {
             tapCount++;
         }
         
         if (tapCount > 0)
         {
             doubleTapTimer += Time.deltaTime;
         }
         if (tapCount >= 2)
         {
         
             if(doubleTapTimer <= 0.3f)
             {
                selectable.DeselectAll();
             }
             //What you want to do
             doubleTapTimer = 0.0f;
             tapCount = 0;
             
         }
         if (doubleTapTimer > 0.5f)
         {
             doubleTapTimer = 0f;
             tapCount = 0;
         }
    }
}
