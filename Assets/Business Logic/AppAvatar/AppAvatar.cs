﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class AppAvatar 
{
    // Start is called before the first frame update
    public string name;
    public string filePath;
}
