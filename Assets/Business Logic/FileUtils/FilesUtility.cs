﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class FilesUtility : ScriptableObject
{
   /// <summary>
   /// Creats the directory.
   /// </summary>
   /// <returns>The directory.</returns>
   /// <param name="dirName">Dir name.</param>
   public static IEnumerator CreatDirectory(string dirName)
    {
        DirectoryInfo dirInfo = new DirectoryInfo(Application.persistentDataPath + "/" + dirName );
        if (!dirInfo.Exists) {
            dirInfo.Create();
            Debug.Log( " Directory Created " + dirName + Environment.NewLine);
        }
        else{
            Debug.Log( " Directory Existed : "+ dirName ) ;
        }
        yield return null;
    }
   
   /// <summary>
    /// Gets Cleaned filename.
    /// </summary>
    /// <returns>The clean file name.</returns>
    /// <param name="originalFileName">Original file name.</param>
   public static string GetCleanFileName(string originalFileName)
    {
        string fileToLoad = originalFileName.Replace('\\', '/');
        fileToLoad = string.Format("file://{0}", fileToLoad);
        return fileToLoad;
    }
    
   /// <summary>
    /// Gets the name of the file from its Path.
    /// </summary>
    /// <returns>The file name.</returns>
    /// <param name="fname">Fname.</param>
   public static string GetFileName(string fname)
   {
      return fname.Substring(fname.LastIndexOf('/') + 1).Replace(".png","");
   }
   
   /// <summary>
   /// Writes the file.
   /// </summary>
   /// <param name="filePath">File path.</param>
   /// <param name="bytes">Bytes.</param>
   public static void SaveFile(string filePath, string bytes)
   {
        try
        {
            System.IO.File.WriteAllBytes(filePath, System.Convert.FromBase64String(bytes));
        }
        catch (Exception ex)
        {
            Debug.Log(" Cann't Create : "+ filePath + " : " + ex.Message + Environment.NewLine);
        }
   }
   
   /// <summary>
   /// Writes the file.
   /// </summary>
   /// <param name="filePath">File path.</param>
   /// <param name="bytes">Bytes.</param>
   public static void SaveFile(string filePath, byte [] bytes)
   {
        try
        {
            System.IO.File.WriteAllBytes(filePath, bytes);
        }
        catch (Exception ex)
        {
            Debug.Log(" Cann't Create : "+ filePath + " : " + ex.Message + Environment.NewLine);
        }
   }
   
   /// <summary>
   /// Gets the file name that has a format.
   /// </summary>
   /// <returns>The file name form format.</returns>
   /// <param name="fname">Fname.</param>
   /// <param name="format">Format.</param>
   public static string GetFileNameFormFormat(string fname,string format)
   {
      return fname.Substring(fname.LastIndexOf('/') + 1).Replace(format,"");
   }
   
   /// <summary>
   /// Deletes the file.
   /// </summary>
   /// <param name="filePath">File path.</param>
   public static void DeleteFile(string filePath)
   {
        try {
            File.Delete(filePath);
        }
        catch(Exception ex) {
            Debug.Log("Cann't Detete File "+ ex.Message);
        }
    }
    
   /// <summary>
    /// Deletes the directory.
    /// </summary>
    /// <param name="filePath">File path.</param>
   public static void DeleteDirectory(string filePath)
    {
        try {
            Directory.Delete(filePath);
        }
        catch(Exception ex) {
            Debug.Log("Cann't Detete Directory "+ ex.Message);
        }
    }
}
