﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.Networking;
using Assets.Models;

public class MediaSynchronizer : MonoBehaviour
{
    /// ================= Fucntions ===================
    // 1. Load Images And Videos From the  Device -- return name of all videos and videos order by date as ASC as List  --- ...
    // 2. Download Photos FromServer With Sync
    // 3. Download Videos From Server With Sync 
    // 4. Download Photo By ID
    // 5. Download Video By ID
    // 6. Upload Photo To Server 
    // 7. Upload Video To Server
    // 8. Delete Photo From Server By Name
    // 9. Delete Video From Server By Name
    // 10. Load Thumnail From the Device -- retuen name of all thumbnails as List  -- ...
    // 11. Download Thumbnail From Server With Sync
    // 12. Share Media
    /// -----------------------------------------------

    public PhotoModel[] photos;
    public VideoModel[] videos;

    public FileInfo[] filesOfThumnails;
    public string[] thumbnails;
    public List<string> numsThumbnailsInDevice;
    public List<string> numsThumbnailsInServer;

    public List<String> numsPhotosInServer;
    public List<String> numsPhotosInDeveice;

    public List<String> numsVideosInServer;
    public List<String> numsVideosInDeveice;



    /// <summary>
    /// Loads the images and videos paths.
    /// </summary>
    public FileInfo[] LoadImagesAndVideo()
    {
        print("Start Function LoadImages");  // removing this line

        //textures = new List<Texture2D>();
        DirectoryInfo di = new DirectoryInfo(Application.persistentDataPath);
        FileInfo[] filesOfImages = di.GetFiles("*.jpg");   // store path  images in array

        //#if UNITY_EDITOR || UNITY_IOS
        FileInfo[] filesVideosMP4 = di.GetFiles("*.mp4");   // store path of  videos in array
        FileInfo[] filesVideosMov = di.GetFiles("*.mov");
        //#endif

        FileInfo[] newArray1 = new FileInfo[filesOfImages.Length + filesVideosMP4.Length];
        Array.Copy(filesOfImages, newArray1, filesOfImages.Length);
        Array.Copy(filesVideosMP4, 0, newArray1, filesOfImages.Length, filesVideosMP4.Length);

        FileInfo[] newArray = new FileInfo[newArray1.Length + filesVideosMov.Length];
        Array.Copy(newArray1, newArray, newArray1.Length);
        Array.Copy(filesVideosMov, 0, newArray, newArray1.Length, filesVideosMov.Length);

        var result = newArray.ToList<FileInfo>().OrderBy(ob => ob.CreationTime);
        newArray = result.ToArray<FileInfo>();

        Array.Reverse(newArray);


        return newArray;

        //foreach (var file in newArray)
        //{
        //    if (file.FullName.Contains(".jpg"))
        //    {
        //        //yield return StartCoroutine(CreateSprite(file.FullName));  //here create sprite  after get on all paths for image and raw-image for Image 
        //        AddNumPhotoInDeviceToList(file.Name);
        //    }
        //    else if (file.FullName.Contains(".mov") || file.FullName.Contains(".mp4"))
        //    {
        //        //yield return StartCoroutine(VideoDisplayInCell(file.FullName));
        //        AddNumVideosInDeviceToList(file.Name);
        //    }

        //    //StartCoroutine(CreateTexture(imageFile.FullName));  //here create sprite  after get on all paths for image and raw-image for video 
        //}

    }

    private void AddNumPhotoInDeviceToList(string numPhotoInDevice)
    {
        numPhotoInDevice = numPhotoInDevice.Replace("pic-", "");
        numPhotoInDevice = numPhotoInDevice.Replace(".jpg", "");
        numsPhotosInDeveice.Add(numPhotoInDevice);
    }

    // 
    private void AddNumVideosInDeviceToList(string numVideoInDevice)
    {
        numVideoInDevice = numVideoInDevice.Replace("video-", "");
        numVideoInDevice = numVideoInDevice.Replace(".mp4", "");
        numsVideosInDeveice.Add(numVideoInDevice);
    }

    ///// <summary>
    ///// Syncs And Load the photos From the Server.
    ///// </summary>
    ///// <param name="PhotosURL">Photos URL.</param>
    ///// <param name="headers">Headers.</param>
    //public IEnumerator SyncPhotos(string PhotosURL, Dictionary<string, string> headers)
    //{
    //    // Store name of the photos on device in list 
    //    DirectoryInfo di = new DirectoryInfo(Application.persistentDataPath);
    //    FileInfo[] filesOfImages = di.GetFiles("*.jpg");   // store path  images in array

    //    foreach (var file in filesOfImages)
    //    {
    //        AddNumPhotoInDeviceToList(file.Name);
    //    }

    //    // sync photos
    //    using (UnityWebRequest www = UnityWebRequest.Get(PhotosURL))
    //    {
    //        www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
    //        yield return www.SendWebRequest();

    //        if (www.isNetworkError || www.isHttpError)
    //        {
    //            Debug.Log(" Download Photos Error : " + www.error);
    //        }
    //        else
    //        {
    //            //Debug.Log(www.downloadHandler.text);
    //            string response = www.downloadHandler.text;
    //            photos = JsonHelper.FromJson<PhotoModel>(response);

    //            foreach (PhotoModel photo in photos)
    //            {
    //                // Get Only on the num of photo
    //                String numPhotoInServer = photo.fileName.Replace("pic-", "");


    //                if (!(numsPhotosInDeveice.Contains(numPhotoInServer)))
    //                {
    //                    // start load photo to device 
    //                    //Debug.Log("Start Load Photo To Device");
    //                    StartCoroutine(DownloadPhotoByID(photo.id));
    //                }
    //            }

    //        }
    //    }
    //}

   

    // ===================== Upload the image to server =====================
    /// <summary>
    /// Upload the photo to the server.
    /// </summary>
    /// <param name="path">Path.</param>
    /// <param name="num">Number.</param>
    public IEnumerator UploadPhoto(string path, string num, int id)
    {
        WWW picFile = new WWW("file://" + path);
        yield return picFile;

        WWWForm form = new WWWForm();

        form.AddBinaryData("file", picFile.bytes, "pic-" + num + ".jpg");

        string UploadUrl = "https://hookline.azurewebsites.net/api/action/AddNewPhoto";
        Dictionary<string, string> headers = form.headers;
        byte[] rawData = form.data;

        // Add a custom header to the request.
        headers["Authorization"] = "Bearer " + PlayerPrefs.GetString("auth_token");
        headers["filename"] = "pic-" + num + ".jpg";
        headers["photoID"] = id.ToString();
        WWW www = new WWW(UploadUrl, rawData, headers);
        yield return www;
        if (www.error == null)
        {
            Debug.Log("Picture uploaded successfully!");
        }
        else
        {
            Debug.Log(www.error);
        }

        yield return null;
    }



    ///// <summary>
    ///// Syncs And Load The videos From The Server.
    ///// </summary>
    ///// <returns>The videos.</returns>
    ///// <param name="VideosURL">Videos URL.</param>
    ///// <param name="headers">Headers.</param>
    //public IEnumerator SyncVideos(string VideosURL, Dictionary<string, string> headers)
    //{
    //    // Store name of the photos on device in list 
    //    DirectoryInfo di = new DirectoryInfo(Application.persistentDataPath);
    //    FileInfo[] filesVideosMP4 = di.GetFiles("*.mp4");

    //    foreach (var file in filesVideosMP4)
    //    {
    //        AddNumVideosInDeviceToList(file.Name);
    //    }


    //    // sync videos
    //    using (UnityWebRequest www = UnityWebRequest.Get(VideosURL))
    //    {
    //        www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
    //        yield return www.SendWebRequest();

    //        if (www.isNetworkError || www.isHttpError)
    //        {
    //            Debug.Log(" Download Photos Error : " + www.error);
    //        }
    //        else
    //        {
    //            //Debug.Log(www.downloadHandler.text);
    //            string response = www.downloadHandler.text;
    //            videos = JsonHelper.FromJson<VideoModel>(response);

    //            foreach (VideoModel video in videos)
    //            {
    //                // Get Only on the num of video
    //                String numVideoInServer = video.fileName.Replace("video-", "");

    //                if (!(numsPhotosInDeveice.Contains(numVideoInServer)))
    //                {
    //                    // start load video to device 
    //                    Debug.Log("Start Load video To Device");
    //                    StartCoroutine(DownloadVideoByID(video.id));
    //                }

    //            }

    //        }
    //    }
    //}

    ///// <summary>
    ///// Download the video by ID.
    ///// </summary>
    ///// <param name="vidID">Vid identifier.</param>
    //public IEnumerator DownloadVideoByID(string vidID)
    //{
    //    Debug.Log("Downloading video with id : " + vidID);
    //    using (UnityWebRequest www = UnityWebRequest.Get(API_Settings.DOWNLOAD_VIDEO_URL + vidID))
    //    {
    //        //Debug.Log("Token : "+ PlayerPrefs.GetString("auth_token"));
    //        www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
    //        yield return www.SendWebRequest();

    //        if (www.isNetworkError || www.isHttpError)
    //        {
    //            Debug.Log(" Download Video Error : " + www.error);
    //        }
    //        else
    //        {
    //            string response = www.downloadHandler.text;
    //            Debug.Log("response : " + response);
    //            VideoModel vid = JsonUtility.FromJson<VideoModel>(response);

    //            Debug.Log("Model : " + vid.videoContent.Length);
    //            string filePath = Application.persistentDataPath + "/" + vid.fileName + vid.fileExtension;
    //            Debug.Log(filePath);

    //            Debug.Log("Data Length : " + System.Convert.FromBase64String(vid.videoContent).Length);
    //            System.IO.File.WriteAllBytes(filePath, System.Convert.FromBase64String(vid.videoContent));

    //            if (File.Exists(filePath))
    //            {
    //                Debug.Log("Success Dwonload video  : " + vid.fileName);

    //                //yield return StartCoroutine(VideoDisplayInCell(filePath));
    //            }
    //            else
    //            {
    //                Debug.Log("Couldn't Write " + vid.fileName);
    //            }
    //        }
    //    }
    //}


    /// <summary>
    /// Uploads the video to the Server.
    /// </summary>
    /// <returns>The video.</returns>
    ///// <param name="filePath">File path.</param>
    //public IEnumerator UploadVideo(string filePath, int id)
    //{
    //    Debug.Log("Upload Video Start");
    //    Debug.Log("Name Video Is: " + filePath);

    //    int index = filePath.LastIndexOf('/') + 1;
    //    string videoName = filePath.Substring(index);


    //    WWW vidFile = new WWW("file://" + filePath);
    //    yield return vidFile;

    //    Debug.Log("Video Length : " + vidFile.bytes.Length);
    //    Debug.Log("Upload Video Request");

    //    WWWForm form = new WWWForm();

    //    //form.AddBinaryData("file", vidFile.bytes, "video-" + videosCount + ".mp4");
    //    form.AddBinaryData("file", vidFile.bytes, filePath);


    //    string UploadUrl = "https://hookline.azurewebsites.net/api/action/AddNewVideo";
    //    Dictionary<string, string> headers = form.headers;
    //    byte[] rawData = form.data;

    //    // Add a custom header to the request.
    //    headers["Authorization"] = "Bearer " + PlayerPrefs.GetString("auth_token");
    //    headers["filename"] = videoName + ".mp4";
    //    headers["videoID"] = id.ToString();
    //    WWW www = new WWW(UploadUrl, rawData, headers);
    //    yield return www;
    //    if (www.error == null)
    //    {
    //        Debug.Log("Video upload complete!");
    //    }
    //    else
    //    {
    //        Debug.Log(www.error);
    //    }

    //}

    // ======================================================
    //                  DELETE PHOTO FROM SERVER By Name it
    // ======================================================
    /// <summary>
    /// Delete the photo from the server.
    /// </summary>
    /// <param name="photoName">Photo name.</param>
    public IEnumerator DeletePhotoFromServer(string photoName)
    {
        photoName = photoName.Replace(".jpg", "");
        Debug.Log("Delete The Next Photo From Server:  " + photoName);
        using (UnityWebRequest www = UnityWebRequest.Delete(API_Settings.STAGING_SERVICE_URL + API_Settings.DELETE_PHOTO_URL + photoName))
        {
            www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(" Download Photo Error : " + www.error);
            }
            else
            {
                yield return null;
                //StartCoroutine(GalleryIsEmpty());

            }
        }

    }

    // ======================================================
    //                  DELETE VIDEO FROM  SERVER By Name it
    // ======================================================
    /// <summary>
    /// Delete the video from the server.
    /// </summary>
    /// <returns>The video from server.</returns>
    /// <param name="videoName">Video name.</param>
    public IEnumerator DeleteVideoFromServer(string videoName)
    {
        videoName = videoName.Replace(".mp4", "");
        videoName = videoName.Replace(".mov", "");
        Debug.Log("Delete The Next Video From Server:  " + videoName);
        using (UnityWebRequest www = UnityWebRequest.Delete(API_Settings.ONLINE_SERVICE_URL + API_Settings.DELETE_VIDEO_URL + videoName))
        {
            www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString("auth_token"));
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(" Download Photo Error : " + www.error);
            }
            else
            {
                yield return null;
                //StartCoroutine(GalleryIsEmpty());

            }
        }
    }

    /// <summary>
    /// Shares the Image Or Video.
    /// </summary>
    /// <param name="filePath">File path.</param>
    public void ShareMedia(string filePath)
    {
        Debug.Log("Share Media");
    }


}