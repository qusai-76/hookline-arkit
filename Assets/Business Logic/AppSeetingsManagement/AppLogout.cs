﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Facebook.Unity;
using UnityEngine.UI;
using Google;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
//using Instagram;
using System.IO;
using System;
using System.Linq;

public class AppLogout : MonoBehaviour {

    public List<GameObject> sceneObjects;

    public void LogoutApp()
    {
        StartCoroutine(ApplicationLogout());
    }

    public void LogoutAppFromGallery()
    {
        StartCoroutine(SendApplicationLogoutFromGalery());
    }

    public void LogoutAppFromSetting()
    {
        StartCoroutine(SendApplicationLogoutFromSetting());

        //StartCoroutine(DeleteAllFilesInPersistentDataPath());
    }


    // Delete photos and videos from persistentDataPath
    IEnumerator DeleteAllFilesInPersistentDataPath()
    {
        DirectoryInfo di = new DirectoryInfo(Application.persistentDataPath);

        var filesOfImages = di.GetFiles("*.jpg");   // store path  images in array

        //#if UNITY_EDITOR || UNITY_IOS
        var filesVideosMP4 = di.GetFiles("*.mp4");   // store path of  videos in array
        var filesVideosMov = di.GetFiles("*.mov");
        //#endif

        FileInfo[] newArray1 = new FileInfo[filesOfImages.Length + filesVideosMP4.Length];
        Array.Copy(filesOfImages, newArray1, filesOfImages.Length);
        Array.Copy(filesVideosMP4, 0, newArray1, filesOfImages.Length, filesVideosMP4.Length);

        FileInfo[] newArray = new FileInfo[newArray1.Length + filesVideosMov.Length];
        Array.Copy(newArray1, newArray, newArray1.Length);
        Array.Copy(filesVideosMov, 0, newArray, newArray1.Length, filesVideosMov.Length);

        var result = newArray.ToList<FileInfo>().OrderBy(ob => ob.CreationTime);
        newArray = result.ToArray<FileInfo>();

        foreach(var file in newArray)
        {
            File.Delete(file.FullName);
        }

        yield return null;

    }

    public IEnumerator SendApplicationLogoutFromGalery()
    {
        yield return StartCoroutine(Logout());
        yield return StartCoroutine(GoToLoginFromGallery());
        yield return null;
    }

    public IEnumerator SendApplicationLogoutFromSetting()
    {
        yield return StartCoroutine(Logout());
        yield return StartCoroutine(GoToLoginFromSetting());
        yield return null;
    }

    public IEnumerator ApplicationLogout()
    {
        yield return StartCoroutine(Logout());
        //yield return StartCoroutine(GoToLogin());
        yield return null;
    }

    public IEnumerator Logout()
    {
        PlayerPrefs.SetString("user_id", "");
        PlayerPrefs.SetString("social_site", "");
        PlayerPrefs.SetInt("logged", 0);
        PlayerPrefs.SetString("firstname", "");
        PlayerPrefs.SetString("lastname", "");
        PlayerPrefs.SetString("auth_token", "");
        PlayerPrefs.SetString("profileimage", "");
        PlayerPrefs.SetString("ForBetterExperienceMessage_Statue", "");

        yield return null;
    }



    // Logout Instagram 
    //public IEnumerator LogoutInstagramSession()
    //{   
    //    print("Start LogoutInstagramSession >>>>>>>>");
    //    using (UnityWebRequest www = UnityWebRequest.Get("https://instagram.com/accounts/logout"))
    //    {
    //        yield return www.SendWebRequest();
    //        if (www.isHttpError || www.isNetworkError)
    //        {
    //            Debug.Log("Logout Error");
    //        }
    //        else
    //        {
    //            Debug.Log("Success Logout : " + www.downloadHandler.text);
    //        }
    //    }
    //    yield return null;
    //}


    // Google Sign out
    public void OnSignOut()
    {
        Debug.Log("Calling Google SignOut");
       // GoogleSignIn.DefaultInstance.SignOut();
    }
    public void GoToLogin()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Login");
    }

    public IEnumerator GoToLoginFromGallery()
    {
        // UnityEngine.SceneManagement.SceneManager.UnloadScene("HallOfFame");
        //var scene = SceneManager.GetSceneByName("Gallery");
        //SetGameObjectsActive(scene.GetRootGameObjects(), false);

        UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync("Gallery");
        UnityEngine.SceneManagement.SceneManager.LoadScene("Login");

        yield return null;
        UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync("Gallery");

    }

    public IEnumerator GoToLoginFromSetting()
    {
        // UnityEngine.SceneManagement.SceneManager.UnloadScene("HallOfFame");
        //var scene = SceneManager.GetSceneByName("Gallery");
        //SetGameObjectsActive(scene.GetRootGameObjects(), false);

        UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync("Setting");
        UnityEngine.SceneManagement.SceneManager.LoadScene("Login");

        yield return null;
        UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync("Setting");

    }

    private void SetGameObjectsActive(GameObject[] objects, bool active)
    {
        for (int i = 0; i < objects.Length; i++)
        {
            objects[i].gameObject.SetActive(active);
        }
    }

    public IEnumerator SendApplicationLogout()
    {
        Debug.Log(" Calling Application Logout ");
        string URL = API_Settings.STAGING_SERVICE_URL + API_Settings.LOGOUT_ROUT;
        using (UnityWebRequest www = UnityWebRequest.Post(URL, ""))
        {
            www.SetRequestHeader("Authorization", "Bearer " + ApplicationGlobal.AUTHENTICATION_TOKEN);
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError) {
                Debug.Log(" Error Logout : " + www.error);
            }
            else
            {
                Debug.Log(" Success Logout ");
            }
        }
    }



}
