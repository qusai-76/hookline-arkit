﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class JsonHelper
{
    [System.Serializable]
    private class JSONArrayWrapper<T>
    {
        public T[] array;
    }

    public static T[] FromJson<T>(string json) {
        string newJson = "{ \"array\": " + json + "}";
        JSONArrayWrapper<T> wrapper = JsonUtility.FromJson<JSONArrayWrapper<T>>(newJson);
        return wrapper.array;
        
        
    }
    
   

    public static string ToJson<T>(T[] array) {
        JSONArrayWrapper<T> wrapper = new JSONArrayWrapper<T>();
        wrapper.array = array;
        return JsonUtility.ToJson(wrapper);
    }

    public static string ToJson<T>(T[] array, bool prettyPrint) {
        JSONArrayWrapper<T> wrapper = new JSONArrayWrapper<T>();
        wrapper.array = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }
    
    
}