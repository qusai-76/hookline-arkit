﻿using System;
using UnityEngine;

namespace AppSeetingsManagement
{
    [Serializable]
    public class UserSession
    {
        public string id="";
        public string auth_token = "";
        public string expires_in = "";
        public string refresh_token = "";
        public string username = "";
        public string firstname = "";
        public string lastname = "";
        public string profileimage = "";
        
        public string[] user_groups;

        public string ToJson()
        {
            return JsonUtility.ToJson(this);
        }

        public byte[] ToPostData()
        {
            return System.Text.Encoding.UTF8.GetBytes(ToJson());
        }

    }
}
