﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ApplicationGlobal {

    public enum AthenticationType{
        Facebook,Google,Instagram,Application
    }

    public static AthenticationType AUTH_TYPE;
    public static string AUTHENTICATION_TOKEN = "";
    public static string REFRESH_TOKEN = "";
    public static string USER_ID = "";
}
