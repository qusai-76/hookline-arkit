﻿using System.Collections;
using System.Collections.Generic;
using Assets.Models;
using UnityEngine;
using UnityEngine.UI;
using Google;
//using System.Threading.Tasks;
//using AppSeetingsManagement;
//using Facebook.Unity;
//using Instagram;
using UnityEngine.Networking;
using AppSeetingsManagement;
using UnityEngine.SceneManagement;

public class LoginController : MonoBehaviour {

    /// <summary>
    /// Define the parametrs
    /// </summary>
    [SerializeField] private InputField userName;
    [SerializeField] private InputField password;
    [SerializeField] private Text validationText;
    public GameObject loginWithEmailButton;
    public GameObject socialButtons;
    public GameObject loginWindow;

    public GameObject Message_VerficationEmailSent;
    public GameObject Message_VerficationFailed;

    public GameObject logoImage;

    public GameObject loadingIndicator;

    public GameObject splitAndroidFiles;

    public bool LoginRequestSent = false;

    private TouchScreenKeyboard keyboard;
    private bool keyboard_open;

    [SerializeField]
    private string redirect="https://www.google.com";

    /// <summary>
    /// API Login.
    /// </summary>
    public IEnumerator Login()
    {
        yield return StartCoroutine(LoginProcess());
    }
    
    
    public bool InternetReachable()
    {
        return (Application.internetReachability != NetworkReachability.NotReachable);
    }
    
    /// <summary>
    /// Runs the login.
    /// </summary>
    public void RunLogin()
    {
        if (InternetReachable())
        {
            if (!LoginRequestSent)
            {
                LoginRequestSent = true;
                StartCoroutine(Login());
            }
        }
        else
        {
            LoginRequestSent = false;
            validationText.text = "Please connect to the internet to log in.";
        }
    }

    /// <summary>
    /// Logins the process.
    /// </summary>
    ///

    private void Update()
    {
        if(userName.isFocused == true && keyboard_open == false)
        {
            keyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.EmailAddress);
            keyboard_open = true;
        }
    }

    public IEnumerator LoginProcess()
    {
        validationText.text = "";
        keyboard_open = false;

        Debug.Log("PROCESS IS ON");

        //Check intrnet connection 
        LoginModel model = new LoginModel();

        //if (!string.IsNullOrEmpty(userName.text) && !string.IsNullOrEmpty(password.text))
        if (!string.IsNullOrEmpty(userName.text))
        {
            if (IsValidEmail(userName.text))
            {
                model.Username = userName.text;
                model.Password = password.text;
                byte[] postData = model.ToPostData();
                Debug.Log(model.ToJson());
                string loginUrl = API_Settings.ONLINE_SERVICE_URL + API_Settings.LOGIN_ROUTE;
                Dictionary<string, string> headers = new Dictionary<string, string>();
                headers.Add("Content-Type", "application/json");

                LoginRequestSent = false;

                yield return StartCoroutine(SendLoginRequest(loginUrl, postData, headers));
            }
            else
            {
                validationText.text = "Email format is incorrect";
                LoginRequestSent = false;
                userName.text = "";
                yield return null;
            }
        }
        else{
            //Debug.Log("Username or password is Empty");
            //validationText.text = "Username or password is Empty";

            model.Username = "";
            model.Password = password.text;

            byte[] postData = model.ToPostData();
            Debug.Log(model.ToJson());
            string loginUrl = API_Settings.ONLINE_SERVICE_URL + API_Settings.LOGIN_ROUTE;
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/json");
            validationText.text = "";
            yield return StartCoroutine(SendLoginRequest(loginUrl, postData, headers));
        }
    }

    float timer = 0;
    
    public IEnumerator LoginTimer()
    {
        while(timer<5)
        {
            timer += 0.001f;
        }
        yield return null;
    }
    
    /// <summary>
    /// Sends the login request.
    /// </summary>
    /// <returns>The login request.</returns>
    /// <param name="URL">URL.</param>
    /// <param name="postData">Post data.</param>
    /// <param name="headers">Headers.</param>
    IEnumerator     SendLoginRequest(string URL,byte [] postData,Dictionary<string,string> headers)
    {
        logoImage.SetActive(false);
        
        loadingIndicator.SetActive(true);
       
        WWW www = new WWW(URL, postData, headers);
        yield return www;

        if (www.error == null) {
            // Login Succeed
            //Debug.Log("Login Succeed");
            //Debug.Log(www.text);
            UserSession userSession = JsonUtility.FromJson<UserSession>(www.text);
            ApplicationGlobal.AUTH_TYPE = ApplicationGlobal.AthenticationType.Application;
            ApplicationGlobal.AUTHENTICATION_TOKEN = userSession.auth_token;
            ApplicationGlobal.REFRESH_TOKEN = userSession.refresh_token;
            ApplicationGlobal.USER_ID = userSession.id;

            PlayerPrefs.SetString("user_id", userSession.id);
            PlayerPrefs.SetString("auth_token", userSession.auth_token);
            PlayerPrefs.SetString("social_site", "app");
            PlayerPrefs.SetInt("logged", 1);
            PlayerPrefs.SetString("user_name", userSession.username);

            // send location to server
            // StartCoroutine(GetLocation());
            while (timer < 1f)
            {
                timer += Time.deltaTime; 
                yield return null;
            }

            yield return UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("HallOfFameGround");

            ///yield return null;
        }
        else {

            ErrorMessage errorMessage = JsonUtility.FromJson<ErrorMessage>(www.text);
            Debug.Log("Error In Login: " + errorMessage);
            Debug.Log("Web Text: " + www.text);
            Debug.Log("Error Text: " + www.error);
            
            //validationText.text = errorMessage.status;
            //Check intrnet connection
            validationText.text = "Check your internet connection";
            loadingIndicator.SetActive(false);

            if (errorMessage.errornum == "1") {
                // show Message: "Email is sent"
                Message_VerficationEmailSent.SetActive(true);
                validationText.text = "";
            }
            else if (errorMessage.errornum == "2") {
                // show Message: "verfiy faield"
                Message_VerficationFailed.SetActive(true);
                validationText.text = "";
            }

            yield return null;

        }
        loadingIndicator.SetActive(false);
        yield return null;

    }

    public void Awake()
    {
        keyboard_open = false;
        if (PlayerPrefs.GetInt("logged") == 0)
        {
        }
        else
        {
            SceneManager.LoadScene("HallOfFameGround");
        }
    }

    public void Start()
    {

        if ( userName.onValueChanged != null)
        {
            // Applay function if inputFiled of Email is changed
            #pragma warning disable CS0618 // Type or member is obsolete
                 userName.onValueChange.AddListener(delegate
            #pragma warning restore CS0618 // Type or member is obsolete
            {
                StartCoroutine(ValueInputFiledUserNameChanged());
            });
        }

    }

    /// <summary>
    /// Facebook Login.
    /// </summary>
    public void LoginWithFaceBook()
    {
        var perms = new List<string>() { "public_profile", "email" };
        //FB.LogInWithReadPermissions(perms, AuthCallback);
    }

    /// <summary>
    /// Facebook Initialize Callback
    /// </summary>
    private void InitCallback() {
        //if (FB.IsInitialized) {
        //    // Signal an app activation App Event
        //    FB.ActivateApp();
        //}
        //else {
        //    Debug.Log("Failed to Initialize the Facebook SDK");
        //}
    }

  
    private List<string> messages = new List<string>();

    /// <summary>
    /// Adds the status text.
    /// </summary>
    /// <param name="text">Text.</param>
    public void AddStatusText(string text)
    {
        if (messages.Count == 5)
        {
            messages.RemoveAt(0);
        }
        messages.Add(text);
        string txt = "";
        foreach (string s in messages)
        {
            txt += "\n" + s;
        }
        Debug.Log(text);
    }

    /// <summary>
    /// Hides the local windwo.
    /// </summary>
    public void HideLocalWindwo()
    {
        loginWindow.SetActive(false);
    }

    /// <summary>
    /// Shows the local login windwo.
    /// </summary>
    public void ShowLocalLoginWindwo()
    {
        loginWindow.SetActive(true);
    }

    /// <summary>
    /// Go to Login Screen.
    /// </summary>
    public void GoToLocalLogin()
    {
        socialButtons.SetActive(false);
        ShowLocalLoginWindwo();
        loginWithEmailButton.SetActive(false);
    }

    /// <summary>
    /// Logout Button.
    /// </summary>
    public void Logout()
    {
        PlayerPrefs.SetString("user_id", "");
        PlayerPrefs.SetString("social_site", "");
        PlayerPrefs.SetString("auth_token", "");
        PlayerPrefs.SetString("user_name", "");
        PlayerPrefs.SetInt("logged", 0);
    }

    /// <summary>
    /// Go to start page.
    /// </summary>
    public void GoToStartPage()
    {
        socialButtons.SetActive(true);
        loginWindow.SetActive(false);
        loginWithEmailButton.SetActive(true);
    }

    /// <summary>
    /// Logins with instagram.
    /// </summary>
    public void LoginWithInstagram()
    {
        loadingIndicator.SetActive(true);
    }

    /// <summary>
    /// Check if email is vaild.
    /// </summary>
    bool IsValidEmail(string Useremail)
    {
        System.Text.RegularExpressions.Regex emailRegex = new System.Text.RegularExpressions.Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
        System.Text.RegularExpressions.Match emailMatch = emailRegex.Match(Useremail);
        return emailMatch.Success;
    }

    /// <summary>
    /// Button for close the message verfication email sent.
    /// </summary>
    public void closeButton_Message_VerficationEmailSent()
    {
        Message_VerficationEmailSent.SetActive(false);
    }

    /// <summary>
    /// Button for close the message verfication Failed.
    /// </summary>
    public void closeButton_Message_VerficationFailed()
    {
        Message_VerficationFailed.SetActive(false);
    }
    
    /// <summary>
   /// When value of the input filed for username is changed.
   /// </summary>
   public IEnumerator ValueInputFiledUserNameChanged()
   {
       yield return new WaitForSeconds(0.2F);
       string textUserName = userName.transform.Find("Text").GetComponent<Text>().text;

       if(textUserName.Length > 0)
       {
           loginWithEmailButton.transform.GetChild(0).GetComponent<Text>().text = "Log in";
       }
       else
       {
           loginWithEmailButton.transform.GetChild(0).GetComponent<Text>().text = "Log in as guest";
       }
       yield return null;
   }

   /// <summary>
   /// Get the location.
   /// </summary>
   /// <returns>The location.</returns>
   private IEnumerator GetLocation()
   {
        Debug.Log("Start getting deveice location ");
       // First, check if user has location service enabled
       if (!Input.location.isEnabledByUser)
           yield break;

       // Start service before querying location
       Input.location.Start();

       // Wait until service initializes
       int maxWait = 20;
       while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
       {
           yield return new WaitForSeconds(1);
           maxWait--;
       }

       // Service didn't initialize in 20 seconds
       if (maxWait < 1)
       {
           print("Timed out");
           yield break;
       }

       // Connection has failed
       if (Input.location.status == LocationServiceStatus.Failed)
       {
           print("Unable to determine device location");
           yield break;
       }
       else
       {
           // Access granted and location value could be retrieved
           print("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
           StartCoroutine(SendLocationToServer(Input.location.lastData.latitude , Input.location.lastData.longitude));
       }
       // Stop service if there is no need to query location updates continuously
       Input.location.Stop();
   }

   public IEnumerator SendLocationToServer(float latitude, float longitude)
   {
       LocationModel model = new LocationModel();


       model.latitude  = latitude;
       model.longitude = longitude;

       byte[] postData = model.ToPostData();
       Debug.Log(model.ToJson());
       string URL = API_Settings.ONLINE_SERVICE_URL + API_Settings.SendLocation_URL;
       Dictionary<string, string> headers = new Dictionary<string, string>();
       headers.Add("Content-Type", "application/json");

       WWW www = new WWW(URL, postData, headers);
       yield return www;

       if (www.error == null) {
           // Login Succeed
           Debug.Log("Send Location Succeed");
           Debug.Log(www.text);

           yield return null;
       }
       else {

           ErrorMessage errorMessage = JsonUtility.FromJson<ErrorMessage>(www.text);
           Debug.Log("Error In Login: " + errorMessage);

       }

       yield return null;
   }
    
}
