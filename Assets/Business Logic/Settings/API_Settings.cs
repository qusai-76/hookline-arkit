﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class API_Settings {

    public static string ONLINE_SERVICE_URL="https://hookline.azurewebsites.net/api/";

    public static string LOGIN_ROUTE="auth/login";
    public static string LOGOUT_ROUT = "auth/logout";
    public static string REGISTER_ROUTE="account/add";
    
    public static string LOCAL_SERVICE_URL = "http://10.99.11.100:3000/api/";
    
    public static string STAGING_SERVICE_URL = "https://hooklinestaging.azurewebsites.net/api/";
    
    public static string LOAD_ALL_VIDEOS_URL = "action/UserVideosGetAllIDS";
    public static string LOAD_ALL_PHOTOS_URL = "action/UserPhotosGetAllIDS";

    public static string LOAD_ALL_PHOTOS_INFOS_URL = "action/UserPhotosGetAll";
    public static string LOAD_ALL_VIDEOS_INFOS_URL = "action/UserVideosGetAll";

    public static string DOWNLOAD_FBX_BY_ID = "action/DownloadFbxFile/";

    //Get All thumbnails
    public static string LOAD_ALL_THUMBNAILS_URL = "action/UserThumbnailsGet";

    //Get Thumbnails IDS
    public static string GET_THUMBNAIL_IDS = "action/UserThumbnailsGetIDs";

    // Download Thumbnail By ID
    public static string DOWNLOAD_THUMBNAIL_BY_ID = "action/UserThumbnailsGetById/";

    // Download Photo By ID
    public static string DOWNLOAD_PHOTO_URL = "https://hookline.azurewebsites.net/api/action/DownloadPhoto/";

    // Download Video by id 
    public static string DOWNLOAD_VIDEO_URL = "https://hookline.azurewebsites.net/api/action/DownloadVideo/";

    // Pass the ID at the end of the object
    public static string DOWNLOAD_AVATAT_URL = "https://hookline.azurewebsites.net/api/action/DownloadObject/"; 
    
    // Pass the avatar ID at the end of the object
    public static string DOWNLOAD_AVATAR_URL = "action/DownloadAvatarFiles/"; 
    
    public static string DOWNLOAD_AVATAT_LOCAL_URL = "action/DownloadObject/"; 
     
    // Old version of Downloading Music
    public static string DOWNLOAD_MUSIC_URL = "https://hookline.azurewebsites.net/api/action/AvatarMusicGet/";
   
    // Download Music For the Animation
    public static string DOWNLOAD_MUSIC_BY_ID = "action/AnimationMusicGet/";
    
    
    //public static string CHECK_AVATAR_MUSIC ="https://hookline.azurewebsites.net/api/action/AvatarHasMusic/";
    
    //// HAS Animation Model
    //public static string CHECK_ANIMATION_URL = "https://hookline.azurewebsites.net/api/action/AvatarHasAnimation/";
    

    // Clothing Feature URLs
    public static string DOWNLOAD_DRESS_LIST = "action/DressList";
    public static string DOWNLOAD_CLOTHED_AVATARS_LIST = "action/AvatarsWithDressList";
    
    public static string DOWNLOAD_DRESS_BY_ID = "action/DownloadDress/";
    
  
    // Get Method with parameter ID (AVATAR_ID)
    public static string LOAD_AVATR_BY_ID = "action/DownloadObject";

    public static string AUTHENTICATION_SERVICE = "auth/";
    public static string FACEBOOK_AUTH = "facebook";
    public static string GOOGLE_AUTH = "google";
    public static string INSTAGRAM_AUTH = "insta";

    // Google Client ID
    public static string GOOGLE_WEB_CLIENT_ID = "951498253681-d1ij2alsfvlkk6ei6tovavoj690hd1sa.apps.googleusercontent.com";

    // Dlete Video or photo
    public static string DELETE_PHOTO_URL = "action/UserPhotoDelete/";
    public static string DELETE_VIDEO_URL = "action/UserVideoDelete/";

    public static string ValidateMessage_URL = "http://hookline.azurewebsites.net/api/action/ValidateMessage";

    // Location Service
    public static string SendLocation_URL = "action/location";

    // Tags IDs with Thumbnails (Avatars Categories)
    public static string GET_ALL_AVATARS_CATEGORIES = "action/clienttagsget";
    
     // Tags IDs with Thumbnails (Avatars Categories)
    public static string GET_ALL_CATEGORIES = "action/GetAllData";

    // Get Data for the Selfie Avatars
    public static string GET_SELFIE_CATEGORIES = "action/GetSelfieModeAvatars";

    // Animation Service (Take Avatar Id and return List Of Animations)
    public static string GET_AVATAR_ANIMATION = "action/GetAnimationDetails/";

    public static string DOWNLOAD_CATEGORIES_URL = "";
      
}
