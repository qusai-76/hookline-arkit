﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class JsonAvatarResponse {
        public int fbxId;
        public string fileContent;
}