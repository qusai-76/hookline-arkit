﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class JsonAvatar {
        public int avatarId;
        public string thumbnailContent;
        public string fileName;
        
        // Bubble Icon Fields
        public string bubbleIcon;
        public string bubbleLink;
    
        public string avatarName;
        public string fileExtension;
        public bool complex;

        public string mode;
        
        public List<JsonFBX> fbx;
}