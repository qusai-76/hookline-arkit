﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class JsonMusic {
        public int musicId;
        public int order;
        public float delay;
}