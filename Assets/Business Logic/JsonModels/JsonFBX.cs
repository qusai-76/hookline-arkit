﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class JsonFBX {
        public int fbxId;
        public bool isMain;
        public string behaviour;
        public bool runMultipleAnimation;
        public string colliderShape;
        public bool cutOutMaterial;
        public List<JsonAnimationBehaiviour> eventBehaviour;
        public List<JsonAnimation> animation;
    }