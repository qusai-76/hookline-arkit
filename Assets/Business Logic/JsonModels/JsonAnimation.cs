﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class JsonAnimation {
        public int id;
        public string order;
        public string repeat;
        public int repeatCount;
        public int avatarId;
        //public int musicId;
        
        public List<JsonMusic> music_list;
        
        public string path;
        public string fileName;
        public float delay;
        public int animationId;
        public string animationName;
        public string avatarName;
}