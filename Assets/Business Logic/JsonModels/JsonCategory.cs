﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class JsonCategory 
{
    public int id;
    public string tag;
    public string type;
    public string tagPhotoContent;
    public List<JsonAvatar> avatars;
}