﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class JsonAnimationBehaiviour
{
    public string id;
	public string eventName;
	public string animationName;
}