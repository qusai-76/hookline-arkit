﻿
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using System;

public class DBScript : MonoBehaviour {

    private DataService dbService;
    
	// Database Initialization
	void Start () {
		StartSync();
	}

    private void StartSync()
    {
        dbService = new DataService("Hookline.db");
        dbService.CreateDB();
    }
	
    /// <summary>
    /// Inserts the photo to Database
    /// </summary>
    /// <returns>The photo.</returns>
    /// <param name="title">Title.</param>
    public Photo InsertPhoto(string title)
    {
        return dbService.InserPhoto(title);
    }
    
    /// <summary>
    /// Inserts the video to Database
    /// </summary>
    /// <returns>The video.</returns>
    /// <param name="title">Title.</param>
    public Video InsertVideo(string title)
    {
        return dbService.InsertVideo(title);
    }
    
    /// <summary>
    /// Inserts the dress into database
    /// </summary>
    /// <param name="dress">Dress.</param>
    public void InsertDress(Dress dress)
    {
        dbService.InsertDress(dress);
    }

    /// <summary>
    /// Inserts the clothes into database
    /// </summary>
    /// <param name="avatar">Avatar.</param>
    public void InsertAvatar(ClotheAvatar avatar)
    {
       dbService.InsertAvatar(avatar);
    }
    
    /// <summary>
    /// Inserts the clothes into database
    /// </summary>
    /// <param name="avatar">Avatar.</param>
    public void InsertDBAvatar(DB_Avatar avatar)
    {
        dbService.InsertDBAvatar(avatar);
    }
    
    /// <summary>
    /// Inserts the category.
    /// </summary>
    /// <returns>The category.</returns>
    /// <param name="id">Identifier.</param>
    /// <param name="name">Name.</param>
    public int InsertCategory(int id,string name)
    {
        if (!CategoryInDataBase(id))
        {
            Debug.Log("Category : " + name + " Not Existed In Database" );
            return dbService.InsertCategory(id, name);
        }
        else {
            Debug.Log("Category : " + name + " Existed In Database");
            return -1;
        }
    }

    /// <summary>
    /// Inserts the category.
    /// </summary>
    /// <returns>The category.</returns>
    /// <param name="id">Identifier.</param>
    /// <param name="name">Name.</param>
    public int UpdateCategory(int id, string name, string type)
    {
            return dbService.UpdateCategory(id, name, type);
    }

    /// <summary>
    /// Inserts the category.
    /// </summary>
    /// <returns>The category.</returns>
    /// <param name="id">Identifier.</param>
    /// <param name="name">Name.</param>
    public int InsertCategory(int id, string name, string type)
    {
        if (!CategoryInDataBase(id))
        {
            Debug.Log("Category : " + name + " Not Existed In Database");
            return dbService.InsertCategory(id, name, type);
        }
        else
        {
            Debug.Log("Category : " + name + " Existed In Database");
            return -1;
        }
    }

    /// <summary>
    /// Inserts the avatar.
    /// </summary>
    /// <returns>The avatar.</returns>
    /// <param name="id">Identifier.</param>
    /// <param name="name">Name.</param>
    /// <param name="_categoryid">Categoryid.</param>
    public int InsertAvatar(int id,string name, string _categoryid)
    {
        return dbService.InsertAvatar(id,name,_categoryid);
    }
    
    /// <summary>
    /// Inserts the DBA vatar.
    /// </summary>
    /// <returns>The DBA vatar.</returns>
    /// <param name="id">Identifier.</param>
    /// <param name="name">Name.</param>
    /// <param name="_categoryid">Categoryid.</param>
    public int InsertDBAvatar(int id,string name, string _categoryid)
    {
        return dbService.InsertAvatar(id,name,_categoryid);
    }
    
    /// <summary>
    /// Inserts the avatar info.
    /// </summary>
    /// <returns>The avatar info.</returns>
    /// <param name="avatar">Avatar.</param>
    public int InsertAvatarInfo(AvatarInfo avatar)
    {
        return dbService.InsertAvatarInfo(avatar);
    }

    /// <summary>
    /// Gets the avatar info by identifier.
    /// </summary>
    /// <returns>The avatar info by identifier.</returns>
    public AvatarInfo GetAvatarInfoById(int id) {
        return dbService.GetAvatarInfoById(id);
    }

    /// <summary>
    /// Deletes the avatar info.
    /// </summary>
    /// <returns>The avatar info.</returns>
    /// <param name="avatar">Avatar.</param>
    public int DeleteAvatarInfo(AvatarInfo avatar) { 
        return dbService.DeleteAvatarInfo(avatar);
    }
    
    /// <summary>
    /// Deletes the avatar info.
    /// </summary>
    /// <returns>The avatar info.</returns>
    //public int DeleteAvatarInfo( AvatarInfo _avatar) { 
    
    //    // TODO Delete FBX 
        
    //    // TODO Delete Animations
                
    //    return dbService.DeleteAvatarInfo(_avatar);
    //}
    
    /// <summary>
    /// Check if the Category existed in the data base.
    /// </summary>
    /// <returns><c>true</c>, if in data base was categoryed, <c>false</c> otherwise.</returns>
    /// <param name="id">Identifier.</param>
    public bool CategoryInDataBase(int id)
    {
        return dbService.CategoryInDataBase(id);
    }
    
    /// <summary>
    /// Check if the Avatar in the data base.
    /// </summary>
    /// <returns><c>true</c>, if in data base was avatared, <c>false</c> otherwise.</returns>
    /// <param name="id">Identifier.</param>
    public bool AvatarInDataBase(int id)
    {
        return dbService.AvatarInDataBase(id);
    }

    /// <summary>
    /// Gets the category by identifier.
    /// </summary>
    /// <returns>The category by identifier.</returns>
    /// <param name="id">Identifier.</param>
    public AvatarCategory GetCategoryById(int id)
    {
        return dbService.GetCategoryById(id);
    }
    
    /// <summary>
    /// Gets all categories.
    /// </summary>
    /// <returns>The all categories.</returns>
    public IEnumerable<AvatarCategory> GetAllCategories()
    {
        return dbService.GetAllCategories();
    }
    
    /// <summary>
    /// Gets the category by identifier.
    /// </summary>
    /// <returns>The category by identifier.</returns>
    /// <param name="id">Identifier.</param>
    public DB_Avatar GetAvatrById(int id) {
        return dbService.GetDBAvatarByID(id);
    }
    
    /// <summary>
    /// Inserts the animation.
    /// </summary>
    /// <returns>The animation.</returns>
    /// <param name="animModel">Animation model.</param>
    public int InsertAnimation(AnimationModel animModel) {
        return dbService.InsertAnimation(animModel);
    }
   
    /// <summary>
    /// Inserts the animation info.
    /// </summary>
    /// <returns>The animation info.</returns>
    /// <param name="animation">Animation.</param>
    public int InsertAnimationInfo(AnimationInfo animation)
    {
        return dbService.InsertAnimationInfo(animation);
    }
    
    /// <summary>
    /// Deletes the animation info.
    /// </summary>
    /// <returns>The animation info.</returns>
    public int DeleteAnimationInfo(AnimationInfo animation)
    {
        return dbService.DeleteAnimationInfo(animation);
    }

    /// <summary>
    /// Inserts the FBXD etails.
    /// </summary>
    /// <returns>The FBXD etails.</returns>
    /// <param name="fbxInfo">Fbx info.</param>
    public int InsertFBXDetails(DB_FBX fbxInfo) {
        return dbService.InserFBX(fbxInfo);
    }
    
    /// <summary>
    /// Deletes the music item.
    /// </summary>
    /// <returns>The music item.</returns>
    /// <param name="musicItem">Music item.</param>
    public int DeleteMusicItem(MusicDB musicItem) {
        try
        {
            return dbService.DeleteMusicItem(musicItem);
        }
        catch (Exception ex)
        {
            return -1;
        }
    }
    
    /// <summary>
    /// Deletes the fbx.
    /// </summary>
    /// <returns>The fbx.</returns>
    /// <param name="id">Identifier.</param>
    public int DeleteFBX(DB_FBX fbx) {
        return dbService.DeleteFBX(fbx);
    }

    /// <summary>
    /// Animations the exists.
    /// </summary>
    /// <returns><c>true</c>, if exists was animationed, <c>false</c> otherwise.</returns>
    /// <param name="id">Identifier.</param>
    public bool AnimationExists(int id)
    {
        return dbService.AnimationExists(id);
    }

    /// <summary>
    /// Gets the animation by avatar Id
    /// </summary>
    /// <returns>The animation by avatar.</returns>
    /// <param name="avatarID">Avatar identifier.</param>
    public AvatarAnimation GetAnimationByAvatar(int avatarID)
    {
        return dbService.GetAnimationByAvatar(avatarID);
    }
    
    /// <summary>
    /// Deletes the category by identifier.
    /// </summary>
    /// <param name="category">Category.</param>
    public void DeleteCategoryByID(AvatarCategory category)
    {
        dbService.DeleteCategoryByID(category);
    }
    
    /// <summary>
    /// Deletes the avatar by identifier.
    /// </summary>
    /// <param name="avatar">Avatar.</param>
    public void DeleteAvatarByID(DB_Avatar avatar)
    {
        dbService.DeleteAvatarByID(avatar);
    }
     
    /// <summary>
     /// Deletes all categories.
     /// </summary>
    public void DeleteAllCategories() {
        dbService.DeleteAllCategories();
    }
    
    /// <summary>
    /// Deletes all avatars.
    /// </summary>
    public void DeleteAllAvatars() {
        dbService.DeleteAllAvatars();
    }
    
    /// <summary>
    /// Deletes all animation.
    /// </summary>
    public void DeleteAllAnimationInfo() {
        dbService.DeleteAllAnimationInfo();
    }
    
    /// <summary>
    /// Deletes all FBX info.
    /// </summary>
    public void DeleteAllFBXinfo() {
        dbService.DeleteAllFBXinfo();
    }
    
    /// <summary>
    /// Deletes all music info.
    /// </summary>
    public void DeleteAllMusicInfo() {
          dbService.DeleteAllMusicInfo();
    }
    
    /// <summary>
    /// Gets the avatars by category identifier.
    /// </summary>
    /// <returns>The avatars by category identifier.</returns>
    /// <param name="id">Identifier.</param>
    public IEnumerable<AvatarInfo> GetAvatarsByCategoryID(int id) {
        return dbService.GetAvatarsByCategoryID(id);
    }
    
    /// <summary>
    /// Inserts the music.
    /// </summary>
    /// <returns>The music.</returns>
    /// <param name="music">Music.</param>
    public int InsertMusic(MusicDB music)
    {
        return dbService.InsertMusicDB(music);
    }
    
    /// <summary>
    /// Updates the music.
    /// </summary>
    /// <returns>The music.</returns>
    /// <param name="music">Music.</param>
    public int UpdateMusic(MusicDB music)
    {
        try
        {
            return dbService.UpdateMusic(music);
        }
        catch (Exception e){
            //Debug.Log("Cant update music");
            return -1;
        }
    }

    ///// <summary>
    ///// Print All Photos
    ///// </summary>
    ///// <param name="photos">Photos.</param>
    //private void ToConsole(IEnumerable<Photo> photos){
    //    foreach (var photo in photos) {
    //        ToConsole(photo.ToString());
    //    }
    //}

    //private void ToConsole(string msg){
    //    //DebugText.text += System.Environment.NewLine + msg;
    //    Debug.Log (msg);
    //}

    /// <summary>
    /// Gets the fbx by avatar identifier.
    /// </summary>
    /// <returns>The fbx by avatar identifier.</returns>
    /// <param name="avatar_id">Avatar identifier.</param>
    public IEnumerable<DB_FBX> GetFbxByAvatarId(int avatar_id)
    {
        return dbService.GetFbxByAvatarId(avatar_id);
    }
    
    /// <summary>
    /// Gets the animation by fbx identifier.
    /// </summary>
    /// <returns>The animation by fbx identifier.</returns>
    /// <param name="fbxID">Fbx identifier.</param>
    public IEnumerable<AnimationInfo> GetAnimationByFbxID(int fbxID)
    {
        return dbService.GetAnimationByFbxID(fbxID);
    }
    
    
    public IEnumerable<MusicDB> GetMusicIdsByAnimationID(int id)
    {
        return dbService.GetMusicIdsByAnimationID(id);
    }

    public DB_FBX GetFBXByID(int id)
    {
        return dbService.GetFBXByID(id);
    }
    
    
    public MusicDB GetMusicItemById(int animationID,int musicId)
    {
        return dbService.GetMusicItemById( animationID , musicId);
    }


    #region Bubble CRUD
    
    public int InsertBubbleInfo(int Id, string link)
    {
        return dbService.InsertBubble(Id,link);
    }
    
    public void DeleteBubbleInfo(int Id )
    {
         dbService.DeleteBubble(Id);
    }
    
    
    public BubbleDB GetBubbleInfo(int Id)
    {
        return dbService.GetBubbleInfo(Id);
    }
    
       
    #endregion
}
