﻿
using SQLite4Unity3d;

public class DB_FBX  {

    [PrimaryKey]
    public int Id { get; set; }
    public int AvatarID { get; set; }
    public bool IsMain { get; set; }
    public string Behaviour { get; set; }
    public string ColliderShape { get; set; }
    public bool RunMultipleAnimation { get; set; }
    public bool CutOutMaterial { get; set; }
    
    public override string ToString () {
        return string.Format ("[FBX : Id={0}, AvatarID={1}]", Id, AvatarID);
    }
    
}