﻿
using SQLite4Unity3d;


public class BubbleDB  {

    [PrimaryKey]
    public int AvatarId  { get; set; }
    public string BubbleLink { get; set; }
    
    public override string ToString ()
    {
        return string.Format ("[Link : Id={0}, Avatar={1}]", BubbleLink, AvatarId);
    }
}
