﻿
using SQLite4Unity3d;

public class MusicDB  {
    [PrimaryKey, AutoIncrement]
    public int Id  { get; set; }
    public int MusicId { get; set; }
    public int animationId { get; set; }
    public int order { get; set; }
    public float delay { get; set; }
}