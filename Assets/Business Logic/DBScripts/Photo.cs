﻿using SQLite4Unity3d;

public class Photo {

    [PrimaryKey, AutoIncrement]
    public int Id { get; set; }
    public string Title { get; set; }

    public override string ToString ()
    {
        return string.Format("[Photo: Id={0}, Title={1}]", Id, Title);
    }
}
