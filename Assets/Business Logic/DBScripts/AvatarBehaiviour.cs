﻿using SQLite4Unity3d;

public class AnimationBehaiviour
{
    [PrimaryKey]
    public int ID { get; set; }
    public int FbxId { get; set; }
    public string EventName { get; set; }
    public string AnimationName { get; set; }
}