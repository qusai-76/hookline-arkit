﻿using SQLite4Unity3d;

public class AvatarCategory  {

    [PrimaryKey]
    public int Id { get; set; }
    public string Name { get; set; }
    public string Type { get; set; }

    public override string ToString ()
    {
        return string.Format ("[Category : Id={0}, Title={1}]", Id, Name);
    }
}
