﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DressModel
{
    public string id;
    public string filename;
}
