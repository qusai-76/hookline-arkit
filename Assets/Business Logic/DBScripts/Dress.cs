﻿using System;
using System.Collections;
using System.Collections.Generic;
using SQLite4Unity3d;
using UnityEngine;

public class Dress 
{   
    [PrimaryKey]
    public int id { get; set; }
    public string filename { get; set; }
}
