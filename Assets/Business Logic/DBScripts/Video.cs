﻿using SQLite4Unity3d;

public class Video  {

    [PrimaryKey, AutoIncrement]
    public int Id { get; set; }
    public string Title { get; set; }

    public override string ToString ()
    {
        return string.Format ("[Video: Id={0}, Title={1}]", Id, Title);
    }
}
