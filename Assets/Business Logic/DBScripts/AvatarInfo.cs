﻿
using SQLite4Unity3d;

public class AvatarInfo  {

    [PrimaryKey]
    public int avatarId{ get; set; }
    public int category { get; set; }
    public string fileName { get; set; }
    public string avatarName { get; set; }
    public string fileExtension { get; set; }
    public bool complex { get; set; }
    public string mode { get; set; }


    public override string ToString () {
        return string.Format ("[Avatar : Id={0}, Title={1}]", avatarId, fileName);
    }
    
}
