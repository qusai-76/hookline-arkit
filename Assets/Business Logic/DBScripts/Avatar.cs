﻿using System;
using System.Collections;
using System.Collections.Generic;
using SQLite4Unity3d;
using UnityEngine;

[Serializable]
public class ClotheAvatar 
{
    [PrimaryKey]
    public int id { get; set; }
    public string filename { get; set; }
    
    public override string ToString ()
    {
        return string.Format ("[Avatar : Id={0}, Title={1}]", id, filename);
    }
}

