﻿using SQLite4Unity3d;
using UnityEngine;
using System.IO;
#if !UNITY_EDITOR
using System.Collections;
using System.IO;
#endif
using System.Collections.Generic;

public class DataService {

    private SQLiteConnection _connection;
    private string dbPath;

    /// <summary>
    /// Initializes the Database Instance
    /// </summary>
    /// <param name="DatabaseName">Database name.</param>
    public DataService(string DatabaseName) {

#if UNITY_EDITOR
        dbPath = string.Format(@"Assets/StreamingAssets/{0}", DatabaseName);
#else
        // check if file exists in Application.persistentDataPath
        var filepath = string.Format("{0}/{1}", Application.persistentDataPath, DatabaseName);

        if (!File.Exists(filepath))
        {
            Debug.Log("Database not in Persistent path");
            // if it doesn't ->
            // open StreamingAssets directory and load the db ->

#if UNITY_ANDROID 
            var loadDb = new WWW("jar:file://" + Application.dataPath + "!/assets/" + DatabaseName);  
            // this is the path to your StreamingAssets in android
            while (!loadDb.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
            // then save to Application.persistentDataPath
            File.WriteAllBytes(filepath, loadDb.bytes);
#elif UNITY_IOS
                 var loadDb = Application.dataPath + "/Raw/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
                // then save to Application.persistentDataPath
                File.Copy(loadDb, filepath);
#elif UNITY_WP8
                var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
                // then save to Application.persistentDataPath
                File.Copy(loadDb, filepath);

#elif UNITY_WINRT
		var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);
		
#elif UNITY_STANDALONE_OSX
		var loadDb = Application.dataPath + "/Resources/Data/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);
#else
	var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
	// then save to Application.persistentDataPath
	File.Copy(loadDb, filepath);

#endif

            Debug.Log("Database written");
            
        }

        var dbPath = filepath;
#endif
        _connection = new SQLiteConnection(dbPath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create);
        Debug.Log("Final PATH : " + dbPath);
        CreateDB();
    }

    /// <summary>
    /// Creates the Hookline Database.
    /// </summary>
	public void CreateDB() {

        _connection.CreateTable<AvatarCategory>();
        _connection.CreateTable<MusicDB>();
        _connection.CreateTable<AvatarInfo>();
        _connection.CreateTable<DB_FBX>();
        _connection.CreateTable<AnimationInfo>();
        _connection.CreateTable<BubbleDB>();
        
        _connection.CreateTable<Video>();
        _connection.CreateTable<Photo>();
    }

    /// <summary>
    /// Selects All Users From Database
    /// </summary>
    /// <returns>The all users.</returns>
    public IEnumerable<User> GetAllUsers() {
        return _connection.Table<User>();
    }

    /// <summary>
    /// Creates User By Name
    /// </summary>
    /// <returns>The user.</returns>
    /// <param name="_username">Username.</param>
    public User CreateUser(string _username) {
        var _user = new User {
            Name = _username,
        };
        _connection.Insert(_user);
        return _user;
    }

    /// <summary>
    /// Insers Photo By Name
    /// </summary>
    /// <returns>The photo.</returns>
    /// <param name="_title">Title.</param>
    public Photo InserPhoto(string _title) {
        var _picture = new Photo {
            Title = _title,
        };
        _connection.Insert(_picture);
        return _picture;
    }

    /// <summary>
    /// Insers Video By Name
    /// </summary>
    /// <returns>The video.</returns>
    /// <param name="_title">Title.</param>
    public Video InsertVideo(string _title) {
        var _video = new Video {
            Title = _title,
        };
        _connection.Insert(_video);
        return _video;
    }

    /// <summary>
    /// Inserts the dress into database
    /// </summary>
    /// <param name="dress">Dress.</param>
    public void InsertDress(Dress dress)
    {
        _connection.Insert(dress);
    }

    /// <summary>
    /// Inserts the clothes into database
    /// </summary>
    /// <param name="avatar">Avatar.</param>
    public void InsertAvatar(ClotheAvatar avatar)
    {
        _connection.Insert(avatar);
    }

    /// <summary>
    /// Inserts the clothes into database
    /// </summary>
    /// <param name="avatar">Avatar.</param>
    public void InsertDBAvatar(DB_Avatar avatar)
    {
        _connection.Insert(avatar);
    }

    /// <summary>
    /// Selects All Photos From Database
    /// </summary>
    /// <returns>The all photos.</returns>
    public IEnumerable<Photo> GetAllPhotos() {
        return _connection.Table<Photo>();
    }

    /// <summary>
    /// Inserts the category.
    /// </summary>
    /// <returns>The category.</returns>
    /// <param name="id">Identifier.</param>
    /// <param name="name">Name.</param>
    public int InsertCategory(int id, string name)
    {
        var _category = new AvatarCategory { Id = id, Name = name };
        return _connection.Insert(_category);
    }

    /// <summary>
    /// Inserts the category.
    /// </summary>
    /// <returns>The category.</returns>
    /// <param name="id">Identifier.</param>
    /// <param name="name">Name.</param>
    public int UpdateCategory(int id, string name, string type)
    {
        var _category = new AvatarCategory { Id = id, Name = name, Type=type };
        return _connection.Update(_category);
    }


    /// <summary>
    /// Inserts the category.
    /// </summary>
    /// <returns>The category.</returns>
    /// <param name="id">Identifier.</param>
    /// <param name="name">Name.</param>
    public int InsertCategory(int id, string name, string type)
    {
        var _category = new AvatarCategory { Id = id, Name = name , Type=type};
        return _connection.Insert(_category);
    }

    /// <summary>
    /// Inserts the avatar.
    /// </summary>
    /// <returns>The avatar.</returns>
    /// <param name="id">Identifier.</param>
    /// <param name="name">Name.</param>
    /// <param name="_categoryid">Categoryid.</param>
    public int InsertAvatar(int id, string name, string _categoryid)
    {
        var _avatar = new DB_Avatar
        {
            Id = id,
            Name = name,
            CategoryID = _categoryid
        };
        return _connection.Insert(_avatar);
    }

    /// <summary>
    /// Inserts the music to the databse.
    /// </summary>
    /// <returns>The music db.</returns>
    public int InsertMusicDB(MusicDB musicDB)
    {
        return _connection.Insert(musicDB);
    }

    /// <summary>
    /// Check Categorie if existed in the database.
    /// </summary>
    /// <returns><c>true</c>, if in data base was categoryed, <c>false</c> otherwise.</returns>
    /// <param name="id">Identifier.</param>
    public bool CategoryInDataBase(int id)
    {
        AvatarCategory category = _connection.Table<AvatarCategory>().Where(cat => cat.Id == id).FirstOrDefault();
        if (category != null)
        {
            return true;
        }
        else {
            return false;
        }
    }

    /// <summary>
    /// Check Categorie if existed in the database.
    /// </summary>
    /// <returns><c>true</c>, if in data base was categoryed, <c>false</c> otherwise.</returns>
    /// <param name="id">Identifier.</param>
    public DB_Avatar GetDBAvatarByID(int id)
    {
        return _connection.Table<DB_Avatar>().Where(ava => ava.Id == id).FirstOrDefault();
    }

    /// <summary>
    /// Check Avatars is Existed in the database.
    /// </summary>
    /// <returns><c>true</c>, if in data base was avatared, <c>false</c> otherwise.</returns>
    /// <param name="id">Identifier.</param>
    public bool AvatarInDataBase(int id)
    {
        DB_Avatar _avatar = _connection.Table<DB_Avatar>().Where(avatar => avatar.Id == id).FirstOrDefault();
        if (_avatar != null)
        {
            return true;
        }
        else {
            return false;
        }
    }

    public bool AnimationExists(int id)
    {
        AvatarAnimation _anim = _connection.Table<AvatarAnimation>().Where(anim => anim.Id == id).FirstOrDefault();
        if (_anim != null)
        {
            return true;
        }
        else {
            return false;
        }
    }

    /// <summary>
    /// Gets category by identifier.
    /// </summary>
    /// <returns>The category by identifier.</returns>
    /// <param name="id">Identifier.</param>
    public AvatarCategory GetCategoryById(int id)
    {
        return _connection.Table<AvatarCategory>().Where(cat => cat.Id == id).FirstOrDefault();
    }

    public IEnumerable<AvatarCategory> GetAllCategories()
    {
        return _connection.Table<AvatarCategory>();
    }

    public IEnumerable<AvatarInfo> GetAvatarsByCategoryID(int id) {
        return _connection.Table<AvatarInfo>().Where(avatar => avatar.category == id);
    }

    /// <summary>
    /// Deletes the category by identifier.
    /// </summary>
    /// <param name="cat">Cat.</param>
    public void DeleteCategoryByID(AvatarCategory cat) {
        _connection.Delete<AvatarCategory>(cat.Id);
    }

    /// <summary>
    /// Deletes the avatar by identifier.
    /// </summary>
    /// <param name="avatar">Avatar.</param>
    public void DeleteAvatarByID(DB_Avatar avatar)
    {
        _connection.Delete<DB_Avatar>(avatar.Id);
    }

    /// <summary>
    /// Deletes all categories.
    /// </summary>
    public void DeleteAllCategories() {
        _connection.DeleteAll<AvatarCategory>();
    }

    /// <summary>
    /// Deletes all avatars.
    /// </summary>
    public void DeleteAllAvatars() {
        _connection.DeleteAll<DB_Avatar>();
    }

    /// <summary>
    /// Gets the animation by avatar Id
    /// </summary>
    /// <returns>The animation by avatar.</returns>
    /// <param name="avatarID">Avatar identifier.</param>
    public AvatarAnimation GetAnimationByAvatar(int avatarID)
    {
        return _connection.Table<AvatarAnimation>().Where(anim => anim.AvatarId == avatarID).FirstOrDefault();
    }

    /// <summary>
    /// Inserts the animation.
    /// </summary>
    /// <returns>The animation.</returns>
    /// <param name="animModel">Animation model.</param>
    public int InsertAnimation(AnimationModel animModel)
    {
        var _animation = new AvatarAnimation
        {
            Id = int.Parse(animModel.animationId),
            Order = animModel.order,
            Repeat = animModel.repeat,
            RepeatCount = int.Parse(animModel.repeatCount),
            AvatarId = int.Parse(animModel.avatarId),
            MusicId = int.Parse(animModel.musicId),
            Path = animModel.path,
            FileName = animModel.fileName,
            Delay = float.Parse(animModel.delay),
            AnimationId = int.Parse(animModel.animationId),
            AnimationName = animModel.animationName,
            AvatarName = animModel.avatarName
        };
        return _connection.Insert(_animation);
    }

    /// <summary>
    /// Inserts the animation info.
    /// </summary>
    /// <returns>The animation info.</returns>
    /// <param name="animation">Animation.</param>
    public int InsertAnimationInfo(AnimationInfo animation)
    {
        AnimationInfo _animation = _connection.Table<AnimationInfo>().Where(anim => anim.Id == animation.Id).FirstOrDefault();
        if (_animation == null)
        {
            return _connection.Insert(animation);
        }
        else
        {
            return _connection.Update(animation);
        }
    }

    /// <summary>
    /// Deletes the animation info.
    /// </summary>
    /// <returns>The animation info.</returns>
    /// <param name="animation">Animation.</param>
    public int DeleteAnimationInfo(AnimationInfo animation)
    {
        return _connection.Delete(animation);
    }


    public int DeleteMusicItem(MusicDB musicItem)
    {
        return _connection.Delete(musicItem);
    }

    /// <summary>
    /// Deletes all animation.
    /// </summary>
    public void DeleteAllAnimationInfo() {
        _connection.DeleteAll<AnimationInfo>();
    }
    
    public void DeleteAllFBXinfo() {
        _connection.DeleteAll<DB_FBX>();
    }
    
    public void DeleteAllMusicInfo() {
        _connection.DeleteAll<MusicDB>();
    }
    
    /// <summary>
    /// Inserts the avatar info.
    /// </summary>
    /// <returns>The avatar info.</returns>
    /// <param name="avatar">Avatar.</param>
    public int InsertAvatarInfo(AvatarInfo avatar)
    {
        AvatarInfo _avatar = _connection.Table<AvatarInfo>().Where(av => av.avatarId == avatar.avatarId).FirstOrDefault();
        if (_avatar == null) {
            return _connection.Insert(avatar);
        }
        else {
            return _connection.Update(avatar);
        }
    }


    public AvatarInfo GetAvatarInfoById(int id) {
        return _connection.Table<AvatarInfo>().Where(av => av.avatarId == id).FirstOrDefault();
    }

    /// <summary>
    /// Deletes the avatar info.
    /// </summary>
    /// <returns>The avatar info.</returns>
    /// <param name="avatar">Avatar.</param>
    public int DeleteAvatarInfo(AvatarInfo avatar)
    {
        return _connection.Delete(avatar);
    }



    /// <summary>
    /// Deletes the avatar info by ID
    /// </summary>
    /// <returns>The avatar info.</returns>
    /// <param name="id">Identifier.</param>
    public int DeleteAvatarInfo(int id) {
        return _connection.Delete(id);
    }

    /// <summary>
    /// Insers the fbx.
    /// </summary>
    /// <returns>The fbx.</returns>
    /// <param name="fbx">Fbx.</param>
    public int InserFBX(DB_FBX fbx)
    {
        DB_FBX _fbx = _connection.Table<DB_FBX>().Where(fb => fb.Id == fbx.Id).FirstOrDefault();
        if (_fbx == null) {
            return _connection.Insert(fbx);
        }
        else {
            return _connection.Update(fbx);
        }
    }



    /// <summary>
    /// Deletes the fbx.
    /// </summary>
    /// <returns>The fbx.</returns>
    /// <param name="id">Identifier.</param>
    public int DeleteFBX(DB_FBX fbx) {
        //DB_FBX fbx = _connection.Table<DB_FBX>().Where(fb=> fb.Id==id).FirstOrDefault();
        return _connection.Delete(fbx);
    }


    /// <summary>
    /// Gets the fbx by avatar identifier.
    /// </summary>
    /// <returns>The fbx by avatar identifier.</returns>
    /// <param name="avatar_id">Avatar identifier.</param>
    public IEnumerable<DB_FBX> GetFbxByAvatarId(int avatar_id)
    {
        return _connection.Table<DB_FBX>().Where(fbx => fbx.AvatarID == avatar_id);
    }

    /// <summary>
    /// Gets the animation by fbx identifier.
    /// </summary>
    /// <returns>The animation by fbx identifier.</returns>
    /// <param name="fbxID">Fbx identifier.</param>
    public IEnumerable<AnimationInfo> GetAnimationByFbxID(int fbxID)
    {
        return _connection.Table<AnimationInfo>().Where(anim => anim.FbxId == fbxID);
    }

    public DB_FBX GetFBXByID(int id)
    {
        return _connection.Table<DB_FBX>().Where(fbx => fbx.Id == id).FirstOrDefault();
    }

    public int UpdateMusic(MusicDB music)
    {
        MusicDB _music = _connection.Table<MusicDB>().Where(m => m.MusicId == music.MusicId).FirstOrDefault();
        if (_music == null) {
            return _connection.Insert(music);
        }
        else {
            return _connection.Update(music);
        }
    }

    public IEnumerable<MusicDB> GetMusicIdsByAnimationID(int Id)
    {
        return _connection.Table<MusicDB>().Where(music => music.animationId == Id);
    }

    public MusicDB GetMusicItemById(int animationID,int musicId){
        return _connection.Table<MusicDB>().Where(music=> music.animationId==animationID && music.MusicId==musicId).FirstOrDefault();
    }
    
    /// <summary>
    /// Inserts the bubble.
    /// </summary>
    /// <returns>The bubble.</returns>
    /// <param name="Id">Identifier.</param>
    /// <param name="link">Link.</param>
    public int InsertBubble(int Id, string link) {
    
        BubbleDB bubble = new BubbleDB();
        bubble.AvatarId = Id;
        bubble.BubbleLink = link;
        
        return InsertBubbleInfo(bubble);
    }
    
    public int DeleteBubble(int Id) {
         //Debug.Log("Deleting Bubble ID : "+ Id);
         return _connection.Delete<BubbleDB>(Id);
    }
    
     public int InsertBubbleInfo(BubbleDB bubbledb) {
    
        BubbleDB bubble = _connection.Table<BubbleDB>().Where(bub=>bub.AvatarId==bubbledb.AvatarId).FirstOrDefault();
        if (bubble == null)
        {
            return  _connection.Insert(bubbledb);
        }
        else { 
        
            return  _connection.Update(bubbledb);
            
        }
    }
    
    public BubbleDB GetBubbleInfo(int id)
    {
        return _connection.Table<BubbleDB>().Where(bub=>bub.AvatarId==id).FirstOrDefault();
    }


}
