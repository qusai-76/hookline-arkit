﻿
using SQLite4Unity3d;

public class AvatarAnimation 
{
    [PrimaryKey]
    public int Id { get; set; }
    public int FbxId { get; set; }
    public string Order { get; set; }
    public string Repeat { get; set; } // "Once" or "Multiple" or "Loop"
    public int RepeatCount { get; set; } // Repetition Count
    public int AvatarId { get; set; } 
    public int MusicId { get; set; }
    public string Path { get; set; }
    public string FileName { get; set; }
    public float Delay { get; set; } // Delay Before Animation
    public int AnimationId { get; set; } // Animation ID
    public string AnimationName { get; set; } // The Animation Name
    public string AvatarName { get; set; } // Aries
}