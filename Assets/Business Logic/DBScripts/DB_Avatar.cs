﻿
using SQLite4Unity3d;

public class DB_Avatar  {

    [PrimaryKey]
    public int Id { get; set; }
    public string Name { get; set; }
    public string CategoryID { get; set; }
    public bool HasAnimation { get; set; }
    public bool ContinuousAnimation { get; set; }
    public bool Complex { get; set; }
    public string RelatedAvatars { get; set; }
    public string AnimationSequence { get; set; }
    
    public override string ToString () {
        return string.Format ("[Avatar : Id={0}, Title={1}]", Id, Name);
    }
    
}
