﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingIndicatior : MonoBehaviour
{

    public GameObject Loading;
    
    // Start is called before the first frame update
    void Start()
    {
        Vector3 center = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width/2,Screen.height/2,1));
        Loading.transform.position = center;
    }

    
}
