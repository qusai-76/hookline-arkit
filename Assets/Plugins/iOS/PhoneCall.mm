#import <Foundation/Foundation.h>
#import <CallKit/CallKit.h>
#import <AVFoundation/AVAudioSession.h>

@interface PhoneCall : NSObject <CXCallObserverDelegate>
{
    CXCallObserver *callObserver;
    AVAudioSession *audioSession;
    bool callStatus;
}
@end

@implementation PhoneCall

static PhoneCall *_sharedInstance;

+(PhoneCall*) sharedInstance {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[PhoneCall alloc] init];
        NSLog(@"Creating PHoneCall");
    });
    return _sharedInstance;
}

- (id)init
{
    self = [super init];
    
    if(self){
        [self initHelper];
    }
    return self;
}


-(void) initHelper{
    callObserver = [[CXCallObserver alloc] init];
    [callObserver setDelegate:self queue:dispatch_get_main_queue()];
    audioSession = [AVAudioSession sharedInstance];
    callStatus =false;
    NSLog(@"Init Helper Called");
}

- (void)callObserver:(CXCallObserver *)callObserver callChanged:(CXCall *)call{
    if (call == nil || call.hasEnded == YES) {
        NSLog(@"CXCallState : Disconnected");
        callStatus=false;
    }
    
    if (call.isOutgoing == YES && call.hasConnected == NO) {
        NSLog(@"CXCallState : Dialing");
        callStatus=true;
    }
    
    if (call.isOutgoing == NO  && call.hasConnected == NO && call.hasEnded == NO && call != nil) {
        NSLog(@"CXCallState : Incoming");
        callStatus=true;
    }
    
    if (call.hasConnected == YES && call.hasEnded == NO) {
        NSLog(@"CXCallState : Connected");
        callStatus=true;
    }
}

-(bool) isOnPhoneCall
{
    for (CXCall *call in callObserver.calls)
    {
        if (call.isOnHold || call.isOutgoing || call.hasConnected ) {
            return true;
        }
        if(call.hasEnded)
        {
            NSError *err = nil;
            [audioSession setActive:NO error:&err];
            if(err){
                NSLog(@"Error in Disabling Audio : %@ %d %@", [err domain], [err code], [[err userInfo] description]);
                return true;
            }
        }
    }
    
    return false;
}

-(int) numcerOfCalls{
    return callObserver.calls.count;
}

-(bool) isMicrophonInUsage
{
    NSError *err = nil;
    [audioSession setCategory :AVAudioSessionCategoryPlayAndRecord error:&err];
    if(err){
        NSLog(@"audioSession: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        return NO;
    }
    [audioSession setActive:YES error:&err];
    //err = nil;
    if(err){
        NSLog(@"audioSession: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        return NO;
    }
    return YES;
}
-(bool) currentCallStatus
{
    return callStatus;
}

-(bool) isMicPresent
{
    NSArray *availableInputs = [[AVAudioSession sharedInstance] availableInputs];
    bool micPresent = false;
    for (AVAudioSessionPortDescription *port in availableInputs)
    {
        if ([port.portType isEqualToString:AVAudioSessionPortBuiltInMic] ||
            [port.portType isEqualToString:AVAudioSessionPortHeadsetMic])
        {
            micPresent = true;
        }
    }
    return micPresent;
}

@end

extern "C" {
    
    bool _InPhoneCall()
    {
        return ([[PhoneCall sharedInstance] isOnPhoneCall]);
    }
    
    bool _IsMicInUsage()
    {
        return ([[PhoneCall sharedInstance] isMicrophonInUsage]);
    }
    
    bool _Is_Mic_Present()
    {
        return ([[PhoneCall sharedInstance] isMicPresent]);
    }
    
    int NumcerOfCalls()
    {
        return ([[PhoneCall sharedInstance] numcerOfCalls]);
    }
    
    bool CallStatus()
    {
         return ([[PhoneCall sharedInstance] currentCallStatus]);
    }
}

