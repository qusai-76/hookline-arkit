﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;

public class FireBaseNotificationController : MonoBehaviour
{
	Firebase.FirebaseApp app;
	Firebase.DependencyStatus dependencyStatus = Firebase.DependencyStatus.UnavailableOther;

	protected bool isFirebaseInitialized = false;

	public ApplicationLoader loader;

	public GameObject databaseService;

	// Start is called before the first frame update
	void Start()
	{
		Debug.Log("Start Registering for Push Notification");
		//Application.runInBackground = true;

		StartCoroutine(StartInitialization());

	}

	public IEnumerator StartInitialization()
	{
		Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
			dependencyStatus = task.Result;
			if (dependencyStatus == Firebase.DependencyStatus.Available)
			{
				InitializeFirebase();
			}
			else
			{
				Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
			}
		});
		Debug.Log("Firebase Registered for Push Notification");

		yield return null;

		//yield return new WaitUntil(() => isFirebaseInitialized == true);
	}

	private void InitializeFirebase()
	{
		Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
		Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;

		Firebase.Messaging.FirebaseMessaging.SubscribeAsync("/topics/news").ContinueWith(task => {
			LogTaskCompletion(task, "SubscribeAsync");
		});

		DebugLog("Firebase Messaging Initialized");

		Firebase.Messaging.FirebaseMessaging.RequestPermissionAsync().ContinueWith(task => {
			LogTaskCompletion(task, "RequestPermissionAsync");
		});

		isFirebaseInitialized = true;
	}

	private bool LogTaskCompletion(Task task, string operation)
	{
		bool complete = false;
		if (task.IsCanceled)
		{
			DebugLog(operation + " canceled... ");
		}
		else if (task.IsFaulted)
		{
			DebugLog(operation + " encounted an error... ");
			foreach (Exception exception in task.Exception.Flatten().InnerExceptions)
			{
				string errorCode = "";
				Firebase.FirebaseException firebaseEx = exception as Firebase.FirebaseException;
				if (firebaseEx != null)
				{
					errorCode = String.Format("Error {0} : ",
					  ((Firebase.Messaging.Error)firebaseEx.ErrorCode).ToString());
				}
				DebugLog(errorCode + exception.ToString());
			}
		}
		else if (task.IsCompleted)
		{
			DebugLog(operation + " completed ");
			complete = true;
		}
		return complete;

	}

	/// <summary>
	/// Print To Log
	/// </summary>
	/// <param name="message">Message.</param>
	public void DebugLog(string message)
	{
		print(message);
	}

	public void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
	{
		Debug.Log("Token Received : " + token.Token);
	}

	public void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
	{
		foreach (System.Collections.Generic.KeyValuePair<string, string> iter in e.Message.Data)
		{
			DebugLog("  " + iter.Key + ": " + iter.Value);

			if (iter.Key == "refresh")
			{

				StartCoroutine(DeleteData());
			}

			if (iter.Key == "deleteA")
			{
				// Get the Avatar from the Database 

				string[] avatarsToDelete = iter.Value.Split(',');

				Debug.Log("AvatarsToDelete : " + avatarsToDelete.Length);

				for (int i = 0; i < avatarsToDelete.Length; i++)
				{
					Debug.Log("Delete ID : " + int.Parse(avatarsToDelete[i]));

					DB_Avatar avaDB = databaseService.GetComponent<DBScript>().GetAvatrById(int.Parse(avatarsToDelete[i]));

					DebugLog(" Deleting Avatar : " + avaDB.Name);

					string[] catIDS = avaDB.CategoryID.Split(',');

					if (catIDS != null)
					{
						for (int j = 0; j < catIDS.Length; j++)
						{
							string avatarThumbnailPath = Application.persistentDataPath + "/Category-" + catIDS[j] + "/" + avaDB.Id + ".png";
							string avatarModelPath = Application.persistentDataPath + "/Category-" + catIDS[j] + "/" + avaDB.Id + ".fbx";
							// Delete Avatar Thumbnail
							if (File.Exists(avatarThumbnailPath))
							{
								File.Delete(avatarThumbnailPath);
							}
							// Delete Avatar Model
							if (File.Exists(avatarModelPath))
							{
								File.Delete(avatarModelPath);
							}
						}
					}

					// Delete From Database
					if (avaDB != null)
					{
						databaseService.GetComponent<DBScript>().DeleteAvatarByID(avaDB);
					}

				}
			}

			if (iter.Key == "deleteC")
			{
				// Delete Category Image and Path 
				if (File.Exists(Application.persistentDataPath + "/Category-" + iter.Value + ".png"))
				{
					File.Delete(Application.persistentDataPath + "/Category-" + iter.Value + ".png");
					Directory.Delete(Application.persistentDataPath + "/Category-" + iter.Value);
				}

				// Delete From Database
				AvatarCategory CatDb = databaseService.GetComponent<DBScript>().GetCategoryById(int.Parse(iter.Value));
				if (CatDb != null)
				{
					databaseService.GetComponent<DBScript>().DeleteCategoryByID(CatDb);
				}
			}

			// Download the newAvatar
			if (iter.Key == "addA")
			{
				string[] avatarsToAdd = iter.Value.Split(',');
				for (int i = 0; i < avatarsToAdd.Length; i++)
				{
					// StartCoroutine(loader.DownloadAvatarByID(avatarsToAdd[i]));
				}
			}

			// Download the new Category
			if (iter.Key == "addC")
			{
				//StartCoroutine(loader.DownloadCategoryById(iter.Value));
			}
		}

	}

	public IEnumerator DeleteData()
	{

		Debug.Log("Here We go Let's Refresh Data");
		#region Delete All Avatars from the Database
		databaseService.GetComponent<DBScript>().DeleteAllCategories();
		databaseService.GetComponent<DBScript>().DeleteAllAvatars();
		databaseService.GetComponent<DBScript>().DeleteAllAnimationInfo();
		databaseService.GetComponent<DBScript>().DeleteAllFBXinfo();
		databaseService.GetComponent<DBScript>().DeleteAllMusicInfo();
		#endregion

		yield return null;

		#region Delete All Catergories from the Phone 
		DirectoryInfo di = new DirectoryInfo(Application.persistentDataPath);
		FileInfo[] filesOfThumnails = di.GetFiles("Category*.png");

		foreach (FileInfo file in di.GetFiles())
		{
			file.Delete();
		}
		foreach (DirectoryInfo dir in di.GetDirectories())
		{
			if (dir.Name.Contains("Category"))
			{
				dir.Delete(true);
			}
		}
		yield return null;

		// Delete Cureent Categories Pictures
		loader.DeleteCategoriesFromPanel();

		#endregion

		string categories_URL = API_Settings.ONLINE_SERVICE_URL + API_Settings.GET_ALL_CATEGORIES;
		StartCoroutine(loader.DownloadAllCategories(categories_URL));

	}

}
